package com.mx.sivale;

import com.mx.sivale.controller.UserController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SivaleApplicationTests {

	@Autowired
	private UserController userController;

	@Test
	public void contextLoads() {

	}

}

package com.mx.sivale.unit.integration;

import com.mx.sivale.model.dto.CatalogDTO;
import org.apache.log4j.Logger;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest( webEnvironment =RANDOM_PORT)
public class IntegrationTest {
    
    Logger logger= Logger.getLogger(IntegrationTest.class);
            
    @Autowired
    TestRestTemplate testRestTemplate;

    @Test
    public void getEvidenceType()throws Exception{
        ResponseEntity<Object[]> response=testRestTemplate.getForEntity("/secure/evidenceType", Object[].class);
        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        Assertions.assertThat(response.getBody().length).isEqualTo(4);
    }
}

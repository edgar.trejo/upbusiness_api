package com.mx.sivale.repository;

import com.mx.sivale.model.Client;
import com.mx.sivale.model.JobPosition;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface JobPositionRepository extends JpaRepository<JobPosition, Long> {

	List<JobPosition> findByActiveTrueOrderByPosition();

	List<JobPosition> findByClientAndActiveTrueOrderByPosition(Client client);

	List<JobPosition> findByActiveTrueAndClientAndNameContainingOrderByPosition(Client client, String name);

	JobPosition findByClientAndPosition(Client client, Integer position);

	JobPosition findTopByClientOrderByPositionDesc(Client client);

	JobPosition findByClient_NumberClientAndCodeAndActive(Long clientId, String code, Boolean active);

	JobPosition findByClient_idAndNameAndCodeAndActiveTrue(Long clientId,String name,String code);

}

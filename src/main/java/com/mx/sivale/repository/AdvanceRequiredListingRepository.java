package com.mx.sivale.repository;

import com.mx.sivale.model.dto.AdvanceRequiredListingDTO;

import java.util.List;

public interface AdvanceRequiredListingRepository {

    List<AdvanceRequiredListingDTO> findAdvancesRequiredListing(Long clientId) throws Exception;

    List<AdvanceRequiredListingDTO> findAllAdvancesRequiredListingByUserId(Long clientId, Long userId) throws Exception;

}

package com.mx.sivale.repository;

import com.mx.sivale.model.Client;
import com.mx.sivale.model.Team;
import com.mx.sivale.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TeamRepository extends JpaRepository<Team, Long> {

    List<Team> findByActiveTrueAndClient(Client client);

    List<Team> findByActiveTrueAndClientAndUsersIn(Client client, List<User> listUsers);

    @Query("SELECT team FROM Team team WHERE team.active = TRUE AND team.client = :client AND (team.name LIKE %:name% OR team.code LIKE %:name%) ")
    List<Team> findByNameAndCode(@Param("client") Client client, @Param("name") String name);

}

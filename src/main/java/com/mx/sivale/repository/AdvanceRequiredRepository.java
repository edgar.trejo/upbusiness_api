package com.mx.sivale.repository;

import com.mx.sivale.model.AdvanceRequired;
import com.mx.sivale.model.AdvanceStatus;
import com.mx.sivale.model.Client;
import com.mx.sivale.model.User;
import com.mx.sivale.repository.exception.RepositoryException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by amartinezmendoza on 20/12/2017.
 */
public interface AdvanceRequiredRepository extends JpaRepository<AdvanceRequired, Long> {

    List<AdvanceRequired> findByClientAndActiveTrueOrderByIdDesc(Client client) throws RepositoryException;

    Page<AdvanceRequired> findAllByClientAndTransferDateBetween(Client client, Date date1, Date date2, Pageable pageable) throws RepositoryException;

    List<AdvanceRequired> findByClientAndUserAndActiveTrueOrderByIdDesc(Client client,User user) throws RepositoryException;

    Page<AdvanceRequired> findByClientAndActiveTrueAndNameIgnoreCaseContainingAndAdvanceStatus_IdInOrderByIdDesc(Client client, String name, List<Long> status, Pageable pageable) throws RepositoryException;

    Page<AdvanceRequired> findByClientAndActiveTrueAndNameIgnoreCaseContainingAndAdvanceStatus_IdInAndUser_IdOrderByIdDesc(Client client, String name, List<Long> status,  Long userId, Pageable pageable) throws RepositoryException;

    //@Query("SELECT payMethod FROM PayMethod payMethod WHERE active = true")
    @Query( value = "select sum( CAST(advance.amount_required as DECIMAL(9,2)) ) amount_required from mercurio.advance_required advance where advance.client_id= :clientId and advance.active=TRUE " +
            "and advance.user_id=:userId and advance.advance_status_id= :statusId and month(advance.date_created)=MONTH(CURRENT_DATE())", nativeQuery = true)
    Double getAmountAdvanceByClientAndActiveTrueAndUserAndAdvanceStatus_Id(@Param("clientId") Long clientId,
                                                                       @Param("userId") Long userId,
                                                                       @Param("statusId") Long statusId) throws RepositoryException;

    @Modifying
	@Transactional
	@Query("UPDATE  #{#entityName} ar SET ar.advanceStatus = 3  WHERE ar.user.id = :userId ")
	int updatePendingAdvances(@Param("userId") Long userId);
}

package com.mx.sivale.repository;

import com.mx.sivale.model.*;
import com.mx.sivale.repository.exception.RepositoryException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface SpendingRepository extends JpaRepository<Spending, Long>{

	List<Spending> findByEvent(Event event) throws RepositoryException;

	List<Spending> findByClientAndUser(Client client, User user) throws RepositoryException;

	List<Spending> findByClient(Client client) throws RepositoryException;

	List<Spending> findByUserAndInvoiceIsNull(User user) throws RepositoryException;

	Page<Spending> findAllByClientAndDateCreatedBetween(Client client, Date date1, Date date2, Pageable pageable) throws RepositoryException;

	List<Spending> findByTransaction(Transaction transaction);

	List<Spending> findByInvoice_Id(Long invoiceId);

	List<Spending> findByInvoice(Invoice invoice);

	@Query("SELECT s FROM Spending s WHERE s.id IN(:spendingsId) ")
	Page<Spending> findAllByIdIn(@Param("spendingsId") List<Long> spendingsId, Pageable pageable) throws RepositoryException;

}

package com.mx.sivale.repository;

import com.mx.sivale.model.Client;
import com.mx.sivale.model.EventApproverReport;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface EventApproverReportRepository extends JpaRepository<EventApproverReport, Long> {

    EventApproverReport findTopByEvent_IdOrderByApproverDateDesc(Long eventId);
    
    List<EventApproverReport> findByEvent_id(Long eventId);

    Page<EventApproverReport> findByEvent_ClientAndApproverDateIsBetweenOrderByApproverDateDesc(Client client, Date from, Date to, Pageable pageable);

    @Query("SELECT report FROM  EventApproverReport report WHERE report.event.client = :client and report.approverDate between :from and :to")
    Page<EventApproverReport> findAllByApproverDateIsBetweenAndEvent_ClientOrderByApproverDateDesc(@Param("from") Date from, @Param("to") Date to, @Param("client") Client client, Pageable pageable);

    List<EventApproverReport> findByEvent_idAndEventApprover_id(Long eventId, Long eventApproverId);

}

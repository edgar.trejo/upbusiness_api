package com.mx.sivale.repository;

import com.mx.sivale.model.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Date;

public interface SpendingTypeCustomReportRepository {

    public Page<AmountSpendingType> findSpendingTypesByCustomFilter(
            Client client, Date from, Date to, Long costCenterId,
            ApprovalStatus approvalStatus, SpendingType spendingType, Pageable pageable) throws Exception;
}

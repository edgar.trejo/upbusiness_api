package com.mx.sivale.repository;

import com.mx.sivale.model.Event;

import java.util.Date;
import java.util.List;

public interface EventPaidDetailRepository {
    List<Event> findAllEventsPaid(Long clientId, Date from, Date to) throws Exception;
}

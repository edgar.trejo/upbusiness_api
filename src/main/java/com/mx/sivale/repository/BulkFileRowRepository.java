package com.mx.sivale.repository;

import com.mx.sivale.model.BulkFileRow;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BulkFileRowRepository extends JpaRepository<BulkFileRow, Long> {

    List<BulkFileRow> findByBulkFileIdAndStatus(Long bulkFileId, int status);
}

package com.mx.sivale.repository;

import com.mx.sivale.model.Client;
import com.mx.sivale.model.SpendingType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SpendingTypeRepository extends JpaRepository<SpendingType, Long> {

	List<SpendingType> findByClientAndActiveTrue(Client client);

	List<SpendingType> findByActiveTrue();

	List<SpendingType> findByActiveTrueAndClientAndNameContaining(Client client, String name);
	
}

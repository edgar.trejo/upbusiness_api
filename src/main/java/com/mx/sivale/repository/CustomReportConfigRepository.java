package com.mx.sivale.repository;

import com.mx.sivale.model.CustomReportConfig;
import com.mx.sivale.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CustomReportConfigRepository extends JpaRepository<CustomReportConfig, Long>{

    List<CustomReportConfig> findByUserIsNullAndCustomReportConfigColumn_CustomReportColumn_Active(Boolean active);

    List<CustomReportConfig> findByUser(User user);

}

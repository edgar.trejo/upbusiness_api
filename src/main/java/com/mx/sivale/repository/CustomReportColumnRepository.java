package com.mx.sivale.repository;

import com.mx.sivale.model.CustomReportColumn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CustomReportColumnRepository extends JpaRepository<CustomReportColumn, Long> {

    @Query("SELECT c FROM CustomReportColumn c WHERE c.nameColumn IN (:names)")
    List<CustomReportColumn> findByNameColumn(@Param("names") List<String> names);

    CustomReportColumn findByNameColumn(String nameColumn);

}

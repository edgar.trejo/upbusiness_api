package com.mx.sivale.repository;

import com.mx.sivale.model.ApproverUser;
import com.mx.sivale.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ApproverUserRepository extends JpaRepository<ApproverUser, Long> {

	List<ApproverUser> findByUserIn(User user);
}

package com.mx.sivale.repository;

import com.mx.sivale.model.CustomReportConfigColumn;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomReportConfigColumnRepository extends JpaRepository<CustomReportConfigColumn, Long> {

}

package com.mx.sivale.repository;

import com.mx.sivale.model.Client;
import com.mx.sivale.model.Event;
import com.mx.sivale.model.LogEmail;
import com.mx.sivale.model.User;
import com.mx.sivale.repository.exception.RepositoryException;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface LogEmailRepository extends JpaRepository<LogEmail, Long> {

}

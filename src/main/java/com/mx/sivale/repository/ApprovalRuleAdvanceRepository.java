package com.mx.sivale.repository;

import com.mx.sivale.model.ApprovalRuleAdvance;
import com.mx.sivale.model.Client;
import com.mx.sivale.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by amartinezmendoza on 19/12/2017.
 */
public interface ApprovalRuleAdvanceRepository extends JpaRepository<ApprovalRuleAdvance, Long> {

    List<ApprovalRuleAdvance> findByClientAndActiveTrue(Client client);

    List<ApprovalRuleAdvance> findDistinctByClientAndActiveTrueAndAppliesToIn(Client client,List<Team> teams);
}

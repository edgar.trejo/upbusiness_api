package com.mx.sivale.repository;

import com.mx.sivale.model.UserClientRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserClientRoleRepository extends JpaRepository<UserClientRole, Long> {

    UserClientRole findByUserClientIdAndRoleId(Long userClient, Long roleId) throws Exception;
}

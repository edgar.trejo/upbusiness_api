package com.mx.sivale.repository.exception;

import com.mx.sivale.model.AdvanceApproverReport;
import com.mx.sivale.model.AdvanceApproverReportAlternative;
import com.mx.sivale.model.AdvanceRequired;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AdvanceApproverReportAlternativeRepository extends JpaRepository<AdvanceApproverReportAlternative, Long> {

    List<AdvanceApproverReportAlternative> findByAdvanceRequired(AdvanceRequired advanceRequired) throws Exception;

}

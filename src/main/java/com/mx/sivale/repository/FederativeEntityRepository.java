package com.mx.sivale.repository;

import com.mx.sivale.model.FederativeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface FederativeEntityRepository extends JpaRepository<FederativeEntity, Long > { 

	@Query("SELECT federative FROM FederativeEntity federative WHERE active = true")
	List<FederativeEntity> findActiveFederativeEntitys();

	FederativeEntity findByName(String entityName);
}

package com.mx.sivale.repository;

import com.mx.sivale.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface UserRepository extends JpaRepository<User, Long> {

	User findByEmailAndActiveTrue(String email) throws Exception;

	User findByEmail(String email) throws Exception;

	User findBySiValeIdAndActiveTrue(String sivaleId) throws Exception;

	User findByIdAndActiveTrue(Long userId) throws Exception;
	
	@Modifying
	@Transactional
	@Query("UPDATE  #{#entityName} u SET u.advanceAvailable = :advanceAvailable WHERE u.id = :userId ")
	int updateUserAdvanceAvailable(@Param("userId") Long userId, @Param("advanceAvailable") Boolean advanceAvailable);

}

package com.mx.sivale.repository;

import com.mx.sivale.model.Transfer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;

/**
 * Created by amartinezmendoza on 20/12/2017.
 */
public interface TransferRepository extends JpaRepository<Transfer, Long> {

    Page<Transfer> findAllByClient_IdAndRequestTypeInAndDateCreatedBetween(Long clientId,int[] requestTypes, Date date1, Date date2, Pageable pageable);

}

package com.mx.sivale.repository;

import com.mx.sivale.model.AdvanceApproverReport;
import com.mx.sivale.model.AdvanceRequired;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by amartinezmendoza on 27/12/2017.
 */
public interface AdvanceApproverReportRepository extends JpaRepository<AdvanceApproverReport, Long> {

    AdvanceApproverReport findTopByadvanceRequired_IdOrderByApproverDateDesc(Long advanceId);

}

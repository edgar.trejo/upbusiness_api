package com.mx.sivale.repository;

import com.mx.sivale.model.InvoiceSpendingAssociate;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface InvoiceSpendingAssociateRepository extends JpaRepository<InvoiceSpendingAssociate, Long> {
	
	List<InvoiceSpendingAssociate> findBySpending_id(Long id);

}

package com.mx.sivale.repository;

import com.mx.sivale.model.ImageEvidence;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImageEvidenceRepository extends JpaRepository<ImageEvidence, Long>{

}

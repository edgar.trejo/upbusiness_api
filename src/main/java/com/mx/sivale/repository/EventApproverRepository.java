package com.mx.sivale.repository;

import com.mx.sivale.model.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EventApproverRepository extends JpaRepository<EventApprover, Long> {

    List<EventApprover> findUniqueByAdminFalseAndClientAndPendingAndApproverTeamIn(Client client, int pending, List<Team> teams);

    List<EventApprover> findUniqueByAdminFalseAndClientAndPendingAndApproverUserIn(Client client, int pending, List<ApproverUser> approverUsers);

    List<EventApprover> findUniqueByAdminTrueAndClientAndApproverTeamIn(Client client, List<Team> teams);

    List<EventApprover> findUniqueByAdminTrueAndClientAndApproverUserIn(Client client, List<ApproverUser> approverUsers);

    List<EventApprover> findUniqueByApprovalRuleAndPendingAndPositionLessThan(ApprovalRule rule, int pending, Integer position);

    List<EventApprover> findByEventOrderByLoopVersionDesc(Event event);
    
    List<EventApprover> findByEvent_id(Long id);

    List<EventApprover> findByEventAndLoopVersionOrderByLoopVersionDesc(Event event, int loopVersion);

    List<EventApprover> findByEventAndPendingAndApprovalRuleIsNotNull(Event event, int pending);

    List<EventApprover> findByEventAndPendingNot(Event event,int pending);

    @Query("SELECT COALESCE(MAX(e.loopVersion),0) FROM EventApprover e WHERE e.event = :event")
    int getMaxApproverByEvent(@Param("event") Event event);



}

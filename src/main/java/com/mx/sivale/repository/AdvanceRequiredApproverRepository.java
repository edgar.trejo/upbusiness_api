package com.mx.sivale.repository;

import com.mx.sivale.model.*;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by amartinezmendoza on 21/12/2017.
 */
public interface AdvanceRequiredApproverRepository extends JpaRepository<AdvanceRequiredApprover, Long> {

    //List<AdvanceRequiredApprover> findByAdminFalseAndClientAndApproverTeamInOrderByIdDesc(Client client, List<Team> teams) throws Exception;

    List<AdvanceRequiredApprover> findByAdminFalseAndClientAndPendingAndApproverTeamInOrderByIdDesc(Client client,Integer pending, List<Team> teams) throws Exception;

    //List<AdvanceRequiredApprover> findByAdminFalseAndClientAndApproverUserInOrderByIdDesc(Client client, List<ApproverUser> approverUsers) throws Exception;

    List<AdvanceRequiredApprover> findByAdminFalseAndClientAndPendingAndApproverUserInOrderByIdDesc(Client client, Integer pending, List<ApproverUser> approverUsers) throws Exception;

    List<AdvanceRequiredApprover> findByAdminTrueAndClientOrderByIdDesc(Client client);

    List<AdvanceRequiredApprover> findByAdminFalseAndClientOrderByIdDesc(Client client);

    List<AdvanceRequiredApprover> findByAdvanceRequiredAndPendingAndApprovalRuleAdvanceAndPositionLessThan(AdvanceRequired advanceRequired, Integer pending, ApprovalRuleAdvance rule, Integer position);

    List<AdvanceRequiredApprover> findByAdvanceRequired(AdvanceRequired advanceRequired) throws Exception;

    List<AdvanceRequiredApprover> findByAdvanceRequiredAndApprovalRuleAdvanceIsNotNull(AdvanceRequired advanceRequired) throws Exception;

    List<AdvanceRequiredApprover> findByAdvanceRequiredAndAdminTrue(AdvanceRequired advanceRequired) throws Exception;

    List<AdvanceRequiredApprover> findByAdvanceRequiredAndPendingAndApprovalRuleAdvanceIsNotNull(AdvanceRequired advanceRequired, Integer pending) throws Exception;

    List<AdvanceRequiredApprover> findByAdvanceRequiredAndPendingNot(AdvanceRequired advanceRequired, Integer pending) throws Exception;

    List<AdvanceRequiredApprover> findByAdvanceRequiredAndApprovalRuleAdvanceIsNotNullAndApproverUserIsNotNull(AdvanceRequired advanceRequired) throws Exception;

    List<AdvanceRequiredApprover> findByApproverUserIsNotNullAndApproverUser_User(User user);
}

package com.mx.sivale.repository;

import com.mx.sivale.model.Client;
import com.mx.sivale.model.CostCenter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CostCenterRepository extends JpaRepository<CostCenter, Long> {

	List<CostCenter> findByClientAndActiveTrue(Client client);

	List<CostCenter> findByActiveTrue();

	@Query("SELECT cc FROM CostCenter cc WHERE cc.active = TRUE AND cc.client = :client AND (cc.name LIKE %:name% OR cc.code LIKE %:name%) ")
	List<CostCenter> findByNameAndCode(@Param("client") Client client, @Param("name") String name);

	CostCenter findByClient_NumberClientAndCodeAndActive(Long clientId, String code, Boolean active);
	
	//@Query("SELECT cc FROM CostCenter cc WHERE cc.client.id = :idClient AND cc.name = :name AND cc.code = :code AND cc.active = TRUE")
	//CostCenter getExistCostCenter(@Param("idClient") Long idClient,@Param("name") String name,@Param("code") String code); 
	
	CostCenter findByClient_idAndNameAndCodeAndActiveTrue(Long idClient, String name,String code);

}

package com.mx.sivale.repository;

import com.mx.sivale.model.InvoiceBatchProcess;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InvoiceBatchProcessRepository extends JpaRepository<InvoiceBatchProcess, Long>{

    InvoiceBatchProcess findFirstByOrderByIdDesc();

}

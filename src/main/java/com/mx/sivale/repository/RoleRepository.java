package com.mx.sivale.repository;

import com.mx.sivale.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface RoleRepository extends JpaRepository<Role, Long > {

	@Query("SELECT role FROM Role role WHERE active = true")
    List<Role> findActiveRoles();

}

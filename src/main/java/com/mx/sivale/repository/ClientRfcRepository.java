package com.mx.sivale.repository;

import com.mx.sivale.model.ClientRfc;
import com.mx.sivale.repository.exception.RepositoryException;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ClientRfcRepository extends JpaRepository<ClientRfc, Long> {
	
	List<ClientRfc> findByClientId(Long clientId) throws RepositoryException;
}

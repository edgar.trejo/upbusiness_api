package com.mx.sivale.repository;

import com.mx.sivale.model.dto.EventDetailDTO;

public interface EventDetailRepository {

    EventDetailDTO findApproversByEventId(Long eventId) throws Exception;

}

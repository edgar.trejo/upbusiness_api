package com.mx.sivale.repository;

import com.mx.sivale.model.EvidenceType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EvidenceTypeRepository extends JpaRepository<EvidenceType, Long> {

	@Query("SELECT evidenceType FROM EvidenceType evidenceType WHERE active = true")
	List<EvidenceType> findActiveEvidenceTypes();

	EvidenceType findByName(String name);
}

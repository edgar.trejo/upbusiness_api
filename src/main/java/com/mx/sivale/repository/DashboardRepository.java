package com.mx.sivale.repository;

import com.mx.sivale.model.dto.EventDashBoardDTO;
import com.mx.sivale.model.dto.SpendingDashBoardDTO;
import com.mx.sivale.model.dto.TransferDashBoardDTO;

import java.util.List;

public interface DashboardRepository {

    List<EventDashBoardDTO> findAllEventsGroupByMonthAndStatus(Long clientId) throws Exception;

    List<SpendingDashBoardDTO> findThreeSpendingsGroupBySpendingType(Long clientId) throws Exception;

    List<SpendingDashBoardDTO> findOtherSpendings(Long clientId, String notInSQL) throws Exception;

    List<TransferDashBoardDTO> getTransfers(Long clientId) throws Exception;
}

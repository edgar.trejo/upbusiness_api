package com.mx.sivale.repository;

import com.mx.sivale.model.CustomReportTypeColumn;
import com.mx.sivale.model.CustomReportColumn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CustomReportTypeColumnRepository extends JpaRepository<CustomReportTypeColumn, Long> {

    @Query("SELECT typeColumn FROM CustomReportTypeColumn typeColumn WHERE typeColumn.id IN( " +
            " SELECT column.customReportTypeColumn.id FROM CustomReportColumn column " +
            " WHERE column.nameColumn IN (:names) ) ")
    List<CustomReportTypeColumn> findAllByColumnName(@Param("names") List<String> names);
}

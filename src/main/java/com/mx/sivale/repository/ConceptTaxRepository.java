package com.mx.sivale.repository;

import com.mx.sivale.model.ConceptTax;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;
import java.util.List;

public interface ConceptTaxRepository extends JpaRepository<ConceptTax, BigInteger> {
    List<ConceptTax> findByInvoiceConceptId(Long conceptId);
    List<ConceptTax> findByInvoiceConceptIdAndTax(Long conceptId, String tax);
}

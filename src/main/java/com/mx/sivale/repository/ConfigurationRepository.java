package com.mx.sivale.repository;

import com.mx.sivale.model.Configuration;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConfigurationRepository extends JpaRepository<Configuration, Long> {

    Configuration findByName(String name);

}

package com.mx.sivale.repository.impl;

import com.mx.sivale.model.ApprovalStatus;
import com.mx.sivale.model.Client;
import com.mx.sivale.model.Invoice;
import com.mx.sivale.model.SpendingType;
import com.mx.sivale.repository.InvoiceCustomReportRepository;
import com.mx.sivale.repository.InvoiceRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class InvoiceCustomReportRepositoryImpl implements InvoiceCustomReportRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private InvoiceRepository invoiceRepository;

    private Logger logger=Logger.getLogger(InvoiceCustomReportRepositoryImpl.class);

    public Page<Invoice> findInvoicesByCustomFilter(
            Client client, Date from, Date to, Long costCenterId,
            ApprovalStatus approvalStatus, SpendingType spendingType, Pageable pageable) throws Exception{

        Page page = null;

        try {

            String query = " SELECT distinct i.id\n" +
                    " from invoice i\n" +
                    " left join spending s on i.id = s.invoice_evidence_id\n" +
                    " left join event e on s.event_id = e.id\n" +
                    " left join amount_spending_type ast on s.id = ast.spending_id\n" +
                    " inner join user_client uc on e.user_id = uc.user_id and e.client_id = uc.client_id\n" +
                    " where i.client_id=?\n" +
                    " and i.original_date between ? and ?\n";
            if(costCenterId != null){
                query += " and uc.cost_center_id = " + costCenterId + " \n";
            }
            if(approvalStatus != null){
                query += " and e.approval_status_id = " + approvalStatus.getId() + " \n";
            }
            if(spendingType != null){
                query += " and ast.spending_type_id = " + spendingType.getId() + " \n";
            }

            query +=  " order by e.date_start desc;";

            Query q = entityManager.createNativeQuery(query);
            q.setParameter(1, client.getId());
            q.setParameter(2, from);
            q.setParameter(3, to);

            List<BigInteger> result = q.getResultList();
            List<Long> invoicesIds = new ArrayList<Long>();
            for(BigInteger bi : result){
                invoicesIds.add(bi.longValue());
            }

            page = invoiceRepository.findAllByIdIn(invoicesIds, pageable);

        }catch (Exception e){
            logger.error("Error findInvoicesByCustomFilter: ",e);
        }

        return page;

    }
}

package com.mx.sivale.repository.impl;

import com.mx.sivale.model.*;
import com.mx.sivale.model.dto.EventApproverDetailDTO;
import com.mx.sivale.model.dto.EventDetailDTO;
import com.mx.sivale.repository.*;
import com.mx.sivale.service.util.UtilBean;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class EventDetailRepositoryImpl implements EventDetailRepository {

    private Logger logger=Logger.getLogger(EventDetailRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private ApprovalStatusRepository approvalStatusRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EventRepository eventRepository;

    @Override
    public EventDetailDTO findApproversByEventId(Long eventId) throws Exception {
        try{
            String query="select user_id, \n" +
                    "      pending, \n" +
                    "      approver_date, \n" +
                    "      comment, \n" +
                    "      pre_status, \n" +
                    "      event_id, \n" +
                    "      teamId, \n" +
                    "      teamName, \n" +
                    "      if(isAdmin = 0,False,True) isAdmin, \n" +
                    "      automatic, \n" +
                    "      position, \n" +
                    "      loop_version\n" +
                    "   from ( \n" +
                    "\t\t(select coalesce(u.id,u_t.id) user_id,\n" +
                    "\t\tea.pending, \n" +
                    "\t\tnull approver_date, \n" +
                    "\t\tnull comment, \n" +
                    "\t\tnull pre_status, \n" +
                    "\t\tevent.id event_id, \n" +
                    "\t\tt.id as teamId, \n" +
                    "\t\tt.name as teamName, \n" +
                    "\t\tea.admin as isAdmin, \n" +
                    "\t\tNULL automatic, \n" +
                    "\t\tcoalesce(ea.position,au.position)  position, \n" +
                    "\t\t0 position_admin, \n" +
                    "\t\ttu.id position_group,\n" +
                    "\t\tea.loop_version\n" +
                    "\t\tfrom mercurio.event event inner join \n" +
                    "\t\tmercurio.event_approver ea on event.id = \n" +
                    "\t\tea.event_id left outer join \n" +
                    "\t\tmercurio.approver_user au on ea.approver_user_id = au.id left outer join \n" +
                    "\t\tmercurio.team t on ea.team_id = t.id left outer join \n" +
                    "\t\tmercurio.team_users tu on t.id = tu.team_id left outer join \n" +
                    "\t\tmercurio.user u on au.user_id = u.id left outer join \n" +
                    "\t\tmercurio.user u_t on tu.users_id = u_t.id \n" +
                    "\t\twhere 1=1 \n" +
                    "\t\tand case when ea.admin is true then \n" +
                    "\t\tea.event_id not in ( \n" +
                    "\t\tselect epr.event_id \n" +
                    "\t\tfrom mercurio.event_approver_report epr \n" +
                    "\t\twhere 1=1 \n" +
                    "\t\tand epr.event_id = ea.event_id \n" +
                    "\t\t) \n" +
                    "\t\telse au.user_id not in ( \n" +
                    "\t\tselect epr.approver_user \n" +
                    "\t\tfrom mercurio.event_approver_report epr \n" +
                    "\t\twhere 1=1 \n" +
                    "\t\tand epr.event_id = ea.event_id) \n" +
                    "\t\tor \n" +
                    "\t\ttu.users_id not in ( \n" +
                    "\t\tselect epr.approver_user \n" +
                    "\t\tfrom mercurio.event_approver_report epr \n" +
                    "\t\twhere 1=1 \n" +
                    "\t\tand epr.event_id = ea.event_id) \n" +
                    "\t\tend)\n" +
                    "UNION ALL \n" +
                    "(select \n" +
                    "      ear.approver_user user_id, \n" +
                    "      ea.pending, \n" +
                    "      ear.approver_date, \n" +
                    "      ear.comment, \n" +
                    "      ear.pre_status, \n" +
                    "      event.id event_id, \n" +
                    "      t.id teamId, \n" +
                    "      t.name as teamName, \n" +
                    "      ea.admin as isAdmin, \n" +
                    "      ear.automatic, \n" +
                    "      coalesce(ea.position,au.position) position, \n" +
                    "      ear.admin position_admin,\n" +
                    "      tu.id position_group,\n" +
                    "      ea.loop_version\n" +
                    "   from mercurio.event event inner join \n" +
                    "   mercurio.event_approver ea on event.id = ea.event_id left outer join \n" +
                    "   mercurio.approver_user au on ea.approver_user_id = au.id inner join \n" +
                    "   mercurio.event_approver_report ear on ea.event_id = ear.event_id and ea.id=ear.event_approver_id\n" +
                    "    and (ear.approver_user = au.user_id or ea.admin is true or ea.team_id is not null or ear.automatic is true) left outer join \n" +
                    "   mercurio.user u on ear.approver_user = u.id left outer join \n" +
                    "   mercurio.team t on ea.team_id = t.id left outer join \n" +
                    "   mercurio.team_users tu on t.id = tu.team_id and tu.users_id = ear.approver_user \n" +
                    "   where 1=1 \n" +
                    "    and ea.pending = 0 \n" +
                    "   )) flujo \n" +
                    "   where event_id = ?\n" +
                    "   order by event_id, loop_version desc, position, position_admin, position_group";
            Query q = entityManager.createNativeQuery(query);
            q.setParameter(1, eventId);

            List<Object[]> detail = q.getResultList();

            List<EventApproverDetailDTO> listApprovers = new ArrayList<>();
            EventApproverDetailDTO eventApproverDetailDTO;
            for (Object[] a : detail) {
                eventApproverDetailDTO = new EventApproverDetailDTO();
                eventApproverDetailDTO.setUserId(a[0]==null?0L:Long.parseLong(a[0].toString()));
                if(a[0]!=null){
                    User user=userRepository.findOne(Long.parseLong(a[0].toString()));
                    eventApproverDetailDTO.setUser(user);
                }
                eventApproverDetailDTO.setPending(a[1]==null?0:Integer.parseInt(a[1].toString()));
                eventApproverDetailDTO.setApproverDate(a[2]==null?null:Timestamp.valueOf(a[2].toString()));
                eventApproverDetailDTO.setComment(a[3]==null?"":a[3].toString());

                ApprovalStatus pre_status=new ApprovalStatus();
                Long pre_status_id=a[4]==null?0L:Long.parseLong(a[4].toString());
                pre_status=approvalStatusRepository.findOne(pre_status_id);

                eventApproverDetailDTO.setPre_status(pre_status);
                eventApproverDetailDTO.setEventId(a[5]==null?0L:Long.parseLong(a[5].toString()));

                eventApproverDetailDTO.setTeamId(a[6]==null?0L:Long.parseLong(a[6].toString()));
                eventApproverDetailDTO.setTeamName(a[7]==null?"":a[7].toString());
                eventApproverDetailDTO.setAdmin(a[8]==null?Boolean.FALSE:Boolean.valueOf(a[8].toString()));
                Boolean automatic=Boolean.FALSE;
                if(a[9]!=null){
                    Integer isAutomatic=Integer.parseInt(a[9].toString());
                    automatic=isAutomatic ==1?Boolean.TRUE:Boolean.FALSE;
                }
                eventApproverDetailDTO.setAutomatic(automatic);

                eventApproverDetailDTO.setLoop(a[11]==null?0:Integer.parseInt(a[11].toString()));
                if(eventApproverDetailDTO.getApproverDate()!=null){
                    TimeZone timeZone=TimeZone.getTimeZone(ZoneId.of("America/Mexico_City"));
                    DateFormat formatDate = new SimpleDateFormat("dd MM yyyy - HH:mm:ss");
                    formatDate.setTimeZone(timeZone);
                    eventApproverDetailDTO.setDateApproved(formatDate.format(eventApproverDetailDTO.getApproverDate()));
                }else
                    eventApproverDetailDTO.setDateApproved("");

                listApprovers.add(eventApproverDetailDTO);
            }
            logger.info("Ya fui a la BD: "+new Date().toString());
            Event event=eventRepository.findOne(eventId);
            EventDetailDTO eventDetailDTO = UtilBean.eventToEventDetailDTO(event);
            if(listApprovers!=null && !listApprovers.isEmpty()){

                Map<Integer, List<EventApproverDetailDTO>> mapApprovers=listApprovers.stream()
                        .collect(Collectors.groupingBy(
                                EventApproverDetailDTO::getLoop,
                                LinkedHashMap::new,
                                Collectors.toList()));
                if(mapApprovers.size() == 1){
                    listApprovers=mapApprovers.get(1);
                    eventDetailDTO.setListCurrentApprovers(listApprovers);
                }else if(mapApprovers.size()>1){
                    Integer loopVersion=mapApprovers.entrySet().stream()
                            .map(Map.Entry::getKey)
                            .max(Comparator.comparing(Integer::valueOf))
                            .get();
                    eventDetailDTO.setListCurrentApprovers(mapApprovers.get(loopVersion));
                    mapApprovers.remove(loopVersion);
                }
                eventDetailDTO.setEventApproverDetailMap(mapApprovers);

            }

            return eventDetailDTO;

        }catch (Exception e){
            logger.error("Error findApproversByAdvanceRequiredId: ",e);
            return null;
        }
    }

}

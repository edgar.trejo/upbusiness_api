package com.mx.sivale.repository.impl;

import com.mx.sivale.model.dto.AdvanceRequiredListingDTO;
import com.mx.sivale.repository.AdvanceRequiredListingRepository;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class AdvanceRequiredListingRepositoryImpl implements AdvanceRequiredListingRepository {

    private Logger logger=Logger.getLogger(AdvanceRequiredListingRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<AdvanceRequiredListingDTO> findAdvancesRequiredListing(Long clientId) throws Exception {
        try {
            String query="select \n" +
                    "ar.id advanceId,\n" +
                    "CONCAT(coalesce(u.name), \" \", coalesce(u.first_name), \" \", coalesce(u.last_name)) as advanceUser,\n" +
                    "ar.date_created advanceDate,\n" +
                    "ar.amount_required advanceAmount,\n" +
                    "CASE\n" +
                    "    WHEN ar.advance_status_id =1 and ara.admin is false THEN 10\n" +
                    "    WHEN ar.advance_status_id =1 and ara.admin is true THEN 1\n" +
                    "    ELSE ar.advance_status_id\n" +
                    "END as advance_status_id,\n" +
                    "ar.advance_status_id advance_status_id_original,\n" +
                    "CASE\n" +
                    "    WHEN ar.advance_status_id =1 and ara.admin is false THEN \"En Revisión\"\n" +
                    "    WHEN ar.advance_status_id =1 and ara.admin is true THEN \"Pendiente\"\n" +
                    "    WHEN ar.advance_status_id = 2 or ar.advance_status_id = 4 then \"Aprobado\"\n" +
                    "    WHEN ar.advance_status_id = 3 then \"Rechazado\"\n" +
                    "END as status,\n" +
                    "ara.admin,\n" +
                    "coalesce(ar.name,\" \") as advanceName, \n" +
                    "u.id as userId \n"+
                    "from mercurio.advance_required ar\n" +
                    "left join mercurio.advance_required_approver ara\n" +
                    "on ar.id=ara.advance_required_id\n" +
                    "inner join mercurio.user u\n" +
                    "on u.id=ar.user_id\n" +
                    "where ar.client_id=?\n" +
                    "group by advanceId, advanceUser, advanceDate, advanceAmount,advance_status_id, advance_status_id_original,status,admin \n"+
                    "order by ar.date_created desc;";


            Query q = entityManager.createNativeQuery(query);
            q.setParameter(1,clientId);
            List<Object[]> detail = q.getResultList();

            List<AdvanceRequiredListingDTO> listAdvances = new ArrayList<>();
            AdvanceRequiredListingDTO advanceRequiredListing;
            for (Object[] a : detail) {
                advanceRequiredListing = new AdvanceRequiredListingDTO();
                advanceRequiredListing.setAdvanceId(a[0]==null?0L:Long.parseLong(a[0].toString()));
                advanceRequiredListing.setAdvanceUser(a[1]==null?"":a[1].toString());
                advanceRequiredListing.setAdvanceDate(a[2]==null?null:Timestamp.valueOf(a[2].toString()));
                advanceRequiredListing.setAdvanceAmount(a[3]==null?0:Double.parseDouble(a[3].toString()));
                advanceRequiredListing.setAdvanceStatusId(a[4]==null?0L:Long.parseLong(a[4].toString()));
                advanceRequiredListing.setAdvanceStatusIdOriginal(a[5]==null?0L:Long.parseLong(a[5].toString()));
                advanceRequiredListing.setStatus(a[6]==null?"":a[6].toString());
                advanceRequiredListing.setAdmin(a[7]==null?Boolean.FALSE:Boolean.valueOf(a[7].toString()));
                advanceRequiredListing.setAdvanceName(a[8]==null?"":a[8].toString());
                advanceRequiredListing.setUserId(a[9]==null?0L:Long.parseLong(a[9].toString()));
                listAdvances.add(advanceRequiredListing);
            }

            return listAdvances!=null && listAdvances.size()>0?listAdvances:Collections.EMPTY_LIST;

        }catch (Exception e){
            logger.error("Error findAdvancesRequiredListing: ",e);
            return null;
        }
    }

    @Override
    public List<AdvanceRequiredListingDTO> findAllAdvancesRequiredListingByUserId(Long clientId, Long userId) throws Exception {
        try {
            String query="select \n" +
                    "ar.id advanceId,\n" +
                    "CONCAT(coalesce(u.name), \" \", coalesce(u.first_name), \" \", coalesce(u.last_name)) as advanceUser,\n" +
                    "ar.date_created advanceDate,\n" +
                    "ar.amount_required advanceAmount,\n" +
                    "CASE\n" +
                    "    WHEN ar.advance_status_id =1 and ara.admin is false THEN 10\n" +
                    "    WHEN ar.advance_status_id =1 and ara.admin is true THEN 1\n" +
                    "    ELSE ar.advance_status_id\n" +
                    "END as advance_status_id,\n" +
                    "ar.advance_status_id advance_status_id_original,\n" +
                    "CASE\n" +
                    "    WHEN ar.advance_status_id =1 and ara.admin is false THEN \"En Revisión\"\n" +
                    "    WHEN ar.advance_status_id =1 and ara.admin is true THEN \"Pendiente\"\n" +
                    "    WHEN ar.advance_status_id = 2 or ar.advance_status_id = 4 then \"Aprobado\"\n" +
                    "    WHEN ar.advance_status_id = 3 then \"Rechazado\"\n" +
                    "END as status,\n" +
                    "ara.admin,\n" +
                    "coalesce(ar.name,\" \") as advanceName, \n" +
                    "u.id as userId \n"+
                    "from mercurio.advance_required ar\n" +
                    "left join mercurio.advance_required_approver ara\n" +
                    "on ar.id=ara.advance_required_id\n" +
                    "inner join mercurio.user u\n" +
                    "on u.id=ar.user_id\n" +
                    "where ar.client_id=? and ar.user_id=?\n" +
                    "group by advanceId, advanceUser, advanceDate, advanceAmount,advance_status_id, advance_status_id_original,status,admin \n"+
                    "order by ar.date_created desc;";


            Query q = entityManager.createNativeQuery(query);
            q.setParameter(1, clientId);
            q.setParameter(2, userId);
            List<Object[]> detail = q.getResultList();

            List<AdvanceRequiredListingDTO> listAdvances = new ArrayList<>();
            AdvanceRequiredListingDTO advanceRequiredListing;
            for (Object[] a : detail) {
                advanceRequiredListing = new AdvanceRequiredListingDTO();
                advanceRequiredListing.setAdvanceId(a[0]==null?0L:Long.parseLong(a[0].toString()));
                advanceRequiredListing.setAdvanceUser(a[1]==null?"":a[1].toString());
                advanceRequiredListing.setAdvanceDate(a[2]==null?null:Timestamp.valueOf(a[2].toString()));
                advanceRequiredListing.setAdvanceAmount(a[3]==null?0:Double.parseDouble(a[3].toString()));
                advanceRequiredListing.setAdvanceStatusId(a[4]==null?0L:Long.parseLong(a[4].toString()));
                advanceRequiredListing.setAdvanceStatusIdOriginal(a[5]==null?0L:Long.parseLong(a[5].toString()));
                advanceRequiredListing.setStatus(a[6]==null?"":a[6].toString());
                advanceRequiredListing.setAdmin(a[7]==null?Boolean.FALSE:Boolean.valueOf(a[7].toString()));
                advanceRequiredListing.setAdvanceName(a[8]==null?"":a[8].toString());
                advanceRequiredListing.setUserId(a[9]==null?0L:Long.parseLong(a[9].toString()));
                listAdvances.add(advanceRequiredListing);
            }

            return listAdvances!=null && listAdvances.size()>0?listAdvances:Collections.EMPTY_LIST;

        }catch (Exception e){
            logger.error("Error findAllAdvancesRequiredListingByUserId: ",e);
            return null;
        }
    }
}

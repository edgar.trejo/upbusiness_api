package com.mx.sivale.repository.impl;

import com.mx.sivale.model.Event;
import com.mx.sivale.repository.EventPaidDetailRepository;
import com.mx.sivale.repository.EventRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Component
public class EventPaidDetailRepositoryImpl implements EventPaidDetailRepository {

    private Logger logger=Logger.getLogger(EventPaidDetailRepositoryImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private EventRepository eventRepository;

    @Override
    public List<Event> findAllEventsPaid(Long clientId, Date from, Date to ) throws Exception {
        try {
            logger.debug("clientId: "+clientId);
            logger.debug("from: "+from);
            logger.debug("to: "+to);

            String query = " select distinct event.id\n" +
                    " from mercurio.event event\n" +
                    " left join mercurio.event_approver_report report\n" +
                    " on event.id=report.event_id\n" +
                    " where client_id=?\n" +
                    " and ( ( event.date_paid is not null and event.date_paid between ? and ? ) \n" +
                    " or ( event.date_paid is null and event.date_created between ? and ? ) ) \n" +
                    " order by event.date_paid desc, event.date_created desc;";

            Query q = entityManager.createNativeQuery(query);
            q.setParameter(1, clientId);
            q.setParameter(2, from);
            q.setParameter(3, to);
            q.setParameter(4, from);
            q.setParameter(5, to);
            List<BigInteger> eventsIds = q.getResultList();

            List<Event> eventList = new ArrayList<>();
            for (BigInteger bi : eventsIds) {
                Long eventId=bi.longValue();
                if(eventId>0){
                    Event e=eventRepository.findOne(eventId);
                    eventList.add(e);
                }
            }
            return eventList;

        }catch (Exception e){
            logger.error("Error findAllEventsPaid: ",e);
            return Collections.emptyList();
        }
    }
}

package com.mx.sivale.repository;

import com.mx.sivale.model.Client;
import com.mx.sivale.model.Event;
import com.mx.sivale.model.User;
import com.mx.sivale.repository.exception.RepositoryException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;
import java.util.Set;

public interface EventRepository extends JpaRepository<Event, Long> {

	List<Event> findByClientOrderByDateStartDescNameDesc(Client client) throws RepositoryException;

	Page<Event> findByClientAndNameIgnoreCaseContainingAndApprovalStatus_IdIn(Client client,String name,List<Long> status,Pageable pageable);

	Page<Event> findByClientAndNameIgnoreCaseContainingAndApprovalStatus_IdInAndUser_Id(Client client,String name,List<Long> status, Long userId, Pageable pageable);

	Page<Event> findAllByClientAndDateStartIsBetween(Client client, Date from, Date to, Pageable pageable) throws RepositoryException;

	List<Event> findAllByClientAndDateStartIsBetween(Client client, Date from, Date to) throws RepositoryException;

	@Query("SELECT e FROM Event e WHERE e.client = :client " +
				"AND e.dateStart between :from AND :to AND e.user.id IN(:users) ")
	Page<Event> findAllByClientAndDateStartIsBetweenAndUsers(@Param("client") Client client,
														@Param("from")  Date from,
														@Param("to")  Date to,
														@Param("users") Set<Long> users,
														Pageable pageable) throws RepositoryException;

	//Page<Event> findAllByClientAndDatePaidIsBetweenOrDatePaidIsNullAndDateStartIsBetweenOrderByDatePaidDescDateStartDesc(Client client, Date from, Date to,Date from1, Date to2, Pageable pageable) throws RepositoryException;

	List<Event> findByUserAndClientOrderByDateStartDescNameDesc(User user,Client client) throws RepositoryException;

	@Query("SELECT e FROM Event e WHERE e.id IN(:eventsId) ")
	Page<Event> findAllByIdIn(@Param("eventsId") List<Long> eventIds, Pageable pageable) throws RepositoryException;

}

package com.mx.sivale.repository;

import com.mx.sivale.model.BulkFileStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BulkFileStatusRepository extends JpaRepository<BulkFileStatus, Long> {

    List<BulkFileStatus> findByClientIdAndFileTypeAndFinished(String clientId,String fileType, Boolean finished);

    List<BulkFileStatus> findByClientIdAndFileTypeAndStatusIsNotInOrderByIdDesc(String clientId,String fileType, int status);
    
}

package com.mx.sivale.repository;

import com.mx.sivale.model.AccountingTypeOperation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountingTypeOperationRepository extends JpaRepository<AccountingTypeOperation, Integer> {

}

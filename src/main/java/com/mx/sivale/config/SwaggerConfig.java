package com.mx.sivale.config;

import com.mx.sivale.controller.*;
import com.mx.sivale.model.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 
 * @author amartinezmendoza 07/07/2017
 *
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket api() {
		Class[] clazz = {/*AccountingReport.class, AccountingTypeOperation.class,  AdvanceApproverReport.class,
				AdvanceApproverReportAlternative.class, AdvanceRequired.class, AdvanceRequiredApprover.class,
				AdvanceStatus.class, AmountSpendingType.class, ApprovalRule.class, ApprovalRuleAdvance.class,
				ApprovalRuleFlowType.class, ApprovalStatus.class, ApproverUser.class,BulkFileRow.class,
				BulkFileStatus.class, Client.class, ClientRfc.class, ClientRfcId.class, ConceptTax.class,
				com.mx.sivale.model.Configuration.class, CostCenter.class, Event.class, EventApprover.class,
				EventApproverReport.class, EvidenceType.class, FederativeEntity.class, ImageEvidence.class,

				ImageRfc.class, Invoice.class, InvoiceBatchProcess.class, InvoiceConcept.class, InvoiceFile.class,
				InvoiceSpendingAssociate.class, JobPosition.class, LogEmail.class, PayMethod.class, Project.class,

				RequestTransferType.class, Role.class, Spending.class, SpendingType.class, Team.class, TeamUsers.class,

				Transaction.class, Transfer.class,TransferType.class,

				 UserClient.class,

				 UserClientRole.class
				User.class*/
				};
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.mx.sivale.controller")).paths(PathSelectors.any()).build()
				.apiInfo(apiInformation())
				.ignoredParameterTypes(clazz);
	}

	private ApiInfo apiInformation() {
		return new ApiInfo(
				"PALO-IT SPRING API DOCUMENTATION", 
				"Inteliviajes", 
				"0.1", 
				"Terms of service",
				new Contact("Palo-IT", "http://palo-it.com/", "http://palo-it.com/"), 
				"License Version 1.0",
				"https://www.sivale.mx/");
	}
}

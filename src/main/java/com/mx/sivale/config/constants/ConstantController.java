package com.mx.sivale.config.constants;

public class ConstantController {
	
	/**
	 * SECTION TO API CONTROLLER
	 */
	public static final String API_USER_VALUE = "user_controller";
	public static final String API_USER_DESCRIPTION = "This controller contains services for different user movements.";
	public static final String API_CARD_VALUE = "card_controller";
	public static final String API_CARD_DESCRIPTION = "This controller contains services for different card movements.";

	/**
	 * SECTION TO API OPERATIOS
	 */
	public static final String API_OPERATION_VALUE_BALANCE = "Service contain method for get the balance of a card by your IUT.";
	public static final String API_OPERATION_NOTE_BALANCE = "This service get balance of a card by IUT.";

	public static final String API_OPERATION_VALUE_VALIDATE = "Service that contain method for validate the card by your number.";
	public static final String API_OPERATION_NOTE_VALIDATE = "This service validate the card by your number.";
	
	
	/**
	 * SECTION RESPONSE CONTAINERS
	 */
	public static final String CONTAINER_OPTIONS = "List[HttpMethod]";
	
	/**
	 * SECTION TO ENDPOINT's RESTFUL SERVICES
	 */
	public static final String URL_USER_LOGIN = "user/login";
	public static final String URL_USER_RESET = "/user/resetPassword";
	
	
	
	public static final String URL_USER_UPLOAD = "/secure/users";

	public static final String URL_CARD_VALIDATE = "/secure/card/validate";
	public static final String URL_CARD_BALANCE = "/secure/card/balance";
	
	
	private ConstantController() { }

}

package com.mx.sivale.config.constants;

public class ConstantConfiguration {
	
	public static final String WS_NAMESPACE_APPS = "http://mx.com.sivale.ws/exposition/servicios/apps";
	public static final String WS_NAMESPACE_CARD = "http://mx.com.sivale.ws/exposition/servicios/tarjeta";
	public static final String WS_NAMESPACE_GASOLINA = "http://mx.com.sivale.ws/exposition/servicios/appgasolina";
	public static final String WS_NAMESPACE_GPV = "http://corporativogpv.mx/CORE/01CORE/Legados";

	public static final String SERVICE_APP = "ServiciosApps";
	public static final String SERVICE_APP_PORT = "ServiciosAppsTypePort";

	public static final String SERVICE_CARD = "ServiciosTarjeta";
	public static final String SERVICE_CARD_PORT = "ServiciosTarjetaTypePort";

	public static final String SERVICE_GPV = "SI_01CORELegadosReqSyncOutService";
	public static final String SERVICE_GPV_PORT_HTTP = "HTTP_Port";
	public static final String SERVICE_GPV_PORT_HTTPS = "HTTPS_Port";

	public static final String IDENTIFICATION = "identificacion";

	public static final String USER = "usuario";
	public static final String PASS = "password";
	public static final String SOLI = "solicitante";
	public static final String INVO = "invoke";

	public static final String SOAPUI = "SoapUI";

	public static final String STATUS_SERVICE_GPV = "STATUS_SERVICE_GPV";
	public static final String STATUS_ADVANCE_GPV = "STATUS_ADVANCE_GPV";
	public static final String STATUS_EVENT_GPV = "STATUS_EVENT_GPV";
	public static final String STATUS_REFUND_GPV = "STATUS_REFUND_GPV";
	public static final String CLIENT_GPV = "CLIENT_GPV";
	public static final String BP_SIVALE = "BP_SIVALE";
	public static final String URL_SERVICE_GPV_RESOURCES = "wsdl/gpv.wsdl";

	private ConstantConfiguration() {}
}

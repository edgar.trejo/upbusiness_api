package com.mx.sivale.config.constants;

public final class ConstantInteliviajes {

	public static final String CLIENT = "client";
	public static final String CLAIMS = "claims";
	public static final String EMAIL = "email";
	public static final String CLIENT_ID = "clientId";
	public static final String CLIENT_NAME = "clientName";
	public static final String CONTACT_ID = "contactId";
	public static final String ROLE_ID = "roleId";
	public static final String ORIGIN = "origin";

	public static final String ORIGIN_WEB = "WEBM";
	public static final String ORIGIN_APP = "APPM";

	public static final String OPERATION_SYSTEM = "Linux";

	public static final String CONTROL_CT = "TRASPASO DE CONCENTRADORA A TITULAR INTELIVIAJES";
	public static final String CONTROL_TC = "TRASPASO DE TITULAR A CONCENTRADORA INTELIVIAJES";

	//GPV
	public static final String DOCUMENTHEADER_USERNAME = "SIVALE-BC";
	public static final String IDENTIFIER_CC = "CC";
	public static final String DOCTYPE = "GV";
	public static final String DOCTYPE_CC = "CH";
	public static final String REFDOCNO_ANTICIPO = "SV Anticipo";
	public static final String REFDOCNO_ANTICIPO_CC = "SV Anticipo CC";
	public static final String REFDOCNO_DEVOLUCION = "SV Devolución";
	public static final String REFDOCNO_COMPROBACIONGV = "SV Comprobación";
	public static final String REFDOCNO_COMPROBACIONGV_CC = "SV Comprob CC";
	public static final String PMNTTRMS = "0001";
	public static final String BUS_AREA_ADVANCED = "000";
	public static final String SP_GL_IND_BPSIVALE = "A";
	public static final String PMNT_BLOCK = "A";
	public static final String SP_GL_IND_BPEMPLEADO = "D";
	public static final String CURRTYPE = "MXN";
	public static final String BUS_AREA = "003";
	public static final String BUS_AREA_COMPROBACION = "000";
	public static final String SUCCESS_TYPE = "S";
	public static final String SUCCESS_ID = "RW";
	//public static final String SUCCESS_NUMBER = "638";
	public static final String SUCCESS_NUMBER = "605";
	public static final String TAXCODE_P1 = "P1";
	public static final String TAXCODE_P2 = "P2";
	public static final String TAXCODE_P5 = "P5";
	public static final String TAXCODE_P8 = "P8";
	public static final String TAX_IVA="002";
	public static final Double TAX_P1 = 0.0;
	public static final Double TAX_P2 = 0.16;
	public static final Double TAX_P8 = 0.08;
	public static final String GLACCOUNT = "1633010000";


	private ConstantInteliviajes() {

	}

}

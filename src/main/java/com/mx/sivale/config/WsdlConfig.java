package com.mx.sivale.config;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class WsdlConfig {

    private static final Logger log = Logger.getLogger(WsdlConfig.class);

    public static String WS_URL_APPS;
    @Value("${url.inteliviajes.apps}")
    public void setWsUrlApps(String url) {
        WS_URL_APPS = url;
    }
    
    public static String WS_URL_TARJETAS;
    @Value("${url.inteliviajes.tarjeta}")
    public void setWsUrlTarjetas(String url) {
        WS_URL_TARJETAS = url;
    }

    public static String WS_URL_GPV;
    @Value("${url.gpv.legados}")
    public void setWsUrlGpv(String url) {
        WS_URL_GPV = url;
    }

    public static String PROP_WS_USER;
    @Value("${ws.inteliviajes.user}")
    public void setPropWsUser(String user) {
        PROP_WS_USER = user;
    }

    public static String PROP_WS_PASS;
    @Value("${ws.inteliviajes.password}")
    public void setPropWsPass(String password) {
        PROP_WS_PASS = password;
    }

    public static String PROP_WS_GPV_USER;
    @Value("${ws.gpv.user}")
    public void setPropWsUserGpv(String userGpv) {
        PROP_WS_GPV_USER = userGpv;
    }

    public static String PROP_WS_GPV_PASS;
    @Value("${ws.gpv.password}")
    public void setPropWsPassGpv(String passwordGpv) {
        PROP_WS_GPV_PASS = passwordGpv;
    }

    public static String PROP_WS_ORIGIN;
    @Value("${ws.inteliviajes.origin}")
    public void setPropWsOrigin(String origin) {
        PROP_WS_ORIGIN = origin;
    }

}

package com.mx.sivale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SivaleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SivaleApplication.class, args);
	}

}

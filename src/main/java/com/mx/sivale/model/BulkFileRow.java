package com.mx.sivale.model;

import com.mx.sivale.model.dto.UserRequestDTO;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "bulk_file_row", schema = "mercurio")
public class BulkFileRow {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long idRow;

    @JoinColumn(name = "bulk_file_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private BulkFileStatus bulkFileId;

    @Column(name = "name")
    private String name;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "gender")
    private String gender;

    @Column(name = "email")
    private String email;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "birth_date")
    private String birthDate;

    @Column(name = "federative_entity")
    private String federativeEntityName;

    @Column(name = "number_employee")
    private String numberEmployee;

    @Column(name = "job_position_code")
    private String jobPositionCode;

    @Column(name = "cost_center_code")
    private String costCenterCode;

    @Column(name = "email_approver")
    private String emailApprover;
    
    @Column(name = "advance_available")
    private Boolean advanceAvailable;

    @Column(name = "card_one")
    private String cardOne;

    @Column(name = "card_two")
    private String cardTwo;

    @Column(name = "card_three")
    private String cardThree;

    @Column(name = "card_four")
    private String cardFour;

    @Column(name = "status")
    private int status;

    @Column(name = "result_message")
    private String resultMessage;

    @Transient
    private UserRequestDTO userDTO;

    @Transient
    private  StringBuilder rowErrors;

    public Long getIdRow() {
        return idRow;
    }

    public void setIdRow(Long idRow) {
        this.idRow = idRow;
    }

    public BulkFileStatus getBulkFileId() {
        return bulkFileId;
    }

    public void setBulkFileId(BulkFileStatus bulkFileId) {
        this.bulkFileId = bulkFileId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getFederativeEntityName() {
        return federativeEntityName;
    }

    public void setFederativeEntityName(String federativeEntityName) {
        this.federativeEntityName = federativeEntityName;
    }

    public String getNumberEmployee() {
        return numberEmployee;
    }

    public void setNumberEmployee(String numberEmployee) {
        this.numberEmployee = numberEmployee;
    }

    public String getJobPositionCode() {
        return jobPositionCode;
    }

    public void setJobPositionCode(String jobPositionCode) {
        this.jobPositionCode = jobPositionCode;
    }

    public String getCostCenterCode() {
        return costCenterCode;
    }

    public void setCostCenterCode(String costCenterCode) {
        this.costCenterCode = costCenterCode;
    }

    public String getEmailApprover() {
        return emailApprover;
    }

    public void setEmailApprover(String emailApprover) {
        this.emailApprover = emailApprover;
    }

    public String getCardOne() {
        return cardOne;
    }

    public void setCardOne(String cardOne) {
        this.cardOne = cardOne;
    }

    public String getCardTwo() {
        return cardTwo;
    }

    public void setCardTwo(String cardTwo) {
        this.cardTwo = cardTwo;
    }

    public String getCardThree() {
        return cardThree;
    }

    public void setCardThree(String cardThree) {
        this.cardThree = cardThree;
    }

    public String getCardFour() {
        return cardFour;
    }

    public void setCardFour(String cardFour) {
        this.cardFour = cardFour;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public UserRequestDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserRequestDTO userDTO) {
        this.userDTO = userDTO;
    }

    public StringBuilder getRowErrors() {
        return rowErrors;
    }

    public void setRowErrors(StringBuilder rowErrors) {
        this.rowErrors = rowErrors;
    }

    
    public Boolean getAdvanceAvailable() {
		return advanceAvailable;
	}

	public void setAdvanceAvailable(Boolean advanceAvailable) {
		this.advanceAvailable = advanceAvailable;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BulkFileRow that = (BulkFileRow) o;
        return status == that.status &&
                Objects.equals(idRow, that.idRow) &&
                Objects.equals(bulkFileId, that.bulkFileId) &&
                Objects.equals(name, that.name) &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(gender, that.gender) &&
                Objects.equals(email, that.email) &&
                Objects.equals(phoneNumber, that.phoneNumber) &&
                Objects.equals(birthDate, that.birthDate) &&
                Objects.equals(federativeEntityName, that.federativeEntityName) &&
                Objects.equals(numberEmployee, that.numberEmployee) &&
                Objects.equals(jobPositionCode, that.jobPositionCode) &&
                Objects.equals(costCenterCode, that.costCenterCode) &&
                Objects.equals(emailApprover, that.emailApprover) &&
                Objects.equals(cardOne, that.cardOne) &&
                Objects.equals(cardTwo, that.cardTwo) &&
                Objects.equals(cardThree, that.cardThree) &&
                Objects.equals(cardFour, that.cardFour) &&
                Objects.equals(resultMessage, that.resultMessage);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idRow, bulkFileId, name, firstName, lastName, gender, email, phoneNumber, birthDate, federativeEntityName, numberEmployee, jobPositionCode, costCenterCode, emailApprover, cardOne, cardTwo, cardThree, cardFour, status, resultMessage);
    }

    @Override
    public String toString() {
        return "BulkFileRow{" +
                "idRow=" + idRow +
                ", bulkFileId=" + bulkFileId +
                ", name='" + name + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", gender='" + gender + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", federativeEntityName='" + federativeEntityName + '\'' +
                ", numberEmployee='" + numberEmployee + '\'' +
                ", jobPositionCode='" + jobPositionCode + '\'' +
                ", costCenterCode='" + costCenterCode + '\'' +
                ", emailApprover='" + emailApprover + '\'' +
                ", cardOne='" + cardOne + '\'' +
                ", cardTwo='" + cardTwo + '\'' +
                ", cardThree='" + cardThree + '\'' +
                ", cardFour='" + cardFour + '\'' +
                ", status=" + status +
                ", resultMessage='" + resultMessage + '\'' +
                '}';
    }
}

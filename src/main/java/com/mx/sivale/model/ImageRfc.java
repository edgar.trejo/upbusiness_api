package com.mx.sivale.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * 
 * @author amartinezmendoza
 * 
 *         The persistent class for the image_rfc database table.
 *
 */
@Entity
@Table(name = "image_rfc", schema = "mercurio")
public class ImageRfc implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(unique = true, nullable = false)
	private Long id;

	@Column(name = "time_created", nullable = false)
	private Timestamp timeCreated;

	@Column(name = "rfc", length = 14)
	private String rfc;

	@Column(name = "name", length = 150)
	private String name;

	@Column(name = "fiscal_address", length = 250)
	private String fiscalAddress;

	@ManyToOne
	@JoinColumn(name = "user_creater_id", referencedColumnName = "id")
	private User user;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getTimeCreated() {
		return timeCreated;
	}

	public void setTimeCreated(Timestamp timeCreated) {
		this.timeCreated = timeCreated;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFiscalAddress() {
		return fiscalAddress;
	}

	public void setFiscalAddress(String fiscalAddress) {
		this.fiscalAddress = fiscalAddress;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
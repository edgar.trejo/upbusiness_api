package com.mx.sivale.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "accounting_type_operation", schema = "mercurio")
public class AccountingTypeOperation {

    public static final Integer ADVANCE = 1;
    public static final Integer EVENT = 2;
    public static final Integer REFUND = 3;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "accountingTypeOperation")
    private List<AccountingReport> accountingReportList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<AccountingReport> getAccountingReportList() {
        return accountingReportList;
    }

    public void setAccountingReportList(List<AccountingReport> accountingReportList) {
        this.accountingReportList = accountingReportList;
    }
}

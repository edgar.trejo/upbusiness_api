package com.mx.sivale.model;

import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the custom_report_config database table.
 */
@Entity
@Table(name = "custom_report_config", schema = "mercurio")
public class CustomReportConfig {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "active")
    private Boolean active;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(targetEntity = CustomReportConfigColumn.class, mappedBy = "customReportConfig", cascade = CascadeType.ALL)
    private List<CustomReportConfigColumn> customReportConfigColumn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<CustomReportConfigColumn> getCustomReportConfigColumn() {
        return customReportConfigColumn;
    }

    public void setCustomReportConfigColumn(List<CustomReportConfigColumn> customReportConfigColumn) {
        this.customReportConfigColumn = customReportConfigColumn;
    }

    @Override
    public String toString() {
        return "CustomReportConfig{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", active=" + active +
                ", user=" + user +
                ", customReportConfigColumn=" + customReportConfigColumn +
                '}';
    }
}
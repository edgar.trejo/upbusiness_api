package com.mx.sivale.model;

import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the invoice database table.
 * 
 */
@Entity
@Table(name = "log_email", schema = "mercurio")
public class LogEmail {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User user;

	@Column(name = "date")
	private String date;

	@Column(name = "subject")
	private String subject;

	@Column(name = "attachments")
	private Integer attachments;

	@Column(name = "sender")
	private String sender;

	@Transient
	private Date formatDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Integer getAttachments() {
		return attachments;
	}

	public void setAttachments(Integer attachments) {
		this.attachments = attachments;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public Date getFormatDate() {
		return formatDate;
	}

	public void setFormatDate(Date formatDate) {
		this.formatDate = formatDate;
	}
}
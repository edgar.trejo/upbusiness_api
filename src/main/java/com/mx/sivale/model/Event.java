package com.mx.sivale.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mx.sivale.model.dto.ApproverDetailDTO;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "event", schema = "mercurio")
public class Event {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "code")
	private String code;

	@Column(name = "description")
	private String description;

	@Column(name = "date_start")
	private Timestamp dateStart;

	@Column(name = "date_end")
	private Timestamp dateEnd;

	@Column(name = "date_created")
	private Timestamp dateCreated;

	@Column(name = "date_finished")
	private Timestamp dateFinished;

	@Column(name = "date_paid")
	private Timestamp datePaid;

	@Column(name = "active")
	private Boolean active;

	@ManyToOne
	@JoinColumn(name = "approval_status_id", referencedColumnName = "id")
	private ApprovalStatus approvalStatus;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	@ManyToOne
	@JoinColumn(name = "client_id", referencedColumnName = "id")
	private Client client;

	@OneToMany(targetEntity = Spending.class, mappedBy = "event", cascade = CascadeType.ALL)
    private List<Spending> spendings;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "event")
	private List<EventApprover> eventApproverEntityList;

	@OneToMany(mappedBy = "event")
	private List<EventApproverReport> eventApproverReportList;

	@Transient
	List<ApproverDetailDTO> eventApproverList;

	@Transient
	Map<Integer,List<ApproverDetailDTO>> eventApproverHistory;

	@Transient
	private double totalAmount;

	@Transient
	private double totalInvoicedAmount;

	@Transient
	private boolean hasAnyEvidence;

	@Transient
	private boolean isOwner;

	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@JoinColumns({
			@JoinColumn(
					name = "user_id",
					referencedColumnName = "user_id", insertable = false, updatable = false),
			@JoinColumn(
					name = "client_id",
					referencedColumnName = "client_id", insertable = false, updatable = false)
	})
	@NotFound(action = NotFoundAction.IGNORE)
	@JsonIgnore
	private UserClient userClient;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getDateStart() {
		return dateStart;
	}

	public void setDateStart(Timestamp dateStart) {
		this.dateStart = dateStart;
	}

	public Timestamp getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(Timestamp dateEnd) {
		this.dateEnd = dateEnd;
	}

	public Timestamp getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Timestamp dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Timestamp getDateFinished() {
		return dateFinished;
	}

	public void setDateFinished(Timestamp dateFinished) {
		this.dateFinished = dateFinished;
	}

	public Timestamp getDatePaid() {
		return datePaid;
	}

	public void setDatePaid(Timestamp datePaid) {
		this.datePaid = datePaid;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public ApprovalStatus getApprovalStatus() {
		return approvalStatus;
	}

	public void setApprovalStatus(ApprovalStatus approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public List<Spending> getSpendings() {
		return spendings;
	}

	public void setSpendings(List<Spending> spendings) {
		this.spendings = spendings;
	}

	public List<ApproverDetailDTO> getEventApproverList() {
		return eventApproverList;
	}

	public void setEventApproverList(List<ApproverDetailDTO> eventApproverList) {
		this.eventApproverList = eventApproverList;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getTotalInvoicedAmount() {
		return totalInvoicedAmount;
	}

	public void setTotalInvoicedAmount(double totalInvoicedAmount) {
		this.totalInvoicedAmount = totalInvoicedAmount;
	}

	public boolean isHasAnyEvidence() {
		return hasAnyEvidence;
	}

	public void setHasAnyEvidence(boolean hasAnyEvidence) {
		this.hasAnyEvidence = hasAnyEvidence;
	}

	public boolean isOwner() {
		return isOwner;
	}

	public void setOwner(boolean owner) {
		isOwner = owner;
	}

	public Map<Integer, List<ApproverDetailDTO>> getEventApproverHistory() {
		return eventApproverHistory;
	}

	public void setEventApproverHistory(Map<Integer, List<ApproverDetailDTO>> eventApproverHistory) {
		this.eventApproverHistory = eventApproverHistory;
	}

	public UserClient getUserClient() {
		return userClient;
	}

	public void setUserClient(UserClient userClient) {
		this.userClient = userClient;
	}

	public List<EventApprover> getEventApproverEntityList() {
		return eventApproverEntityList;
	}

	public void setEventApproverEntityList(List<EventApprover> eventApproverEntityList) {
		this.eventApproverEntityList = eventApproverEntityList;
	}

	public List<EventApproverReport> getEventApproverReportList() {
		return eventApproverReportList;
	}

	public void setEventApproverReportList(List<EventApproverReport> eventApproverReportList) {
		this.eventApproverReportList = eventApproverReportList;
	}
}
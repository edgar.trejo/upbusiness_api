package com.mx.sivale.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "concept_tax", schema = "mercurio")
public class ConceptTax implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @JoinColumn(name = "concept_id", referencedColumnName = "id")
    @ManyToOne
    private InvoiceConcept invoiceConceptId;

    @Column(name = "tax")
    private String tax;

    @Column(name = "factor_type")
    private String factorType;

    @Column(name = "base")
    private String base;

    @Column(name = "fee")
    private String fee;

    @Column(name = "amount")
    private String amount;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public InvoiceConcept getInvoiceConceptId() {
        return invoiceConceptId;
    }

    public void setInvoiceConceptId(InvoiceConcept invoiceConceptId) {
        this.invoiceConceptId = invoiceConceptId;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getFactorType() {
        return factorType;
    }

    public void setFactorType(String factorType) {
        this.factorType = factorType;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "ConceptTax{" +
                "id=" + id +
                ", invoiceConceptId=" + invoiceConceptId +
                ", tax='" + tax + '\'' +
                ", factorType='" + factorType + '\'' +
                ", base='" + base + '\'' +
                ", fee='" + fee + '\'' +
                ", amount='" + amount + '\'' +
                '}';
    }
}

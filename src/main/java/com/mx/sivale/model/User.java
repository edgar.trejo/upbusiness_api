package com.mx.sivale.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * @author amartinezmendoza
 * <p>
 * The persistent class for the user database table.
 */
@Entity
@Table(name = "user", schema = "mercurio")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "name")
    private String name;

    @Transient
    private String completeName;

    @Column(name = "number_employee")
    private String numberEmployee;

    @Column(name = "email")
    private String email;

    @Column(name = "authentication_token")
    private String authenticationToken;

    @Column(name = "birth_date")
    private Timestamp birthDate;

    @Column(name = "device_token")
    private String deviceToken;

    @Column(name = "gender")
    private String gender;

    @Column(name = "reference")
    private String reference;

    @Column(name = "si_vale_id")
    private Long siValeId;

    @Column(name = "active")
    private Boolean active;
    
    @Column(name = "advance_available")
    private Boolean advanceAvailable;

    // bi-directional many-to-one association to FederativeEntity
    @ManyToOne
    @JoinColumn(name = "federative_entity_id", referencedColumnName = "id")
    private FederativeEntity federativeEntity;

    @Transient
    private boolean isUpdated;

    @Transient
    private String group;

    @Transient
    private String groupCode;

    @Transient
    private String invoiceEmail;

    @Transient
    @JsonBackReference
    private String invoiceEmailPassword;

    /*
    @OneToMany(mappedBy = "user")
    private List<InvoiceFile> invoiceFileList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private List<AdvanceRequired> advanceRequiredList;

    @OneToMany(mappedBy = "user")
    private List<Spending> spendingList;

    @OneToMany(mappedBy = "user")
    private List<Transfer> transferList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private List<Transfer> transferList1;

    @OneToMany(mappedBy = "user")
    private List<TeamUsers> teamUsersList;

    @OneToMany(mappedBy = "user")
    private List<EventApproverReport> eventApproverReportList;

    @OneToMany(mappedBy = "user")
    private List<Event> eventList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private List<LogEmail> logEmailList;

    @OneToMany(mappedBy = "user")
    private List<ApproverUser> approverUserList;

    @OneToMany(mappedBy = "user")
    private List<ImageRfc> imageRfcList;

    @OneToMany(mappedBy = "user")
    private List<AdvanceApproverReport> advanceApproverReportList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private List<UserClient> userClientList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private List<Invoice> invoiceList;

    @OneToMany(mappedBy = "user")
    private List<Transaction> transactionList;
*/
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthenticationToken() {
        return this.authenticationToken;
    }

    public void setAuthenticationToken(String authenticationToken) {
        this.authenticationToken = authenticationToken;
    }

    public Timestamp getBirthDate() {
        return this.birthDate;
    }

    public void setBirthDate(Timestamp birthDate) {
        this.birthDate = birthDate;
    }

    public String getCompleteName() {
        return this.getName() + " " + (this.getFirstName() != null ? " " + this.getFirstName() : "") + (this.getLastName() != null ? " " + this.getLastName() : "");
    }

    public void setCompleteName(String completeName) {
        this.completeName = completeName;
    }

    public String getDeviceToken() {
        return this.deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumberEmployee() {
        return this.numberEmployee;
    }

    public void setNumberEmployee(String numberEmployee) {
        this.numberEmployee = numberEmployee;
    }

    public String getReference() {
        return this.reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public FederativeEntity getFederativeEntity() {
        return this.federativeEntity;
    }

    public void setFederativeEntity(FederativeEntity federativeEntity) {
        this.federativeEntity = federativeEntity;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getSiValeId() {
        return siValeId;
    }

    public void setSiValeId(Long siValeId) {
        this.siValeId = siValeId;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public boolean isUpdated() {
        return isUpdated;
    }

    public void setUpdated(boolean updated) {
        isUpdated = updated;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getInvoiceEmail() {
        return invoiceEmail;
    }

    public void setInvoiceEmail(String invoiceEmail) {
        this.invoiceEmail = invoiceEmail;
    }

    public String getInvoiceEmailPassword() {
        return invoiceEmailPassword;
    }

    public void setInvoiceEmailPassword(String invoiceEmailPassword) {
        this.invoiceEmailPassword = invoiceEmailPassword;
    }

    public Boolean getAdvanceAvailable() {
		return advanceAvailable;
	}

	public void setAdvanceAvailable(Boolean advanceAvailable) {
		this.advanceAvailable = advanceAvailable;
	}

	@Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", name='" + name + '\'' +
                ", completeName='" + completeName + '\'' +
                ", numberEmployee='" + numberEmployee + '\'' +
                ", email='" + email + '\'' +
                ", authenticationToken='" + authenticationToken + '\'' +
                ", birthDate=" + birthDate +
                ", deviceToken='" + deviceToken + '\'' +
                ", gender='" + gender + '\'' +
                ", reference='" + reference + '\'' +
                ", siValeId=" + siValeId +
                ", active=" + active +
                ", federativeEntity=" + federativeEntity +
                ", isUpdated=" + isUpdated +
                ", group='" + group + '\'' +
                ", groupCode='" + groupCode + '\'' +
                '}';
    }
/*
    public List<InvoiceFile> getInvoiceFileList() {
        return invoiceFileList;
    }

    public void setInvoiceFileList(List<InvoiceFile> invoiceFileList) {
        this.invoiceFileList = invoiceFileList;
    }

    public List<AdvanceRequired> getAdvanceRequiredList() {
        return advanceRequiredList;
    }

    public void setAdvanceRequiredList(List<AdvanceRequired> advanceRequiredList) {
        this.advanceRequiredList = advanceRequiredList;
    }

    public List<Spending> getSpendingList() {
        return spendingList;
    }

    public void setSpendingList(List<Spending> spendingList) {
        this.spendingList = spendingList;
    }

    public List<Transfer> getTransferList() {
        return transferList;
    }

    public void setTransferList(List<Transfer> transferList) {
        this.transferList = transferList;
    }

    public List<Transfer> getTransferList1() {
        return transferList1;
    }

    public void setTransferList1(List<Transfer> transferList1) {
        this.transferList1 = transferList1;
    }

    public List<TeamUsers> getTeamUsersList() {
        return teamUsersList;
    }

    public void setTeamUsersList(List<TeamUsers> teamUsersList) {
        this.teamUsersList = teamUsersList;
    }

    public List<EventApproverReport> getEventApproverReportList() {
        return eventApproverReportList;
    }

    public void setEventApproverReportList(List<EventApproverReport> eventApproverReportList) {
        this.eventApproverReportList = eventApproverReportList;
    }

    public List<Event> getEventList() {
        return eventList;
    }

    public void setEventList(List<Event> eventList) {
        this.eventList = eventList;
    }

    public List<LogEmail> getLogEmailList() {
        return logEmailList;
    }

    public void setLogEmailList(List<LogEmail> logEmailList) {
        this.logEmailList = logEmailList;
    }

    public List<ApproverUser> getApproverUserList() {
        return approverUserList;
    }

    public void setApproverUserList(List<ApproverUser> approverUserList) {
        this.approverUserList = approverUserList;
    }

    public List<ImageRfc> getImageRfcList() {
        return imageRfcList;
    }

    public void setImageRfcList(List<ImageRfc> imageRfcList) {
        this.imageRfcList = imageRfcList;
    }

    public List<AdvanceApproverReport> getAdvanceApproverReportList() {
        return advanceApproverReportList;
    }

    public void setAdvanceApproverReportList(List<AdvanceApproverReport> advanceApproverReportList) {
        this.advanceApproverReportList = advanceApproverReportList;
    }

    public List<UserClient> getUserClientList() {
        return userClientList;
    }

    public void setUserClientList(List<UserClient> userClientList) {
        this.userClientList = userClientList;
    }

    public List<Invoice> getInvoiceList() {
        return invoiceList;
    }

    public void setInvoiceList(List<Invoice> invoiceList) {
        this.invoiceList = invoiceList;
    }

    public List<Transaction> getTransactionList() {
        return transactionList;
    }

    public void setTransactionList(List<Transaction> transactionList) {
        this.transactionList = transactionList;
    }

 */
}
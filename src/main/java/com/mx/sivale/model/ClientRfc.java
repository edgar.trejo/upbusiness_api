package com.mx.sivale.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 
 * @author amartinezmendoza
 * 
 *        The persistent class for the client_rfc database table.
 *
 */
@Entity
@IdClass(ClientRfcId.class)
@Table(name = "client_rfc", schema = "mercurio")
public class ClientRfc implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "client_id")
	private Long clientId;

	@Id
	@Column(name = "rfc_id")
	private Long imageRfcId;

	@Column(name = "active", columnDefinition = "TINYINT")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean active;

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public Long getImageRfcId() {
		return imageRfcId;
	}

	public void setImageRfcId(Long imageRfcId) {
		this.imageRfcId = imageRfcId;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

}
package com.mx.sivale.model.dto;

import java.io.Serializable;

public class UserSessionDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long roleId;
	private String email;
	private ClientDTO client;
	private String origin;

	public UserSessionDTO() {
		super();
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public ClientDTO getClient() {
		return client;
	}

	public void setClient(ClientDTO client) {
		this.client = client;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

}

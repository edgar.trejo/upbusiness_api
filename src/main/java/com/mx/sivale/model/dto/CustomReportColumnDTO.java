package com.mx.sivale.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomReportColumnDTO {

    private Long id;
    private String name;
    private String description;
    private Long order;

    private CustomReportTypeColumnDTO typeColumn;

    public Long getOrder() {
        return order;
    }

    public void setOrder(Long order) {
        this.order = order;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CustomReportColumnDTO(Long id, String name, String description, Long order) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.order = order;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CustomReportTypeColumnDTO getTypeColumn() {
        return typeColumn;
    }

    public void setTypeColumn(CustomReportTypeColumnDTO typeColumn) {
        this.typeColumn = typeColumn;
    }

    public CustomReportColumnDTO(Long id, String name, String description, Long order, CustomReportTypeColumnDTO typeColumn) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.order = order;
        this.typeColumn = typeColumn;
    }

    public CustomReportColumnDTO(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public CustomReportColumnDTO() {
    }
}

package com.mx.sivale.model.dto;

import com.mx.sivale.model.User;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

public class EventDetailDTO {
    private Long eventId;
    private User user;
    private Timestamp eventDateCreated;
    private Timestamp eventDateStart;
    private Timestamp eventDateEnd;
    private String dateEvent;
    private String hourEvent;
    private String eventName;
    private String eventDescription;
    private String eventApprovalStatusCode;
    private String completeNameUserEvent;
    private List<SpendingDetailDTO> spendings;
    private List<EventApproverDetailOld> eventApproverList;
    private Map<Integer,List<EventApproverDetailDTO>> eventApproverDetailMap;
    private List<EventApproverDetailDTO> listCurrentApprovers;
    private double totalAmount;
    private double totalInvoicedAmount;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public Timestamp getEventDateCreated() {
        return eventDateCreated;
    }

    public void setEventDateCreated(Timestamp eventDateCreated) {
        this.eventDateCreated = eventDateCreated;
    }

    public Timestamp getEventDateStart() {
        return eventDateStart;
    }

    public void setEventDateStart(Timestamp eventDateStart) {
        this.eventDateStart = eventDateStart;
    }

    public Timestamp getEventDateEnd() {
        return eventDateEnd;
    }

    public void setEventDateEnd(Timestamp eventDateEnd) {
        this.eventDateEnd = eventDateEnd;
    }

    public String getDateEvent() {
        return dateEvent;
    }

    public void setDateEvent(String dateEvent) {
        this.dateEvent = dateEvent;
    }

    public String getHourEvent() {
        return hourEvent;
    }

    public void setHourEvent(String hourEvent) {
        this.hourEvent = hourEvent;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public String getEventApprovalStatusCode() {
        return eventApprovalStatusCode;
    }

    public void setEventApprovalStatusCode(String eventApprovalStatusCode) {
        this.eventApprovalStatusCode = eventApprovalStatusCode;
    }

    public String getCompleteNameUserEvent() {
        return completeNameUserEvent;
    }

    public void setCompleteNameUserEvent(String completeNameUserEvent) {
        this.completeNameUserEvent = completeNameUserEvent;
    }

    public List<SpendingDetailDTO> getSpendings() {
        return spendings;
    }

    public void setSpendings(List<SpendingDetailDTO> spendings) {
        this.spendings = spendings;
    }

    public List<EventApproverDetailOld> getEventApproverList() {
        return eventApproverList;
    }

    public void setEventApproverList(List<EventApproverDetailOld> eventApproverList) {
        this.eventApproverList = eventApproverList;
    }

    public Map<Integer, List<EventApproverDetailDTO>> getEventApproverDetailMap() {
        return eventApproverDetailMap;
    }

    public void setEventApproverDetailMap(Map<Integer, List<EventApproverDetailDTO>> eventApproverDetailMap) {
        this.eventApproverDetailMap = eventApproverDetailMap;
    }

    public List<EventApproverDetailDTO> getListCurrentApprovers() {
        return listCurrentApprovers;
    }

    public void setListCurrentApprovers(List<EventApproverDetailDTO> listCurrentApprovers) {
        this.listCurrentApprovers = listCurrentApprovers;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public double getTotalInvoicedAmount() {
        return totalInvoicedAmount;
    }

    public void setTotalInvoicedAmount(double totalInvoicedAmount) {
        this.totalInvoicedAmount = totalInvoicedAmount;
    }
}

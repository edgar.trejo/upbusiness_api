package com.mx.sivale.model.dto;

import com.mx.sivale.model.AdvanceRequired;

import java.io.Serializable;

/**
 * Created by amartinezmendoza on 09/11/2017.
 */
public class SenderEmailDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    public static final String TYPE_INFORMATIVE="informativa"; //Para el aprobador por defecto
    public static final String TYPE_ADMIN="admin"; //Notificación para el admin
    public static final String TYPE_APPROVER="aprovador"; //Notivicación para el aprovador
    public static final String TYPE_USER="user"; //Notificación para el user, solo se manda cuando es aprobado o rechazado el anticipo/informe
    public static final String TYPE_ERROR="error"; //Error al dispersar automaticamente

    private String emailBussiness;
    private String nameBussiness;
    private String commentBussiness;
    private String type;
    private AdvanceRequired advanceRequired;
    private EventDetailDTO event;

    public String getEmailBussiness() {
        return emailBussiness;
    }

    public void setEmailBussiness(String emailBussiness) {
        this.emailBussiness = emailBussiness;
    }

    public String getNameBussiness() {
        return nameBussiness;
    }

    public void setNameBussiness(String nameBussiness) {
        this.nameBussiness = nameBussiness;
    }

    public String getCommentBussiness() {
        return commentBussiness;
    }

    public void setCommentBussiness(String commentBussiness) {
        this.commentBussiness = commentBussiness;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public AdvanceRequired getAdvanceRequired() {
        return advanceRequired;
    }

    public void setAdvanceRequired(AdvanceRequired advanceRequired) {
        this.advanceRequired = advanceRequired;
    }

    public EventDetailDTO getEvent() {
        return event;
    }

    public void setEvent(EventDetailDTO event) {
        this.event = event;
    }
}

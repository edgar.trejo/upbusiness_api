package com.mx.sivale.model.dto;

import java.io.Serializable;

public class InvoiceConceptResponseDTO implements Serializable {

    private Long conceptId;
    private Long invoiceId;
    private String description;
    private String total;
    private String subtotal;
    private String tax;

    public Long getConceptId() {
        return conceptId;
    }

    public void setConceptId(Long conceptId) {
        this.conceptId = conceptId;
    }

    public Long getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Long invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    @Override
    public String toString() {
        return "InvoiceConceptResponseDTO{" +
                "conceptId=" + conceptId +
                ", invoiceId=" + invoiceId +
                ", description='" + description + '\'' +
                ", total='" + total + '\'' +
                ", subtotal='" + subtotal + '\'' +
                ", tax='" + tax + '\'' +
                '}';
    }
}

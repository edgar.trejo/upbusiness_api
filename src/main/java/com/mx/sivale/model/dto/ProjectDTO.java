package com.mx.sivale.model.dto;

import com.mx.sivale.model.User;

import java.io.Serializable;
import java.util.List;

public class ProjectDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private CatalogDTO catalogDTO;
    private List<User> users;

    public CatalogDTO getCatalogDTO() { return catalogDTO; }

    public void setCatalogDTO(CatalogDTO catalogDTO) { this.catalogDTO = catalogDTO; }

    public List<User> getUsers() { return users; }

    public void setUsers(List<User> users) { this.users = users; }

}

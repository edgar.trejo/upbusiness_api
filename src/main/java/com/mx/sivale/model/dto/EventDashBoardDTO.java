package com.mx.sivale.model.dto;

public class EventDashBoardDTO {

    private String approvalStatus;

    private Integer monthEvent;

    private Integer totalEvents;

    private Double totalAmount;

    private Integer yearEvent;

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public Integer getMonthEvent() {
        return monthEvent;
    }

    public void setMonthEvent(Integer monthEvent) {
        this.monthEvent = monthEvent;
    }

    public Integer getTotalEvents() {
        return totalEvents;
    }

    public void setTotalEvents(Integer totalEvents) {
        this.totalEvents = totalEvents;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getYearEvent() {
        return yearEvent;
    }

    public void setYearEvent(Integer yearEvent) {
        this.yearEvent = yearEvent;
    }
}

package com.mx.sivale.model.dto;

import java.util.List;

public class AdvanceResponse {

    private int operacionesDispersadas;
    private List<String> errores;

    public int getOperacionesDispersadas() {
        return operacionesDispersadas;
    }

    public void setOperacionesDispersadas(int operacionesDispersadas) {
        this.operacionesDispersadas = operacionesDispersadas;
    }

    public List<String> getErrores() {
        return errores;
    }

    public void setErrores(List<String> errores) {
        this.errores = errores;
    }
}

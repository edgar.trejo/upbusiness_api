package com.mx.sivale.model.dto;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author amartinezmendoza
 *
 */
public class UserRequestDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private Long sivaleId;
    @NotNull(message = "Valor no puede ser null")
    @NotEmpty(message = "Campo obligatorio")
    private String name;
    @NotNull(message = "Valor no puede ser null")
    @NotEmpty(message = "Campo obligatorio")
    private String firstName;
    private String lastName;
    private String completeName;
    private String numberEmployee;
    private String phoneNumber;
    @NotNull(message = "Valor no puede ser null")
	@NotEmpty(message = "Campo obligatorio")
	@Email(message = "Debe tener un formato correcto")
    private String email;
    private String emailAdmin;
    private String authenticationToken;
    @NotNull(message = "Valor no puede ser null")
    private Long birthDate;
    private String deviceToken;
    @NotNull(message = "Valor no puede ser null")
    @NotEmpty(message = "Campo obligatorio")
    private String gender;
    private String reference;
    private String costCenter;
    private String costCenterName;
    private String role;
    private String roleName;
    @NotNull(message = "Valor no puede ser null")
	@NotEmpty(message = "Campo obligatorio")
    private String federativeEntity;
    private String federativeEntityName;
    private String jobPosition;
    private String jobPositionName;
    private String emailApprover;
    private List<String> numberCards;
    private String cardOne;
    private String cardTwo;
    private String cardThree;
    private String cardFour;
    private String birthdateOriginal;
    private String jobPositionCode;
    private String costCenterCode;
    private boolean isBulkFile;
    private String roleId;
    private String origin;
    private String clientId;
    private String advanceAvailable;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSivaleId() {
        return sivaleId;
    }

    public void setSivaleId(Long sivaleId) {
        this.sivaleId = sivaleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompleteName() {
        return completeName;
    }

    public void setCompleteName(String completeName) {
        this.completeName = completeName;
    }

    public String getNumberEmployee() {
        return numberEmployee;
    }

    public void setNumberEmployee(String numberEmployee) {
        this.numberEmployee = numberEmployee;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailAdmin() {
        return emailAdmin;
    }

    public void setEmailAdmin(String emailAdmin) {
        this.emailAdmin = emailAdmin;
    }

    public String getAuthenticationToken() {
        return authenticationToken;
    }

    public void setAuthenticationToken(String authenticationToken) {
        this.authenticationToken = authenticationToken;
    }

    public Long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Long birthDate) {
        this.birthDate = birthDate;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(String costCenter) {
        this.costCenter = costCenter;
    }

    public String getCostCenterName() {
        return costCenterName;
    }

    public void setCostCenterName(String costCenterName) {
        this.costCenterName = costCenterName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getFederativeEntity() {
        return federativeEntity;
    }

    public void setFederativeEntity(String federativeEntity) {
        this.federativeEntity = federativeEntity;
    }

    public String getFederativeEntityName() {
        return federativeEntityName;
    }

    public void setFederativeEntityName(String federativeEntityName) {
        this.federativeEntityName = federativeEntityName;
    }

    public String getJobPosition() {
        return jobPosition;
    }

    public void setJobPosition(String jobPosition) {
        this.jobPosition = jobPosition;
    }

    public String getJobPositionName() {
        return jobPositionName;
    }

    public void setJobPositionName(String jobPositionName) {
        this.jobPositionName = jobPositionName;
    }

    public String getEmailApprover() {
        return emailApprover;
    }

    public void setEmailApprover(String emailApprover) {
        this.emailApprover = emailApprover;
    }

    public List<String> getNumberCards() {
        return numberCards;
    }

    public void setNumberCards(List<String> numberCards) {
        this.numberCards = numberCards;
    }

    public String getCardOne() {
        return cardOne;
    }

    public void setCardOne(String cardOne) {
        this.cardOne = cardOne;
    }

    public String getCardTwo() {
        return cardTwo;
    }

    public void setCardTwo(String cardTwo) {
        this.cardTwo = cardTwo;
    }

    public String getCardThree() {
        return cardThree;
    }

    public void setCardThree(String cardThree) {
        this.cardThree = cardThree;
    }

    public String getCardFour() {
        return cardFour;
    }

    public void setCardFour(String cardFour) {
        this.cardFour = cardFour;
    }

    public String getBirthdateOriginal() {
        return birthdateOriginal;
    }

    public void setBirthdateOriginal(String birthdateOriginal) {
        this.birthdateOriginal = birthdateOriginal;
    }

    public String getJobPositionCode() {
        return jobPositionCode;
    }

    public void setJobPositionCode(String jobPositionCode) {
        this.jobPositionCode = jobPositionCode;
    }

    public String getCostCenterCode() {
        return costCenterCode;
    }

    public void setCostCenterCode(String costCenterCode) {
        this.costCenterCode = costCenterCode;
    }

    public boolean isBulkFile() {
        return isBulkFile;
    }

    public void setBulkFile(boolean bulkFile) {
        isBulkFile = bulkFile;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }
    
    public String getAdvanceAvailable() {
		return advanceAvailable;
	}

	public void setAdvanceAvailable(String advanceAvailable) {
		this.advanceAvailable = advanceAvailable;
	}

	@Override
    public String toString() {
        return "UserRequestDTO{" +
                "id=" + id +
                ", sivaleId=" + sivaleId +
                ", name='" + name + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", completeName='" + completeName + '\'' +
                ", numberEmployee='" + numberEmployee + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", email='" + email + '\'' +
                ", emailAdmin='" + emailAdmin + '\'' +
                ", authenticationToken='" + authenticationToken + '\'' +
                ", birthDate=" + birthDate +
                ", deviceToken='" + deviceToken + '\'' +
                ", gender='" + gender + '\'' +
                ", reference='" + reference + '\'' +
                ", costCenter='" + costCenter + '\'' +
                ", costCenterName='" + costCenterName + '\'' +
                ", role='" + role + '\'' +
                ", roleName='" + roleName + '\'' +
                ", federativeEntity='" + federativeEntity + '\'' +
                ", federativeEntityName='" + federativeEntityName + '\'' +
                ", jobPosition='" + jobPosition + '\'' +
                ", jobPositionName='" + jobPositionName + '\'' +
                ", emailApprover='" + emailApprover + '\'' +
                ", numberCards=" + numberCards +
                '}';
    }
}

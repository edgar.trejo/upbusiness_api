package com.mx.sivale.model.dto;

import java.io.Serializable;
import java.sql.Timestamp;

public class AdvanceRequiredListingDTO implements Serializable {

    private Long advanceId;
    private Long userId;
    private String advanceName;
    private String advanceUser;
    private Timestamp advanceDate;
    private Double advanceAmount;
    private Long advanceStatusId;
    private Long advanceStatusIdOriginal;
    private String status;
    private Boolean admin;

    public Long getAdvanceId() {
        return advanceId;
    }

    public void setAdvanceId(Long advanceId) {
        this.advanceId = advanceId;
    }

    public String getAdvanceUser() {
        return advanceUser;
    }

    public void setAdvanceUser(String advanceUser) {
        this.advanceUser = advanceUser;
    }

    public Timestamp getAdvanceDate() {
        return advanceDate;
    }

    public void setAdvanceDate(Timestamp advanceDate) {
        this.advanceDate = advanceDate;
    }

    public Double getAdvanceAmount() {
        return advanceAmount;
    }

    public void setAdvanceAmount(Double advanceAmount) {
        this.advanceAmount = advanceAmount;
    }

    public Long getAdvanceStatusId() {
        return advanceStatusId;
    }

    public void setAdvanceStatusId(Long advanceStatusId) {
        this.advanceStatusId = advanceStatusId;
    }

    public Long getAdvanceStatusIdOriginal() {
        return advanceStatusIdOriginal;
    }

    public void setAdvanceStatusIdOriginal(Long advanceStatusIdOriginal) {
        this.advanceStatusIdOriginal = advanceStatusIdOriginal;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public String getAdvanceName() {
        return advanceName;
    }

    public void setAdvanceName(String advanceName) {
        this.advanceName = advanceName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}

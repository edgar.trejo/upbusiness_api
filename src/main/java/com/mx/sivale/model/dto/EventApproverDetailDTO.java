package com.mx.sivale.model.dto;

import com.mx.sivale.model.ApprovalStatus;
import com.mx.sivale.model.User;

import java.io.Serializable;
import java.sql.Timestamp;

public class EventApproverDetailDTO implements Serializable {

    private User user;
    private Long userId;
    private String teamName;
    private Long teamId;
    private Integer pending;
    private Timestamp approverDate;
    private String comment;
    private ApprovalStatus pre_status;
    private Long eventId;
    private Boolean isAdmin;
    private String dateApproved;
    private Boolean automatic;
    private Integer loop;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    public Integer getPending() {
        return pending;
    }

    public void setPending(Integer pending) {
        this.pending = pending;
    }

    public Timestamp getApproverDate() {
        return approverDate;
    }

    public void setApproverDate(Timestamp approverDate) {
        this.approverDate = approverDate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public ApprovalStatus getPre_status() {
        return pre_status;
    }

    public void setPre_status(ApprovalStatus pre_status) {
        this.pre_status = pre_status;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public Boolean getAdmin() {
        return isAdmin;
    }

    public void setAdmin(Boolean admin) {
        isAdmin = admin;
    }

    public String getDateApproved() {
        return dateApproved;
    }

    public void setDateApproved(String dateApproved) {
        this.dateApproved = dateApproved;
    }

    public Boolean getAutomatic() {
        return automatic;
    }

    public void setAutomatic(Boolean automatic) {
        this.automatic = automatic;
    }

    public Integer getLoop() {
        return loop;
    }

    public void setLoop(Integer loop) {
        this.loop = loop;
    }

}

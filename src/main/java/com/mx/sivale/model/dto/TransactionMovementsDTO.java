package com.mx.sivale.model.dto;

import java.io.Serializable;

public class TransactionMovementsDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String authorizationNumber;
	private String numberCard;
	private String transactionDate;
	private Double amount;
	private String acceptorName;
	private String transactionType;

	public String getAuthorizationNumber() {
		return authorizationNumber;
	}

	public void setAuthorizationNumber(String authorizationNumber) {
		this.authorizationNumber = authorizationNumber;
	}

	public String getNumberCard() {
		return numberCard;
	}

	public void setNumberCard(String numberCard) {
		this.numberCard = numberCard;
	}

	public String getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getAcceptorName() {
		return acceptorName;
	}

	public void setAcceptorName(String acceptorName) {
		this.acceptorName = acceptorName;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	@Override
	public String toString() {
		return "TransactionMovementsDTO{" +
				"authorizationNumber='" + authorizationNumber + '\'' +
				", numberCard='" + numberCard + '\'' +
				", transactionDate='" + transactionDate + '\'' +
				", amount=" + amount +
				", acceptorName='" + acceptorName + '\'' +
				", transactionType='" + transactionType + '\'' +
				'}';
	}

}

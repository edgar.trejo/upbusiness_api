package com.mx.sivale.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomReportConfigDTO {

    private Long id;
    private String name;
    private UserResponseDTO user;
    private List<CustomReportTypeColumnDTO> typeColumn;
    private List<CustomReportColumnDTO> columns;
    private Long userId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserResponseDTO getUser() {
        return user;
    }

    public void setUser(UserResponseDTO user) {
        this.user = user;
    }

    public List<CustomReportTypeColumnDTO> getTypeColumn() {
        return typeColumn;
    }

    public void setTypeColumn(List<CustomReportTypeColumnDTO> typeColumn) {
        this.typeColumn = typeColumn;
    }

    public List<CustomReportColumnDTO> getColumns() {
        return columns;
    }

    public void setColumns(List<CustomReportColumnDTO> columns) {
        this.columns = columns;
    }

    @Override
    public String toString() {
        return "CustomReportConfigDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", user=" + user +
                ", typeColumn=" + typeColumn +
                ", columns=" + columns +
                '}';
    }

    public CustomReportConfigDTO(String name, List<CustomReportColumnDTO> columns, Long userId) {
        this.name = name;
        this.columns = columns;
        this.userId = userId;
    }

    public CustomReportConfigDTO() {
    }
}

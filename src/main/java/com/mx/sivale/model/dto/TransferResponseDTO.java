package com.mx.sivale.model.dto;

import java.io.Serializable;
import java.sql.Timestamp;

public class TransferResponseDTO implements Serializable {

    private Long transferId;

    private String userName;

    private Long userId;

    private String iut;

    private Long clientId;

    private String maskCardNumber;

    private Long solicitudeId;

    private String appliedTransferNumber;

    private String declinedKey;

    private String declinedDescription;

    private Timestamp transferDate;

    private Boolean applied;

    public Long getTransferId() {
        return transferId;
    }

    public void setTransferId(Long transferId) {
        this.transferId = transferId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getIut() {
        return iut;
    }

    public void setIut(String iut) {
        this.iut = iut;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public String getMaskCardNumber() {
        return maskCardNumber;
    }

    public void setMaskCardNumber(String maskCardNumber) {
        this.maskCardNumber = maskCardNumber;
    }

    public Long getSolicitudeId() {
        return solicitudeId;
    }

    public void setSolicitudeId(Long solicitudeId) {
        this.solicitudeId = solicitudeId;
    }

    public String getAppliedTransferNumber() {
        return appliedTransferNumber;
    }

    public void setAppliedTransferNumber(String appliedTransferNumber) {
        this.appliedTransferNumber = appliedTransferNumber;
    }

    public String getDeclinedKey() {
        return declinedKey;
    }

    public void setDeclinedKey(String declinedKey) {
        this.declinedKey = declinedKey;
    }

    public String getDeclinedDescription() {
        return declinedDescription;
    }

    public void setDeclinedDescription(String declinedDescription) {
        this.declinedDescription = declinedDescription;
    }

    public Timestamp getTransferDate() {
        return transferDate;
    }

    public void setTransferDate(Timestamp transferDate) {
        this.transferDate = transferDate;
    }

    public Boolean isApplied() {
        return applied;
    }

    public void setApplied(Boolean applied) {
        this.applied = applied;
    }
}

package com.mx.sivale.model.dto;

import java.io.Serializable;

/**
 * 
 * @author amartinezmendoza
 *
 */
public class TransferBaseDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long userId;
	private String iutDestiny;
	private String ammount;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getIutDestiny() {
		return iutDestiny;
	}

	public void setIutDestiny(String iutDestiny) {
		this.iutDestiny = iutDestiny;
	}

	public String getAmmount() {
		return ammount;
	}

	public void setAmmount(String ammount) {
		this.ammount = ammount;
	}

	@Override
	public String toString() {
		return "TransferBaseDTO{" +
				"userId=" + userId +
				", iutDestiny='" + iutDestiny + '\'' +
				", ammount='" + ammount + '\'' +
				'}';
	}
}

package com.mx.sivale.model.dto;

import java.util.List;
import java.util.Map;

public class FilterDTO {

    String initDate;
    String endDate;
    int pageNumber;
    int pageSize;
    String filterColumn;
    String criteriaSearch;
    Long userId;
    List<Long> status;
    Map<String, String> orders;

    public String getInitDate() {
        return initDate;
    }

    public void setInitDate(String initDate) {
        this.initDate = initDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getFilterColumn() {
        return filterColumn;
    }

    public void setFilterColumn(String filterColumn) {
        this.filterColumn = filterColumn;
    }

    public String getCriteriaSearch() {
        return criteriaSearch;
    }

    public void setCriteriaSearch(String criteriaSearch) {
        this.criteriaSearch = criteriaSearch;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public List<Long> getStatus() {
        return status;
    }

    public void setStatus(List<Long> status) {
        this.status = status;
    }

    public Map<String, String> getOrders() {
        return orders;
    }

    public void setOrders(Map<String, String> orders) {
        this.orders = orders;
    }

}

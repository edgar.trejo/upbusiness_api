package com.mx.sivale.model.dto;

import java.io.Serializable;

/**
 * 
 * @author amartinezmendoza
 *
 */
public class ClientDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String name;
	private Long numberClient;
	private String logo;
	private Boolean active;
	private String sivaleId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getNumberClient() {
		return numberClient;
	}

	public void setNumberClient(Long numberClient) {
		this.numberClient = numberClient;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getSivaleId() {
		return sivaleId;
	}

	public void setSivaleId(String sivaleId) {
		this.sivaleId = sivaleId;
	}

}
package com.mx.sivale.model.dto;

import com.mx.sivale.model.AdvanceStatus;
import com.mx.sivale.model.User;

import java.io.Serializable;

/**
 * Created by amartinezmendoza on 26/12/2017.
 */
public class ApproverUserDTO implements Serializable {

    private User user;
    private AdvanceStatus preStatus;
    private Boolean automatic;
    private Boolean admin;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public AdvanceStatus getPreStatus() {
        return preStatus;
    }

    public void setPreStatus(AdvanceStatus preStatus) {
        this.preStatus = preStatus;
    }

    public Boolean getAutomatic() {
        return automatic;
    }

    public void setAutomatic(Boolean automatic) {
        this.automatic = automatic;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }
}

package com.mx.sivale.model.dto;

import java.io.Serializable;
import java.util.Date;

public class AdvanceReportDTO implements Serializable {

    private String userName;
    private String employeeNumber;
    private String jobPosition;
    private String jobCode;
    private String area;
    private String areaCode;
    private String group;
    private String groupCode;
    private String advanceName;
    private String advanceId;
    private Date advanceDate;
    private Date dispersionAdvanceDate;
    private String requestedAmount;
    private String status;

    private String dispersionAdvanceTime;
    private String description;
    private String originCard;
    private String destinationCard;
    private Double originBalance;
    private Double destinationInitialBalance;
    private Double destinationFinalBalance;
    private String product;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getJobPosition() {
        return jobPosition;
    }

    public void setJobPosition(String jobPosition) {
        this.jobPosition = jobPosition;
    }

    public String getJobCode() {
        return jobCode;
    }

    public void setJobCode(String jobCode) {
        this.jobCode = jobCode;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getAdvanceName() {
        return advanceName;
    }

    public void setAdvanceName(String advanceName) {
        this.advanceName = advanceName;
    }

    public String getAdvanceId() {
        return advanceId;
    }

    public void setAdvanceId(String advanceId) {
        this.advanceId = advanceId;
    }

    public Date getAdvanceDate() {
        return advanceDate;
    }

    public void setAdvanceDate(Date advanceDate) {
        this.advanceDate = advanceDate;
    }

    public Date getDispersionAdvanceDate() {
        return dispersionAdvanceDate;
    }

    public void setDispersionAdvanceDate(Date dispersionAdvanceDate) {
        this.dispersionAdvanceDate = dispersionAdvanceDate;
    }

    public String getRequestedAmount() {
        return requestedAmount;
    }

    public void setRequestedAmount(String requestedAmount) {
        this.requestedAmount = requestedAmount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDispersionAdvanceTime() {
        return dispersionAdvanceTime;
    }

    public void setDispersionAdvanceTime(String dispersionAdvanceTime) {
        this.dispersionAdvanceTime = dispersionAdvanceTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOriginCard() {
        return originCard;
    }

    public void setOriginCard(String originCard) {
        this.originCard = originCard;
    }

    public String getDestinationCard() {
        return destinationCard;
    }

    public void setDestinationCard(String destinationCard) {
        this.destinationCard = destinationCard;
    }

    public Double getOriginBalance() {
        return originBalance;
    }

    public void setOriginBalance(Double originBalance) {
        this.originBalance = originBalance;
    }

    public Double getDestinationInitialBalance() {
        return destinationInitialBalance;
    }

    public void setDestinationInitialBalance(Double destinationInitialBalance) {
        this.destinationInitialBalance = destinationInitialBalance;
    }

    public Double getDestinationFinalBalance() {
        return destinationFinalBalance;
    }

    public void setDestinationFinalBalance(Double destinationFinalBalance) {
        this.destinationFinalBalance = destinationFinalBalance;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }
}

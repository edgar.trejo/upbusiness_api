package com.mx.sivale.model.dto;

import java.io.Serializable;

public class AmountSpendingTypeDetailDTO implements Serializable {

    private Long amountSpendingTypeId;
    private Double amountSpendingType;
    private Long spendingTypeId;
    private String spendingTypeName;
    private String spendingTypeCode;
    private String spendingTypeIcon;

    public Long getAmountSpendingTypeId() {
        return amountSpendingTypeId;
    }

    public void setAmountSpendingTypeId(Long amountSpendingTypeId) {
        this.amountSpendingTypeId = amountSpendingTypeId;
    }

    public Double getAmountSpendingType() {
        return amountSpendingType;
    }

    public void setAmountSpendingType(Double amountSpendingType) {
        this.amountSpendingType = amountSpendingType;
    }

    public Long getSpendingTypeId() {
        return spendingTypeId;
    }

    public void setSpendingTypeId(Long spendingTypeId) {
        this.spendingTypeId = spendingTypeId;
    }

    public String getSpendingTypeName() {
        return spendingTypeName;
    }

    public void setSpendingTypeName(String spendingTypeName) {
        this.spendingTypeName = spendingTypeName;
    }

    public String getSpendingTypeCode() {
        return spendingTypeCode;
    }

    public void setSpendingTypeCode(String spendingTypeCode) {
        this.spendingTypeCode = spendingTypeCode;
    }

    public String getSpendingTypeIcon() {
        return spendingTypeIcon;
    }

    public void setSpendingTypeIcon(String spendingTypeIcon) {
        this.spendingTypeIcon = spendingTypeIcon;
    }
}

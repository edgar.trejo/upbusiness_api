package com.mx.sivale.model.dto;

import java.io.Serializable;
import java.math.BigInteger;

public class LogInfoDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer code;
	private String message;
	private String clientId;
	private BigInteger userId;
	private String iut;
	private String email;

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public BigInteger getUserId() {
		return userId;
	}

	public String getIut() {
		return iut;
	}

	public void setIut(String iut) {
		this.iut = iut;
	}

	public void setUserId(BigInteger userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}

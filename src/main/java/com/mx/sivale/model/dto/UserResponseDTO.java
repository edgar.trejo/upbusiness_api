package com.mx.sivale.model.dto;

import java.util.List;

public class UserResponseDTO {

	private Long id;
	private Long sivaleId;
	private String name;
	private String firstName;
	private String lastName;
	private String completeName;
	private String numberEmployee;
	private String phoneNumber;
	private String email;
	private String emailAdmin;
	private String authenticationToken;
	private Long birthDate;
	private String deviceToken;
	private String gender;
	private String reference;
	private String costCenter;
	private String costCenterName;
	private String role;
	private String roleName;
	private String federativeEntity;
	private String federativeEntityName;
	private String jobPosition;
	private String jobPositionName;
	private List<CardDTO> cards;
	private String emailInvoce;
	private String emailApprover;
	private Boolean advanceAvailable;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getSivaleId() {
		return sivaleId;
	}

	public void setSivaleId(Long sivaleId) {
		this.sivaleId = sivaleId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCompleteName() {
		return completeName;
	}

	public void setCompleteName(String completeName) {
		this.completeName = completeName;
	}

	public String getNumberEmployee() {
		return numberEmployee;
	}

	public void setNumberEmployee(String numberEmployee) {
		this.numberEmployee = numberEmployee;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmailAdmin() {
		return emailAdmin;
	}

	public void setEmailAdmin(String emailAdmin) {
		this.emailAdmin = emailAdmin;
	}

	public String getAuthenticationToken() {
		return authenticationToken;
	}

	public void setAuthenticationToken(String authenticationToken) {
		this.authenticationToken = authenticationToken;
	}

	public Long getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Long birthDate) {
		this.birthDate = birthDate;
	}

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getCostCenter() {
		return costCenter;
	}

	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}

	public String getCostCenterName() {
		return costCenterName;
	}

	public void setCostCenterName(String costCenterName) {
		this.costCenterName = costCenterName;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getFederativeEntity() {
		return federativeEntity;
	}

	public void setFederativeEntity(String federativeEntity) {
		this.federativeEntity = federativeEntity;
	}

	public String getFederativeEntityName() {
		return federativeEntityName;
	}

	public void setFederativeEntityName(String federativeEntityName) {
		this.federativeEntityName = federativeEntityName;
	}

	public String getJobPosition() {
		return jobPosition;
	}

	public void setJobPosition(String jobPosition) {
		this.jobPosition = jobPosition;
	}

	public String getJobPositionName() {
		return jobPositionName;
	}

	public void setJobPositionName(String jobPositionName) {
		this.jobPositionName = jobPositionName;
	}

	public List<CardDTO> getCards() {
		return cards;
	}

	public void setCards(List<CardDTO> cards) {
		this.cards = cards;
	}

	public String getEmailInvoce() {
		return emailInvoce;
	}

	public void setEmailInvoce(String emailInvoce) {
		this.emailInvoce = emailInvoce;
	}

	public String getEmailApprover() {
		return emailApprover;
	}

	public void setEmailApprover(String emailApprover) {
		this.emailApprover = emailApprover;
	}

	public Boolean getAdvanceAvailable() {
		return advanceAvailable;
	}

	public void setAdvanceAvailable(Boolean advanceAvailable) {
		this.advanceAvailable = advanceAvailable;
	}
	
	
}

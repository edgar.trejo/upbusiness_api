package com.mx.sivale.model.dto;

import java.io.Serializable;


public class SpendingDashBoardDTO implements Serializable {

    private int month;
    private String spendingType;
    private Long spendingTypeId;
    private String spendingTypeIcon;
    private Double totalAmount;
    private Long totalSpendings;
    private int year;

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public String getSpendingType() {
        return spendingType;
    }

    public void setSpendingType(String spendingType) {
        this.spendingType = spendingType;
    }

    public Long getSpendingTypeId() {
        return spendingTypeId;
    }

    public void setSpendingTypeId(Long spendingTypeId) {
        this.spendingTypeId = spendingTypeId;
    }

    public String getSpendingTypeIcon() {
        return spendingTypeIcon;
    }

    public void setSpendingTypeIcon(String spendingTypeIcon) {
        this.spendingTypeIcon = spendingTypeIcon;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Long getTotalSpendings() {
        return totalSpendings;
    }

    public void setTotalSpendings(Long totalSpendings) {
        this.totalSpendings = totalSpendings;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public SpendingDashBoardDTO() {
    }

    public SpendingDashBoardDTO(int year, int month, String spendingType, Double totalAmount) {
        this.year = year;
        this.month = month;
        this.spendingType = spendingType;
        this.totalAmount = totalAmount;
    }
}

package com.mx.sivale.model.dto;

import java.io.Serializable;

public class CardDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String cardNumber;
	private String maskCardNumber;
	private String iut;
	private String productKey;
	private String productDescription;
	private String clientId;
	private String clientName;
	private Double balance;
	private Double ammount;

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getMaskCardNumber() {
		return maskCardNumber;
	}

	public void setMaskCardNumber(String maskCardNumber) {
		this.maskCardNumber = maskCardNumber;
	}

	public String getIut() {
		return iut;
	}

	public void setIut(String iut) {
		this.iut = iut;
	}

	public String getProductKey() {
		return productKey;
	}

	public void setProductKey(String productKey) {
		this.productKey = productKey;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public Double getAmmount() {
		return ammount;
	}

	public void setAmmount(Double ammount) {
		this.ammount = ammount;
	}

	@Override
	public String toString() {
		return "CardDTO{" +
				"cardNumber='" + cardNumber + '\'' +
				", maskCardNumber='" + maskCardNumber + '\'' +
				", iut='" + iut + '\'' +
				", productKey='" + productKey + '\'' +
				", productDescription='" + productDescription + '\'' +
				", clientId='" + clientId + '\'' +
				", clientName='" + clientName + '\'' +
				", balance=" + balance +
				", ammount=" + ammount +
				'}';
	}
}

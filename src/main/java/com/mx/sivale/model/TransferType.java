package com.mx.sivale.model;

public enum TransferType {

    //Concentradora a Titular
    CT("Concentradora a Tarjeta"),

    //Titular a Concentradora
    TC("Tarjeta a Concentradora");

    private String description;

    private TransferType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

}

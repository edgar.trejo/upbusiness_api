package com.mx.sivale.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;

/**
 * The persistent class for the amount_spending_type database table.
 * 
 */
@Entity
@Table(name = "amount_spending_type", schema = "mercurio")
@JsonIdentityInfo(
		generator = ObjectIdGenerators.PropertyGenerator.class,
		property = "uuid")
public class AmountSpendingType {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "amount")
	private Double amount;

	@ManyToOne
	@JoinColumn(name = "spending_id", nullable = false, referencedColumnName = "id")
	@JsonBackReference
	private Spending spending;

	@ManyToOne
	@JoinColumn(name = "spending_type_id", nullable = false, referencedColumnName = "id")
	private SpendingType spendingType;

	@JoinColumn(name = "invoice_concept_id", referencedColumnName = "id")
	@ManyToOne
	private InvoiceConcept invoiceConceptId;

	@Transient
	private String uuid;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getAmount() {
		return this.amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Spending getSpending() {
		return this.spending;
	}

	public void setSpending(Spending spending) {
		this.spending = spending;
	}

	public SpendingType getSpendingType() {
		return this.spendingType;
	}

	public void setSpendingType(SpendingType spendingType) {
		this.spendingType = spendingType;
	}

	public String getUuid() {
		return "ast" + id;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public InvoiceConcept getInvoiceConceptId() {
		return invoiceConceptId;
	}

	public void setInvoiceConceptId(InvoiceConcept invoiceConceptId) {
		this.invoiceConceptId = invoiceConceptId;
	}

	@Override
	public String toString() {
		return "AmountSpendingType{" +
				"id=" + id +
				", amount=" + amount +
				", spending=" + spending.getId() +
				", spendingType=" + spendingType +
				", invoiceConceptId=" + invoiceConceptId +
				'}';
	}
}
package com.mx.sivale.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "advance_required_approver", schema = "mercurio")
public class AdvanceRequiredApprover implements Serializable {

    public static final Integer STATUS_APPROVED = 0;
    public static final Integer STATUS_PENDING = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "admin")
    private Boolean admin;

    @Column(name = "position")
    private Integer position;

    @ManyToOne
    @JoinColumn(name = "advance_required_id", referencedColumnName = "id")
    private AdvanceRequired advanceRequired;

    @ManyToOne
    @JoinColumn(name = "approval_rule_advance_id", referencedColumnName = "id")
    private ApprovalRuleAdvance approvalRuleAdvance;

    @ManyToOne
    @JoinColumn(name = "approver_user_id", referencedColumnName = "id")
    private ApproverUser approverUser;

    @ManyToOne
    @JoinColumn(name = "client_id", referencedColumnName = "id")
    private Client client;

    @ManyToOne
    @JoinColumn(name = "team_id", referencedColumnName = "id")
    private Team approverTeam;

    @Column(name = "pending")
    private Integer pending;

    public AdvanceRequiredApprover() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public AdvanceRequired getAdvanceRequired() {
        return advanceRequired;
    }

    public void setAdvanceRequired(AdvanceRequired advanceRequired) {
        this.advanceRequired = advanceRequired;
    }

    public ApprovalRuleAdvance getApprovalRuleAdvance() {
        return approvalRuleAdvance;
    }

    public void setApprovalRuleAdvance(ApprovalRuleAdvance approvalRuleAdvance) {
        this.approvalRuleAdvance = approvalRuleAdvance;
    }

    public ApproverUser getApproverUser() {
        return approverUser;
    }

    public void setApproverUser(ApproverUser approverUser) {
        this.approverUser = approverUser;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Team getApproverTeam() {
        return approverTeam;
    }

    public void setApproverTeam(Team approverTeam) {
        this.approverTeam = approverTeam;
    }

    public Integer getPending() {
        return pending;
    }

    public void setPending(Integer pending) {
        this.pending = pending;
    }

    @Override
    public String toString() {
        return "AdvanceRequiredApprover{" +
                "id=" + id +
                ", admin=" + admin +
                ", position=" + position +
                ", advanceRequired=" + advanceRequired +
                ", approvalRuleAdvance=" + approvalRuleAdvance +
                ", approverUser=" + approverUser +
                ", client=" + client +
                ", approverTeam=" + approverTeam +
                ", pending=" + pending +
                '}';
    }
}
package com.mx.sivale.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "event_approver_report", schema = "mercurio")
public class EventApproverReport {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "event_id", referencedColumnName = "id")
    @JsonBackReference
    private Event event;

    @ManyToOne
    @JoinColumn(name = "approver_user", referencedColumnName = "id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "pre_status", referencedColumnName = "id")
    private ApprovalStatus preStatus;

    @Column(name = "automatic")
    private Boolean automatic;

    @Column(name = "admin")
    private Boolean admin;

    @Column(name = "comment")
    private String comment;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "event_approver_id", referencedColumnName = "id")
    @JsonIgnore
    private EventApprover eventApprover;

    @Column(name = "approver_date")
    private Timestamp approverDate;

    public EventApproverReport() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ApprovalStatus getPreStatus() {
        return preStatus;
    }

    public void setPreStatus(ApprovalStatus preStatus) {
        this.preStatus = preStatus;
    }

    public Boolean getAutomatic() {
        return automatic;
    }

    public void setAutomatic(Boolean automatic) {
        this.automatic = automatic;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public EventApprover getEventApprover() {
        return eventApprover;
    }

    public void  setEventApprover(EventApprover eventApprover) {
        this.eventApprover = eventApprover;
    }

    public Timestamp getApproverDate() {
        return approverDate;
    }

    public void setApproverDate(Timestamp approverDate) {
        this.approverDate = approverDate;
    }
}

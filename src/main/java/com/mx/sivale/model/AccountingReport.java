package com.mx.sivale.model;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "accounting_report", schema = "mercurio")
public class AccountingReport {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "type_operation_id", referencedColumnName = "id")
    private AccountingTypeOperation accountingTypeOperation;

    @ManyToOne
    @JoinColumn(name = "advance_required_id", referencedColumnName = "id")
    private AdvanceRequired advanceRequired;

    @ManyToOne
    @JoinColumn(name = "event_id", referencedColumnName = "id")
    private Event event;

    @ManyToOne
    @JoinColumn(name = "transfer_id", referencedColumnName = "id")
    private Transfer transfer;

    @Column(name = "date")
    private Timestamp date;

    @Column(name = "return_type")
    private String returnType;

    @Column(name = "return_id")
    private String returnId;

    @Column(name = "return_number")
    private String returnNumber;

    @Column(name = "return_message")
    private String returnMessage;

    @Column(name = "success")
    private Boolean success;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AdvanceRequired getAdvanceRequired() {
        return advanceRequired;
    }

    public void setAdvanceRequired(AdvanceRequired advanceRequired) {
        this.advanceRequired = advanceRequired;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public Transfer getTransfer() {
        return transfer;
    }

    public void setTransfer(Transfer transfer) {
        this.transfer = transfer;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getReturnType() {
        return returnType;
    }

    public void setReturnType(String returnType) {
        this.returnType = returnType;
    }

    public String getReturnId() {
        return returnId;
    }

    public void setReturnId(String returnId) {
        this.returnId = returnId;
    }

    public String getReturnMessage() {
        return returnMessage;
    }

    public void setReturnMessage(String returnMessage) {
        this.returnMessage = returnMessage;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getReturnNumber() {
        return returnNumber;
    }

    public void setReturnNumber(String returnNumber) {
        this.returnNumber = returnNumber;
    }

    public AccountingTypeOperation getAccountingTypeOperation() {
        return accountingTypeOperation;
    }

    public void setAccountingTypeOperation(AccountingTypeOperation accountingTypeOperation) {
        this.accountingTypeOperation = accountingTypeOperation;
    }
}

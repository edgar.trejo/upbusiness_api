package com.mx.sivale.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "advance_approver_report", schema = "mercurio")
public class AdvanceApproverReportAlternative implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "advance_required_id")
    @JsonBackReference
    private AdvanceRequired advanceRequired;

    @ManyToOne
    @JoinColumn(name = "approver_user")
    private User user;

    @ManyToOne
    @JoinColumn(name = "pre_status")
    private AdvanceStatus preStatus;

    private Boolean automatic;

    private Boolean admin;

    @Column(name = "comment")
    private String comment;

    @Column(name = "approver_date")
    private Timestamp approverDate;

    public AdvanceApproverReportAlternative() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AdvanceRequired getAdvanceRequired() {
        return advanceRequired;
    }

    public void setAdvanceRequired(AdvanceRequired advanceRequired) {
        this.advanceRequired = advanceRequired;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public AdvanceStatus getPreStatus() {
        return preStatus;
    }

    public void setPreStatus(AdvanceStatus preStatus) {
        this.preStatus = preStatus;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public Boolean getAutomatic() {
        return automatic;
    }

    public void setAutomatic(Boolean automatic) {
        this.automatic = automatic;
    }

    public String getComment() { return comment; }

    public void setComment(String comment) { this.comment = comment; }

    public Timestamp getApproverDate() {
        return approverDate;
    }

    public void setApproverDate(Timestamp approverDate) {
        this.approverDate = approverDate;
    }
}
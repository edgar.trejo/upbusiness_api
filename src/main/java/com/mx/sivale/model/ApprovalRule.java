package com.mx.sivale.model;

import javax.persistence.*;
import java.util.List;

/**
 *
 * @author armando.reyna
 *
 */

@Entity
@Table(name = "approval_rule", schema = "mercurio")
public class ApprovalRule {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "start_amount")
    private Double startAmount;

    @Column(name = "end_amount")
    private Double endAmount;

    @ManyToOne
    @JoinColumn(name = "client_id", referencedColumnName = "id")
    private Client client;

    @ManyToMany(cascade = {CascadeType.DETACH})
    private List<Team> appliesTo;

    @ManyToOne
    @JoinColumn(name = "approval_rule_flow_type_id", referencedColumnName = "id")
    private ApprovalRuleFlowType approvalRuleFlowType;

    @ManyToMany(cascade = {CascadeType.DETACH})
    private List<Team> approverTeams;

    @ManyToMany(cascade = {CascadeType.ALL})
    private List<ApproverUser> approverUsers;

    @Column(name = "levels")
    private Integer levels;

    @Column(name = "active")
    private Boolean active;

    @OneToMany(mappedBy = "approvalRule")
    private List<EventApprover> eventApproverList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public List<Team> getAppliesTo() {
        return appliesTo;
    }

    public void setAppliesTo(List<Team> appliesTo) {
        this.appliesTo = appliesTo;
    }

    public ApprovalRuleFlowType getApprovalRuleFlowType() {
        return approvalRuleFlowType;
    }

    public void setApprovalRuleFlowType(ApprovalRuleFlowType approvalRuleFlowType) {
        this.approvalRuleFlowType = approvalRuleFlowType;
    }

    public List<Team> getApproverTeams() {
        return approverTeams;
    }

    public void setApproverTeams(List<Team> approverTeams) {
        this.approverTeams = approverTeams;
    }

    public List<ApproverUser> getApproverUsers() {
        return approverUsers;
    }

    public void setApproverUsers(List<ApproverUser> approverUsers) {
        this.approverUsers = approverUsers;
    }

    public Integer getLevels() {
        return levels;
    }

    public void setLevels(Integer levels) {
        this.levels = levels;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Double getStartAmount() {
        return startAmount;
    }

    public void setStartAmount(Double startAmount) {
        this.startAmount = startAmount;
    }

    public Double getEndAmount() {
        return endAmount;
    }

    public void setEndAmount(Double endAmount) {
        this.endAmount = endAmount;
    }

    public List<EventApprover> getEventApproverList() {
        return eventApproverList;
    }

    public void setEventApproverList(List<EventApprover> eventApproverList) {
        this.eventApproverList = eventApproverList;
    }
}
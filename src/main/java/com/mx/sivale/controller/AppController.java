package com.mx.sivale.controller;

import com.mx.sivale.controller.exception.ControllerException;
import com.mx.sivale.model.dto.AppInfoDTO;
import com.mx.sivale.model.dto.ResponseSuccessDTO;
import com.mx.sivale.service.ApiService;
import com.mx.sivale.service.EvidenceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;

import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.Map;

import static com.mx.sivale.config.constants.ConstantController.API_OPERATION_NOTE_VALIDATE;
import static com.mx.sivale.config.constants.ConstantController.API_OPERATION_VALUE_VALIDATE;
import static org.springframework.context.annotation.ScopedProxyMode.TARGET_CLASS;

@CrossOrigin(maxAge = 3600)
@RestController
@Api(value = "", description = "service version application")
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = TARGET_CLASS)
public class AppController {

	private static final Logger log = Logger.getLogger(AppController.class);

	@Autowired
	private EvidenceService  evidenceService;

	@Autowired
	private ApiService apiService;

	/**
	 *
	 * @return
	 * @throws ControllerException
	 */
	@ApiOperation(value = API_OPERATION_VALUE_VALIDATE, notes = API_OPERATION_NOTE_VALIDATE, response = String.class)
	@RequestMapping(value = "/version", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<?> version() throws ControllerException{
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			AppInfoDTO appInfo = apiService.getAppVersion();
			return new ResponseEntity<>(appInfo, httpHeaders, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_FOUND.value(), "No se obtuvo versión"), httpHeaders, HttpStatus.NOT_FOUND);
		}
	}

	/**
	 *
	 * @return
	 * @throws ControllerException
	 */
	@ApiOperation(value = "", notes = "", response = String.class)
    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    @ResponseBody
	public ResponseEntity<?> showProfile() throws ControllerException {
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			Map<String, String> response = new HashMap<>();
			response.put("profile", apiService.getAppProfle());
			return new ResponseEntity<>(response, httpHeaders, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_FOUND.value(), "No se obtuvo profile"), httpHeaders, HttpStatus.NOT_FOUND);
		}
    }

	/**
	 *
	 * @return
	 * @throws ControllerException
	 */
	@ApiOperation(value = "", notes = "", response = String.class)
    @RequestMapping(value = "/environment", method = RequestMethod.GET)
    @ResponseBody
	public ResponseEntity<?> showEnvironment() throws ControllerException{
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			Map<String, String> response = new HashMap<>();
			response.put("environment", apiService.getAppEnvironment());
			return new ResponseEntity<>(response, httpHeaders, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_FOUND.value(), "No se obtuvo versión"), httpHeaders, HttpStatus.NOT_FOUND);
		}
    }

    @GetMapping(value = "/testXML",produces = MediaType.APPLICATION_JSON)
	public String testXML(){
		String respuesta="";
		try{
			respuesta=evidenceService.proccessXMLTest();
		}catch (Exception e){
			log.error("Error testXML: ",e);
		}
		return respuesta;
	}
}

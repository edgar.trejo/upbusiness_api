package com.mx.sivale.controller;

import com.mx.sivale.model.Event;
import com.mx.sivale.model.dto.EventDashBoardDTO;
import com.mx.sivale.model.dto.ResponseSuccessDTO;
import com.mx.sivale.model.dto.SpendingDashBoardDTO;
import com.mx.sivale.model.dto.TransferDashBoardDTO;
import com.mx.sivale.service.DashBoardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author jesus.vicuna
 */
@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/secure")
@Api(value = "dashboardServices", description = "DashBoard Services")
public class DashBoardController {

    private final static Logger logger = Logger.getLogger(ReportController.class);

    @Autowired
    private DashBoardService dashBoardService;

    /**
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "get list of Events", notes = "", response = EventDashBoardDTO.class, responseContainer = "List")
    @RequestMapping(value = "/dashboard/events", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResponseEntity<?> getEventsDashBoard() throws Exception {
        logger.debug("Get Info Events :::::>>>>> ");
        HttpHeaders httpHeaders = new HttpHeaders();
        try {
            Map<Integer, Map<Integer, List<EventDashBoardDTO>>> list = dashBoardService.getEvents();
            return new ResponseEntity<>(list, httpHeaders, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(), e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
        }
    }

    /**
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "get list of Spendings", notes = "", response = SpendingDashBoardDTO.class, responseContainer = "List")
    @RequestMapping(value = "/dashboard/spendings", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResponseEntity<?> getSpendingsDashBoard() throws Exception {
        logger.debug("Get Info Spendings :::::>>>>> ");
        HttpHeaders httpHeaders = new HttpHeaders();
        try {
            Map<Integer, Map<Integer, List<SpendingDashBoardDTO>>> list = dashBoardService.getSpendings();
            return new ResponseEntity<>(list, httpHeaders, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(), e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
        }
    }

    /**
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "get transfers DashBoard", notes = "", response = TransferDashBoardDTO.class)
    @RequestMapping(value = "/dashboard/transfers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResponseEntity<?> getTransfers() throws Exception {
        logger.debug("Get Transfers :::::>>>>> ");
        HttpHeaders httpHeaders = new HttpHeaders();
        try {
            Map<Integer, Map<Integer, List<TransferDashBoardDTO>>> transferDashBoardDTO = dashBoardService.getTransfers();
            return new ResponseEntity<>(transferDashBoardDTO, httpHeaders, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(), e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
        }
    }

}

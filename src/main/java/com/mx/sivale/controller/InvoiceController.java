package com.mx.sivale.controller;

import com.mx.sivale.model.JobPosition;
import com.mx.sivale.model.dto.InvoiceConceptResponseDTO;
import com.mx.sivale.service.InvoiceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.MediaType;
import java.util.List;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/secure")
@Api(value = "Invoice Services", description = "Invoice services")
public class InvoiceController {

    private static final Logger log = Logger.getLogger(InvoiceController.class);

    @Autowired
    private InvoiceService invoiceService;

    @ApiOperation(value = "", notes = "", response = JobPosition.class, responseContainer = "List")
    @RequestMapping(value = "/invoice/concepts/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResponseEntity<?> getConcepts(@PathVariable(name = "id", required = true) Long id) throws Exception {
        log.debug("Get Concepts of Invoice Id :::::>>>>> ");
        HttpHeaders httpHeaders = new HttpHeaders();
        List<InvoiceConceptResponseDTO> concepts = invoiceService.getConceptsByInvoiceId(id);
        return new ResponseEntity<>(concepts, httpHeaders, HttpStatus.OK);
    }

}

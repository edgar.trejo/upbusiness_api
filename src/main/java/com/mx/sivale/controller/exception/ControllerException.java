package com.mx.sivale.controller.exception;

public class ControllerException extends Exception {

	private static final long serialVersionUID = 1L;

	public ControllerException() {
	}

	public ControllerException(String message) {
		super(message);
	}

	public ControllerException(Exception cause) {
		super(cause);
	}

	public ControllerException(String message, Exception cause) {
		super(message, cause);
	}

	public ControllerException(String message, Exception cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}

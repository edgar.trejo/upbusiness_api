package com.mx.sivale.controller;

import com.mx.sivale.model.RequestTransferType;
import com.mx.sivale.model.Transfer;
import com.mx.sivale.model.dto.ResponseSuccessDTO;
import com.mx.sivale.model.dto.TransferDTO;
import com.mx.sivale.model.dto.TransferReportDTO;
import com.mx.sivale.model.dto.TransferResponseDTO;
import com.mx.sivale.service.TransferService;
import com.mx.sivale.service.exception.AssignedCardException;
import com.mx.sivale.service.util.UtilValidator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(maxAge = 3600)
@RestController
@Api(value = "TransferService", description = "This controller contains services for different transfer movements.")
public class TransferController {
	
	private static final Logger log = Logger.getLogger(TransferController.class);
	
	@Autowired
	private TransferService transferService;

	/**
	 *
	 * @param transferDTO
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "Service that contain method for to do tranfer balance to card by your number.", notes = "")
	@RequestMapping(value = "/secure/transfer", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> makeTranfer (@RequestBody @Valid TransferDTO transferDTO, BindingResult bindingResult ) throws Exception {
		log.debug("TRANSFER :::::>>>>> IUT_ORIGIN : " + transferDTO.getIutConcentrator() + " | TRANSFER_TYPE : " + transferDTO.getTransferType());

		HttpHeaders httpHeaders = new HttpHeaders();
		if (bindingResult.hasErrors())
			return new ResponseEntity<>(UtilValidator.isValidObjectRequest(bindingResult), httpHeaders, HttpStatus.BAD_REQUEST);
		try {
			transferDTO.setRequestType(RequestTransferType.ASIGNACION.getId());
			Transfer t = transferService.makeTranfer(transferDTO);
			if(t.isApplied()) {
				Map<String, String> response = new HashMap<>();
				response.put("solicitud", "exitosa");
				return new ResponseEntity<>(response, httpHeaders, HttpStatus.OK);
			}else{
				return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(t.getDeclinedDescription());
			}
		} catch (AssignedCardException e) {
			log.error("Error", e);
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body("Las tarjetas no son del mismo producto.");
		} catch (Exception e) {
			log.error("Error", e);
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(e.getMessage());
		}
	}

	@ApiOperation(value = "Service that contain method for to do tranfer balance to card by your number.", notes = "")
	@RequestMapping(value = "/secure/transfer/cards", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> makeTranfers (@RequestBody @Valid List<TransferDTO> listTransfers, BindingResult bindingResult ) throws Exception {
		//log.debug("TRANSFER :::::>>>>> IUT_ORIGIN : " + transferDTO.getIutConcentrator() + " | TRANSFER_TYPE : " + transferDTO.getTransferType());
		log.debug("TRANSFER :::::>>>>> IUT_ORIGIN : "+listTransfers.get(0).getIutConcentrator() + " | TRANSFER_TYPE : " + listTransfers.get(0).getTransferType());
		HttpHeaders httpHeaders = new HttpHeaders();
		if (bindingResult.hasErrors())
			return new ResponseEntity<>(UtilValidator.isValidObjectRequest(bindingResult), httpHeaders, HttpStatus.BAD_REQUEST);
		try {
			List<TransferResponseDTO> listResponse=transferService.makeTranfers(listTransfers,RequestTransferType.ASIGNACION.getId());

			if(listResponse!=null && !listResponse.isEmpty()) {
				return new ResponseEntity<>(listResponse, httpHeaders, HttpStatus.OK);
			}else{
				return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body("");
			}
		} catch (AssignedCardException e) {
			log.error("Error", e);
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body("Las tarjetas no son del mismo producto.");
		} catch (Exception e) {
			log.error("Error", e);
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(e.getMessage());
		}
	}

	@ApiOperation(value = "", notes = "", response = Object.class, responseContainer = "")
	@RequestMapping(value = "/secure/transfer", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody public ResponseEntity<?> getTransfer() throws Exception {
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
//			TODO: Se tiene que recontruir la logica de negocio dentro del metodo porque por causa de los sercivicios de sivale este metodo tarda demaciado en responder
			List<TransferReportDTO> userDTOList = null;
			return new ResponseEntity<>(userDTOList, httpHeaders, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ResponseSuccessDTO(HttpStatus.NOT_ACCEPTABLE.value(),e.getMessage()), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}
	
}

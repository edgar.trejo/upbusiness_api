package com.mx.sivale.controller;

import com.mx.sivale.model.dto.ImageRfcDTO;
import com.mx.sivale.service.ImageRFCService;
import com.mx.sivale.service.util.UtilValidator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.context.annotation.ScopedProxyMode.TARGET_CLASS;

@CrossOrigin(maxAge = 3600)
@RestController
@Api(value = "FederalContributorRegistrationService", description = "This controller contains services for different RFC movements.")        
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode=TARGET_CLASS)
public class FederalContributorRegistrationController {
	
	private static final Logger log = Logger.getLogger(FederalContributorRegistrationController.class);
	
	@Autowired
	private ImageRFCService imageRFCService;

	/**
	 *
	 * @param imageRfcDTO
	 * @param bindingResult
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = ImageRfcDTO.class, responseContainer = "")
	@RequestMapping(value = "/secure/rfc", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> saveRFC( @Valid @RequestBody ImageRfcDTO imageRfcDTO, BindingResult bindingResult) throws Exception {
		HttpHeaders httpHeaders = new HttpHeaders();
		if (bindingResult.hasErrors())
			return new ResponseEntity<>(UtilValidator.isValidObjectRequest(bindingResult), httpHeaders, HttpStatus.BAD_REQUEST);
		
		try {
			ImageRfcDTO rfcDTO = imageRFCService.insertRFC(imageRfcDTO);
			return new ResponseEntity<>(rfcDTO, httpHeaders, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getStackTrace().toString());
			return new ResponseEntity<>(e.getMessage(), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	/**
	 *
	 * @param imageRfcDTO
	 * @param bindingResult
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = ImageRfcDTO.class, responseContainer = "")
	@RequestMapping(value = "/secure/rfc", method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> updateRFC( @Valid @RequestBody ImageRfcDTO imageRfcDTO, BindingResult bindingResult) throws Exception {
		HttpHeaders httpHeaders = new HttpHeaders();
		if (bindingResult.hasErrors())
			return new ResponseEntity<>(UtilValidator.isValidObjectRequest(bindingResult), httpHeaders, HttpStatus.BAD_REQUEST);
		
		try {
			ImageRfcDTO rfcDTO = imageRFCService.updateRFC(imageRfcDTO);
			return new ResponseEntity<>(rfcDTO, httpHeaders, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getStackTrace().toString());
			return new ResponseEntity<>(e.getMessage(), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	/**
	 *
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = ImageRfcDTO.class, responseContainer = "List")
	@RequestMapping(value = "/secure/rfc", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> listRFC() throws Exception {
		HttpHeaders httpHeaders = new HttpHeaders();
		try {
			List<ImageRfcDTO> imageRfcList = imageRFCService.getListRFC();
			return new ResponseEntity<>(imageRfcList, httpHeaders, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getStackTrace());
			return new ResponseEntity<>(e.getMessage(), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
	}

	/**
	 *
	 * @param rfcId
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "", notes = "", response = Object.class, responseContainer = "")
	@RequestMapping(value = "/secure/rfc", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<?> deleteRFC(@RequestParam(name = "id", value = "id", required = true) Long rfcId) throws Exception {
		
		HttpHeaders httpHeaders = new HttpHeaders();
		log.debug("ELIMINAR ::::>>> " +  rfcId);
		
		try {
			imageRFCService.deleteRFC(rfcId);
			Map<String, String> response =  new HashMap<>();
			response.put("success", "Eliminación exitosa");
 			return new ResponseEntity<>(response, httpHeaders, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(e.getMessage(), httpHeaders, HttpStatus.NOT_ACCEPTABLE);
		}
		
	}
}

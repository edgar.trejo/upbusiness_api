package com.mx.sivale.service;

import com.mx.sivale.model.Transfer;
import com.mx.sivale.model.dto.AdvanceReportDTO;
import com.mx.sivale.model.dto.TransferDTO;
import com.mx.sivale.model.dto.TransferReportDTO;
import com.mx.sivale.model.dto.TransferResponseDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface TransferService {
	
	Transfer makeTranfer(TransferDTO transferDTO) throws Exception;
	
	List<TransferReportDTO> getTransferByClient() throws Exception;

	Page<AdvanceReportDTO> report(long longFrom, long longTo, Pageable pageable, long clientId) throws Exception;

	List<TransferResponseDTO> makeTranfers(List<TransferDTO> transferDTO, Integer requesttransferType) throws Exception;
}

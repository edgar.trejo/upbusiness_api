package com.mx.sivale.service.impl;

import com.mx.sivale.model.Role;
import com.mx.sivale.model.dto.BalanceDTO;
import com.mx.sivale.model.dto.CardDTO;
import com.mx.sivale.model.dto.CatalogDTO;
import com.mx.sivale.model.dto.TransactionMovementsDTO;
import com.mx.sivale.repository.exception.RepositoryException;
import com.mx.sivale.service.CardService;
import com.mx.sivale.service.CatalogService;
import com.mx.sivale.service.exception.AssignCardException;
import com.mx.sivale.service.exception.AssignedCardException;
import com.mx.sivale.service.exception.ServiceException;
import com.mx.sivale.service.request.ClaimsResolver;
import com.mx.sivale.service.util.UtilBean;
import com.mx.sivale.service.util.UtilValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ws.sivale.com.mx.messages.request.appgasolina.RequestTarjeta;
import ws.sivale.com.mx.messages.request.appgs.RequestConsultarTarjetas;
import ws.sivale.com.mx.messages.request.apps.RequestCliente;
import ws.sivale.com.mx.messages.request.apps.RequestClienteIdentificador;
import ws.sivale.com.mx.messages.request.apps.RequestEntregaTarjetas;
import ws.sivale.com.mx.messages.request.apps.RequestIut;
import ws.sivale.com.mx.messages.request.tarjeta.RequestBase;
import ws.sivale.com.mx.messages.request.tarjeta.RequestMovimientos;
import ws.sivale.com.mx.messages.response.appgasolina.ResponseTarjetasChofer;
import ws.sivale.com.mx.messages.response.appgasolina.TypeDatos;
import ws.sivale.com.mx.messages.response.apps.*;
import ws.sivale.com.mx.messages.response.tarjeta.ResponseEstado;
import ws.sivale.com.mx.messages.response.tarjeta.ResponseSaldo;

import javax.xml.ws.WebServiceException;
import java.math.BigDecimal;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import static com.mx.sivale.config.constants.ConstantInteliviajes.ORIGIN_WEB;
import static com.mx.sivale.config.constants.ConstantService.ERROR_NO_CONTROLADO;
import static com.mx.sivale.config.constants.ConstantService.TIEMPO_ESPERA_AGOTADO;

@Service
public class CardServiceImpl extends WsdlConsumeServiceImpl implements CardService {

	private static final Logger log = Logger.getLogger(CardServiceImpl.class);

	@Autowired
	private ClaimsResolver claimsResolver;

	@Autowired
	private CatalogService catalogService;

	@Override
	public void validateCard(Long numberCard) throws ServiceException, AssignCardException, AssignedCardException {
		String origin = claimsResolver.origin();

		if (!UtilValidator.isValidOrigin(origin))
			throw new ServiceException("Origen no valido");

		try {
			Long roleId = Long.parseLong(claimsResolver.roleId());
			CatalogDTO catalogRole = catalogService.findRoleById(roleId);

			if (catalogRole == null)
				throw new ServiceException("Role no valido para esta aplicación");
			else if (!catalogRole.getId().equals(Role.ADMIN))
				throw new ServiceException("Solo el adminstrador puede usar este servicio");
		} catch (NumberFormatException numEx) {
			log.error("SERVICE | validateCard() | Error converción, el role debe ser numerico");
			throw new ServiceException("Error converción, el role debe ser numerico");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getStackTrace());
			log.error(e.getMessage());
			log.error("SERVICE | validateCard() | Fallo validacion");
			throw new ServiceException("Fallo validacion");
		}

		try {
			RequestBase cardRequest = new RequestBase();
			cardRequest.setTarjeta(numberCard.toString());

			ResponseEstado responseEstado = getTarjetaPort().consultarEstado(cardRequest);
			UtilValidator.validateResponseError(responseEstado.getResponseError());

			log.info(responseEstado.getMensaje());
			if (!responseEstado.getMensaje().equalsIgnoreCase("operativa"))
				throw new ServiceException(responseEstado.getMensaje());

			// Se obtienen datos tarjeta, si es que existe
			RequestIut requestIut = new RequestIut();
			requestIut.setOrigen(origin);
			requestIut.setIut(numberCard.toString());

			ResponseDatos responseDatos = getAppsPort().datosTarjeta(requestIut);
			UtilValidator.validateResponseError(responseDatos.getResponseError());

			if (responseDatos.getDatos() != null && !responseDatos.getDatos().getNombreAsignado().trim().isEmpty()){
				throw new ServiceException("Tarjeta ya asociada");
			}

			// Tarjeta existente, validamos que pertenece al cliente que quiere asociar.
			if(responseDatos.getDatos().getNumCliente()== null || responseDatos.getDatos().getNumCliente().isEmpty()
					|| !responseDatos.getDatos().getNumCliente().trim().equalsIgnoreCase(claimsResolver.clientId().trim())){
				throw new ServiceException("Tarjeta no existe");
			}


		} catch (WebServiceException wse) {
			log.error("Error al validar la tarjeta.", wse);
			if (wse.getCause().getClass().equals(SocketTimeoutException.class))
				throw new ServiceException(TIEMPO_ESPERA_AGOTADO, wse);
			else
				throw new ServiceException(ERROR_NO_CONTROLADO);
		}
	}

	/**
	 * Get balance from card by IUT.
	 */
	@Override
	public BalanceDTO getCardBalance(String iut) throws Exception {
		try {

			BalanceDTO balanceDTO = new BalanceDTO();
			String origin = claimsResolver.origin();
	
			TypeDatos card = getCardData(iut, origin);

			ResponseSaldo responseMovimientos = getCardMovements(card.getNumTarjeta(), 0, 20);

			if (origin.equals(ORIGIN_WEB) && responseMovimientos != null
					&&  responseMovimientos.getTransacciones() != null
					&&  responseMovimientos.getTransacciones().getTransaccion() != null
					&& !responseMovimientos.getTransacciones().getTransaccion().isEmpty()) {
				List<TransactionMovementsDTO> movements = UtilBean.typeTransaccionToTransactionMovementsDTO(responseMovimientos.getTransacciones().getTransaccion());
				balanceDTO.setTransactionMovementsDTOList(movements);
			}

			balanceDTO.setBalance(new BigDecimal(responseMovimientos.getSaldo()));
			return balanceDTO;
		} catch (WebServiceException wse) {
			log.error("Error al consultar el balance.", wse);
			if (wse.getCause().getClass().equals(SocketTimeoutException.class))
				throw new RepositoryException(TIEMPO_ESPERA_AGOTADO, wse);
			else
				throw new RepositoryException(ERROR_NO_CONTROLADO);

		} catch (RepositoryException se) {
			log.error("Error al consultar el balance.", se);
			throw se;
		} catch (Exception e) {
			log.error("Error al consultar el balance.", e);
			throw new RepositoryException(ERROR_NO_CONTROLADO);
		}
	}

	@Override
	public void associateCard(Long numberCard, Long sivaleId) throws Exception {
		String origin = claimsResolver.origin();
		RequestTarjeta requestTarjeta = new RequestTarjeta();
		requestTarjeta.setOrigen(origin);
		requestTarjeta.setIdentificador(sivaleId.toString());
		requestTarjeta.setTarjeta(numberCard.toString());

		ResponseGenerico response = getAppsPort().agregarTarjeta(requestTarjeta);
		UtilValidator.validateResponseError(response.getResponseError());
	}

	@Override
	public String unAssociateCard(Long numberCard, Long sivaleId) throws Exception {
		String origin = claimsResolver.origin();
		RequestTarjeta requestTarjeta = new RequestTarjeta();
		requestTarjeta.setOrigen(origin);
		requestTarjeta.setIdentificador(sivaleId.toString());
		requestTarjeta.setTarjeta(numberCard.toString());

		ResponseGenerico response = getAppsPort().eliminarTarjeta(requestTarjeta);
		UtilValidator.validateResponseError(response.getResponseError());
		return "DESASOCIADA";
	}

	@Override
	public TypeDatos getCardData(String iut, String origin) throws Exception {

		log.debug("GET CARD DATA IUT :::::>>>>> " + iut);

		RequestIut requestIut = new RequestIut();
		requestIut.setOrigen(origin);
		requestIut.setIut(iut);

		ResponseDatos responseDatos = getAppsPort().datosTarjeta(requestIut);
		UtilValidator.validateResponseError(responseDatos.getResponseError());
		return responseDatos.getDatos();
	}

	@Override
	public ResponseSaldo getCardMovements(String cardNumber, Integer numAbonos, Integer numCargos) {
		RequestMovimientos requestMovimientos = new RequestMovimientos();
		requestMovimientos.setTarjeta(cardNumber);
		requestMovimientos.setAbonos(numAbonos);
		requestMovimientos.setCargos(numCargos);
		requestMovimientos.setSaldo("S");
		return getTarjetaPort().consultarMovimientos(requestMovimientos);
	}

	@Override
	public List<CardDTO> findCardsByClient(String clientId, String origin) throws Exception {
		log.info("findCardsByClient on CardServiceImpl");
		try {
			clientId = clientId == null ? claimsResolver.clientId() : clientId;
			origin = origin == null ? claimsResolver.origin() : origin;

			List<CardDTO> cards = new ArrayList<>();

			RequestCliente requestCliente = new RequestCliente();
			requestCliente.setOrigen(origin);
			requestCliente.setCliente(clientId);

			ResponseProductos responseProductos = getAppsPort().productosByCliente(requestCliente);
			UtilValidator.validateResponseError(responseProductos.getResponseError());

			if (responseProductos != null && !responseProductos.getProductos().getProducto().isEmpty()) {
				for (TypeProducto producto : responseProductos.getProductos().getProducto()) {

					ResponseTarjetas responseTarjetas = getTitCard(clientId, producto.getClaveEmisor(), "CNT");

					if (responseTarjetas != null && !responseTarjetas.getTarjetas().getTarjeta().isEmpty()) {
						for (TypeTarjetas typeTarjeta : responseTarjetas.getTarjetas().getTarjeta()) {
							CardDTO card = new CardDTO();
							card.setIut(typeTarjeta.getIut());
							card.setMaskCardNumber(typeTarjeta.getTarjeta().substring(12));
							card.setCardNumber(typeTarjeta.getTarjeta());
							card.setProductKey(producto.getClaveEmisor());
							card.setProductDescription(producto.getDescripcion());
							card.setBalance(Double.parseDouble(typeTarjeta.getSaldo()));
							card.setClientId(clientId);
							cards.add(card);
						}
					}
				}

				return cards;
			}

			throw new RepositoryException("No tiene productos asociados");
		} catch (WebServiceException wse) {
			throw wse;
		} catch (ServiceException se) {
			throw se;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RepositoryException(ERROR_NO_CONTROLADO);
		}

	}

	private ResponseTarjetas getTitCard(String clientId, String productKey, String tipo) throws Exception {

		RequestEntregaTarjetas requestEntregaTarjetas = new RequestEntregaTarjetas();
		requestEntregaTarjetas.setCliente(clientId);
		requestEntregaTarjetas.setOrigen(ORIGIN_WEB);
		requestEntregaTarjetas.setProducto(productKey);
		requestEntregaTarjetas.setTipo(tipo);

		ResponseTarjetas responseTarjetas = getAppsPort().entregarTarjeta(requestEntregaTarjetas);
		UtilValidator.validateResponseError(responseTarjetas.getResponseError());

		return responseTarjetas;

	}

	@Override
	public List<CardDTO> findCardsUserByUserId(String userId) throws Exception {

		String clientId = claimsResolver.clientId();
		log.info("clientId: "+clientId);
		log.info("userId: "+userId);

		List<CardDTO> cards = new ArrayList<>();
		//RequestConsultarTarjetas requestConsultarTarjetas = new RequestConsultarTarjetas();
		//requestConsultarTarjetas.setCliente(clientId);
		//requestConsultarTarjetas.setIdentificador(userId);
		RequestClienteIdentificador requestClienteIdentificador = new RequestClienteIdentificador();
		requestClienteIdentificador.setCliente(clientId);
		requestClienteIdentificador.setIdentificador(userId);
		requestClienteIdentificador.setOrigen(ORIGIN_WEB);
		//ResponseTarjetasChofer responseTarjetasChofer = getAppsPort().consultarTarjetas(requestConsultarTarjetas);
		ResponseTarjetasChofer responseTarjetasChofer = getAppsPort().tarjetasUsuario(requestClienteIdentificador);
		UtilValidator.validateResponseError(responseTarjetasChofer.getResponseError());

		for (TypeTarjeta typeTarjeta : responseTarjetasChofer.getTarjetas().getTarjeta()) {
			CardDTO card = new CardDTO();
			card.setIut(typeTarjeta.getIut());
			card.setMaskCardNumber(typeTarjeta.getTarjeta().substring(12));
			card.setProductKey(typeTarjeta.getClaveProducto());
			card.setProductDescription(typeTarjeta.getDescProducto());
			card.setClientId(typeTarjeta.getNumCliente());
			card.setClientName(typeTarjeta.getNombreCliente());
			card.setCardNumber(typeTarjeta.getTarjeta());
			cards.add(card);
		}

		return cards;
	}

	@Override
	public List<TransactionMovementsDTO> findMovementsByIut(String iut) throws Exception{
		List<TransactionMovementsDTO> listResult=new ArrayList<>();
		try {
			String origin = claimsResolver.origin();

			TypeDatos card = getCardData(iut, origin);

			ResponseSaldo responseMovimientos = getCardMovements(card.getNumTarjeta(), 20, 20);

			if (responseMovimientos != null
					&&  responseMovimientos.getTransacciones() != null
					&&  responseMovimientos.getTransacciones().getTransaccion() != null
					&& !responseMovimientos.getTransacciones().getTransaccion().isEmpty()) {
				listResult = UtilBean.typeTransaccionToTransactionMovementsDTO(responseMovimientos.getTransacciones().getTransaccion());
			}else {
				log.info("No hay movimientos en la tarjeta.");
			}

		}catch (Exception e){
			log.error("Error findMovementsByIut: ",e);
		}
		return  listResult;
	}
}

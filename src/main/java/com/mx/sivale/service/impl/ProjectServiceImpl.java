package com.mx.sivale.service.impl;

import com.mx.sivale.model.Project;
import com.mx.sivale.repository.ProjectRepository;
import com.mx.sivale.service.ClientService;
import com.mx.sivale.service.ProjectService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectServiceImpl implements ProjectService {

    private static final Logger log = Logger.getLogger(ProjectServiceImpl.class);

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private ClientService clientService;

    public Project create(Project project) throws Exception {
        project.setActive(Boolean.TRUE);
        project.setClient(clientService.getCurrentClient());
        return projectRepository.saveAndFlush(project);
    }

    public Project update(Project project) throws Exception {
        return projectRepository.saveAndFlush(project);
    }

    public void remove(Long id) throws Exception {
        Project project = projectRepository.findOne(id);
        project.setActive(Boolean.FALSE);
        projectRepository.saveAndFlush(project);
    }

    public List<Project> findAll() throws Exception {
        return projectRepository.findByActiveTrue();
    }

    public Project findOne(Long id) throws Exception {
        return projectRepository.findOne(id);
    }

    public List<Project> findByClient() throws Exception {
        return projectRepository.findByClientAndActiveTrue(clientService.getCurrentClient());
    }

    public List<Project> searchByName(String name) throws Exception {
        return projectRepository.findByActiveTrueAndClientAndNameContaining(clientService.getCurrentClient(), name);
    }

}
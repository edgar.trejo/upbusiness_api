package com.mx.sivale.service.impl;

import com.mx.sivale.model.Configuration;
import com.mx.sivale.repository.ConfigurationRepository;
import com.mx.sivale.service.ConfigurationService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.mx.sivale.config.constants.ConstantConfiguration.*;

@Service
public class ConfigurationServiceImpl implements ConfigurationService {

    @Autowired
    private ConfigurationRepository configurationRepository;

    private static final Logger log = Logger.getLogger(ConfigurationServiceImpl.class);

    @Override
    public Boolean isStatusGPVActive(String name) {

        Configuration configuration = configurationRepository.findByName(name);
        if(configuration != null && !configuration.getValue().isEmpty()
                && configuration.getValue().equals("ON")){
            return true;
        }

        return false;
    }
}

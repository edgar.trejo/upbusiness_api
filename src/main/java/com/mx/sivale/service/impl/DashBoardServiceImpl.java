package com.mx.sivale.service.impl;

import com.mx.sivale.model.Client;
import com.mx.sivale.model.Role;
import com.mx.sivale.model.User;
import com.mx.sivale.model.UserClient;
import com.mx.sivale.model.dto.*;
import com.mx.sivale.repository.*;
import com.mx.sivale.service.ClientService;
import com.mx.sivale.service.DashBoardService;
import com.mx.sivale.service.RoleService;
import com.mx.sivale.service.exception.ServiceException;
import com.mx.sivale.service.request.ClaimsResolver;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.Tuple;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class DashBoardServiceImpl implements DashBoardService {

    private Logger logger = Logger.getLogger(DashBoardServiceImpl.class);

    @Autowired
    private ClaimsResolver claimsResolver;
    @Autowired
    private ClientService clientService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private DashboardRepository dashboardRepository;
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private UserClientRepository userClientRepository;
    @Autowired
    private RoleService roleService;

    @Override
    public Map<Integer, Map<Integer, List<EventDashBoardDTO>>> getEvents() throws ServiceException {
        Map<Integer, Map<Integer, List<EventDashBoardDTO>>> yearGrouped = new HashMap<>();
        Map<Integer, List<EventDashBoardDTO>> listGrouped = new HashMap<>();

        try {
            String userEmail = claimsResolver.email();
            Long clientId = clientService.getCurrentClientId();
            List<EventDashBoardDTO> listEvents;
            User user = userRepository.findByEmailAndActiveTrue(userEmail);
            UserClient userClient = userClientRepository.findByClientIdAndUserIdAndActiveTrue(clientId, user.getId());
            Role r=roleService.getCurrentRole();
            userClient.setCurrentRole(r);
            if (userClient.getCurrentRole() != null && userClient.getCurrentRole().getId().equals(Role.ADMIN)) {
                listEvents = dashboardRepository.findAllEventsGroupByMonthAndStatus(clientId);
                listGrouped = listEvents.stream().collect(Collectors.groupingBy(w -> w.getYearEvent()));
                listGrouped.forEach((k, v) -> {
                    yearGrouped.put(k, v.stream().collect(Collectors.groupingBy(w -> w.getMonthEvent())));
                });
            }
            return yearGrouped;
        } catch (Exception e) {
            logger.error("Error: ", e);
            return null;
        }
    }

    @Override
    public Map<Integer, Map<Integer, List<SpendingDashBoardDTO>>> getSpendings() throws ServiceException {
        Map<Integer, Map<Integer, List<SpendingDashBoardDTO>>> yearGrouped = new HashMap<>();
        Map<Integer, List<SpendingDashBoardDTO>> listGrouped = new HashMap<>();
        Map<Integer, List<SpendingDashBoardDTO>> listMonth = new HashMap<>();
        try {
            String userEmail = claimsResolver.email();
            Long clientId = clientService.getCurrentClientId();
            List<SpendingDashBoardDTO> listSpendings = new ArrayList<>();
            User user = userRepository.findByEmailAndActiveTrue(userEmail);
            UserClient userClient = userClientRepository.findByClientIdAndUserIdAndActiveTrue(clientId, user.getId());
            Role r=roleService.getCurrentRole();
            userClient.setCurrentRole(r);
            if (userClient.getCurrentRole() != null && userClient.getCurrentRole().getId().equals(Role.ADMIN)) {
                listSpendings = dashboardRepository.findThreeSpendingsGroupBySpendingType(clientService.getCurrentClientId());

                listGrouped = listSpendings.stream().collect(Collectors.groupingBy(SpendingDashBoardDTO::getYear));

                Iterator<Map.Entry<Integer, List<SpendingDashBoardDTO>>> it = listGrouped.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry<Integer, List<SpendingDashBoardDTO>> pair = it.next();
                    listMonth = pair.getValue().stream().collect(Collectors.groupingBy(w -> w.getMonth()));
                    listMonth.replaceAll((k, v) -> {

                        Double total = v.stream().skip(3).mapToDouble(t -> t.getTotalAmount()).sum();

                        List<SpendingDashBoardDTO> finalList = v.stream().limit(3).collect(Collectors.toList());
                        if (total != null && total > 0) {
                            finalList.add(new SpendingDashBoardDTO(v.get(0).getYear(), v.get(0).getMonth(), "Otros", total));
                        }
                        return finalList;
                    });
                    yearGrouped.put(pair.getKey(), listMonth);
                }
            }
        } catch (Exception e) {
            logger.error("Error getSpendings: ", e);
            return null;
        }
        return yearGrouped;
    }

    @Override
    public Map<Integer, Map<Integer, List<TransferDashBoardDTO>>> getTransfers() throws ServiceException {
        String userEmail = claimsResolver.email();

        Map<Integer, Map<Integer, List<TransferDashBoardDTO>>> yearGrouped = new HashMap<>();
        Map<Integer, List<TransferDashBoardDTO>> listGrouped = new HashMap<>();
        List<TransferDashBoardDTO> listTransfers = new ArrayList<>();
        try {
            Long clientId = clientService.getCurrentClientId();
            User user = userRepository.findByEmailAndActiveTrue(userEmail);
            UserClient userClient = userClientRepository.findByClientIdAndUserIdAndActiveTrue(clientId, user.getId());
            Role r=roleService.getCurrentRole();
            userClient.setCurrentRole(r);
            if (userClient.getCurrentRole() != null && userClient.getCurrentRole().getId().equals(Role.ADMIN)) {
                listTransfers = dashboardRepository.getTransfers(clientService.getCurrentClientId());
                listGrouped = listTransfers.stream().collect(Collectors.groupingBy(x -> x.getYear()));
                listGrouped.forEach((k, v) -> {
                    yearGrouped.put(k, v.stream().collect(Collectors.groupingBy(w -> w.getMonth())));
                });
            }
            return yearGrouped;
        } catch (Exception e) {
            logger.error("Error: ", e);
            return null;
        }
    }


}

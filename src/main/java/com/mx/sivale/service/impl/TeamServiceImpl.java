package com.mx.sivale.service.impl;

import com.mx.sivale.model.Team;
import com.mx.sivale.repository.TeamRepository;
import com.mx.sivale.service.ClientService;
import com.mx.sivale.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeamServiceImpl implements TeamService {

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private ClientService clientService;

    public Team create(Team team) throws Exception {
        team.setActive(Boolean.TRUE);
        team.setClient(clientService.getCurrentClient());
        return teamRepository.saveAndFlush(team);
    }

    public Team update(Team team) throws Exception {
        return teamRepository.saveAndFlush(team);
    }

    public void remove(Long id) throws Exception {
        Team team = teamRepository.findOne(id);
        team.setActive(Boolean.FALSE);
        teamRepository.saveAndFlush(team);
    }

    public List<Team> findAll() throws Exception {
        return teamRepository.findAll();
    }

    public Team findOne(Long id) throws Exception {
        return teamRepository.findOne(id);
    }

    public List<Team> findByClientId() throws Exception {
        return teamRepository.findByActiveTrueAndClient(clientService.getCurrentClient());
    }

    public List<Team> searchByName(String name) throws Exception {
        return teamRepository.findByNameAndCode(clientService.getCurrentClient(), name);
    }

}
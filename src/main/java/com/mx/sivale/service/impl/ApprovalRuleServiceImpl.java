package com.mx.sivale.service.impl;

import com.mx.sivale.model.ApprovalRule;
import com.mx.sivale.repository.ApprovalRuleRepository;
import com.mx.sivale.service.ApprovalRuleService;
import com.mx.sivale.service.ClientService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * @author armando.reyna
 *
 */

@Service
public class ApprovalRuleServiceImpl implements ApprovalRuleService {

    private static final Logger log = Logger.getLogger(ApprovalRuleServiceImpl.class);

    @Autowired
    private ApprovalRuleRepository approvalRuleRepository;

    @Autowired
    private ClientService clientService;

    public ApprovalRule create(ApprovalRule approvalRule) throws Exception {
        approvalRule.setActive(Boolean.TRUE);
        approvalRule.setClient(clientService.getCurrentClient());
        return approvalRuleRepository.saveAndFlush(approvalRule);
    }

    public ApprovalRule update(ApprovalRule approvalRule) throws Exception {
        return approvalRuleRepository.saveAndFlush(approvalRule);
    }

    public void remove(Long id) throws Exception {
        ApprovalRule approvalRule = approvalRuleRepository.findOne(id);
        approvalRule.setActive(Boolean.FALSE);
        approvalRuleRepository.saveAndFlush(approvalRule);
    }

    public ApprovalRule findOne(Long id) throws Exception {
        return approvalRuleRepository.findOne(id);
    }

    public List<ApprovalRule> findByClientId() throws Exception {
        return approvalRuleRepository.findByClientAndActiveTrue(clientService.getCurrentClient());
    }

}

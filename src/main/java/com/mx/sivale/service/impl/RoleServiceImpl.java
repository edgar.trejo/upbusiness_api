package com.mx.sivale.service.impl;

import com.mx.sivale.config.constants.ConstantInteliviajes;
import com.mx.sivale.model.*;
import com.mx.sivale.repository.ClientRepository;
import com.mx.sivale.repository.UserClientRepository;
import com.mx.sivale.repository.UserRepository;
import com.mx.sivale.service.RoleService;
import com.mx.sivale.service.request.ClaimsResolver;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {

    private Logger log=Logger.getLogger(RoleServiceImpl.class);

    @Autowired
    private ClaimsResolver claimsResolver;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserClientRepository userClientRepository;

    @Override
    public Role getCurrentRole() throws Exception {
        String sessionEmail = claimsResolver.email();
        String sessionRole = claimsResolver.roleId();
        String sessionClient=claimsResolver.clientId();
        Role role;
        try {

            Client client = clientRepository.findByNumberClient(Long.parseLong(sessionClient));

            if (client != null) {
                User u=userRepository.findByEmailAndActiveTrue(sessionEmail);
                UserClient userClient=userClientRepository.findByClientIdAndUserIdAndActiveTrue(client.getId(),u.getId());

                role= userClient.getListUserClientRole().stream().
                        filter(x->x.getRole().getId().toString().equals(sessionRole))
                        .findFirst()
                        .get().getRole()
                        ;
            }else
                throw new Exception("No existe el numero de cliente proporcionado.");

            return role;
        } catch (Exception e) {
            log.error("Error getCurrentRole", e);
            throw new Exception(e.getMessage());
        }

    }

    @Override
    public Boolean isCurrentClientAdmin(UserClient userClient, String origin) throws Exception {
        Boolean isCurrentClientAdmin=Boolean.FALSE;
        UserClientRole ucr;

        try {
            if (userClient != null) {

                ucr = userClient.getListUserClientRole().stream().
                        filter(
                                x->x.getRole().getId().equals(Role.ADMIN)
                        )
                        .findFirst()
                        .orElse(null)
                ;

                if(ucr!=null && ucr.getRole().getId().equals(Role.ADMIN) && origin.equals(ConstantInteliviajes.ORIGIN_WEB))
                    isCurrentClientAdmin=Boolean.TRUE;
            }else
                throw new Exception("No existe relacion usuario/cliente proporcionado.");

        } catch (Exception e) {
            log.error("Error isCurrentClientAdmin", e);
            throw new Exception(e.getMessage());
        }
        return isCurrentClientAdmin;
    }
}

package com.mx.sivale.service.impl;

import com.mx.sivale.model.Client;
import com.mx.sivale.model.CostCenter;
import com.mx.sivale.model.JobPosition;
import com.mx.sivale.model.dto.CatalogDTO;
import com.mx.sivale.model.dto.UserRequestDTO;
import com.mx.sivale.repository.CostCenterRepository;
import com.mx.sivale.service.ClientService;
import com.mx.sivale.service.CostCenterService;
import com.mx.sivale.service.util.UtilLayout;

import jxl.Workbook;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

@Service
public class CostCenterServiceImpl implements CostCenterService {

    private static final Logger log = Logger.getLogger(CostCenterServiceImpl.class);

    @Autowired
    private CostCenterRepository costCenterRepository;

    @Autowired
    private ClientService clientService;

    public CostCenter create(CostCenter costCenter) throws Exception {
        costCenter.setActive(Boolean.TRUE);
        costCenter.setClient(clientService.getCurrentClient());
        return costCenterRepository.saveAndFlush(costCenter);
    }

    public CostCenter update(CostCenter costCenter) throws Exception {
        return costCenterRepository.saveAndFlush(costCenter);
    }

    public void remove(Long id) throws Exception {
        CostCenter costCenter = costCenterRepository.findOne(id);
        costCenter.setActive(Boolean.FALSE);
        costCenterRepository.saveAndFlush(costCenter);
    }

    public List<CostCenter> findAll() throws Exception {
        return costCenterRepository.findByActiveTrue();
    }

    public CostCenter findOne(Long id) throws Exception {
        return costCenterRepository.findOne(id);
    }

    public List<CostCenter> findByClientId() throws Exception {
        return costCenterRepository.findByClientAndActiveTrue(clientService.getCurrentClient());
    }

    public List<CostCenter> searchByName(String name) throws Exception {
        return costCenterRepository.findByNameAndCode(clientService.getCurrentClient(), name);
    }

	@Override
	public int createMassive(List<CostCenter> lCostCenter) throws Exception {
		int rowsInserted = 0;
		Client currentClient = clientService.getCurrentClient();
		for(CostCenter item: lCostCenter) {
			CostCenter costCenterExist = costCenterRepository.findByClient_idAndNameAndCodeAndActiveTrue(currentClient.getId(),item.getName(), item.getCode());
			
			if(costCenterExist == null) {
				item.setActive(Boolean.TRUE);
				item.setClient(currentClient);
				costCenterRepository.save(item);
				rowsInserted ++;
			}
		}		
		return rowsInserted;
	}
	

	@Override
	public WritableWorkbook createMassiveOutputExcel(HttpServletResponse response, MultipartFile file) throws Exception {
		String fileName = "COSTCENTER_LAYOUT.xls";
	    WritableWorkbook writableWorkbook = null;
	    try {
	            response.setContentType("application/vnd.ms-excel");
	            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);

	            writableWorkbook = Workbook.createWorkbook(response.getOutputStream());

	            WritableSheet excelOutputsheet = writableWorkbook.createSheet("CARGA MASIVA ÁREAS", 0);
	            //excelOutputsheet = UtilLayout.addExcelOutputHeader(excelOutputsheet);
	            excelOutputsheet = UtilLayout.addLogosHeader(excelOutputsheet);
	            
	            
	            WritableCellFormat cFormat = new WritableCellFormat();
	            WritableCellFormat cFormat2 = new WritableCellFormat();
	            WritableFont font = new WritableFont(WritableFont.ARIAL, 11, WritableFont.BOLD);
	            font.setColour(Colour.WHITE);
	            cFormat.setFont(font);
	            cFormat.setBackground(Colour.ORANGE);
	            excelOutputsheet.addCell(new Label(0, 5, "Área", cFormat));
	            excelOutputsheet.setColumnView(0, 15);
	            excelOutputsheet.addCell(new Label(1, 5, "Código", cFormat));
	            excelOutputsheet.setColumnView(1, 35);
	            
	            writableWorkbook.write();
	            writableWorkbook.close();

	    } catch (Exception e) {
	    	log.error("Ocurio error mientras se construia archivo", e);
	    }

	    return writableWorkbook;
	}

}
package com.mx.sivale.service.impl;

import com.mx.sivale.config.constants.ConstantInteliviajes;
import com.mx.sivale.model.*;
import com.mx.sivale.model.dto.*;
import com.mx.sivale.repository.*;
import com.mx.sivale.repository.exception.RepositoryException;
import com.mx.sivale.service.AccountingService;
import com.mx.sivale.service.CardService;
import com.mx.sivale.service.ClientService;
import com.mx.sivale.service.TransferService;
import com.mx.sivale.service.exception.ServiceException;
import com.mx.sivale.service.request.ClaimsResolver;
import com.mx.sivale.service.util.UtilBean;
import com.mx.sivale.service.util.UtilValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ws.sivale.com.mx.messages.request.appgasolina.TypeTarjeta;
import ws.sivale.com.mx.messages.request.apps.*;
import ws.sivale.com.mx.messages.request.apps.TypeTraspaso.Tarjetas;
import ws.sivale.com.mx.messages.request.tarjeta.RequestBase;
import ws.sivale.com.mx.messages.response.apps.*;
import ws.sivale.com.mx.messages.response.tarjeta.ResponseSaldo;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.*;

import static com.mx.sivale.config.constants.ConstantInteliviajes.*;

@Service
public class TransferServiceImpl extends WsdlConsumeServiceImpl implements TransferService {

    private static final Logger log = Logger.getLogger(TransferServiceImpl.class);

    @Autowired
    private ClaimsResolver claimsResolver;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserClientRepository userClientRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ClientService clientService;

    @Autowired
    private TransferRepository transferRepository;

    @Autowired
    private CardService cardService;

    @Autowired
    private TeamUsersRepository teamUsersRepository;

    @Autowired
    private AccountingService accountingService;

    String getAmountTransferToConcentrator(String iut, Double originalAmount){
        try{
            BalanceDTO balanceDTO=cardService.getCardBalance(iut);
            Double currentBalance=balanceDTO.getBalance().doubleValue();
            return originalAmount<=currentBalance?originalAmount.toString():currentBalance.toString();
        }catch (Exception e){
            log.error("Error validateBalanceForTransferToConcentrator: ");
        }
        return originalAmount.toString();
    }

    @Override
    public Transfer makeTranfer(TransferDTO transferDTO) throws Exception {
        log.debug("TRANSFER BALANCE :::::>>>> ");

        String clientId = claimsResolver.clientId();
        String origin = claimsResolver.origin();
        String contactId = claimsResolver.contactId();

        User user = userRepository.findByEmailAndActiveTrue(claimsResolver.email());
        if (user == null)
            throw new ServiceException("No existe solicitante");

        TypeSolicitante solicitante = new TypeSolicitante();
        solicitante.setCorreo(claimsResolver.email());
        solicitante.setNombre(user.getCompleteName());

        TypeCliente cliente = new TypeCliente();
        cliente.setNumero(clientId);
        cliente.setIdUsuario(contactId);

        TypeTraspaso.Tarjetas cards = new Tarjetas();

        List<TypeTarjeta> cardList = new ArrayList<>();
        TypeTarjeta tarjeta = new TypeTarjeta();

        Transfer transfer = new Transfer();
        transfer.setUser(user);
        transfer.setClient(clientService.getCurrentClient());
        transfer.setIut(transferDTO.getIutDestiny());
        transfer.setIutConcentradora(transferDTO.getIutConcentrator());
        transfer.setAmount(Double.parseDouble(transferDTO.getAmmount()));
        transfer.setDateCreated(new Timestamp(new Date().getTime()));
        transfer.setOriginAmount(transferDTO.getOriginAmount());
        transfer.setRequestType(transferDTO.getRequestType());
        transfer.setCardNumber(transferDTO.getUserCard());

        if (transferDTO.getTransferType().equals(TransferType.CT.name())) {
            tarjeta.setIutOrigen(transferDTO.getIutConcentrator());
            tarjeta.setIutDestino(transferDTO.getIutDestiny());
            tarjeta.setMonto(transferDTO.getAmmount());
            tarjeta.setMovimiento("1");
            tarjeta.setControl(CONTROL_CT);

            transfer.setType(TransferType.CT);
        }

        if (transferDTO.getTransferType().equals(TransferType.TC.name())) {
            tarjeta.setIutOrigen(transferDTO.getIutDestiny());
            tarjeta.setIutDestino(transferDTO.getIutConcentrator());
            tarjeta.setMonto(getAmountTransferToConcentrator(transferDTO.getIutDestiny(),Double.parseDouble(transferDTO.getAmmount())));
            tarjeta.setMovimiento("1");
            tarjeta.setControl(CONTROL_TC);

            transfer.setType(TransferType.TC);
        }
        cardList.add(tarjeta);
        cards.setTarjeta(cardList);

        TypeTraspaso traspaso = new TypeTraspaso();
        traspaso.setIutConcentradora(transferDTO.getIutConcentrator());
        traspaso.setTipo(transferDTO.getTransferType());
        traspaso.setTarjetas(cards);

        RequestTraspasos requestTraspasos = new RequestTraspasos();
        requestTraspasos.setOrigen(origin);
        requestTraspasos.setSolicitante(solicitante);
        requestTraspasos.setCliente(cliente);
        requestTraspasos.setTraspaso(traspaso);

        ResponseTraspaso responseTraspaso = getAppsPort().solicitarTraspasos(requestTraspasos);
        UtilValidator.validateResponseError(responseTraspaso.getResponseError());

        transfer.setSolicitudeId(Long.valueOf(responseTraspaso.getSolicitud()));

        if (transferDTO.getUserId() != null) {
            User cardOwner = new User();
            cardOwner.setId(transferDTO.getUserId());
            transfer.setCardOwner(cardOwner);
        }

        // Consultar detalle de solictud para obtener saldos antes y después de la operación
        try {
            RequestSolicitud requestSolicitud = new RequestSolicitud();
            requestSolicitud.setOrigen(origin);
            requestSolicitud.setSolicitud(responseTraspaso.getSolicitud());
            ResponseDetalleSolicitud responseDetalle=getAppsPort().detalleSolicitud(requestSolicitud);
            TypeDetalleSolicitud detalle = responseDetalle.getSolicitudes().getSolicitud().get(0);
            transfer.setFinalAmount(transferDTO.getTransferType().equals(TransferType.CT.name()) ? Double.valueOf(detalle.getSaldoDespTransDest()) : Double.valueOf(detalle.getSaldoDespTransOrigen()));
            transfer.setOriginAmount(transferDTO.getTransferType().equals(TransferType.CT.name()) ? Double.valueOf(detalle.getSaldoAntTransDest()) : Double.valueOf(detalle.getSaldoAntTransOrigen()));
            transfer.setConcentradoraAmount(transferDTO.getTransferType().equals(TransferType.CT.name()) ? Double.valueOf(detalle.getSaldoAntTransOrigen()) : Double.valueOf(detalle.getSaldoAntTransDest()));
            transfer.setAppliedTransferNumber(detalle.getNumTransferOrigen());
            transfer.setDeclinedKey(detalle.getCveRechazo());
            transfer.setDeclinedDescription(detalle.getDescRechazo());
            transfer.setApplied(detalle.getNumTransferOrigen().equals("0") ? false : true);
        } catch (Exception balanceEx) {
            log.warn("No se pudo obtener saldo después de transfer" + balanceEx.getMessage());
            transfer.setFinalAmount(0.0);
            transfer.setOriginAmount(0.0);
        }
        //save traspaso
        return transferRepository.saveAndFlush(transfer);
    }

    @Override
    public List<TransferReportDTO> getTransferByClient() throws Exception {
        String clientId = claimsResolver.clientId();
        String emailAdmin = claimsResolver.email();
        String origin = claimsResolver.origin();
        String contactId = claimsResolver.contactId();

        List<TransferReportDTO> transferReportDTOList = new ArrayList<>();


        RequestCliente requestCliente = new RequestCliente();
        requestCliente.setOrigen(origin);
        requestCliente.setCliente(clientId);

        ResponseProductos responseProductos = getAppsPort().productosByCliente(requestCliente);
        UtilValidator.validateResponseError(responseProductos.getResponseError());


        if (responseProductos != null && !responseProductos.getProductos().getProducto().isEmpty()) {

            for (TypeProducto producto : responseProductos.getProductos().getProducto()) {

                TransferReportDTO transferReportDTO = new TransferReportDTO();


                RequestSolicitudes requestSolicitudes = new RequestSolicitudes();
                requestSolicitudes.setOrigen(origin);
                requestSolicitudes.setCliente(clientId);
                requestSolicitudes.setProducto(producto.getClaveEmisor());

                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date());

                int mYear = cal.get(Calendar.YEAR);
                int mMonth = cal.get(Calendar.MONTH) + 1;
                int mDay = cal.get(Calendar.DAY_OF_MONTH);
                String currentdate = (mDay < 10 ? ("0" + mDay) : mDay) + "/" + (mMonth < 10 ? ("0" + mMonth) : mMonth) + "/" + mYear;

                cal.add(Calendar.MONTH, -1);

                mYear = cal.get(Calendar.YEAR);
                mMonth = cal.get(Calendar.MONTH) + 1;
                mDay = cal.get(Calendar.DAY_OF_MONTH);
                String afterdate = (mDay < 10 ? ("0" + mDay) : mDay) + "/" + (mMonth < 10 ? ("0" + mMonth) : mMonth) + "/" + mYear;

                requestSolicitudes.setFechaInicio(afterdate);
                requestSolicitudes.setFechaFin(currentdate);


                ResponseSolicitudes responseSolicitudes = getAppsPort().consultaSolicitudes(requestSolicitudes);
                UtilValidator.validateResponseError(responseSolicitudes.getResponseError());


                if (responseSolicitudes != null && !responseSolicitudes.getSolicitudes().getSolicitud().isEmpty()) {
                    for (TypeSolicitud typeSolicitud : responseSolicitudes.getSolicitudes().getSolicitud()) {


                        RequestSolicitud requestSolicitud = new RequestSolicitud();
                        requestSolicitud.setOrigen(origin);
                        requestSolicitud.setEmisor(producto.getClaveEmisor());
                        requestSolicitud.setSolicitud(typeSolicitud.getNumero());


                        ClientDTO clientDTO = UtilBean.clientModelToClientDTO(clientRepository.findByNumberClient(Long.parseLong(clientId)));
                        UserClient userClient = userClientRepository.findByClientIdAndContactId(clientDTO.getId(), contactId);
                        if (userClient == null)
                            throw new RepositoryException("6# No existe solicitante");

                        UserResponseDTO userDTO = UtilBean.userModelToUserDTO(userRepository.findOne(userClient.getUserId()),userClient);


                        TypeSolicitante solicitante = new TypeSolicitante();
                        solicitante.setCorreo(emailAdmin);
                        solicitante.setNombre(userDTO.getName());

                        TypeCliente cliente = new TypeCliente();
                        cliente.setNumero(clientId);
                        cliente.setIdUsuario(contactId);


                        requestSolicitud.setSolicitante(solicitante);
                        requestSolicitud.setCliente(cliente);


                        ResponseDetalleSolicitud responseDetalleSolicitud = getAppsPort().detalleSolicitud(requestSolicitud);
                        if (responseDetalleSolicitud.getSolicitudes() != null && !responseDetalleSolicitud.getSolicitudes().getSolicitud().isEmpty()) {
                            for (TypeDetalleSolicitud typeDetalleSolicitud : responseDetalleSolicitud.getSolicitudes().getSolicitud()) {
                                RequestIut requestIut = new RequestIut();
                                requestIut.setOrigen(origin);
                                requestIut.setIut(typeDetalleSolicitud.getIdTarjetaDestino());

                                ResponseDatos responseDatos = getAppsPort().datosTarjeta(requestIut);
                                String nombre = (responseDatos.getDatos().getNombreAsignado().trim() != null && !responseDatos.getDatos().getNombreAsignado().isEmpty()) ? responseDatos.getDatos().getNombreAsignado() : "Sin asociar";

                                transferReportDTO.setName(nombre);
                                transferReportDTO.setCard("***********" + responseDatos.getDatos().getNumTarjeta().substring(12));
                                transferReportDTO.setAmmount("$" + new Double(typeSolicitud.getMonto()));
                                transferReportDTO.setTransferDate(typeSolicitud.getFecha());
                                transferReportDTO.setBalancePrevious("$" + new Double(typeDetalleSolicitud.getSaldoAntTransDest()));
                                transferReportDTO.setBalanceCurrent("$" + new Double(typeDetalleSolicitud.getSaldoDespTransDest()));

                                transferReportDTOList.add(transferReportDTO);
                            }
                        }
                    }
                }
            }
        }

        return transferReportDTOList;
    }

    @Override
    public Page<AdvanceReportDTO> report(long longFrom, long longTo, Pageable pageable, long clientId) throws Exception {
        List<AdvanceReportDTO> advanceReportList = new ArrayList<>();

        Client client;

        if (clientId == 0) {
            client = clientService.getCurrentClient();
        } else {
            client = clientRepository.findOne(clientId);
        }
        int[] requestTypes = {RequestTransferType.UNKNOWN.getId(), RequestTransferType.ASIGNACION.getId()};
        Page<Transfer> transfers = transferRepository.findAllByClient_IdAndRequestTypeInAndDateCreatedBetween(client.getId(), requestTypes, new Date(longFrom), new Date(longTo), pageable);
        Map cnts = new HashMap();
        Map products = new HashMap();
        try {
            List<CardDTO> cardDTOS = cardService.findCardsByClient(String.valueOf(client.getNumberClient()), ConstantInteliviajes.ORIGIN_WEB);
            for (CardDTO card : cardDTOS) {
                cnts.put(card.getIut(), card.getCardNumber());
                products.put(card.getIut(), card.getProductDescription());
            }
        }catch(Exception ex){
            log.warn("Error obteniendo productos del cliente para el reporte", ex);
        }

        String originCard = "";
        String productName = "";
        for (Transfer r : transfers) {
            AdvanceReportDTO advanceReportDTO = new AdvanceReportDTO();
            if (r.getCardOwner() != null) {
                advanceReportDTO.setUserName(r.getCardOwner().getCompleteName() != null ? r.getCardOwner().getCompleteName() : "");
                advanceReportDTO.setEmployeeNumber(r.getCardOwner().getNumberEmployee() != null ? r.getCardOwner().getNumberEmployee() : "");
                if(r.getUserClient()!=null) {
                    advanceReportDTO.setJobPosition(r.getUserClient().getJobPosition() == null ? "" : r.getUserClient().getJobPosition().getName());
                    advanceReportDTO.setJobCode(r.getUserClient().getJobPosition() == null ? "" : r.getUserClient().getJobPosition().getCode());
                    advanceReportDTO.setArea(r.getUserClient().getCostCenter() == null ? "" : r.getUserClient().getCostCenter().getName());
                    advanceReportDTO.setAreaCode(r.getUserClient().getCostCenter() == null ? "" : r.getUserClient().getCostCenter().getCode());
                }
                TeamUsers teamUsers = teamUsersRepository.findFirstByUserId(r.getCardOwner().getId());
                advanceReportDTO.setGroup(teamUsers == null ? "" : teamUsers.getTeam().getName());
                advanceReportDTO.setGroupCode(teamUsers == null ? "" : teamUsers.getTeam().getCode());
            }else{
                advanceReportDTO.setUserName("");
                advanceReportDTO.setEmployeeNumber("");
                advanceReportDTO.setJobPosition("");
                advanceReportDTO.setJobCode("");
                advanceReportDTO.setArea("");
                advanceReportDTO.setAreaCode("");
                advanceReportDTO.setGroup("");
                advanceReportDTO.setGroupCode("");
            }
            String type = r.getType().name().toUpperCase().equalsIgnoreCase(TransferType.CT.toString()) ? TransferType.CT.getDescription() : TransferType.TC.getDescription();
            advanceReportDTO.setAdvanceName(type);
            advanceReportDTO.setAdvanceId(String.valueOf(r.getId()));
            advanceReportDTO.setAdvanceDate(r.getDateCreated());
            advanceReportDTO.setDispersionAdvanceDate(r.getDateCreated());
            advanceReportDTO.setRequestedAmount(String.valueOf(r.getAmount()));
            advanceReportDTO.setStatus("OK");
            productName = products.get(r.getIutConcentradora()) != null ? (String) products.get(r.getIutConcentradora()) : "";
            advanceReportDTO.setDescription(productName);
            advanceReportDTO.setProduct(productName);
            DateFormat format = new SimpleDateFormat("hh:mm:ss");
            format.setTimeZone(TimeZone.getTimeZone(ZoneId.of("America/Mexico_City")));
            advanceReportDTO.setDispersionAdvanceTime(format.format(advanceReportDTO.getDispersionAdvanceDate()));
            advanceReportDTO.setDestinationCard(r.getCardNumber());
            advanceReportDTO.setOriginBalance(r.getConcentradoraAmount() != null ? r.getConcentradoraAmount() : 0.0);
            originCard = cnts.get(r.getIutConcentradora()) != null ? (String) cnts.get(r.getIutConcentradora()) : "No disponible";
            originCard = originCard.replaceAll("\\*", "");
            advanceReportDTO.setOriginCard(originCard);
            advanceReportDTO.setDestinationInitialBalance(r.getOriginAmount() != null ? r.getOriginAmount() : 0.0);
            advanceReportDTO.setDestinationFinalBalance(r.getFinalAmount() != null ? r.getFinalAmount() : 0.0);

            advanceReportList.add(advanceReportDTO);
        }
        return new PageImpl<>(advanceReportList, pageable, transfers.getTotalElements());
    }

    @Override
    public List<TransferResponseDTO> makeTranfers(List<TransferDTO> listTransfers, Integer requestTransferType) throws Exception {
        List<TransferResponseDTO> listResponse=new ArrayList<>();
        if(listTransfers!=null && !listTransfers.isEmpty()){
            for (TransferDTO transferDTO :listTransfers) {
                try{
                    transferDTO.setRequestType(requestTransferType);
                    Transfer transfer=makeTranfer(transferDTO);
                    TransferResponseDTO transferResponseDTO=new TransferResponseDTO();
                    transferResponseDTO.setTransferId(transfer!=null?transfer.getId():null);
                    transferResponseDTO.setUserName(transfer!=null && transfer.getCardOwner()!=null?transfer.getCardOwner().getCompleteName():"");
                    transferResponseDTO.setUserId(transfer!=null && transfer.getCardOwner()!=null?transfer.getCardOwner().getId():null);
                    transferResponseDTO.setIut(transferDTO.getIutDestiny());
                    transferResponseDTO.setClientId(transfer!=null && transfer.getClient()!=null?transfer.getClient().getId():null);
                    transferResponseDTO.setMaskCardNumber(transfer!=null?transfer.getCardNumber():"");
                    transferResponseDTO.setSolicitudeId(transfer!=null?transfer.getSolicitudeId():null);
                    transferResponseDTO.setAppliedTransferNumber(transfer!=null?transfer.getAppliedTransferNumber():"");
                    transferResponseDTO.setDeclinedKey(transfer!=null?transfer.getDeclinedKey():"");
                    transferResponseDTO.setDeclinedDescription(transfer!=null?transfer.getDeclinedDescription():"");
                    transferResponseDTO.setTransferDate(transfer!=null?transfer.getDateCreated():null);
                    transferResponseDTO.setApplied(transfer!=null?transfer.isApplied():null);
                    listResponse.add(transferResponseDTO);

                    if (transferDTO.getTransferType().equals(TransferType.TC.name())) {
                        if(transfer.isApplied()){
                            //Registrar movimiento contable para devolucion
                            try{

                                accountingService.createRefundAccounting(transfer);

                            }catch (Exception e){
                                log.error(e.getMessage());
                            }
                            //Registrar movimiento contable para devolucion
                        }
                    }

                }catch (Exception e){
                    log.error("Error makeTranfer: ",e);
                }
            }
        }
        return listResponse;
    }
}

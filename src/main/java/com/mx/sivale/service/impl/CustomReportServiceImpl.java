package com.mx.sivale.service.impl;

import com.mx.sivale.model.*;
import com.mx.sivale.model.dto.CustomReportConfigDTO;
import com.mx.sivale.model.dto.CustomReportDTO;
import com.mx.sivale.repository.*;
import com.mx.sivale.service.*;
import com.mx.sivale.service.exception.ServiceException;
import com.mx.sivale.service.util.UtilBean;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CustomReportServiceImpl implements CustomReportService {

    private static final Logger log = Logger.getLogger(CustomReportServiceImpl.class);

    @Autowired
    private CustomReportConfigRepository customReportConfigRepository;

    @Autowired
    private CustomReportColumnRepository customReportColumnRepository;

    @Autowired
    private CustomReportTypeColumnRepository customReportTypeColumnRepository;

    @Autowired
    private EventService eventService;

    @Autowired
    private SpendingService spendingService;

    @Autowired
    private CostCenterRepository costCenterRepository;

    @Autowired
    private SpendingTypeRepository spendingTypeRepository;

    @Autowired
    private ApprovalStatusRepository approvalStatusRepository;

    @Autowired
    private InvoiceService invoiceService;

    @Autowired
    private SpendingTypeService spendingTypeService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CustomReportConfigColumnRepository customReportConfigColumnRepository;

    @PersistenceContext
    private EntityManager entityManager;

    public static final Long TYPE_TRAVELER = 1L;
    public static final Long TYPE_EVENT = 2L;
    public static final Long TYPE_SPENDING = 3L;
    public static final Long TYPE_SPENDING_TYPE = 4L;
    public static final Long TYPE_INVOICE = 5L;

    @Override
    public CustomReportConfigDTO getCustomReportConfigDefault() throws ServiceException {

        List<CustomReportConfig> lstConfigDefault = new ArrayList<CustomReportConfig>();
        CustomReportConfigDTO customDefaultDTO = null;

        try {

            lstConfigDefault =
                    customReportConfigRepository.findByUserIsNullAndCustomReportConfigColumn_CustomReportColumn_Active(true);
            if (lstConfigDefault != null && !lstConfigDefault.isEmpty()) {
                CustomReportConfig configDefault = lstConfigDefault.get(0);
                customDefaultDTO = UtilBean.customReportConfigToCustomReportConfigDTO(configDefault);
            }

        } catch (Exception ex) {
            log.error("Error al obtener la configuración del reporte personalizado por default.", ex);
            throw new ServiceException("Error al obtener la configuración del reporte personalizado por default.", ex);
        }

        return customDefaultDTO;
    }

    @Override
    public Page<LinkedHashMap> report(Long from, Long to, Long costCenterId, Long spendingTypeId, Long approvalStatusId,
                                        String columnsOrder, Pageable pageable, long clientId) throws ServiceException {

        List<String> columnsList = Arrays.stream(columnsOrder.split(",")).collect(Collectors.toList());
        List<CustomReportTypeColumn> lstTypeColumn = new ArrayList<CustomReportTypeColumn>();
        Page<CustomReportDTO> customReportDTOList = null;
        List<LinkedHashMap> reportResult = new ArrayList<LinkedHashMap>();
        CostCenter costCenter = null;
        SpendingType spendingType = null;
        ApprovalStatus approvalStatus = null;
        Boolean onlyInvoice = false;

        try {

            //Validate and get cost center
            if(costCenterId != null){
                costCenter = costCenterRepository.findOne(costCenterId);
                if(costCenter == null){
                    throw new ServiceException("El área no existe en la BD");
                }
            }

            //Validate and get spending Type
            if(spendingTypeId != null){
                spendingType = spendingTypeRepository.findOne(spendingTypeId);
                if(spendingType == null){
                    throw new ServiceException("El tipo de gasto no existe en la BD");
                }
            }

            //Validate and get approval status
            if(approvalStatusId != null){
                approvalStatus = approvalStatusRepository.findOne(approvalStatusId);
                if(approvalStatus == null){
                    throw new ServiceException("El estatus no existe en la BD");
                }
            }

            //Get type columns
            lstTypeColumn = customReportTypeColumnRepository.findAllByColumnName(columnsList);
            if (lstTypeColumn == null || lstTypeColumn.isEmpty()) {
                throw new ServiceException("Las columnas seleccionadas no estan correctamente mapeadas");
            }

            //Variables for determinate which data to get
            Optional<CustomReportTypeColumn> typeColumnInvoice = null;
            if(lstTypeColumn != null && lstTypeColumn.size() == 1 ){
                if (lstTypeColumn.get(0).getId().equals(TYPE_INVOICE)){
                    onlyInvoice = true;
                }
            }
            Optional<CustomReportTypeColumn> typeColumnSpendingType = lstTypeColumn.stream()
                    .filter(t -> t.getId().equals(TYPE_SPENDING_TYPE)).findFirst();

            Optional<CustomReportTypeColumn> typeColumnSpendingOrInvoice = lstTypeColumn.stream()
                    .filter(t -> t.getId().equals(TYPE_SPENDING) || t.getId().equals(TYPE_INVOICE)).findFirst();

            Optional<CustomReportTypeColumn> typeColumnTravelerOrEvent = lstTypeColumn.stream()
                    .filter(t -> t.getId().equals(TYPE_TRAVELER) || t.getId().equals(TYPE_EVENT)).findFirst();

            //Cases with information to get
            if (onlyInvoice) {
                customReportDTOList = invoiceService.customReport(
                        from, to, costCenterId, spendingType, approvalStatus, pageable, clientId);
            } else if (typeColumnSpendingType.isPresent()) {
                customReportDTOList = spendingTypeService.customReport(
                        from, to, costCenterId, spendingType, approvalStatus, pageable, clientId);
            } else if (typeColumnSpendingOrInvoice.isPresent()) {
                customReportDTOList = spendingService.customReport(
                        from, to, costCenterId, spendingType, approvalStatus, pageable, clientId);
            } else if (typeColumnTravelerOrEvent.isPresent()) {
                customReportDTOList = eventService.customReport(
                        from, to, costCenterId, spendingType, approvalStatus, pageable, clientId);
            }

            //Set Map with only fields to need
            if(customReportDTOList != null){
                customReportDTOList.forEach(
                        t -> {
                            LinkedHashMap l = new LinkedHashMap();
                            columnsList.forEach(
                                    column -> {
                                        try {
                                            l.put(column, PropertyUtils.getProperty(t, column));
                                        } catch (Exception e) {
                                            log.error("Error al obtener el valor de una propiedad dinamicamente");
                                        }
                                    }
                            );
                            reportResult.add(l);
                        });
            }

        } catch (Exception ex) {
            log.error("Error al obtener el Reporte Personalizado.", ex);
            throw new ServiceException("Error al obtener el Reporte Personalizado: " + ex.getMessage(), ex);
        }

        return new PageImpl<>(reportResult, pageable, customReportDTOList.getTotalElements());

    }

    @Override
    public List<CustomReportConfigDTO> getCustomReportConfigsByUser(Long userId) throws ServiceException{
        List<CustomReportConfig> lstConfigs = new ArrayList<CustomReportConfig>();
        List<CustomReportConfigDTO> lstConfigDefaultDTO = new ArrayList<CustomReportConfigDTO>();

        try {

            User user=userRepository.findByIdAndActiveTrue(userId);

            if(user == null){
                throw new ServiceException("Este usuario no existe en la BD");
            }

            lstConfigs =
                    customReportConfigRepository.findByUser(user);

            if (lstConfigs != null && !lstConfigs.isEmpty()) {

                lstConfigDefaultDTO = lstConfigs.stream()
                        .map(temp -> {
                            return UtilBean.customReportConfigToCustomReportConfigsDTOByUser(temp);
                        })
                        .collect(Collectors.toList());

            }

        } catch (Exception ex) {
            log.error("Error al obtener las configuraciones del reporte personalizado por usuario.", ex);
            throw new ServiceException("Error al obtener las configuraciones del reporte personalizado por usuario." + ex.getMessage(), ex);
        }

        return lstConfigDefaultDTO;
    }

    @Override
    @Transactional
    public CustomReportConfigDTO saveConfig(CustomReportConfigDTO customReportConfigDTO) throws ServiceException{

        CustomReportConfigDTO savedConfig = new CustomReportConfigDTO();
        CustomReportConfig customReportConfig = new CustomReportConfig();

        try {

            //Get user by id
            User user=userRepository.findByIdAndActiveTrue(customReportConfigDTO.getUserId());

            //validate user
            if(user == null){
                throw new ServiceException("Este usuario no existe en la BD");
            }

            //validate columns
            if(customReportConfigDTO.getColumns() == null || customReportConfigDTO.getColumns().size() == 0){
                throw new ServiceException("No se seleccionó ninguna columna para guardar");
            }

            //Set custom report config
            customReportConfig.setName(customReportConfigDTO.getName());
            customReportConfig.setActive(Boolean.TRUE);
            customReportConfig.setUser(user);

            //Save custom report config
            customReportConfig = customReportConfigRepository.saveAndFlush(customReportConfig);

            List<CustomReportConfigColumn> columnsConfig = new ArrayList<CustomReportConfigColumn>();
            CustomReportConfigColumn column;

            //Save columns and link them to the configuration
            for(int contOrder = 1; contOrder <= customReportConfigDTO.getColumns().size(); contOrder++){
                column = new CustomReportConfigColumn();
                column.setOrderColumn(new Long(contOrder));
                column.setCustomReportColumn(
                        customReportColumnRepository.findOne(
                                customReportConfigDTO.getColumns().get(contOrder-1).getId()
                        )
                );
                column.setCustomReportConfig(customReportConfig);
                customReportConfigColumnRepository.saveAndFlush(column);
            }

            //Refresh custom report config repository
            entityManager.refresh(customReportConfig);

            //convert to dto
            savedConfig = UtilBean.customReportConfigToCustomReportConfigsDTOByUser(customReportConfig);

        } catch (Exception ex) {
            log.error("Error al guardar la configuracion.", ex);
            throw new ServiceException("Error al guardar la configuracion. " + ex.getMessage(), ex);
        }

        return savedConfig;
    }

    @Override
    @Transactional
    public CustomReportConfigDTO updateConfig(CustomReportConfigDTO customReportConfigDTO) throws ServiceException{

        CustomReportConfigDTO savedConfig = new CustomReportConfigDTO();
        CustomReportConfig customReportConfig;

        try {

            //Get user by id
            User user=userRepository.findByIdAndActiveTrue(customReportConfigDTO.getUserId());

            //validate user
            if(user == null){
                throw new ServiceException("Este usuario no existe en la BD");
            }

            //Get config by id
            customReportConfig=customReportConfigRepository.findOne(customReportConfigDTO.getId());

            //validate config
            if(customReportConfig == null){
                throw new ServiceException("Esta configuracion no existe");
            }

            //validate columns
            if(customReportConfigDTO.getColumns() == null || customReportConfigDTO.getColumns().size() == 0){
                throw new ServiceException("No se seleccionó ninguna columna para guardar");
            }

            //Set custom report config
            customReportConfig.setName(customReportConfigDTO.getName());
            customReportConfig.setActive(Boolean.TRUE);
            customReportConfig.setUser(user);

            //Save custom report config
            customReportConfig = customReportConfigRepository.saveAndFlush(customReportConfig);

            List<CustomReportConfigColumn> columnsConfig = new ArrayList<CustomReportConfigColumn>();
            CustomReportConfigColumn column;

            //delete columns
            if(customReportConfig.getCustomReportConfigColumn()!= null && !customReportConfig.getCustomReportConfigColumn().isEmpty()){
                for(CustomReportConfigColumn customReportConfigColumnTmp : customReportConfig.getCustomReportConfigColumn()){
                    log.info("Elimina column: " + customReportConfigColumnTmp.getId());
                    customReportConfigColumnRepository.delete(customReportConfigColumnTmp.getId());
                }
            }

            customReportConfig.setCustomReportConfigColumn(null);

            //Save columns and link them to the configuration
            for(int contOrder = 1; contOrder <= customReportConfigDTO.getColumns().size(); contOrder++){
                column = new CustomReportConfigColumn();
                column.setOrderColumn(new Long(contOrder));
                column.setCustomReportColumn(
                        customReportColumnRepository.findOne(
                                customReportConfigDTO.getColumns().get(contOrder-1).getId()
                        )
                );
                column.setCustomReportConfig(customReportConfig);
                customReportConfigColumnRepository.saveAndFlush(column);
            }

            //Refresh custom report config repository
            entityManager.refresh(customReportConfig);

            //convert to dto
            savedConfig = UtilBean.customReportConfigToCustomReportConfigsDTOByUser(customReportConfig);

        } catch (Exception ex) {
            log.error("Error al actualizar la configuracion.", ex);
            throw new ServiceException("Error al actualizar la configuracion. " + ex.getMessage(), ex);
        }

        return savedConfig;
    }

    @Override
    @Transactional
    public void deleteConfig(Long id) throws ServiceException{

        try{

            CustomReportConfig customReportConfig = customReportConfigRepository.findOne(id);

            //validate user
            if(customReportConfig == null){
                throw new ServiceException("Esta configuración no existe en la BD");
            }

            //Delete columns
            if(customReportConfig.getCustomReportConfigColumn() != null){
                for(CustomReportConfigColumn customReportConfigColumn : customReportConfig.getCustomReportConfigColumn()){
                    customReportConfigColumnRepository.delete(customReportConfigColumn);
                }
            }

            //Delete configuration
            customReportConfigRepository.delete(customReportConfig);

        } catch (Exception ex) {
            log.error("Error al eliminar la configuracion.", ex);
            throw new ServiceException("Error al eliminar la configuracion. " + ex.getMessage(), ex);
        }

    }

}

package com.mx.sivale.service.impl;

import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import com.google.api.services.gmail.model.Message;
import com.mx.sivale.model.BulkFileStatus;
import com.mx.sivale.model.User;
import com.mx.sivale.model.dto.SenderEmailDTO;
import com.mx.sivale.model.dto.UserDTO;
import com.mx.sivale.service.MailSender;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailParseException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.activation.DataSource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

@Component
public class MailSenderImpl implements MailSender {

    private Logger logger=Logger.getLogger(MailSenderImpl.class);

    private static final String DEFAULT_ENCODING = "utf-8";

    @Value("${spring.mail.cc}")
    public String MAIL_CC;

    @Value("${spring.mail.username}")
    public String MAIL_FROM;

    @Autowired
    private JavaMailSender javaMailService;

    @Autowired
    private Configuration freemarkerConfiguration;

    private void sendMail(String to, String[] cc, String subject, String text,ByteArrayOutputStream bos) {
        javaMailService.send(buildMessage(MAIL_FROM, to, cc, subject, text,bos));
    }

    private MimeMessage buildMessage(String from, String to, String[] cc, String subject, String text,ByteArrayOutputStream bos) {
        MimeMessage message = javaMailService.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(from);
            helper.setTo(to);
            if (cc != null && cc[0] != null && !cc[0].equals("")) {
                helper.setCc(cc);
            }
            helper.setSubject(subject);
            helper.setText(text, true);
            if(bos!=null){
                DataSource aAttachment = new ByteArrayDataSource(bos.toByteArray(),"application/vnd.ms-excel");
                helper.addAttachment("errores_carga_masiva_usuarios.xls",aAttachment);
            }
        } catch (MessagingException e) {
            throw new MailParseException(e);
        }
        return message;
    }

    private String generateContent(String templateName, Map context) throws MessagingException {
        try {
            Template template = freemarkerConfiguration.getTemplate(templateName, DEFAULT_ENCODING);
            return FreeMarkerTemplateUtils.processTemplateIntoString(template, context);
        } catch (Exception e) {
            logger.error("FreeMarker template doesn't exist", e);
            throw new MessagingException("FreeMarker template doesn't exist", e);
        }
    }

    @Async
    public void sendEmailNewAccount(User user, String password) {
        logger.info("send email new account to " + user.getEmail());
        try {
            Map context = new HashMap();
            context.put("name", user.getName());
            context.put("email", user.getEmail());
            context.put("password", password);
            String[] cc = MAIL_CC.split(",");
            sendMail(user.getEmail(), cc, "Nueva Cuenta", generateContent("new_user.html", context),null);
        } catch (final Exception e) {
            logger.error("The user was created successfully, however the email send out failed.", e);
        }

    }

    @Async
    public void sendEmailNewAccount(UserDTO userDTO) {
        logger.info("send email new account to " + userDTO.getEmail());
        try {
            Map context = new HashMap();
            context.put("name", userDTO.getName());
            context.put("email", userDTO.getEmail());
            context.put("password", userDTO.getPassword());
            String[] cc = MAIL_CC.split(",");
            sendMail(userDTO.getEmail(), cc, "Nueva Cuenta", generateContent("new_user.html", context),null);
        } catch (final Exception e) {
            logger.error("The user was created successfully, however the email send out failed.", e);
        }

    }

    public Message buildEmailFiscalData(String from, String to, String name, String rfc, String address, String comment, String userName) {
        try {
            Map context = new HashMap();

            context.put("name", name);
            context.put("rfc", rfc);
            context.put("address", address);
            context.put("comment", comment);
            context.put("userName", userName);

            String[] cc = MAIL_CC.split(",");
            MimeMessage mimeMessage = buildMessage(from, to, cc, "Datos Fiscales", generateContent("email_template.html", context),null);

            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            mimeMessage.writeTo(buffer);

            byte[] bytes = buffer.toByteArray();
            String encodedEmail = Base64.encodeBase64URLSafeString(bytes);
            Message message = new Message();
            message.setRaw(encodedEmail);

            return message;

        } catch (final Exception e) {
            logger.error("Email send out failed.", e);
            return null;
        }

    }

    public String buildHTMLEmailFiscalData(String from, String to, String name, String rfc, String address, String comment, String userName) {
        try {
            Map context = new HashMap();

            context.put("name", name);
            context.put("rfc", rfc);
            context.put("address", address);
            context.put("comment", comment);
            context.put("userName", userName);


            return generateContent("email_template.html", context);

        } catch (final Exception e) {
            logger.error("Email send out failed.", e);
            return null;
        }

    }

    @Async
    public void sendEmailApprovalProcess(SenderEmailDTO senderEmailDTO) {
        if(senderEmailDTO.getEmailBussiness() != null) {
            logger.info("send email approval process to " + senderEmailDTO.getNameBussiness() + " - " + senderEmailDTO.getEmailBussiness());
            try {
                Map<String,Object> context = new HashMap();
                context.put("name", senderEmailDTO.getNameBussiness());
                context.put("comment", senderEmailDTO.getCommentBussiness());
                context.put("advance",senderEmailDTO.getAdvanceRequired());
                context.put("type",senderEmailDTO.getType());
                String[] cc = MAIL_CC.split(",");
                sendMail(senderEmailDTO.getEmailBussiness(), cc, "Proceso de Aprobación", generateContent("notify_template_v3.html", context),null);
            } catch (final Exception e) {
                logger.error("Email send out failed.", e);
            }
        } else {
            logger.error("Email send out failed. Null email.");
        }
    }

    @Async
    public void sendEmailApprovalProcessEvents(SenderEmailDTO senderEmailDTO) {
        if(senderEmailDTO.getEmailBussiness() != null) {
            logger.info("send email approval process to " + senderEmailDTO.getNameBussiness() + " - " + senderEmailDTO.getEmailBussiness());
            try {
                Map<String,Object> context = new HashMap();
                context.put("name", senderEmailDTO.getNameBussiness());
                context.put("comment", senderEmailDTO.getCommentBussiness());
                context.put("type",senderEmailDTO.getType());
                context.put("event",senderEmailDTO.getEvent());
                String[] cc = MAIL_CC.split(",");
                sendMail(senderEmailDTO.getEmailBussiness(), cc, "Proceso de Aprobación", generateContent("notify_template_v4.html", context),null);
            } catch (final Exception e) {
                logger.error("Email send out failed.", e);
            }
        } else {
            logger.error("Email send out failed. Null email.");
        }
    }

    @Async
    public void sendEmailDummy(SenderEmailDTO senderEmailDTO) {
        if(senderEmailDTO.getEmailBussiness() != null) {
            logger.info("send sendEmailDummy to " + senderEmailDTO.getNameBussiness() + " - " + senderEmailDTO.getEmailBussiness());
            try {
                Map<String,Object> context = new HashMap();
                context.put("name", senderEmailDTO.getNameBussiness());
                context.put("comment", senderEmailDTO.getCommentBussiness());
                context.put("advance",senderEmailDTO.getAdvanceRequired());
                context.put("type",senderEmailDTO.getType());
                String[] cc = MAIL_CC.split(",");
                sendMail(senderEmailDTO.getEmailBussiness(), cc, "Proceso de Aprobación Dummy", generateContent("notify_template_v3.html", context),null);
            } catch (final Exception e) {
                logger.error("Email send out failed.", e);
            }
        } else {
            logger.error("Email send out failed. Null email.");
        }
    }
    @Async
    public void sendEmailBulkFileFinished(String email, BulkFileStatus bulkFile,ByteArrayOutputStream bos) {
        logger.info("send finish bulk file " + email);
        try {
            Map context = new HashMap();
            context.put("bulk",bulkFile);
            String [] cc = {"jvicuna@palo-it.com"};
            sendMail(email,cc , "Proceso de carga finalizado", generateContent("finish_bulk_file.html", context),bos);
        } catch (final Exception e) {
            logger.error("Fallo al enviar notificación por correo de carga masiva de usuarios", e);
        }

    }

}

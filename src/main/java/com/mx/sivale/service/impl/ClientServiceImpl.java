package com.mx.sivale.service.impl;

import com.mx.sivale.model.Client;
import com.mx.sivale.model.dto.ClientDTO;
import com.mx.sivale.repository.ClientRepository;
import com.mx.sivale.service.ClientService;
import com.mx.sivale.service.exception.ServiceException;
import com.mx.sivale.service.request.ClaimsResolver;
import com.mx.sivale.service.util.UtilBean;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public class ClientServiceImpl implements ClientService{

	private static final Logger log = Logger.getLogger(ClientServiceImpl.class);
	
	@Autowired
	private ClientRepository clientRepository;
	
	@Autowired
    private ClaimsResolver claimsResolver;
	
	@Override
	public ClientDTO saveLogoClient(MultipartFile file, String dateCreated) throws Exception {
		Client client =  clientRepository.findOne(Long.parseLong(claimsResolver.client()));
		try {
			if (!file.isEmpty()) {
				byte[] logo = file.getBytes();
				client.setLogo(logo);
				return UtilBean.clientModelToClientDTO(clientRepository.save(client));
			} else{
				log.warn("Fallo RFC :::>>>> ");
				throw new ServiceException("No se guardo logo de la empresa");
			}

		} catch (Exception e) {
			log.warn("Fallo ARCHIVO :::>>>> ");
			throw new ServiceException(e.getMessage());
		}
	}

	public Client getCurrentClient() throws Exception {
		Long clientId = Long.parseLong(claimsResolver.client());
		Client client = clientRepository.findOne(clientId);
		if(client == null){
			throw new Exception("Client " + clientId + "not found in mercurio database.");
		} else {
//			log.info("Client: " + client.getId() + " - " + client.getName());
			return client;
		}
	}

	public Long getCurrentClientId() throws Exception {
        return Long.parseLong(claimsResolver.client());
	}

	@Override
	public Client getClientById(Long clientId) {
		return clientRepository.findOne(clientId);
	}

	@Override
	public List<Client> getAll(){
		return clientRepository.findAll();
	}

}

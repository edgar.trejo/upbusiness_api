package com.mx.sivale.service.impl;

import com.mx.sivale.model.*;
import com.mx.sivale.model.dto.*;
import com.mx.sivale.report.Report;
import com.mx.sivale.repository.*;
import com.mx.sivale.service.*;
import com.mx.sivale.service.exception.ServiceException;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.poi.hssf.util.HSSFColor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class ReportServiceImpl implements ReportService {

    private org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ReportServiceImpl.class);

    @Autowired
    private EventService eventService;

    @Autowired
    private SpendingService spendingService;

    @Autowired
    private SpendingTypeService spendingTypeService;

    @Autowired
    private InvoiceService invoiceService;

    @Autowired
    private CostCenterRepository costCenterRepository;

    @Autowired
    private SpendingTypeRepository spendingTypeRepository;

    @Autowired
    private ApprovalStatusRepository approvalStatusRepository;

    @Autowired
    private CustomReportTypeColumnRepository customReportTypeColumnRepository;

    @Autowired
    private CustomReportColumnRepository customReportColumnRepository;

    public static final Long TYPE_TRAVELER = 1L;
    public static final Long TYPE_EVENT = 2L;
    public static final Long TYPE_SPENDING = 3L;
    public static final Long TYPE_SPENDING_TYPE = 4L;
    public static final Long TYPE_INVOICE = 5L;

    private static final String UNKNOWN_INFO = "No aplica";
    private static final String NOT_DISPERSED = "No dispersado";
    private static final String UNAVAILABLE_INFO = "No disponible";

    public Map<String, Object> reportEvents(long from, long to, long clientId) throws Exception {

        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        Map<String, Object> model = new HashMap<>();

        Page<EventReportDTO> eventReports = eventService.report(from, to, null, clientId);

        Report report = new Report();

        //Sheet Name
        model.put("sheetname", "Informes de gasto");

        //Headers
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Nombre usuario");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Número de empleado");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Puesto");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Código de puesto");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Área");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Código de área");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Grupo o proyecto");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Código de grupo");

        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Nombre del informe");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("ID Informe");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Fecha inicio");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Fecha fin");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Fecha de aprobado");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Fecha de pagado");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Nº Gastos");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Monto total");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Total comprobado");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Status");
        model.put("headers", report.getHeaders());
        model.put("headerBackgrounds", report.getHeaderBackgrounds());

        //Rows
        for (EventReportDTO reportDTO : eventReports) {
            List<String> row = new ArrayList<String>();
            row.add(reportDTO.getUserName());
            row.add(reportDTO.getEmployeeNumber());
            row.add(reportDTO.getJobPosition());
            row.add(reportDTO.getJobCode());
            row.add(reportDTO.getArea());
            row.add(reportDTO.getAreaCode());
            row.add(reportDTO.getGroup());
            row.add(reportDTO.getGroupCode());
            row.add(reportDTO.getReportName());
            row.add(reportDTO.getReportId());
            row.add(formatter.format(reportDTO.getStartDate()));
            row.add(formatter.format(reportDTO.getEndDate()));
            row.add(reportDTO.getApproveDate()!=null ? formatter.format(reportDTO.getApproveDate()):"Pendiente de aprobar");
            row.add(reportDTO.getPaidDate()!=null ? formatter.format(reportDTO.getPaidDate()):"Pendiente de pagar");
            row.add(reportDTO.getNumberOfSpendings());
            row.add(reportDTO.getTotalAmount());
            row.add(reportDTO.getTotalChecked());
            row.add(reportDTO.getStatus());
            report.addRow(row);
        }

        model.put("rows", report.getRows());

        return model;
    }

    public Map<String, Object> reportSpendings(long from, long to, long clientId) throws Exception {

        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        Map<String, Object> model = new HashMap<>();

        Page<SpendingReportDTO> spendingList = spendingService.report(from, to, null, clientId);

        Report report = new Report();

        //Sheet Name
        model.put("sheetname", "Gastos");

        //Headers
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Nombre usuario");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Número de empleado");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Puesto");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Código de puesto");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Área");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Código de área");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Grupo o proyecto");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Código de grupo");

        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Nombre del gasto");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("ID Gasto");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Fecha");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Fecha de Aprobación");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Fecha de Pagado");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Monto gasto");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
//        report.addHeader("SubTotal del gasto");
//        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
//        report.addHeader("Impuesto del gasto");
//        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
//        report.addHeader("Total del gasto");
//        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Monto trx");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Informe al que pertenece");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("ID Informe al que pertenece");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Medio de pago");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Tarjeta Nº");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Autorización");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Comercio");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("UUID Factura");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Emisor Factura");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("RFC Factura");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("IVA");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("IEPS");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("ID ticket");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("ID Factura");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Status");
        model.put("headers", report.getHeaders());
        model.put("headerBackgrounds", report.getHeaderBackgrounds());

        //Rows
        for (SpendingReportDTO reportDTO : spendingList) {
            List<String> row = new ArrayList<String>();
            row.add(reportDTO.getUserName());
            row.add(reportDTO.getEmployeeNumber());
            row.add(reportDTO.getJobPosition());
            row.add(reportDTO.getJobCode());
            row.add(reportDTO.getArea());
            row.add(reportDTO.getAreaCode());
            row.add(reportDTO.getGroup());
            row.add(reportDTO.getGroupCode());

            row.add(reportDTO.getSpendingName());
            row.add(reportDTO.getSpendingId());
            row.add(formatter.format(reportDTO.getSpendingDate()));
            row.add(reportDTO.getApproveDate()!=null?formatter.format(reportDTO.getApproveDate()): UNKNOWN_INFO);
            row.add(reportDTO.getPaidDate()!=null?formatter.format(reportDTO.getPaidDate()): UNKNOWN_INFO);
            row.add(reportDTO.getSpendingTypeTotal().isEmpty() ? reportDTO.getSpendingAmount() : reportDTO.getSpendingTypeTotal());
            row.add(reportDTO.getTrxAmount());
            row.add(reportDTO.getReportName());
            row.add(reportDTO.getReportId() == null || reportDTO.getReportId().isEmpty() ? UNKNOWN_INFO : reportDTO.getReportId());
            row.add(reportDTO.getPaymentMethod());
            row.add(reportDTO.getNumberCard() == null || reportDTO.getAuthorizationNumber().isEmpty() ? UNKNOWN_INFO : reportDTO.getNumberCard().isEmpty() ? UNAVAILABLE_INFO : reportDTO.getNumberCard());
            row.add(reportDTO.getAuthorizationNumber() == null || reportDTO.getAuthorizationNumber().isEmpty() ? UNKNOWN_INFO : reportDTO.getAuthorizationNumber());
            row.add(reportDTO.getCommerce() == null || reportDTO.getCommerce().isEmpty() ? UNKNOWN_INFO : reportDTO.getCommerce());
            row.add(reportDTO.getInvoiceUuid() == null || reportDTO.getInvoiceUuid().isEmpty() ? UNKNOWN_INFO : reportDTO.getInvoiceUuid());
            row.add(reportDTO.getInvoiceEstablishment() == null || reportDTO.getInvoiceEstablishment().isEmpty() ? UNKNOWN_INFO : reportDTO.getInvoiceEstablishment());
            row.add(reportDTO.getInvoiceRfc() == null || reportDTO.getInvoiceRfc().isEmpty() ? UNKNOWN_INFO : reportDTO.getInvoiceRfc());
            row.add(reportDTO.getIva() == null || reportDTO.getIva().isEmpty() ? UNKNOWN_INFO : reportDTO.getIva());
            row.add(reportDTO.getIeps() == null || reportDTO.getIeps().isEmpty() ? UNKNOWN_INFO : reportDTO.getIeps());
            row.add(reportDTO.getTicketEvidence() == null || reportDTO.getTicketEvidence().isEmpty()? UNKNOWN_INFO : reportDTO.getTicketEvidence());
            row.add(reportDTO.getInvoiceEvidence() == null || reportDTO.getInvoiceEvidence().isEmpty() ? UNKNOWN_INFO : reportDTO.getInvoiceEvidence());
            row.add(reportDTO.getStatus());

            report.addRow(row);
        }

        model.put("rows", report.getRows());

        return model;
    }
    
    public Map<String, Object> reportSpendingTypes(long from, long to, long clientId) throws Exception {

        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        Map<String, Object> model = new HashMap<>();

        Page<SpendingTypeReportDTO> spendingTypeList = spendingTypeService.report(from, to, null, clientId);

        Report report = new Report();

        //Sheet Name
        model.put("sheetname", "Tipos de Gasto");

        //Headers
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Nombre usuario");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Número de empleado");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Puesto");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Código de puesto");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Área");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Código de área");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Grupo o proyecto");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Código de grupo");

        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("ID Gasto");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Nombre Gasto");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Fecha");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Fecha de Aprobación");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Fecha de Pagado");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Tipo de gasto");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Concepto");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Código tipo de gasto");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("SubTotal tipo de gasto");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("IVA");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("TUA");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("IEPS");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("ISH");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Total tipo de gasto");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);

        report.addHeader("Informe al que pertenece");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("ID Informe al que pertenece");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Medio de pago");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Tarjeta");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Comercio");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("UUID Factura");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Emisor Factura");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("RFC Factura");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Status");
        model.put("headers", report.getHeaders());
        model.put("headerBackgrounds", report.getHeaderBackgrounds());

        //Rows
        for (SpendingTypeReportDTO reportDTO : spendingTypeList) {
            List<String> row = new ArrayList<String>();
            row.add(reportDTO.getUserName());
            row.add(reportDTO.getEmployeeNumber());
            row.add(reportDTO.getJobPosition());
            row.add(reportDTO.getJobCode());
            row.add(reportDTO.getArea());
            row.add(reportDTO.getAreaCode());
            row.add(reportDTO.getGroup());
            row.add(reportDTO.getGroupCode());

            row.add(reportDTO.getSpendingId());
            row.add(reportDTO.getSpendingName());
            row.add(formatter.format(reportDTO.getSpendingDate()));
            row.add(reportDTO.getApproveDate()!=null?formatter.format(reportDTO.getApproveDate()): UNKNOWN_INFO);
            row.add(reportDTO.getPaidDate()!=null?formatter.format(reportDTO.getPaidDate()): UNKNOWN_INFO);
            row.add(reportDTO.getSpendingType());
            row.add(reportDTO.getConcept());
            row.add(reportDTO.getSpendingTypeCode());
            row.add(reportDTO.getSpendingTypeSubTotal().isEmpty() ? UNKNOWN_INFO : reportDTO.getSpendingTypeSubTotal());
            row.add(reportDTO.getIva()!=null && reportDTO.getIva().isEmpty()? UNKNOWN_INFO : reportDTO.getIva());
            row.add(reportDTO.getTua()!=null && reportDTO.getTua().isEmpty()? UNKNOWN_INFO : reportDTO.getTua());
            row.add(reportDTO.getIeps()!=null && reportDTO.getIeps().isEmpty()? UNKNOWN_INFO : reportDTO.getIeps());
            row.add(reportDTO.getIsh()!=null && reportDTO.getIsh().isEmpty()? UNKNOWN_INFO : reportDTO.getIsh());
            row.add(reportDTO.getSpendingTypeTotal().isEmpty() ? reportDTO.getSpendingTypeAmount() : reportDTO.getSpendingTypeTotal());

            row.add(reportDTO.getReportName());
            row.add(reportDTO.getReportId() == null || reportDTO.getReportId().isEmpty() ? UNKNOWN_INFO : reportDTO.getReportId());
            row.add(reportDTO.getPaymentMethod());
            row.add(reportDTO.getNumberCard() == null || reportDTO.getNumberCard().isEmpty() ? UNAVAILABLE_INFO : reportDTO.getNumberCard());
            row.add(reportDTO.getCommerce() == null || reportDTO.getCommerce().isEmpty() ? UNKNOWN_INFO : reportDTO.getCommerce());
            row.add(reportDTO.getInvoiceUuid());
            row.add(reportDTO.getInvoiceEstablishment() == null || reportDTO.getInvoiceEstablishment().isEmpty() ? UNKNOWN_INFO : reportDTO.getInvoiceEstablishment());
            row.add(reportDTO.getInvoiceRfc());
            row.add(reportDTO.getStatus());

            report.addRow(row);
        }

        model.put("rows", report.getRows());

        return model;
    }

    public Map<String, Object> reportAdvances(List<AdvanceReportDTO> advanceList) throws Exception {

        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        Map<String, Object> model = new HashMap<>();

        Report report = new Report();

        //Sheet Name
        model.put("sheetname", "Anticipos");

        //Headers
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Nombre usuario");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Número de empleado");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Puesto");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Código de puesto");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Área");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Código de área");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Grupo o proyecto");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Código de grupo");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Fecha solicitud");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Fecha dispersión");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Hora dispersión");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("ID anticipo");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Nombre anticipo");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Descripción");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Producto");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Cuenta concentradora");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Saldo en cuenta");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Tarjeta");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Saldo inicial en tarjeta");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Monto solicitado");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Saldo final en tarjeta");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Status");
        model.put("headers", report.getHeaders());
        model.put("headerBackgrounds", report.getHeaderBackgrounds());

        //Rows
        for (AdvanceReportDTO reportDTO : advanceList) {
            List<String> row = new ArrayList<String>();
            row.add(reportDTO.getUserName());
            row.add(reportDTO.getEmployeeNumber());
            row.add(reportDTO.getJobPosition());
            row.add(reportDTO.getJobCode());
            row.add(reportDTO.getArea());
            row.add(reportDTO.getAreaCode());
            row.add(reportDTO.getGroup());
            row.add(reportDTO.getGroupCode());
            row.add(formatter.format(reportDTO.getAdvanceDate() == null ? new Date() : reportDTO.getAdvanceDate()));
            row.add(reportDTO.getDispersionAdvanceDate() == null ? NOT_DISPERSED : formatter.format(reportDTO.getDispersionAdvanceDate()));
            row.add(reportDTO.getDispersionAdvanceTime());
            row.add(reportDTO.getAdvanceId());
            row.add(reportDTO.getAdvanceName());
            row.add(reportDTO.getDescription());
            row.add(reportDTO.getProduct() == null || reportDTO.getProduct().isEmpty() ? UNAVAILABLE_INFO : reportDTO.getProduct());
            row.add(reportDTO.getOriginCard() == null || reportDTO.getOriginCard().isEmpty() ? UNAVAILABLE_INFO : reportDTO.getOriginCard());
            row.add(reportDTO.getDispersionAdvanceDate() == null ? NOT_DISPERSED : String.valueOf(reportDTO.getOriginBalance()));
            row.add(reportDTO.getDestinationCard());
            row.add(reportDTO.getDispersionAdvanceDate() == null ? NOT_DISPERSED : String.valueOf(reportDTO.getDestinationInitialBalance()));
            row.add(reportDTO.getRequestedAmount());
            row.add(reportDTO.getDispersionAdvanceDate() == null ? NOT_DISPERSED : String.valueOf(reportDTO.getDestinationFinalBalance()));
            row.add(reportDTO.getStatus());

            report.addRow(row);
        }

        model.put("rows", report.getRows());

        return model;
    }

    public Map<String, Object> reportTransfers(List<AdvanceReportDTO> advanceList) throws Exception {

        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        Map<String, Object> model = new HashMap<>();

        Report report = new Report();

        //Sheet Name
        model.put("sheetname", "Asignaciones");

        //Headers
        report.addHeader("Nombre usuario");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Número de empleado");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Puesto");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Código de puesto");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Área");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Código de área");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Grupo o proyecto");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Código de grupo");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Fecha dispersión");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Hora dispersión");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("ID asignación");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Tipo de asignación");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Producto");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Cuenta concentradora");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Saldo en cuenta");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Tarjeta");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Saldo en tarjeta");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Monto");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Saldo final en tarjeta");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        model.put("headers", report.getHeaders());
        model.put("headerBackgrounds", report.getHeaderBackgrounds());

        //Rows
        for (AdvanceReportDTO reportDTO : advanceList) {
            List<String> row = new ArrayList<String>();
            row.add(reportDTO.getUserName());
            row.add(reportDTO.getEmployeeNumber());
            row.add(reportDTO.getJobPosition());
            row.add(reportDTO.getJobCode());
            row.add(reportDTO.getArea());
            row.add(reportDTO.getAreaCode());
            row.add(reportDTO.getGroup());
            row.add(reportDTO.getGroupCode());
            row.add(formatter.format(reportDTO.getDispersionAdvanceDate() == null ? new Date() : reportDTO.getDispersionAdvanceDate()));
            row.add(reportDTO.getDispersionAdvanceTime());
            row.add(reportDTO.getAdvanceId());
            row.add(reportDTO.getAdvanceName());
            row.add(reportDTO.getDescription() == null || reportDTO.getDescription().isEmpty() ? UNAVAILABLE_INFO : reportDTO.getDescription());
            row.add(reportDTO.getOriginCard() == null || reportDTO.getOriginCard().isEmpty() ? UNAVAILABLE_INFO : reportDTO.getOriginCard());
            row.add(String.valueOf(reportDTO.getOriginBalance()));
            row.add(reportDTO.getDestinationCard());
            row.add(String.valueOf(reportDTO.getDestinationInitialBalance()));
            row.add(reportDTO.getRequestedAmount());
            row.add(String.valueOf(reportDTO.getDestinationFinalBalance()));

            report.addRow(row);
        }

        model.put("rows", report.getRows());

        return model;
    }

    @Override
    public Map<String, Object> reportTransactions(List<Transaction> transactions) throws Exception {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        Map<String, Object> model = new HashMap<>();

        Report report = new Report();

        //Sheet Name
        model.put("sheetname", "Transacciones");

        //Headers
        report.addHeader("Nombre usuario");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Número de empleado");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Puesto");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Código de puesto");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Área");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Código de área");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Grupo o proyecto");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Código de grupo");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);

        report.addHeader("Nº Cuenta");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Tarjeta");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("IUT");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Fecha");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Hora");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Movimiento");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Producto");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Consumo Neto");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Importe");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Consumo");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Ieps");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("IVA");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Litros");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Nº Autorización");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("RFC Comercio");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Nº Afiliación");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Razón Social");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Giro");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        model.put("headers", report.getHeaders());
        model.put("headerBackgrounds", report.getHeaderBackgrounds());

        //Rows
        for (Transaction t : transactions) {
            List<String> row = new ArrayList<String>();
            row.add(t.getUser() != null ? t.getUser().getCompleteName() : "");
            row.add(t.getUser() != null ? t.getUser().getNumberEmployee() : "");
            row.add(t.getUserClient() != null && t.getUserClient().getJobPosition() != null ? t.getUserClient().getJobPosition().getName() : "");
            row.add(t.getUserClient() != null && t.getUserClient().getJobPosition() != null ? t.getUserClient().getJobPosition().getCode() : "");
            row.add(t.getUserClient() != null && t.getUserClient().getCostCenter() != null ? t.getUserClient().getCostCenter().getName() : "");
            row.add(t.getUserClient() != null && t.getUserClient().getCostCenter() != null ? t.getUserClient().getCostCenter().getCode() : "");
            row.add(t.getUser() != null ? t.getUser().getGroup() : "");
            row.add(t.getUser() != null ? t.getUser().getGroupCode() : "");

            row.add(t.getCuenta() != null ? t.getCuenta() : "");
            row.add(t.getTarjeta() != null ? t.getTarjeta() : "");
            row.add(t.getIut());
            row.add(t.getFecha());
            row.add(t.getHora());
            row.add(t.getMovimiento());
            row.add(t.getProducto());
            row.add(String.valueOf(t.getConsumoNeto()));
            row.add(String.valueOf(t.getImporte()));
            row.add(String.valueOf(t.getConsumo()));
            row.add(String.valueOf(t.getIeps()));
            row.add(String.valueOf(t.getIva()));
            row.add(String.valueOf(t.getLitros()));
            row.add(t.getNumAutorizacion());
            row.add(t.getRfcComercio());
            row.add(t.getAfiliacion());
            row.add(t.getNombreComercio());
            row.add(t.getGiro());

            report.addRow(row);
        }

        model.put("rows", report.getRows());

        return model;
    }

    @Override
    public Map<String, Object> reportInvoice(List<InvoiceReportDTO> invoices) throws Exception {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        Map<String, Object> model = new HashMap<>();

        Report report = new Report();

        //Sheet Name
        model.put("sheetname", "Facturas");

        //Headers
        report.addHeader("Nombre usuario");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Número de empleado");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Puesto");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Código de puesto");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Área");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Código de área");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Grupo o proyecto");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
        report.addHeader("Código de grupo");
        report.addHeaderBackgorund(HSSFColor.ORANGE.index);

        report.addHeader("RFC Emisor");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Nombre Emisor");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("RFC Receptor");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Nombre Receptor");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Fecha de emisión");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("UUID");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Folio");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Subtotal");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("IVA");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("IEPS");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("ISH");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("TUA");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Total");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);
        report.addHeader("Moneda");
        report.addHeaderBackgorund(HSSFColor.ROYAL_BLUE.index);

        model.put("headers", report.getHeaders());
        model.put("headerBackgrounds", report.getHeaderBackgrounds());

        //Rows
        for (InvoiceReportDTO i : invoices) {
            List<String> row = new ArrayList<String>();
            row.add(i.getUserName());
            row.add(i.getEmployeeNumber());
            row.add(i.getJobPosition());
            row.add(i.getJobPositionId());
            row.add(i.getCostCenter());
            row.add(i.getCostCenterId());
            row.add(i.getTeam());
            row.add(i.getTeamId());

            row.add(i.getTransmitterRFC());
            row.add(i.getTransmitterName());
            row.add(i.getReceiverRFC());
            row.add(i.getReceiverName());
            row.add(i.getDateInvoice());
            row.add(i.getUuid());
            row.add(i.getFolio());
            row.add(i.getSubtotal());
            row.add(i.getIva());
            row.add(i.getIeps());
            row.add(i.getIsh());
            row.add(i.getTua());
            row.add(i.getTotal());
            row.add(i.getCurrency());

            report.addRow(row);
        }

        model.put("rows", report.getRows());

        return model;
    }

    @Override
    public Map<String, Object> reportCustomReport(Long from, Long to, Long costCenterId,
                                           Long spendingTypeId, Long approvalStatusId,
                                           String columnsOrder,long clientId) throws Exception{

        Map<String, Object> model = new HashMap<>();

        Report report = new Report();

        List<String> columnsList = Arrays.stream(columnsOrder.split(",")).collect(Collectors.toList());
        List<CustomReportTypeColumn> lstTypeColumn = new ArrayList<CustomReportTypeColumn>();
        Page<CustomReportDTO> customReportDTOList = null;
        List<LinkedHashMap> reportResult = new ArrayList<LinkedHashMap>();
        CostCenter costCenter = null;
        SpendingType spendingType = null;
        ApprovalStatus approvalStatus = null;
        Boolean onlyInvoice = false;

        try {

            //Validate and get cost center
            if(costCenterId != null){
                costCenter = costCenterRepository.findOne(costCenterId);
                if(costCenter == null){
                    throw new ServiceException("El área no existe en la BD");
                }
            }

            //Validate and get spending Type
            if(spendingTypeId != null){
                spendingType = spendingTypeRepository.findOne(spendingTypeId);
                if(spendingType == null){
                    throw new ServiceException("El tipo de gasto no existe en la BD");
                }
            }

            //Validate and get approval status
            if(approvalStatusId != null){
                approvalStatus = approvalStatusRepository.findOne(approvalStatusId);
                if(approvalStatus == null){
                    throw new ServiceException("El estatus no existe en la BD");
                }
            }

            //Get type columns
            lstTypeColumn = customReportTypeColumnRepository.findAllByColumnName(columnsList);
            if (lstTypeColumn == null || lstTypeColumn.isEmpty()) {
                throw new ServiceException("Las columnas seleccionadas no estan correctamente mapeadas");
            }

            //Variables for determinate which data to get
            Optional<CustomReportTypeColumn> typeColumnInvoice = null;
            if(lstTypeColumn != null && lstTypeColumn.size() == 1 ){
                if (lstTypeColumn.get(0).getId().equals(TYPE_INVOICE)){
                    onlyInvoice = true;
                }
            }
            Optional<CustomReportTypeColumn> typeColumnSpendingType = lstTypeColumn.stream()
                    .filter(t -> t.getId().equals(TYPE_SPENDING_TYPE)).findFirst();

            Optional<CustomReportTypeColumn> typeColumnSpendingOrInvoice = lstTypeColumn.stream()
                    .filter(t -> t.getId().equals(TYPE_SPENDING) || t.getId().equals(TYPE_INVOICE)).findFirst();

            Optional<CustomReportTypeColumn> typeColumnTravelerOrEvent = lstTypeColumn.stream()
                    .filter(t -> t.getId().equals(TYPE_TRAVELER) || t.getId().equals(TYPE_EVENT)).findFirst();

            //Cases with information to get
            if (onlyInvoice) {
                customReportDTOList = invoiceService.customReport(
                        from, to, costCenterId, spendingType, approvalStatus, null, clientId);
            } else if (typeColumnSpendingType.isPresent()) {
                customReportDTOList = spendingTypeService.customReport(
                        from, to, costCenterId, spendingType, approvalStatus, null, clientId);
            } else if (typeColumnSpendingOrInvoice.isPresent()) {
                customReportDTOList = spendingService.customReport(
                        from, to, costCenterId, spendingType, approvalStatus, null, clientId);
            } else if (typeColumnTravelerOrEvent.isPresent()) {
                customReportDTOList = eventService.customReport(
                        from, to, costCenterId, spendingType, approvalStatus, null, clientId);
            }

            //Sheet Name
            model.put("sheetname", "Reporte Personalizado");

            //Headers
            columnsList.forEach(
                    column -> {
                        CustomReportColumn c = customReportColumnRepository.findByNameColumn(column);
                        report.addHeader(c.getDescColumn());
                        report.addHeaderBackgorund(HSSFColor.ORANGE.index);
                    }
            );

            //Rows
            if(customReportDTOList != null){
                customReportDTOList.forEach(
                    t -> {
                        List<String> row = new ArrayList<String>();

                        columnsList.forEach(
                                column -> {
                                    try {
                                        Object obj = PropertyUtils.getProperty(t, column);
                                        row.add(obj==null?"":obj.toString());
                                    } catch (Exception e) {

                                    }
                                }
                        );
                        report.addRow(row);
                    });
            }

            model.put("rows", report.getRows());

            model.put("headers", report.getHeaders());
            model.put("headerBackgrounds", report.getHeaderBackgrounds());

        } catch (Exception ex) {
            throw new ServiceException("Error al generar el XLS para el Reporte Personalizado: " + ex.getMessage(), ex);
        }

        return model;

    }

    @Override
    public Map<String, Object> reportCustomReportCsv(Long from, Long to, Long costCenterId,
                                                  Long spendingTypeId, Long approvalStatusId,
                                                  String columnsOrder,long clientId) throws Exception{

        Map<String, Object> model = new HashMap<>();

        Report report = new Report();

        List<String> columnsList = Arrays.stream(columnsOrder.split(",")).collect(Collectors.toList());
        List<CustomReportTypeColumn> lstTypeColumn = new ArrayList<CustomReportTypeColumn>();
        Page<CustomReportDTO> customReportDTOList = null;
        List<LinkedHashMap> reportResult = new ArrayList<LinkedHashMap>();
        CostCenter costCenter = null;
        SpendingType spendingType = null;
        ApprovalStatus approvalStatus = null;
        Boolean onlyInvoice = false;

        try {

            //Validate and get cost center
            if(costCenterId != null){
                costCenter = costCenterRepository.findOne(costCenterId);
                if(costCenter == null){
                    throw new ServiceException("El área no existe en la BD");
                }
            }

            //Validate and get spending Type
            if(spendingTypeId != null){
                spendingType = spendingTypeRepository.findOne(spendingTypeId);
                if(spendingType == null){
                    throw new ServiceException("El tipo de gasto no existe en la BD");
                }
            }

            //Validate and get approval status
            if(approvalStatusId != null){
                approvalStatus = approvalStatusRepository.findOne(approvalStatusId);
                if(approvalStatus == null){
                    throw new ServiceException("El estatus no existe en la BD");
                }
            }

            //Get type columns
            lstTypeColumn = customReportTypeColumnRepository.findAllByColumnName(columnsList);
            if (lstTypeColumn == null || lstTypeColumn.isEmpty()) {
                throw new ServiceException("Las columnas seleccionadas no estan correctamente mapeadas");
            }

            //Variables for determinate which data to get
            Optional<CustomReportTypeColumn> typeColumnInvoice = null;
            if(lstTypeColumn != null && lstTypeColumn.size() == 1 ){
                if (lstTypeColumn.get(0).getId().equals(TYPE_INVOICE)){
                    onlyInvoice = true;
                }
            }
            Optional<CustomReportTypeColumn> typeColumnSpendingType = lstTypeColumn.stream()
                    .filter(t -> t.getId().equals(TYPE_SPENDING_TYPE)).findFirst();

            Optional<CustomReportTypeColumn> typeColumnSpendingOrInvoice = lstTypeColumn.stream()
                    .filter(t -> t.getId().equals(TYPE_SPENDING) || t.getId().equals(TYPE_INVOICE)).findFirst();

            Optional<CustomReportTypeColumn> typeColumnTravelerOrEvent = lstTypeColumn.stream()
                    .filter(t -> t.getId().equals(TYPE_TRAVELER) || t.getId().equals(TYPE_EVENT)).findFirst();

            //Cases with information to get
            if (onlyInvoice) {
                customReportDTOList = invoiceService.customReport(
                        from, to, costCenterId, spendingType, approvalStatus, null, clientId);
            } else if (typeColumnSpendingType.isPresent()) {
                customReportDTOList = spendingTypeService.customReport(
                        from, to, costCenterId, spendingType, approvalStatus, null, clientId);
            } else if (typeColumnSpendingOrInvoice.isPresent()) {
                customReportDTOList = spendingService.customReport(
                        from, to, costCenterId, spendingType, approvalStatus, null, clientId);
            } else if (typeColumnTravelerOrEvent.isPresent()) {
                customReportDTOList = eventService.customReport(
                        from, to, costCenterId, spendingType, approvalStatus, null, clientId);
            }

            //Headers
            columnsList.forEach(
                    column -> {
                        CustomReportColumn c = customReportColumnRepository.findByNameColumn(column);
                        report.addHeader(c.getNameColumn());
                        report.addDescHeader(c.getDescColumn());
                    }
            );

            //Rows
            if(customReportDTOList != null){
                customReportDTOList.forEach(
                    t -> {
                        List<String> row = new ArrayList<String>();
                        report.addObjectRow(t);
                    });
            }

            model.put("rows", report.getObjectRows());
            model.put("headers", report.getHeaders());
            model.put("descHeaders", report.getDescHeaders());

        } catch (Exception ex) {
            throw new ServiceException("Error al generar el CSV para el Reporte Personalizado: " + ex.getMessage(), ex);
        }

        return model;

    }

}

package com.mx.sivale.service.impl;

import com.mx.sivale.config.WsdlConfig;
import com.mx.sivale.service.WsdlConsumeService;
import com.mx.sivale.service.exception.ServiceException;
import org.apache.log4j.Logger;
import org.mx.wsdl.sat.tempuri.ConsultaCFDIService;
import org.mx.wsdl.sat.tempuri.IConsultaCFDIService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ws.corporativogpv.mx.servicios.RequestSOAPHandler;
import ws.corporativogpv.mx.servicios.SI01CORELegadosReqSyncOutService;
import ws.corporativogpv.mx.servicios.SI01CORELegadosReqSyncOutType;
import ws.sivale.com.mx.exposition.servicios.apps.RequestSOAPHeaderHandler;
import ws.sivale.com.mx.exposition.servicios.apps.ServiciosApps;
import ws.sivale.com.mx.exposition.servicios.apps.ServiciosAppsType;
import ws.sivale.com.mx.exposition.servicios.tarjeta.ServiciosTarjeta;
import ws.sivale.com.mx.exposition.servicios.tarjeta.ServiciosTarjetaType;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public abstract class WsdlConsumeServiceImpl implements WsdlConsumeService{

	private static Logger log = Logger.getLogger(WsdlConsumeServiceImpl.class);
	
	@Value("${environment.profile.name}")
	private String profileEnvironmet;

	@Value("${url.inteliviajes.apps}")
	private String urlApps;

	@Value("${url.inteliviajes.tarjeta}")
	private String urlCards;

	@Value("${url.gpv.legados}")
	private String urlGpv;

	@Value("${ws.timeout}")
	private Integer timeOut;
	
	@Override
	@SuppressWarnings("rawtypes")
	public ServiciosAppsType getAppsPort() throws ServiceException{
		ServiciosApps serviciosApps = new ServiciosApps();

		ServiciosAppsType serviciosAppsType = null;
		serviciosApps.setHandlerResolver(new HandlerResolver() {
			@Override
            public List<Handler> getHandlerChain(PortInfo portInfo) {
                ArrayList<Handler> handlers = new ArrayList<>();
                handlers.add(new RequestSOAPHeaderHandler());
                return handlers;
            }
        });
        
		serviciosAppsType = serviciosApps.getServiciosAppsTypePort();
        configureTimeout((BindingProvider) serviciosAppsType);
        return  serviciosAppsType;
	}
	
	@Override
	@SuppressWarnings("rawtypes")
	public ServiciosTarjetaType getTarjetaPort() {
		ServiciosTarjeta serviciosTarjeta = new ServiciosTarjeta();
        
		serviciosTarjeta.setHandlerResolver(new HandlerResolver() {
            @Override
            public List<Handler> getHandlerChain(PortInfo portInfo) {
                ArrayList<Handler> handlers = new ArrayList<>();
                handlers.add(new RequestSOAPHeaderHandler());
                return handlers;
            }
        });
		
		ServiciosTarjetaType serviciosTarjetaType = serviciosTarjeta.getServiciosTarjetaTypePort();
		configureTimeout((BindingProvider) serviciosTarjetaType);
        return serviciosTarjetaType;
	}
	
	@Override
	public ConsultaCFDIService getSatPort() {
		
		ConsultaCFDIService consultaCFDIService = new ConsultaCFDIService();
		IConsultaCFDIService iConsultaCFDIService = consultaCFDIService.getBasicHttpBindingIConsultaCFDIService();
		configureTimeout((BindingProvider) iConsultaCFDIService);
        return consultaCFDIService;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public SI01CORELegadosReqSyncOutType getLegadosGpvPort() {

		SI01CORELegadosReqSyncOutService servicioLegadosGpv = new SI01CORELegadosReqSyncOutService();

		servicioLegadosGpv.setHandlerResolver(new HandlerResolver() {
			@SuppressWarnings("rawtypes")
			@Override
			public List<Handler> getHandlerChain(PortInfo portInfo) {
				ArrayList<Handler> handlers = new ArrayList<>();
				handlers.add(new RequestSOAPHandler());
				return handlers;
			}
		});

		SI01CORELegadosReqSyncOutType servicioLegadosGpvType = servicioLegadosGpv.getHTTPSPort();
		configureTimeoutAuth((BindingProvider) servicioLegadosGpvType);
		return servicioLegadosGpvType;
	}

	private void configureTimeoutAuth(BindingProvider bindingProvider) {
		Map<String, Object> requestContext = bindingProvider.getRequestContext();

		requestContext.put(BindingProvider.USERNAME_PROPERTY, WsdlConfig.PROP_WS_GPV_USER);
		requestContext.put(BindingProvider.PASSWORD_PROPERTY, WsdlConfig.PROP_WS_GPV_PASS);

		requestContext.put("com.sun.xml.internal.ws.connect.timeout", timeOut);
		requestContext.put("com.sun.xml.internal.ws.request.timeout", timeOut);
	}
	
	private void configureTimeout(BindingProvider bindingProvider) {
		Map<String, Object> requestContext = bindingProvider.getRequestContext();
		
		log.debug(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
		log.debug("::::::::::::: URL APPS GET :::::::::::::::::::::: >>>>>>>>>> " + urlApps);
		log.debug("::::::::::::: URL TARJETAS GET :::::::::::::::::: >>>>>>>>>> " + urlCards);
		log.debug("::::::::::::: PROFILE ::::::::::::::::::::::::::: >>>>>>>>>> " + profileEnvironmet);
		log.debug("::::::::::::: CONFIGURATION TIME_OUT :::::::::::: >>>>>>>>>> " + timeOut);
		log.debug(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");

		String activeProfile;
	    activeProfile = System.getProperty("spring.profiles.active");
	    log.debug("#################### WsdlConsumeServiceImpl -- configureTimeout #################################");
	    log.debug(activeProfile);
	    log.debug("#####################################################");
		
		requestContext.put("com.sun.xml.internal.ws.connect.timeout", timeOut);
		requestContext.put("com.sun.xml.internal.ws.request.timeout", timeOut);
	}
}

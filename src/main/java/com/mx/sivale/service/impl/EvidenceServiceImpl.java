package com.mx.sivale.service.impl;

import com.mx.sivale.model.*;
import com.mx.sivale.model.dto.ImageEvidenceDTO;
import com.mx.sivale.model.dto.InvoiceAutho;
import com.mx.sivale.repository.*;
import com.mx.sivale.service.CfdiSatConsumeService;
import com.mx.sivale.service.EvidenceService;
import com.mx.sivale.service.S3Service;
import com.mx.sivale.service.request.ClaimsResolver;
import com.mx.sivale.service.util.UtilBean;
import com.mx.sivale.service.util.UtilTime;
import com.mx.sivale.service.util.UtilValidator;
import freemarker.template.Configuration;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.mx.wsdl.sat.consulta.Acuse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.rpc.ServiceException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class EvidenceServiceImpl extends WsdlConsumeServiceImpl implements EvidenceService {

	private static final Logger log = Logger.getLogger(EvidenceServiceImpl.class);

	@Autowired
	private ClaimsResolver claimsResolver;

	@Autowired
	private ImageEvidenceRepository imageEvidenceRepository;

	@Autowired
	private EvidenceTypeRepository evidenceTypeRepository;

	@Autowired
	private SpendingRepository spendingRepository;

	@Autowired
	private InvoiceFileRepository invoiceFileRepository;

	@Autowired
	private InvoiceRepository invoiceRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private InvoiceSpendingAssociateRepository invoiceSpendingAssociateRepository;

	@Autowired
	private S3Service s3Service;

	@Autowired
	private CfdiSatConsumeService cfdiSatConsumeService;

	public static final String PREFIX = "evid";

	@Value("${aws.s3.bucket}")
	private String BUCKET;

	private final String INVOICE_CONCEPT="cfdi:Concepto";
	private final String INVOICE_TAX="cfdi:Traslado";

	@Autowired
	private InvoiceConceptRepository invoiceConceptRepository;

	@Autowired
	private ConceptTaxRepository conceptTaxRepository;

	@Autowired
	private ClientRepository clientRepository;

	@Override
	public String uploadEvidence(MultipartFile file) throws Exception {

		InvoiceAutho invoiceAutho = new InvoiceAutho();
		invoiceAutho.setName(file.getName());
		invoiceAutho.setAssociatSpending(false);
		invoiceAutho.setInputStreamFile(file.getInputStream());

		String fileExt = FilenameUtils.getExtension(file.getOriginalFilename());
		if(fileExt == null || fileExt.equals("")){
			fileExt = file.getContentType();
		}
		invoiceAutho.setExtension(fileExt);

		return uploadEvidence(invoiceAutho, null, null);
	}

	public String uploadEvidence(InvoiceAutho invoiceAutho, Integer attachments, String name) throws Exception {

		String userEmail;
		String fileType = invoiceAutho.getExtension();

		if (!invoiceAutho.getAssociatSpending()){
			userEmail =  claimsResolver.email();
			invoiceAutho.setUser(userRepository.findByEmailAndActiveTrue(userEmail));
		}

		log.info("Processing file: " + invoiceAutho.getName() + ", file type:" + fileType);

		String response = null;

		if (!invoiceAutho.getAssociatSpending()
				&& !UtilValidator.isImage(fileType)
				&& !UtilValidator.isXml(fileType)
				&& !UtilValidator.isPdf(fileType)) {
			throw new ServiceException("Archivo no valido");
		}

		if (UtilValidator.isImage(fileType)) {
			response = proccessImage(invoiceAutho);
		}

		if (UtilValidator.isXml(fileType)) {
			response = proccessXML(invoiceAutho, name);
		}

		if (UtilValidator.isPdf(fileType)) {
			response = proccessPDF(invoiceAutho,attachments,name);
		}

		return response;
	}

	@Override
	public String deleteEvidence(Long evidenceId) throws Exception {
		try {
			imageEvidenceRepository.delete(evidenceId);
			return "Eliminacion de evidencia exitosa";
		} catch (Exception e) {
			throw new ServiceException("Erro al eliminar evidencia", e);
		}
	}

	@Override
	public ImageEvidenceDTO getEvidence(Long evidenceId) throws Exception {
		log.info("getEvidence: "+evidenceId);
		try {

			ImageEvidenceDTO imageEvidenceDTO = UtilBean.spendingEvidenceToSpendingEvidenceDTO(imageEvidenceRepository.findOne(evidenceId));

			log.info("Downloading from S3: bucket: " + BUCKET + "id: " + imageEvidenceDTO.getNameStorage());
			imageEvidenceDTO.setFile(s3Service.downloadAsBase64(BUCKET, imageEvidenceDTO.getNameStorage(), imageEvidenceDTO.getEvidenceType().getName()));

			if (imageEvidenceDTO == null)
				throw new ServiceException("No existe evidencia");

			return imageEvidenceDTO;
		} catch (Exception e) {
			throw new ServiceException("Erro al consultar evidencia", e);
		}
	}

	@Override
	public ImageEvidenceDTO updateEvidence(ImageEvidenceDTO imageEvidenceDTO) throws Exception {
		try {

			ImageEvidence evidenceExist = imageEvidenceRepository.findOne(imageEvidenceDTO.getId());

			if (evidenceExist == null)
				throw new ServiceException("No existe evidencia a editar");

//			if (imageEvidenceDTO.getSpendingId() != null)
//				evidenceExist.setSpending(spendingRepository.findOne(imageEvidenceDTO.getSpendingId()));
//			else
//				evidenceExist.setSpending(null);

			return UtilBean.spendingEvidenceToSpendingEvidenceDTO(imageEvidenceRepository.save(evidenceExist));
		} catch (Exception e) {
			throw new ServiceException("Erro al editar evidencia", e);
		}
	}

	//	@Override
	public static Map<String, String> getAttributesNode(NamedNodeMap namedNodeMap){
		Map<String, String> attributes = new HashMap<>();
		for (int j = 0; j < namedNodeMap.getLength(); j++)
			attributes.put(namedNodeMap.item(j).getNodeName().toLowerCase(), namedNodeMap.item(j).getNodeName());
		return attributes;
	}

	public String proccessImage(InvoiceAutho invoiceAutho) throws Exception {

		log.info("Processing as image: " + invoiceAutho.getName() + ", file type:" + invoiceAutho.getExtension());

		ImageEvidence imageEvidence = new ImageEvidence();
		EvidenceType evidenceType;

		if (UtilValidator.isPng(invoiceAutho.getExtension())) {
			evidenceType = evidenceTypeRepository.findOne(EvidenceType.PNG);
			imageEvidence.setEvidenceType(evidenceType);
		} else if (UtilValidator.isJpg(invoiceAutho.getExtension())) {
			evidenceType = evidenceTypeRepository.findOne(EvidenceType.JPEG);
			imageEvidence.setEvidenceType(evidenceType);
		} else {
			throw new Exception("Tipo de archivo inválido para evidencia.");
		}

		imageEvidence = imageEvidenceRepository.saveAndFlush(imageEvidence);

		String as3Key = PREFIX + invoiceAutho.getUser().getEmail().substring(0, invoiceAutho.getUser().getEmail().indexOf("@")) + imageEvidence.getId();
		InputStream inputStream = invoiceAutho.getInputStreamFile();
		s3Service.upload(inputStream, as3Key);

		imageEvidence.setNameStorage(as3Key);
		imageEvidenceRepository.saveAndFlush(imageEvidence);

		return imageEvidence.getId().toString();
	}


	public static void main(String [] args){
		try {

			String xml2="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
					"<cfdi:Comprobante xmlns:cfdi=\"http://www.sat.gob.mx/cfd/3\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd http://www.sat.gob.mx/implocal http://www.sat.gob.mx/sitio_internet/cfd/implocal/implocal.xsd\" Version=\"3.3\" Serie=\"RM\" Folio=\"2353\" Fecha=\"2019-06-14T08:37:10\" FormaPago=\"04\" SubTotal=\"2761.36\" Moneda=\"MXN\" TipoCambio=\"1\" Total=\"3258.41\" TipoDeComprobante=\"I\" MetodoPago=\"PUE\" LugarExpedicion=\"36740\" xmlns:implocal=\"http://www.sat.gob.mx/implocal\" Sello=\"ZHZTw61NQY2g5yOlOPGvv+pmdldFB/dVkHMLm5Bo+LOcyPY7GidAqZwNf7V+iuSRxcMecImQwgitSorcfaaVEaBNI1xXVnxks8NBKLskaQVcKx9c5xegw3hmE/KdX2tKvuCbTwZSLvRetXFmV76M59gvSVbvxJU3ZbAYV+4G63DPsir3a5zwtvVXRvAJZKQ19flQj1pX7bwyfB6jSbmdZsPy/7P8zOjG4pbGth27cyDyZNcCtzoe34oULjH6QsgxOgqPs/zDn0yE3Y1NdYj9qe3yOHk1T5C1a51woqGEF48tatJA/mc4dsm7vyJafyzn5WcBlH5ntY21+hFdIWa1Iw==\" Certificado=\"MIIGjTCCBHWgAwIBAgIUMDAwMDEwMDAwMDA0MTA4MDUxMDMwDQYJKoZIhvcNAQELBQAwggGyMTgwNgYDVQQDDC9BLkMuIGRlbCBTZXJ2aWNpbyBkZSBBZG1pbmlzdHJhY2nDs24gVHJpYnV0YXJpYTEvMC0GA1UECgwmU2VydmljaW8gZGUgQWRtaW5pc3RyYWNpw7NuIFRyaWJ1dGFyaWExODA2BgNVBAsML0FkbWluaXN0cmFjacOzbiBkZSBTZWd1cmlkYWQgZGUgbGEgSW5mb3JtYWNpw7NuMR8wHQYJKoZIhvcNAQkBFhBhY29kc0BzYXQuZ29iLm14MSYwJAYDVQQJDB1Bdi4gSGlkYWxnbyA3NywgQ29sLiBHdWVycmVybzEOMAwGA1UEEQwFMDYzMDAxCzAJBgNVBAYTAk1YMRkwFwYDVQQIDBBEaXN0cml0byBGZWRlcmFsMRQwEgYDVQQHDAtDdWF1aHTDqW1vYzEVMBMGA1UELRMMU0FUOTcwNzAxTk4zMV0wWwYJKoZIhvcNAQkCDE5SZXNwb25zYWJsZTogQWRtaW5pc3RyYWNpw7NuIENlbnRyYWwgZGUgU2VydmljaW9zIFRyaWJ1dGFyaW9zIGFsIENvbnRyaWJ1eWVudGUwHhcNMTgwNTE0MTYyNjI2WhcNMjIwNTE0MTYyNjI2WjCCASwxODA2BgNVBAMTL0dSVVBPIEFMRE8gUFJPTU9UT1JBIERFIElOVkVSU0lPTiBTIEEgUCBJIERFIENWMTgwNgYDVQQpEy9HUlVQTyBBTERPIFBST01PVE9SQSBERSBJTlZFUlNJT04gUyBBIFAgSSBERSBDVjE4MDYGA1UEChMvR1JVUE8gQUxETyBQUk9NT1RPUkEgREUgSU5WRVJTSU9OIFMgQSBQIEkgREUgQ1YxJTAjBgNVBC0THEdBUDE0MDQwN1RQOSAvIEdBVkk2MzAyMDFJQTYxHjAcBgNVBAUTFSAvIEdBVkk2MzAyMDFIR1RNTEcwNjE1MDMGA1UECxMsR1JVUE8gQUxETyBQUk9NT1RPUkEgREUgSU5WRVJTSU9OIFNBUEkgREUgQ1YwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDFUQCFlfUpKfuy24grAdbAuDHd0GFO2ocwtRxFFvQQ/fOfDMUG/jHWoPnntKkIMEgsV9ypQm2KbCiZCmbVUR8PnOreuO23pEeE5PqCklr5+lk8f32++tkO2MFLHaI7LIVuzvEKGpuqgb+5W1o29/4JawU3NCm2WKE58IStOyHhFHkja4tQzVZ0xXAQRm3KcDDeFLOZW/8xAF2TNUF288UbbHy1GsEp3LutWRvFiqSMKySjs/bwFeGXF43HHc7Jbx5FESsE1rPV3knMFY5frtoxUPcPbidWHz/IXhH3IOyMntKqz2qlH2jmYP8wHEo3JTLISND2QV3Yb1nZyp0lqmEtAgMBAAGjHTAbMAwGA1UdEwEB/wQCMAAwCwYDVR0PBAQDAgbAMA0GCSqGSIb3DQEBCwUAA4ICAQA5zRTB5agCxSVP4k4nq71vYzDDnPYAr2jDpy+cGK4O8nQ7Di8uDQmfZ2Lor6Jf1mzyi/WZz6ekK0JFQHhHlOdcRawYsVss3ehyIl+BsUeAaNiEypMexyUVvtIvuzmPF0bedvnuy3o84/zwMuQILJ/cMy18j2/Y8QXXzHZdkQLY47Q2BdgY97V0YpJ3r8ozWSQGMeHtCN9LwEGofheFx/l/NvSnMeQJ2tgOVcsi3cxhmg7Kcc/Jynuni0ZhCj7DkCAOiLC5rizOlPMaBMMZ2M1tOtRVvl1OAlHPrfsZt9e7781S+MJR7Xfcrb4OYwXHeXKWpIEPbSufrctVSKeMNI7DeIzOJZxNI3I0d1tyvderWfGaijWGRyjs0PqeGDrPrXEUv6BH7wOi1mNKqRRac4QnRjoVjlr4ziedYhnnf+FeGwuleP0Oz+tX7A/CZN3do5aLycRQAybJ1KpIieB//fzjL3S3pqWYPtNQ9+jvqC72faTMKdEj907YOS0qsaZl5go84OUJwYSIWnC01xiQ0JoeX/KECfCgE24OWX9Ym6C4mZhE0PefNCg8DtNUfAY5LRH33LVxt2WKvpZLLPmNqdAJt04Y6g0C1UyFaUUGsKkbxWBZUu+nGdW8JGCZhFEmjLS55yDfB1BpK54BbDfvc7pkOd+kaJXQ9F925xh5uQkDvw==\" NoCertificado=\"00001000000410805103\">\n" +
					"  <cfdi:Emisor Rfc=\"GAP140407TP9\" Nombre=\"GRUPO ALDO PROMOTORA DE INVERSION\" RegimenFiscal=\"601\" />\n" +
					"  <cfdi:Receptor Rfc=\"JSS160223522\" Nombre=\"JIGYOU SUPPORT STRATEGY S.C\" UsoCFDI=\"G03\" />\n" +
					"  <cfdi:Conceptos>\n" +
					"    <cfdi:Concepto ClaveProdServ=\"90111503\" NoIdentificacion=\"020\" Cantidad=\"1.00\" ClaveUnidad=\"ROM\" \n" +
					"\tUnidad=\"Habitacion\" Descripcion=\"Hospedaje\" ValorUnitario=\"2761.36\" Importe=\"2761.36\">\n" +
					"      <cfdi:Impuestos>\n" +
					"        <cfdi:Traslados>\n" +
					"          <cfdi:Traslado Base=\"2761.36\" Impuesto=\"002\" TipoFactor=\"Tasa\" TasaOCuota=\"0.160000\" Importe=\"441.82\" />\n" +
					"        </cfdi:Traslados>\n" +
					"      </cfdi:Impuestos>\n" +
					"    </cfdi:Concepto>\n" +
					"  </cfdi:Conceptos>\n" +
					"  <cfdi:Impuestos TotalImpuestosTrasladados=\"441.82\">\n" +
					"    <cfdi:Traslados>\n" +
					"      <cfdi:Traslado Impuesto=\"002\" TipoFactor=\"Tasa\" TasaOCuota=\"0.160000\" Importe=\"441.82\" />\n" +
					"    </cfdi:Traslados>\n" +
					"  </cfdi:Impuestos>\n" +
					"  <cfdi:Complemento>\n" +
					"    <implocal:ImpuestosLocales version=\"1.0\" TotaldeRetenciones=\"0.00\" TotaldeTraslados=\"55.23\">\n" +
					"      <implocal:TrasladosLocales ImpLocTrasladado=\"ISH\" TasadeTraslado=\"2.00\" Importe=\"55.23\" />\n" +
					"    </implocal:ImpuestosLocales>\n" +
					"    <tfd:TimbreFiscalDigital Version=\"1.1\" UUID=\"55746341-644E-409A-833F-A1E5934E2402\" \n" +
					"\tFechaTimbrado=\"2019-06-14T08:37:17\" \n" +
					"\tSelloCFD=\"ZHZTw61NQY2g5yOlOPGvv+pmdldFB/dVkHMLm5Bo+LOcyPY7GidAqZwNf7V+iuSRxcMecImQwgitSorcfaaVEaBNI1xXVnxks8NBKLskaQVcKx9c5xegw3hmE/KdX2tKvuCbTwZSLvRetXFmV76M59gvSVbvxJU3ZbAYV+4G63DPsir3a5zwtvVXRvAJZKQ19flQj1pX7bwyfB6jSbmdZsPy/7P8zOjG4pbGth27cyDyZNcCtzoe34oULjH6QsgxOgqPs/zDn0yE3Y1NdYj9qe3yOHk1T5C1a51woqGEF48tatJA/mc4dsm7vyJafyzn5WcBlH5ntY21+hFdIWa1Iw==\" \n" +
					"\tNoCertificadoSAT=\"00001000000405295359\" \n" +
					"\tRfcProvCertif=\"MCO000823CK3\" \n" +
					"\txsi:schemaLocation=\"http://www.sat.gob.mx/TimbreFiscalDigital http://www.sat.gob.mx/sitio_internet/cfd/timbrefiscaldigital/TimbreFiscalDigitalv11.xsd\" \n" +
					"\tSelloSAT=\"S647hUvMFaVner0JY84XyvOvAKZ4yt2XLmF/Or2y7uFx685pyLdspD+xrI7qRdTmZWd6TJbGHpGHjunmBuEuqNmi1TUJA/k3+DLkfU6VroPhdVcPswpXDDNBTjGlAQo0uJosAaquzxVSA4a0c06fJngJhKKHL2vWAXi1nRorxhpx40so6X5Bmh0YSKNaRkt6mtYI1oLo3dt7M6JdJAYDbNQPBueTeMFU8V65MIQYVIGPhJYx7HWduNBO7qz5Yn5FARl/y9PTKRgf9gTibUpvOFeL0XRkvljWFlk/gOihk6qK1QD2JBjI1BdE9KXPQCpVcfY+UO3MurYIKfNndxuwGA==\" \n" +
					"\txmlns:tfd=\"http://www.sat.gob.mx/TimbreFiscalDigital\" />\n" +
					"  </cfdi:Complemento>\n" +
					"</cfdi:Comprobante>";
			String claveProdServ="", base="", base2="";
			InputStream inputStream = new ByteArrayInputStream(xml2.getBytes(Charset.forName("UTF-8")));
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = docFactory.newDocumentBuilder();
			Document doc = builder.parse(inputStream);

			doc.getDocumentElement().normalize();
			XPath xPath =  XPathFactory.newInstance().newXPath();

			//NodeList nodeConceptosOriginal = (NodeList) xPath.compile("/Comprobante/Conceptos/Concepto | /Comprobante/Conceptos/Concepto/Impuestos/Traslados/Traslado").evaluate(doc, XPathConstants.NODESET);

			Node nodeConcept = (Node) xPath.compile("/Comprobante/Conceptos").evaluate(doc, XPathConstants.NODE);
			NodeList nodeIsh = (NodeList) xPath.compile("/Comprobante/Complemento/ImpuestosLocales/TrasladosLocales").evaluate(doc, XPathConstants.NODESET);


			List<Node> nodesISH = IntStream.range(0, nodeIsh.getLength())
					.mapToObj(nodeIsh::item)
					.collect(Collectors.toList());

			ConceptTax conceptTaxISH=new ConceptTax();
			nodesISH.forEach(n-> {
				Element element = (Element) n;
				Map<String, String> attributesish = getAttributesNode(element.getAttributes());
				if (attributesish.get("imploctrasladado") != null){
					conceptTaxISH.setTax(element.getAttribute(attributesish.get("imploctrasladado"))==null || element.getAttribute(attributesish.get("imploctrasladado")).isEmpty()?"":element.getAttribute(attributesish.get("imploctrasladado")));
					System.out.println("imploctrasladado: "+element.getAttribute(attributesish.get("imploctrasladado")));
				}
				if (attributesish.get("tasadetraslado") != null){
					conceptTaxISH.setFee(element.getAttribute(attributesish.get("tasadetraslado"))==null || element.getAttribute(attributesish.get("tasadetraslado")).isEmpty()?"":element.getAttribute(attributesish.get("tasadetraslado")));
					System.out.println("TasadeTraslado: "+element.getAttribute(attributesish.get("tasadetraslado")));
				}

				if (attributesish.get("importe") != null){
					conceptTaxISH.setAmount(element.getAttribute(attributesish.get("importe"))==null || element.getAttribute(attributesish.get("importe")).isEmpty()?"":element.getAttribute(attributesish.get("importe")));
					System.out.println("Importe: "+element.getAttribute(attributesish.get("importe")));
				}
			});


			List<Node> nodes = IntStream.range(0, nodeConcept.getChildNodes().getLength())
					.mapToObj(nodeConcept.getChildNodes()::item)
					.filter(node -> node.getNodeName().equals("cfdi:Concepto"))
					.collect(Collectors.toList());

			nodes.forEach(n->{
				InvoiceConcept invoiceConcept=new InvoiceConcept();
				Element element = (Element) n;
				Map<String, String> attributes = getAttributesNode(element.getAttributes());
				if (attributes.get("descripcion") != null){
					System.out.println("Concepto: "+element.getAttribute(attributes.get("descripcion")));
					invoiceConcept.setDescription(element.getAttribute(attributes.get("descripcion")));
				}

				if (attributes.get("importe") != null){
					System.out.println("Importe: "+element.getAttribute(attributes.get("importe")));
					invoiceConcept.setAmount(element.getAttribute(attributes.get("importe")));
				}
				/*VALIDACIÓN ENTRE EL CONCEPTO Y EL ISH*/
				System.out.println("------------------------------------------");
				Double factorPorcentaje=Double.parseDouble(conceptTaxISH.getFee());
				if(factorPorcentaje>0){
					factorPorcentaje/=100;
				}
				Double importeCalculado=Double.parseDouble(invoiceConcept.getAmount()) * factorPorcentaje;
				Double importeTax=Double.parseDouble(conceptTaxISH.getAmount());
				Double diference=importeTax - importeCalculado;

				System.out.println("importeTax: "+importeTax);
				System.out.println("importeCalculado: "+importeCalculado);
				System.out.println("diference: "+diference);
				if(diference<.5){
					System.out.println("ya es este");
				}



				if(n.getChildNodes().getLength()>0){
					List<Node> nodesImpuestos=IntStream.range(0, n.getChildNodes().getLength())
							.mapToObj(n.getChildNodes()::item)
							.filter(n1 -> n1.getNodeName().equals("cfdi:Impuestos"))
							.collect(Collectors.toList());

					if(nodesImpuestos.size()==1){
						List<Node> nodeTraslados=IntStream.range(0, nodesImpuestos.get(0).getChildNodes().getLength())
								.mapToObj(nodesImpuestos.get(0).getChildNodes()::item)
								.filter(n1 -> n1.getNodeName().equals("cfdi:Traslados"))
								.collect(Collectors.toList());

						if(nodeTraslados.size()==1){
							List<Node> nodeTraslado=IntStream.range(0, nodeTraslados.get(0).getChildNodes().getLength())
									.mapToObj(nodeTraslados.get(0).getChildNodes()::item)
									.filter(n1 -> n1.getNodeName().equals("cfdi:Traslado"))
									.collect(Collectors.toList());
							nodeTraslado.forEach(
									t->{
										Element elementTraslado = (Element) t;
										Map<String, String> attributesTraslado = getAttributesNode(elementTraslado.getAttributes());
										if (attributesTraslado.get("base") != null)
											System.out.println("Base: "+elementTraslado.getAttribute(attributesTraslado.get("base")));
									}

							);
						}
					}
				}
			});



		} catch (Exception e) {
			System.out.println("Exception Main: "+e.getMessage());
			e.printStackTrace();
		}
	}

	public String proccessXMLTest() throws Exception {

		String xml3="<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n" +
				"<cfdi:Comprobante Version=\"3.3\" Serie=\"MEX\" Folio=\"240129795\" Fecha=\"2019-06-12T14:26:11\" Sello=\"PjE4k+DAloAdZM+nz3ezLKsZRKgHiATL8wBLu7gWneBOkIeeRZqYZQSZVuuX7dE+Hdyg7uIMk10VR91qirH2/5TkLMNHsxIpcOLlGzSkOAPjCne813V5hyYRzXlYFbXgNCsBv4EyG4pcVA0tvxYueXIbEX8yfylXrdj7RFOxFC00CFrfRl4uOabXdtrqRWhidWwq4TPCnRD43XUDgVxZmYR0/55l98K4OFtbt1gc5BqbB+4ghtxp7XP+Etc091B7tzhSvFPqtCx3UdIPRoqL60m+9NACowZUZbZqOAzFSFBV9i1bmTBmR5OuqptYIl+UtySB9RSI6oawNe2KcUhAtw==\" FormaPago=\"99\" NoCertificado=\"00001000000405409176\" Certificado=\"MIIGSzCCBDOgAwIBAgIUMDAwMDEwMDAwMDA0MDU0MDkxNzYwDQYJKoZIhvcNAQELBQAwggGyMTgwNgYDVQQDDC9BLkMuIGRlbCBTZXJ2aWNpbyBkZSBBZG1pbmlzdHJhY2nDs24gVHJpYnV0YXJpYTEvMC0GA1UECgwmU2VydmljaW8gZGUgQWRtaW5pc3RyYWNpw7NuIFRyaWJ1dGFyaWExODA2BgNVBAsML0FkbWluaXN0cmFjacOzbiBkZSBTZWd1cmlkYWQgZGUgbGEgSW5mb3JtYWNpw7NuMR8wHQYJKoZIhvcNAQkBFhBhY29kc0BzYXQuZ29iLm14MSYwJAYDVQQJDB1Bdi4gSGlkYWxnbyA3NywgQ29sLiBHdWVycmVybzEOMAwGA1UEEQwFMDYzMDAxCzAJBgNVBAYTAk1YMRkwFwYDVQQIDBBEaXN0cml0byBGZWRlcmFsMRQwEgYDVQQHDAtDdWF1aHTDqW1vYzEVMBMGA1UELRMMU0FUOTcwNzAxTk4zMV0wWwYJKoZIhvcNAQkCDE5SZXNwb25zYWJsZTogQWRtaW5pc3RyYWNpw7NuIENlbnRyYWwgZGUgU2VydmljaW9zIFRyaWJ1dGFyaW9zIGFsIENvbnRyaWJ1eWVudGUwHhcNMTcwMzA4MTUzNDQxWhcNMjEwMzA4MTUzNDQxWjCB6zEnMCUGA1UEAxMeQ0FERU5BIENPTUVSQ0lBTCBPWFhPIFNBIERFIENWMScwJQYDVQQpEx5DQURFTkEgQ09NRVJDSUFMIE9YWE8gU0EgREUgQ1YxJzAlBgNVBAoTHkNBREVOQSBDT01FUkNJQUwgT1hYTyBTQSBERSBDVjElMCMGA1UELRMcQ0NPODYwNTIzMU40IC8gUEVQTzYxMDIwM0FFMjEeMBwGA1UEBRMVIC8gUEVQTzYxMDIwM0hUU1JSUzA2MScwJQYDVQQLEx5DQURFTkEgQ09NRVJDSUFMIE9YWE8gU0EgREUgQ1YwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCED3Tdoh/YXzIZ6H0d/+L88kB1t8FsYLJQK9UnkNByW8IRzmKKxcK7Ns2MmQzXtDqzL351IH46WwDjzI3Vw1WiyEf4y4Ez1lpRvvhIKiIbKbix8Nm6u3nr2AYqYTIQdKy9V6frVPUadM1J7wME7pHq4eslX2T5dS8veR7grIcgtos1bDDgH5QhK4kbSu7e/99Xs52ZlKjdJUrgS9M7BlNeL0Ld3NLaNLBS1sMDvxq5dVVLTJqzJkSc1BMcy1fH474Qu3iCiy1rlHPbjyG/1HWiI8pfSX/MHI/l6rz8PX60UCkBbqU9Z9anTaeCII99dTteMcUNyl0Gk34e/6knJ3ipAgMBAAGjHTAbMAwGA1UdEwEB/wQCMAAwCwYDVR0PBAQDAgbAMA0GCSqGSIb3DQEBCwUAA4ICAQA/rqcGyXcIcUg4kKLXUVf0EUhkMK7CIBA0OiP2Y9A6ieIKWWnMrZUONvDFNcBBVnG3tUE67Tny1lsXn5hU+CvLnH4wkh6YfrA1zQlDABsNNx2B0BOrzpgbf8XgaHnHyW3gxkTE/CzYQ7QonuvZWIXudzFvjbUbtoYWWqmfWMP6tCPYypBOKny85dwnGSUkIO8nbirs4r3hmfIVWK7FsusNuuK4aLD7+PZEq9C3NI4ZjDzL2sku8i2Sjs9as3xKEuQHU1Sd4nuT8mPjEJ8asI8hfA+fJPcU3kmBV3L4qcJgFb6mXi2ji6IB7QtXwzIlu/h0id5NPmlv9+5xp8fYrpnbjiJA8VKf9wIklBGsgCQQqBoGpNzc3avPUOFXLcGsvj9AqW0YUq40VheWLLSMshmNjocyaGNJB+MKykG8y4w0syDs7nX3XHWSuy/YCcQC3cJd07RZMzTECUo+mPzTzKlsedPCE9Y6qBThu7M6Qdptah0LlsZ5FnIF/X4sBwIteBqVGQb+84MqA6j9YBPenzsUPT9OcmFsFnbma9pyMoat2P9Q51b6vddCsgT21wbN4UrRODtdFXhWvyycN5ZgDwn3gUhMDp0wLJxOPyhedQu4YUZ1YjD2/qzwFhu1oALxlrtAFX6xx0ZORfHXJgEt26kLZNQDRZIx3SbeMUCeOpPGaQ==\" SubTotal=\"16.20\" Moneda=\"MXN\" Total=\"17.50\" TipoDeComprobante=\"I\" MetodoPago=\"PUE\" LugarExpedicion=\"06600\" xsi:schemaLocation=\"http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd\" xmlns:cfdi=\"http://www.sat.gob.mx/cfd/3\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:pago10=\"http://www.sat.gob.mx/Pagos\" xmlns:implocal=\"http://www.sat.gob.mx/implocal\">\n" +
				"  <cfdi:Emisor Rfc=\"CCO8605231N4\" Nombre=\"CADENA COMERCIAL OXXO, SA DE CV\" RegimenFiscal=\"623\"/>\n" +
				"  <cfdi:Receptor Rfc=\"PUN9810229R0\" Nombre=\"SI VALE MEXICO SA DE CV\" UsoCFDI=\"G03\"/>\n" +
				"  <cfdi:Conceptos>\n" +
				"    <cfdi:Concepto ClaveProdServ=\"50181900\" Cantidad=\"1.000000\" ClaveUnidad=\"EA\" Unidad=\"EA\" Descripcion=\"PAN DULCE MANTECADAS BIMBO 125 GR BOLSA VAINILLA\" ValorUnitario=\"16.20\" Importe=\"16.20\">\n" +
				"      <cfdi:Impuestos>\n" +
				"        <cfdi:Traslados>\n" +
				"          <cfdi:Traslado Base=\"16.20\" Impuesto=\"003\" TipoFactor=\"Tasa\" TasaOCuota=\"0.080000\" Importe=\"1.30\"/>\n" +
				"          <cfdi:Traslado Base=\"17.50\" Impuesto=\"002\" TipoFactor=\"Tasa\" TasaOCuota=\"0.000000\" Importe=\"0.00\"/>\n" +
				"        </cfdi:Traslados>\n" +
				"      </cfdi:Impuestos>\n" +
				"    </cfdi:Concepto>\n" +
				"  </cfdi:Conceptos>\n" +
				"  <cfdi:Impuestos TotalImpuestosTrasladados=\"1.30\">\n" +
				"    <cfdi:Traslados>\n" +
				"      <cfdi:Traslado Impuesto=\"002\" TipoFactor=\"Tasa\" TasaOCuota=\"0.000000\" Importe=\"0.00\"/>\n" +
				"      <cfdi:Traslado Impuesto=\"003\" TipoFactor=\"Tasa\" TasaOCuota=\"0.080000\" Importe=\"1.30\"/>\n" +
				"    </cfdi:Traslados>\n" +
				"  </cfdi:Impuestos>\n" +
				"  <cfdi:Complemento>\n" +
				"        <tfd:TimbreFiscalDigital xmlns:tfd=\"http://www.sat.gob.mx/TimbreFiscalDigital\" xsi:schemaLocation=\"http://www.sat.gob.mx/TimbreFiscalDigital http://www.sat.gob.mx/sitio_internet/cfd/TimbreFiscalDigital/TimbreFiscalDigitalv11.xsd\" Version=\"1.1\" UUID=\"1a14d305-45e1-45b7-8748-8228089a7329\" RfcProvCertif=\"SNF171020F3A\" FechaTimbrado=\"2019-06-12T14:26:11\" SelloCFD=\"PjE4k+DAloAdZM+nz3ezLKsZRKgHiATL8wBLu7gWneBOkIeeRZqYZQSZVuuX7dE+Hdyg7uIMk10VR91qirH2/5TkLMNHsxIpcOLlGzSkOAPjCne813V5hyYRzXlYFbXgNCsBv4EyG4pcVA0tvxYueXIbEX8yfylXrdj7RFOxFC00CFrfRl4uOabXdtrqRWhidWwq4TPCnRD43XUDgVxZmYR0/55l98K4OFtbt1gc5BqbB+4ghtxp7XP+Etc091B7tzhSvFPqtCx3UdIPRoqL60m+9NACowZUZbZqOAzFSFBV9i1bmTBmR5OuqptYIl+UtySB9RSI6oawNe2KcUhAtw==\" NoCertificadoSAT=\"00001000000414211380\" SelloSAT=\"IYrTmMxwIOiKkF+/RM5zxae+LAbO5gE4JGGJ92H11TerrTA16RpoTjqm2LUL3g937IO/sNrnanNLUxSYmY+sKnzKhmIQ22gqn+Va9Q28IHmyxJpMuFQDZ3jYbpwUnBT+LU8Cn/Ywry/QOFVrx9fyfCdS43Iu8w4EVgxTda8e/8rzrvr7Op9T0rZ6I1/Ru56SYbpxZEZ78p2r4bAjf0hj80S0K29+xz5K0gt2rsbvm9hZyMyFttCQswCJpRgvs+4Iph4zZK/kXPNIq+WipptrZDba8EZ5mI1hZjwFjrhmBiHCwDgyAsL8+LaQ94UteriGV8yFg/6G9/28xMzxSKMNmA==\"/>\n" +
				"    </cfdi:Complemento><cfdi:Addenda>\n" +
				"    <ns:diverza version=\"1.1\" xmlns:ns=\"http://www.diverza.com/ns/addenda/diverza/1\">\n" +
				"      <ns:generales tipoDocumento=\"Factura\"/>\n" +
				"      <ns:clavesDescripcion c_TipoDeComprobante=\"Ingreso\" c_MetodoPago=\"PAGO EN UNA SOLA EXHIBICION\" c_RegimenFiscal=\"Opcional para Grupos de Sociedades\" c_UsoCFDI=\"Gastos en general\"/>\n" +
				"      <ns:receptor>\n" +
				"        <ns:domicilioFiscalR calle=\"PASEO DE LA REFORMA\" numero=\"284\" colonia=\"JUAREZ\" ciudad=\"CUAUHTEMOC\" municipio=\"CUAUHTEMOC\" estado=\"CIUDAD DE MEXICO\" pais=\"México\" codigoPostal=\"06600\"/>\n" +
				"      </ns:receptor>\n" +
				"    </ns:diverza>\n" +
				"  </cfdi:Addenda>\n" +
				"</cfdi:Comprobante>";
		try {
			Invoice invoice = new Invoice();
			InputStream inputStream = new ByteArrayInputStream(xml3.getBytes(Charset.forName("UTF-8")));

			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = docFactory.newDocumentBuilder();
			Document doc = builder.parse(inputStream);

			doc.getDocumentElement().normalize();
			XPath xPath =  XPathFactory.newInstance().newXPath();

			NodeList nodeComprobante = (NodeList) xPath.compile("Comprobante").evaluate(doc, XPathConstants.NODESET);
			NodeList nodeEmisor = (NodeList) xPath.compile("/Comprobante/Emisor").evaluate(doc, XPathConstants.NODESET);
			NodeList nodeReceptor = (NodeList) xPath.compile("/Comprobante/Receptor").evaluate(doc, XPathConstants.NODESET);
			Node nodeConcept = (Node) xPath.compile("/Comprobante/Conceptos").evaluate(doc, XPathConstants.NODE);
			NodeList nodeImpuestos = (NodeList) xPath.compile("/Comprobante/Impuestos/Traslados/Traslado").evaluate(doc, XPathConstants.NODESET);
			NodeList nodeComplemento = (NodeList) xPath.compile("/Comprobante/Complemento/TimbreFiscalDigital").evaluate(doc, XPathConstants.NODESET);
			//NodeList nodeAddenda = (NodeList) xPath.compile("/Comprobante/Addenda").evaluate(doc, XPathConstants.NODESET);
			NodeList nodeIsh = (NodeList) xPath.compile("/Comprobante/Complemento/ImpuestosLocales/TrasladosLocales").evaluate(doc, XPathConstants.NODESET);
			NodeList nodeAerolineas = (NodeList) xPath.compile("/Comprobante/Complemento/Aerolineas").evaluate(doc, XPathConstants.NODESET);

			NodeList nodeDomicilioFiscalEmisor = (NodeList) xPath.compile("/Comprobante/Emisor/DomicilioFiscal").evaluate(doc, XPathConstants.NODESET);
			NodeList nodeExpedidoEnEmisor = (NodeList) xPath.compile("/Comprobante/Emisor/ExpedidoEn").evaluate(doc, XPathConstants.NODESET);

			String nombreEmisor = null, rfcReceptor =  null, rfcEmisor = null , direccionFiscal = null;
			String direccionExpedicion = null , fechaFactura = null , lugarExpedicion = null;
			String subtotal = null, impuestoTotal = null, total = null, uuid = null;
			String folio = null, moneda = null, nombreReceptor = null, fechaFacturaOriginal=null;

			List<Node> nodesISH = IntStream.range(0, nodeIsh.getLength())
					.mapToObj(nodeIsh::item)
					.collect(Collectors.toList());

			ConceptTax conceptTaxISH=new ConceptTax();
			nodesISH.forEach(n-> {
				Element element = (Element) n;
				Map<String, String> attributesish = getAttributesNode(element.getAttributes());
				if (attributesish.get("imploctrasladado") != null){
					conceptTaxISH.setTax(element.getAttribute(attributesish.get("imploctrasladado"))==null || element.getAttribute(attributesish.get("imploctrasladado")).isEmpty()?"":element.getAttribute(attributesish.get("imploctrasladado")));
					System.out.println("imploctrasladado: "+element.getAttribute(attributesish.get("imploctrasladado")));
				}
				if (attributesish.get("tasadetraslado") != null){
					conceptTaxISH.setFee(element.getAttribute(attributesish.get("tasadetraslado"))==null || element.getAttribute(attributesish.get("tasadetraslado")).isEmpty()?"":element.getAttribute(attributesish.get("tasadetraslado")));
					System.out.println("TasadeTraslado: "+element.getAttribute(attributesish.get("tasadetraslado")));
				}

				if (attributesish.get("importe") != null){
					conceptTaxISH.setAmount(element.getAttribute(attributesish.get("importe"))==null || element.getAttribute(attributesish.get("importe")).isEmpty()?"":element.getAttribute(attributesish.get("importe")));
					System.out.println("Importe: "+element.getAttribute(attributesish.get("importe")));
				}
			});

			if (nodeComprobante != null && nodeComprobante.getLength() > 0){
				for (int i = 0; i < nodeComprobante.getLength(); i++) {
					Node node = nodeComprobante.item(i);
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						Element element = (Element) node;
						Map<String, String> attributes = getAttributesNode(element.getAttributes());
						if (attributes.get("fecha") != null)
							fechaFacturaOriginal = element.getAttribute(attributes.get("fecha"));
						fechaFactura = fechaFacturaOriginal.split("T")[0];
						if (attributes.get("subtotal") != null)
							subtotal = element.getAttribute(attributes.get("subtotal"));
						if (attributes.get("total") != null)
							total = element.getAttribute(attributes.get("total"));
						if (attributes.get("lugarexpedicion") != null)
							lugarExpedicion = element.getAttribute(attributes.get("lugarexpedicion"));
						if (attributes.get("folio") != null)
							folio = element.getAttribute(attributes.get("folio"));
						if (attributes.get("moneda") != null)
							moneda = element.getAttribute(attributes.get("moneda"));
					}
				}
			}

			if (nodeEmisor != null && nodeEmisor.getLength() > 0){
				for (int i = 0; i < nodeEmisor.getLength(); i++) {
					Node node = nodeEmisor.item(i);
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						Element element = (Element) node;
						Map<String, String> attributes = getAttributesNode(element.getAttributes());
						if (attributes.get("nombre") != null)
							nombreEmisor = element.getAttribute(attributes.get("nombre"));
						if (attributes.get("rfc") != null)
							rfcEmisor = element.getAttribute(attributes.get("rfc"));
					}
				}
			}

			if (nodeReceptor != null && nodeReceptor.getLength() > 0){
				for (int i = 0; i < nodeReceptor.getLength(); i++) {
					Node node = nodeReceptor.item(i);
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						Element element = (Element) node;
						Map<String, String> attributes = getAttributesNode(element.getAttributes());
						if (attributes.get("rfc") != null)
							rfcReceptor = element.getAttribute(attributes.get("rfc"));
						if (attributes.get("nombre") != null)
							nombreReceptor = element.getAttribute(attributes.get("nombre"));
					}
				}
			}

			if (nodeDomicilioFiscalEmisor != null && nodeDomicilioFiscalEmisor.getLength() > 0){
				for (int i = 0; i < nodeDomicilioFiscalEmisor.getLength(); i++) {
					Node node = nodeDomicilioFiscalEmisor.item(i);
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						Element element = (Element) node;
						Map<String, String> attributes = getAttributesNode(element.getAttributes());

						StringBuilder address = new StringBuilder();
						if (attributes.get("calle") != null)
							address.append(element.getAttribute(attributes.get("calle")) + " ");
						if (attributes.get("noexterior") != null)
							address.append(element.getAttribute(attributes.get("noexterior")) + " ");
						if (attributes.get("colonia") != null)
							address.append(element.getAttribute(attributes.get("colonia")) + " ");
						if (attributes.get("localidad") != null)
							address.append(element.getAttribute(attributes.get("localidad")) + " ");
						if (attributes.get("municipio") != null)
							address.append(element.getAttribute(attributes.get("municipio")) + " ");
						if (attributes.get("estado") != null)
							address.append(element.getAttribute(attributes.get("estado")) + " ");
						if (attributes.get("pais") != null)
							address.append(element.getAttribute(attributes.get("pais")) + " ");
						if (attributes.get("codigopostal") != null)
							address.append(element.getAttribute(attributes.get("codigopostal")));

						direccionFiscal = address.toString();
					}
				}

			}

			if (nodeExpedidoEnEmisor != null && nodeExpedidoEnEmisor.getLength() > 0){
				for (int i = 0; i < nodeExpedidoEnEmisor.getLength(); i++) {
					Node node = nodeExpedidoEnEmisor.item(i);
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						Element element = (Element) node;
						Map<String, String> attributes = getAttributesNode(element.getAttributes());

						StringBuilder address = new StringBuilder();
						if (attributes.get("calle") != null)
							address.append(element.getAttribute(attributes.get("calle")) + " ");
						if (attributes.get("noexterior") != null)
							address.append(element.getAttribute(attributes.get("noexterior")) + " ");
						if (attributes.get("colonia") != null)
							address.append(element.getAttribute(attributes.get("colonia")) + " ");
						if (attributes.get("localidad") != null)
							address.append(element.getAttribute(attributes.get("localidad")) + " ");
						if (attributes.get("municipio") != null)
							address.append(element.getAttribute(attributes.get("municipio")) + " ");
						if (attributes.get("estado") != null)
							address.append(element.getAttribute(attributes.get("estado")) + " ");
						if (attributes.get("pais") != null)
							address.append(element.getAttribute(attributes.get("pais")) + " ");
						if (attributes.get("codigopostal") != null)
							address.append(element.getAttribute(attributes.get("codigopostal")));

						direccionExpedicion = address.toString();
					}
				}
			}

			Double ivaTotal = 0.0;
			Double ieps = 0.0;
			if (nodeImpuestos != null && nodeImpuestos.getLength() > 0) {
				for (int i = 0; i < nodeImpuestos.getLength(); i++) {
					Node node = nodeImpuestos.item(i);
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						Element element = (Element) node;
						Map<String, String> attributes = getAttributesNode(element.getAttributes());
						String tipoImpuesto = element.getAttribute(attributes.get("impuesto"));
						if (tipoImpuesto.equalsIgnoreCase("002")) {
							ivaTotal += element.getAttribute(attributes.get("importe")) != null ? Double.parseDouble(element.getAttribute(attributes.get("importe"))) : 0.0;
						} else if (tipoImpuesto.equalsIgnoreCase("003")) {
							ieps += element.getAttribute(attributes.get("importe")) != null ? Double.parseDouble(element.getAttribute(attributes.get("importe"))) : 0.0;
						}
					}
				}
			}

			if (nodeComplemento != null && nodeComplemento.getLength() > 0){
				for (int i = 0; i < nodeComplemento.getLength(); i++) {
					Node node = nodeComplemento.item(i);
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						Element element = (Element) node;
						Map<String, String> attributes = getAttributesNode(element.getAttributes());
						uuid = element.getAttribute(attributes.get("uuid")).toUpperCase();
					}
				}
			}


			Double ish = 0.0;
			Double tua = 0.0;
			if (nodeIsh != null && nodeIsh.getLength() > 0){
				for (int i = 0; i < nodeIsh.getLength(); i++) {
					Node node = nodeIsh.item(i);
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						Element element = (Element) node;
						Map<String, String> attributes = getAttributesNode(element.getAttributes());
						String impuestoLocal = element.getAttribute(attributes.get("imploctrasladado")).toUpperCase();
						if(impuestoLocal!=null && !impuestoLocal.isEmpty() && (impuestoLocal.toUpperCase().trim().equalsIgnoreCase("ISH")
								|| impuestoLocal.toUpperCase().trim().equalsIgnoreCase("IMPUESTO SOBRE HOSPEDAJE"))){
							ish += element.getAttribute(attributes.get("importe")) != null ? Double.parseDouble(element.getAttribute(attributes.get("importe"))) : 0.0;
						}
					}
				}
			}

			if (nodeAerolineas != null && nodeAerolineas.getLength() > 0){
				for (int i = 0; i < nodeAerolineas.getLength(); i++) {
					Node node = nodeAerolineas.item(i);
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						Element element = (Element) node;
						Map<String, String> attributes = getAttributesNode(element.getAttributes());
						tua += element.getAttribute(attributes.get("tua")) != null ? Double.parseDouble(element.getAttribute(attributes.get("tua"))) : 0.0;
					}
				}
			}

			invoice.setSatVerification(false);
			try {
				Acuse acuse = cfdiSatConsumeService.consulta(rfcEmisor,rfcReceptor,total,uuid);
				log.info("Factura :::::>>>> " + acuse.getEstado().getValue());
				log.info("Factura :::::>>>> " + acuse.getCodigoEstatus().getValue());
				if (acuse.getCodigoEstatus().getValue().toUpperCase().contains("S - COMPROBANTE OBTENIDO SATISFACTORIAMENTE") && acuse.getEstado().getValue().toLowerCase().equalsIgnoreCase("vigente")) {
					invoice.setSatVerification(true);
				}
			}catch(Exception ex){
				log.error("Hubo un problema al verificar factura en SAT", ex);
			}

			invoice.setEstablishment(nombreEmisor);
			invoice.setDateInvoice(fechaFactura);
			invoice.setExpeditionAddress(direccionExpedicion != null ? direccionExpedicion : lugarExpedicion);
			invoice.setFiscalAddress(direccionFiscal);
			invoice.setRfcReceiver(rfcReceptor);
			invoice.setRfcTransmitter(rfcEmisor);
			invoice.setReceiverName(nombreReceptor);
			invoice.setSubtotal(subtotal);
			invoice.setTotal(total);
			invoice.setUuid(uuid.toUpperCase());
			invoice.setIva(ivaTotal > 0 ? String.valueOf(ivaTotal) : null);
			invoice.setIeps(ieps > 0 ? String.valueOf(ieps) : null);
			invoice.setFolio(folio);
			invoice.setCurrency(moneda);
			invoice.setIsh(ish > 0 ? String.valueOf(ish) : null);
			invoice.setTua(tua > 0 ? String.valueOf(tua) : null);
			invoice.setCreatedDate(new Timestamp(Calendar.getInstance().getTime().getTime()));

			try {
				Date fechaOriginal = UtilTime.parseStringToDate(fechaFacturaOriginal, "yyyy-MM-dd'T'HH:mm:ss");
				invoice.setOriginalDate(new Timestamp(fechaOriginal.getTime()));
			}catch (Exception e){
				log.warn("No se puedo convertir el formato de la fecha",e);
				invoice.setOriginalDate(invoice.getCreatedDate());
			}

			EvidenceType evidenceType = evidenceTypeRepository.findByName("XML");
			invoice.setEvidenceType(evidenceType);

			Invoice invoiceExist = invoiceRepository.findByEvidenceTypeAndUuid(evidenceType,uuid);
			if (invoiceExist != null){
				log.warn("Ya se encuntra registrada esta factura: " + evidenceType.getName() + " - " + invoiceExist.getUuid());
				return "Ya se encuentra registrada esta factura";
			}
			User user=userRepository.findOne(236L);
			Client client=clientRepository.findOne(3L);
			invoice.setUser(user);
			invoice.setClient(client);
			invoice.setMessageId("20");
			invoice.setXmlStorageSystem("cronevicuna200");
			invoice = invoiceRepository.saveAndFlush(invoice);
			final Long invoiceId=invoice.getId();

			List<Node> nodes = IntStream.range(0, nodeConcept.getChildNodes().getLength())
					.mapToObj(nodeConcept.getChildNodes()::item)
					.filter(node -> node.getNodeName().equals("cfdi:Concepto"))
					.collect(Collectors.toList());

			for (Node n : nodes) {
				InvoiceConcept invoiceConcept=new InvoiceConcept();
				Element element = (Element) n;
				Map<String, String> attributes = getAttributesNode(element.getAttributes());

				if (attributes.get("claveprodserv") != null)
					invoiceConcept.setProductServiceKey(element.getAttribute(attributes.get("claveprodserv"))==null || element.getAttribute(attributes.get("claveprodserv")).isEmpty()?"":element.getAttribute(attributes.get("claveprodserv")));
				if (attributes.get("noidentificacion") != null)
					invoiceConcept.setIdentificationNumber(element.getAttribute(attributes.get("noidentificacion"))==null || element.getAttribute(attributes.get("noidentificacion")).isEmpty()?"":element.getAttribute(attributes.get("noidentificacion")));
				if (attributes.get("claveunidad") != null)
					invoiceConcept.setUnitKey(element.getAttribute(attributes.get("claveunidad"))==null || element.getAttribute(attributes.get("claveunidad")).isEmpty()?"":element.getAttribute(attributes.get("claveunidad")));
				if (attributes.get("unidad") != null)
					invoiceConcept.setUnit(element.getAttribute(attributes.get("unidad"))==null || element.getAttribute(attributes.get("unidad")).isEmpty()?"":element.getAttribute(attributes.get("unidad")));
				if (attributes.get("descripcion") != null)
					invoiceConcept.setDescription(element.getAttribute(attributes.get("descripcion"))==null || element.getAttribute(attributes.get("descripcion")).isEmpty()?"":element.getAttribute(attributes.get("descripcion")));
				if (attributes.get("cantidad") != null)
					invoiceConcept.setQuantity(element.getAttribute(attributes.get("cantidad"))==null || element.getAttribute(attributes.get("cantidad")).isEmpty()?"":element.getAttribute(attributes.get("cantidad")));
				if (attributes.get("valorunitario") != null)
					invoiceConcept.setUnitValue(element.getAttribute(attributes.get("cantidad"))==null || element.getAttribute(attributes.get("cantidad")).isEmpty()?"":element.getAttribute(attributes.get("cantidad")));
				if (attributes.get("importe") != null)
					invoiceConcept.setAmount(element.getAttribute(attributes.get("importe"))==null || element.getAttribute(attributes.get("importe")).isEmpty()?"":element.getAttribute(attributes.get("importe")));

				invoiceConcept.setInvoiceId(invoice);
				invoiceConcept = invoiceConceptRepository.saveAndFlush(invoiceConcept);

				if(n.getChildNodes().getLength()>0){
					List<Node> nodesImpuestos=IntStream.range(0, n.getChildNodes().getLength())
							.mapToObj(n.getChildNodes()::item)
							.filter(n1 -> n1.getNodeName().equals("cfdi:Impuestos"))
							.collect(Collectors.toList());

					if(nodesImpuestos.size()==1){
						List<Node> nodeTraslados=IntStream.range(0, nodesImpuestos.get(0).getChildNodes().getLength())
								.mapToObj(nodesImpuestos.get(0).getChildNodes()::item)
								.filter(n1 -> n1.getNodeName().equals("cfdi:Traslados"))
								.collect(Collectors.toList());

						if(nodeTraslados.size()==1){
							List<Node> nodeTraslado=IntStream.range(0, nodeTraslados.get(0).getChildNodes().getLength())
									.mapToObj(nodeTraslados.get(0).getChildNodes()::item)
									.filter(n1 -> n1.getNodeName().equals("cfdi:Traslado"))
									.collect(Collectors.toList());
							for(Node t: nodeTraslado) {
								ConceptTax conceptTax=new ConceptTax();
								Element elementTraslado = (Element) t;
								Map<String, String> attributesTraslado = getAttributesNode(elementTraslado.getAttributes());
								if (attributesTraslado.get("impuesto") != null)
									conceptTax.setTax(elementTraslado.getAttribute(attributesTraslado.get("impuesto"))==null || elementTraslado.getAttribute(attributesTraslado.get("impuesto")).isEmpty()?"":elementTraslado.getAttribute(attributesTraslado.get("impuesto")));
								if (attributesTraslado.get("tipofactor") != null)
									conceptTax.setFactorType(elementTraslado.getAttribute(attributesTraslado.get("tipofactor"))==null || elementTraslado.getAttribute(attributesTraslado.get("tipofactor")).isEmpty()?"":elementTraslado.getAttribute(attributesTraslado.get("tipofactor")));
								if (attributesTraslado.get("base") != null)
									conceptTax.setBase(elementTraslado.getAttribute(attributesTraslado.get("base"))==null || elementTraslado.getAttribute(attributesTraslado.get("base")).isEmpty()?"":elementTraslado.getAttribute(attributesTraslado.get("base")));
								if (attributesTraslado.get("tasaocuota") != null)
									conceptTax.setFee(elementTraslado.getAttribute(attributesTraslado.get("tasaocuota"))==null || elementTraslado.getAttribute(attributesTraslado.get("tasaocuota")).isEmpty()?"":elementTraslado.getAttribute(attributesTraslado.get("tasaocuota")));
								if (attributesTraslado.get("importe") != null)
									conceptTax.setAmount(elementTraslado.getAttribute(attributesTraslado.get("importe"))==null || elementTraslado.getAttribute(attributesTraslado.get("importe")).isEmpty()?"":elementTraslado.getAttribute(attributesTraslado.get("importe")));

								conceptTax.setInvoiceConceptId(invoiceConcept);
								conceptTaxRepository.save(conceptTax);
							}

						}
					}
				}
			}

			return invoice.getId().toString();
		} catch (Exception ex){
			log.error("Error al procesar el xml", ex);
		}
		return null;
	}
	public String proccessXML(InvoiceAutho invoiceAutho, String name) throws Exception {

		log.info("Processing as xml: " + invoiceAutho.getName() + ", file type:" + invoiceAutho.getExtension());

		try {
			Invoice invoice = new Invoice();
			InputStream inputStream = invoiceAutho.getInputStreamFile();

			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = docFactory.newDocumentBuilder();
			Document doc = builder.parse(inputStream);

			doc.getDocumentElement().normalize();
			XPath xPath =  XPathFactory.newInstance().newXPath();

			NodeList nodeComprobante = (NodeList) xPath.compile("Comprobante").evaluate(doc, XPathConstants.NODESET);
			NodeList nodeEmisor = (NodeList) xPath.compile("/Comprobante/Emisor").evaluate(doc, XPathConstants.NODESET);
			NodeList nodeReceptor = (NodeList) xPath.compile("/Comprobante/Receptor").evaluate(doc, XPathConstants.NODESET);
			Node nodeConcept = (Node) xPath.compile("/Comprobante/Conceptos").evaluate(doc, XPathConstants.NODE);
			NodeList nodeImpuestos = (NodeList) xPath.compile("/Comprobante/Impuestos/Traslados/Traslado").evaluate(doc, XPathConstants.NODESET);
			NodeList nodeComplemento = (NodeList) xPath.compile("/Comprobante/Complemento/TimbreFiscalDigital").evaluate(doc, XPathConstants.NODESET);
			//NodeList nodeAddenda = (NodeList) xPath.compile("/Comprobante/Addenda").evaluate(doc, XPathConstants.NODESET);
			NodeList nodeIsh = (NodeList) xPath.compile("/Comprobante/Complemento/ImpuestosLocales/TrasladosLocales").evaluate(doc, XPathConstants.NODESET);
			NodeList nodeAerolineas = (NodeList) xPath.compile("/Comprobante/Complemento/Aerolineas").evaluate(doc, XPathConstants.NODESET);

			NodeList nodeDomicilioFiscalEmisor = (NodeList) xPath.compile("/Comprobante/Emisor/DomicilioFiscal").evaluate(doc, XPathConstants.NODESET);
			NodeList nodeExpedidoEnEmisor = (NodeList) xPath.compile("/Comprobante/Emisor/ExpedidoEn").evaluate(doc, XPathConstants.NODESET);

			String nombreEmisor = null, rfcReceptor =  null, rfcEmisor = null , direccionFiscal = null;
			String direccionExpedicion = null , fechaFactura = null , lugarExpedicion = null;
			String subtotal = null, impuestoTotal = null, total = null, uuid = null;
			String folio = null, moneda = null, nombreReceptor = null, fechaFacturaOriginal=null;

			if (nodeComprobante == null || nodeComprobante.getLength() < 1
					|| nodeEmisor == null || nodeEmisor.getLength() < 1
					|| nodeReceptor == null || nodeReceptor.getLength() < 1
					//|| nodeImpuestos == null || nodeImpuestos.getLength() < 1
					|| nodeComplemento == null || nodeComplemento.getLength() < 1){
				if (!invoiceAutho.getAssociatSpending())
					throw new Exception("Factura con estructura no valida");
				else{
					log.error("Factura con estructura no valida: ");
					return "La estructura de la factura " + name +
							" no es v&aacute;lida. Contacta al emisor de la factura y solicita que reemita el documento.";
				}
			}
			if (nodeComprobante != null && nodeComprobante.getLength() > 0){
				for (int i = 0; i < nodeComprobante.getLength(); i++) {
					Node node = nodeComprobante.item(i);
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						Element element = (Element) node;
						Map<String, String> attributes = getAttributesNode(element.getAttributes());
						if (attributes.get("fecha") != null)
							fechaFacturaOriginal = element.getAttribute(attributes.get("fecha"));
						fechaFactura = fechaFacturaOriginal.split("T")[0];
						if (attributes.get("subtotal") != null)
							subtotal = element.getAttribute(attributes.get("subtotal"));
						if (attributes.get("total") != null)
							total = element.getAttribute(attributes.get("total"));
						if (attributes.get("lugarexpedicion") != null)
							lugarExpedicion = element.getAttribute(attributes.get("lugarexpedicion"));
						if (attributes.get("folio") != null)
							folio = element.getAttribute(attributes.get("folio"));
						if (attributes.get("moneda") != null)
							moneda = element.getAttribute(attributes.get("moneda"));
					}
				}
			}

			if (nodeEmisor != null && nodeEmisor.getLength() > 0){
				for (int i = 0; i < nodeEmisor.getLength(); i++) {
					Node node = nodeEmisor.item(i);
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						Element element = (Element) node;
						Map<String, String> attributes = getAttributesNode(element.getAttributes());
						if (attributes.get("nombre") != null)
							nombreEmisor = element.getAttribute(attributes.get("nombre"));
						if (attributes.get("rfc") != null)
							rfcEmisor = element.getAttribute(attributes.get("rfc"));
					}
				}
			}

			if (nodeReceptor != null && nodeReceptor.getLength() > 0){
				for (int i = 0; i < nodeReceptor.getLength(); i++) {
					Node node = nodeReceptor.item(i);
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						Element element = (Element) node;
						Map<String, String> attributes = getAttributesNode(element.getAttributes());
						if (attributes.get("rfc") != null)
							rfcReceptor = element.getAttribute(attributes.get("rfc"));
						if (attributes.get("nombre") != null)
							nombreReceptor = element.getAttribute(attributes.get("nombre"));
					}
				}
			}

			if (nodeDomicilioFiscalEmisor != null && nodeDomicilioFiscalEmisor.getLength() > 0){
				for (int i = 0; i < nodeDomicilioFiscalEmisor.getLength(); i++) {
					Node node = nodeDomicilioFiscalEmisor.item(i);
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						Element element = (Element) node;
						Map<String, String> attributes = getAttributesNode(element.getAttributes());

						StringBuilder address = new StringBuilder();
						if (attributes.get("calle") != null)
							address.append(element.getAttribute(attributes.get("calle")) + " ");
						if (attributes.get("noexterior") != null)
							address.append(element.getAttribute(attributes.get("noexterior")) + " ");
						if (attributes.get("colonia") != null)
							address.append(element.getAttribute(attributes.get("colonia")) + " ");
						if (attributes.get("localidad") != null)
							address.append(element.getAttribute(attributes.get("localidad")) + " ");
						if (attributes.get("municipio") != null)
							address.append(element.getAttribute(attributes.get("municipio")) + " ");
						if (attributes.get("estado") != null)
							address.append(element.getAttribute(attributes.get("estado")) + " ");
						if (attributes.get("pais") != null)
							address.append(element.getAttribute(attributes.get("pais")) + " ");
						if (attributes.get("codigopostal") != null)
							address.append(element.getAttribute(attributes.get("codigopostal")));

						direccionFiscal = address.toString();
					}
				}

			}

			if (nodeExpedidoEnEmisor != null && nodeExpedidoEnEmisor.getLength() > 0){
				for (int i = 0; i < nodeExpedidoEnEmisor.getLength(); i++) {
					Node node = nodeExpedidoEnEmisor.item(i);
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						Element element = (Element) node;
						Map<String, String> attributes = getAttributesNode(element.getAttributes());

						StringBuilder address = new StringBuilder();
						if (attributes.get("calle") != null)
							address.append(element.getAttribute(attributes.get("calle")) + " ");
						if (attributes.get("noexterior") != null)
							address.append(element.getAttribute(attributes.get("noexterior")) + " ");
						if (attributes.get("colonia") != null)
							address.append(element.getAttribute(attributes.get("colonia")) + " ");
						if (attributes.get("localidad") != null)
							address.append(element.getAttribute(attributes.get("localidad")) + " ");
						if (attributes.get("municipio") != null)
							address.append(element.getAttribute(attributes.get("municipio")) + " ");
						if (attributes.get("estado") != null)
							address.append(element.getAttribute(attributes.get("estado")) + " ");
						if (attributes.get("pais") != null)
							address.append(element.getAttribute(attributes.get("pais")) + " ");
						if (attributes.get("codigopostal") != null)
							address.append(element.getAttribute(attributes.get("codigopostal")));

						direccionExpedicion = address.toString();
					}
				}
			}

			Double ivaTotal = 0.0;
			Double ieps = 0.0;
			if (nodeImpuestos != null && nodeImpuestos.getLength() > 0) {
				for (int i = 0; i < nodeImpuestos.getLength(); i++) {
					Node node = nodeImpuestos.item(i);
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						Element element = (Element) node;
						Map<String, String> attributes = getAttributesNode(element.getAttributes());
						String tipoImpuesto = element.getAttribute(attributes.get("impuesto"));
						if (tipoImpuesto.equalsIgnoreCase("002")) {
							ivaTotal += element.getAttribute(attributes.get("importe")) != null ? Double.parseDouble(element.getAttribute(attributes.get("importe"))) : 0.0;
						} else if (tipoImpuesto.equalsIgnoreCase("003")) {
							ieps += element.getAttribute(attributes.get("importe")) != null ? Double.parseDouble(element.getAttribute(attributes.get("importe"))) : 0.0;
						}
					}
				}
			}

			if (nodeComplemento != null && nodeComplemento.getLength() > 0){
				for (int i = 0; i < nodeComplemento.getLength(); i++) {
					Node node = nodeComplemento.item(i);
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						Element element = (Element) node;
						Map<String, String> attributes = getAttributesNode(element.getAttributes());
						uuid = element.getAttribute(attributes.get("uuid")).toUpperCase();
					}
				}
			}


			Double ish = 0.0;
			Double tua = 0.0;
			if (nodeIsh != null && nodeIsh.getLength() > 0){
				for (int i = 0; i < nodeIsh.getLength(); i++) {
					Node node = nodeIsh.item(i);
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						Element element = (Element) node;
						Map<String, String> attributes = getAttributesNode(element.getAttributes());
						String impuestoLocal = element.getAttribute(attributes.get("imploctrasladado")).toUpperCase();
						if(impuestoLocal!=null && !impuestoLocal.isEmpty() && (impuestoLocal.toUpperCase().trim().equalsIgnoreCase("ISH")
								|| impuestoLocal.toUpperCase().trim().equalsIgnoreCase("IMPUESTO SOBRE HOSPEDAJE"))){
							ish += element.getAttribute(attributes.get("importe")) != null ? Double.parseDouble(element.getAttribute(attributes.get("importe"))) : 0.0;
						}
					}
				}
			}

			if (nodeAerolineas != null && nodeAerolineas.getLength() > 0){
				for (int i = 0; i < nodeAerolineas.getLength(); i++) {
					Node node = nodeAerolineas.item(i);
					if (node.getNodeType() == Node.ELEMENT_NODE) {
						Element element = (Element) node;
						Map<String, String> attributes = getAttributesNode(element.getAttributes());
						tua += element.getAttribute(attributes.get("tua")) != null ? Double.parseDouble(element.getAttribute(attributes.get("tua"))) : 0.0;
					}
				}
			}

			int count = 0;
			int maxTries = 2;
			boolean continuar=Boolean.TRUE;

			invoice.setSatVerification(false);

			do{
				try {
					count++;
					Acuse acuse = cfdiSatConsumeService.consulta(rfcEmisor,rfcReceptor,total,uuid);
					log.info("Factura :::::>>>> " + acuse.getEstado().getValue());
					log.info("Factura :::::>>>> " + acuse.getCodigoEstatus().getValue());
					if (acuse.getCodigoEstatus().getValue().toUpperCase().contains("S - COMPROBANTE OBTENIDO SATISFACTORIAMENTE") && acuse.getEstado().getValue().toLowerCase().equalsIgnoreCase("vigente")) {
						invoice.setSatVerification(true);
					}
					continuar=Boolean.FALSE;
				}catch(Exception ex){
					if (count == maxTries){
						log.error("Hubo un problema al verificar factura en SAT", ex);
						continuar=Boolean.FALSE;
					}
				}
			}while (continuar);

			if(!invoice.getSatVerification()){
				return "Error al validar la factura " + name +
						" con el SAT. Aunque puede ser una falla en el servicio del SAT, valida el certificado de la factura en https://verificacfdi.facturaelectronica.sat.gob.mx/";
			}

			invoice.setEstablishment(nombreEmisor);
			invoice.setDateInvoice(fechaFactura);
			invoice.setExpeditionAddress(direccionExpedicion != null ? direccionExpedicion : lugarExpedicion);
			invoice.setFiscalAddress(direccionFiscal);
			invoice.setRfcReceiver(rfcReceptor);
			invoice.setRfcTransmitter(rfcEmisor);
			invoice.setReceiverName(nombreReceptor);
			invoice.setSubtotal(subtotal);
			invoice.setTotal(total);
			invoice.setUuid(uuid.toUpperCase());
			invoice.setIva(ivaTotal > 0 ? String.valueOf(ivaTotal) : null);
			invoice.setIeps(ieps > 0 ? String.valueOf(ieps) : null);
			invoice.setFolio(folio);
			invoice.setCurrency(moneda);
			invoice.setIsh(ish > 0 ? String.valueOf(ish) : null);
			invoice.setTua(tua > 0 ? String.valueOf(tua) : null);
			invoice.setCreatedDate(new Timestamp(Calendar.getInstance().getTime().getTime()));

			try {
				Date fechaOriginal = UtilTime.parseStringToDate(fechaFacturaOriginal, "yyyy-MM-dd'T'HH:mm:ss");
				invoice.setOriginalDate(new Timestamp(fechaOriginal.getTime()));
			}catch (Exception e){
				log.warn("No se puedo convertir el formato de la fecha",e);
				invoice.setOriginalDate(invoice.getCreatedDate());
			}

			EvidenceType evidenceType = evidenceTypeRepository.findByName("XML");
			invoice.setEvidenceType(evidenceType);

			Invoice invoiceExist = invoiceRepository.findByEvidenceTypeAndUuid(evidenceType,uuid);
			if (invoiceExist != null){
				log.warn("Ya se encuntra registrada esta factura: " + evidenceType.getName() + " - " + invoiceExist.getUuid());
				if (!invoiceAutho.getAssociatSpending())
					throw new ServiceException("Ya se encuentra registrada esta factura");
				else{
					return "La factura " + name +
							" est&aacute; duplicada. Esta factura ya se encuentra en tu buz&oacute;n de facturas, por favor ay&uacute;danos a validar.";
				}
			}

			invoice.setUser(invoiceAutho.getUser());
			invoice.setClient(invoiceAutho.getClient());
			invoice.setMessageId(invoiceAutho.getMessageId());
			invoice.setXmlStorageSystem(invoiceAutho.getName());
			invoice.setAssociated(Boolean.FALSE);
			invoice = invoiceRepository.saveAndFlush(invoice);

			List<Node> nodesISH = IntStream.range(0, nodeIsh.getLength())
					.mapToObj(nodeIsh::item)
					.collect(Collectors.toList());

			ConceptTax conceptTaxISH=nodesISH!=null && nodeIsh.getLength()>=1 ? new ConceptTax() : null;
			nodesISH.forEach(n-> {
				Element element = (Element) n;
				Map<String, String> attributesish = getAttributesNode(element.getAttributes());
				String impuestoLocal = element.getAttribute(attributesish.get("imploctrasladado")).toUpperCase();

				if(impuestoLocal!=null && !impuestoLocal.isEmpty() && (impuestoLocal.trim().equalsIgnoreCase("ISH")
						|| impuestoLocal.trim().equalsIgnoreCase("IMPUESTO SOBRE HOSPEDAJE"))){
					impuestoLocal="ISH";
					conceptTaxISH.setTax(impuestoLocal);
				}

				if (attributesish.get("tasadetraslado") != null){
					conceptTaxISH.setFee(element.getAttribute(attributesish.get("tasadetraslado"))==null || element.getAttribute(attributesish.get("tasadetraslado")).isEmpty()?"":element.getAttribute(attributesish.get("tasadetraslado")));
				}

				if (attributesish.get("importe") != null){
					conceptTaxISH.setAmount(element.getAttribute(attributesish.get("importe"))==null || element.getAttribute(attributesish.get("importe")).isEmpty()?"":element.getAttribute(attributesish.get("importe")));
				}
			});

			List<Node> nodes = IntStream.range(0, nodeConcept.getChildNodes().getLength())
					.mapToObj(nodeConcept.getChildNodes()::item)
					.filter(node -> node.getNodeName().equals("cfdi:Concepto"))
					.collect(Collectors.toList());

			for (Node n : nodes) {
				InvoiceConcept invoiceConcept=new InvoiceConcept();
				Element element = (Element) n;
				Map<String, String> attributes = getAttributesNode(element.getAttributes());

				if (attributes.get("claveprodserv") != null)
					invoiceConcept.setProductServiceKey(element.getAttribute(attributes.get("claveprodserv"))==null || element.getAttribute(attributes.get("claveprodserv")).isEmpty()?"":element.getAttribute(attributes.get("claveprodserv")));
				if (attributes.get("noidentificacion") != null)
					invoiceConcept.setIdentificationNumber(element.getAttribute(attributes.get("noidentificacion"))==null || element.getAttribute(attributes.get("noidentificacion")).isEmpty()?"":element.getAttribute(attributes.get("noidentificacion")));
				if (attributes.get("claveunidad") != null)
					invoiceConcept.setUnitKey(element.getAttribute(attributes.get("claveunidad"))==null || element.getAttribute(attributes.get("claveunidad")).isEmpty()?"":element.getAttribute(attributes.get("claveunidad")));
				if (attributes.get("unidad") != null)
					invoiceConcept.setUnit(element.getAttribute(attributes.get("unidad"))==null || element.getAttribute(attributes.get("unidad")).isEmpty()?"":element.getAttribute(attributes.get("unidad")));
				if (attributes.get("descripcion") != null)
					invoiceConcept.setDescription(element.getAttribute(attributes.get("descripcion"))==null || element.getAttribute(attributes.get("descripcion")).isEmpty()?"":element.getAttribute(attributes.get("descripcion")));
				if (attributes.get("cantidad") != null)
					invoiceConcept.setQuantity(element.getAttribute(attributes.get("cantidad"))==null || element.getAttribute(attributes.get("cantidad")).isEmpty()?"":element.getAttribute(attributes.get("cantidad")));
				if (attributes.get("valorunitario") != null)
					invoiceConcept.setUnitValue(element.getAttribute(attributes.get("cantidad"))==null || element.getAttribute(attributes.get("cantidad")).isEmpty()?"":element.getAttribute(attributes.get("cantidad")));
				if (attributes.get("importe") != null)
					invoiceConcept.setAmount(element.getAttribute(attributes.get("importe"))==null || element.getAttribute(attributes.get("importe")).isEmpty()?"":element.getAttribute(attributes.get("importe")));

				invoiceConcept.setInvoiceId(invoice);
				invoiceConcept = invoiceConceptRepository.saveAndFlush(invoiceConcept);

				if(conceptTaxISH !=null){
					Double factorPorcentaje=Double.parseDouble(conceptTaxISH.getFee());
					if(factorPorcentaje>0){
						factorPorcentaje/=100;
					}
					Double importeCalculado=Double.parseDouble(invoiceConcept.getAmount()) * factorPorcentaje;
					Double importeTax=Double.parseDouble(conceptTaxISH.getAmount());
					Double diference=importeTax - importeCalculado;

					if(nodes.size()==1){
						conceptTaxISH.setInvoiceConceptId(invoiceConcept);
					}else if(diference <.5){
						log.info("importeTax: "+importeTax);
						log.info("importeCalculado: "+importeCalculado);
						log.info("diference: "+diference);
						conceptTaxISH.setInvoiceConceptId(invoiceConcept);
					}
				}

				if(n.getChildNodes().getLength()>0){
					List<Node> nodesImpuestos=IntStream.range(0, n.getChildNodes().getLength())
							.mapToObj(n.getChildNodes()::item)
							.filter(n1 -> n1.getNodeName().equals("cfdi:Impuestos"))
							.collect(Collectors.toList());

					if(nodesImpuestos.size()==1){
						List<Node> nodeTraslados=IntStream.range(0, nodesImpuestos.get(0).getChildNodes().getLength())
								.mapToObj(nodesImpuestos.get(0).getChildNodes()::item)
								.filter(n1 -> n1.getNodeName().equals("cfdi:Traslados"))
								.collect(Collectors.toList());

						if(nodeTraslados.size()==1){
							List<Node> nodeTraslado=IntStream.range(0, nodeTraslados.get(0).getChildNodes().getLength())
									.mapToObj(nodeTraslados.get(0).getChildNodes()::item)
									.filter(n1 -> n1.getNodeName().equals("cfdi:Traslado"))
									.collect(Collectors.toList());
							for(Node t : nodeTraslado) {
								ConceptTax conceptTax=new ConceptTax();
								Element elementTraslado = (Element) t;
								Map<String, String> attributesTraslado = getAttributesNode(elementTraslado.getAttributes());
								if (attributesTraslado.get("impuesto") != null)
									conceptTax.setTax(elementTraslado.getAttribute(attributesTraslado.get("impuesto"))==null || elementTraslado.getAttribute(attributesTraslado.get("impuesto")).isEmpty()?"":elementTraslado.getAttribute(attributesTraslado.get("impuesto")));
								if (attributesTraslado.get("tipofactor") != null)
									conceptTax.setFactorType(elementTraslado.getAttribute(attributesTraslado.get("tipofactor"))==null || elementTraslado.getAttribute(attributesTraslado.get("tipofactor")).isEmpty()?"":elementTraslado.getAttribute(attributesTraslado.get("tipofactor")));
								if (attributesTraslado.get("base") != null)
									conceptTax.setBase(elementTraslado.getAttribute(attributesTraslado.get("base"))==null || elementTraslado.getAttribute(attributesTraslado.get("base")).isEmpty()?"":elementTraslado.getAttribute(attributesTraslado.get("base")));
								if (attributesTraslado.get("tasaocuota") != null)
									conceptTax.setFee(elementTraslado.getAttribute(attributesTraslado.get("tasaocuota"))==null || elementTraslado.getAttribute(attributesTraslado.get("tasaocuota")).isEmpty()?"":elementTraslado.getAttribute(attributesTraslado.get("tasaocuota")));
								if (attributesTraslado.get("importe") != null)
									conceptTax.setAmount(elementTraslado.getAttribute(attributesTraslado.get("importe"))==null || elementTraslado.getAttribute(attributesTraslado.get("importe")).isEmpty()?"":elementTraslado.getAttribute(attributesTraslado.get("importe")));

								conceptTax.setInvoiceConceptId(invoiceConcept);
								conceptTaxRepository.save(conceptTax);
							}


						}
					}
				}
			}

			if (invoiceAutho.getAssociatSpending()){
				List<Spending> spendingsUser = spendingRepository.findByUserAndInvoiceIsNull(invoiceAutho.getUser());
				log.info("Gastos sin asociar: " + spendingsUser.size());
				if (spendingsUser != null && !spendingsUser.isEmpty()){
					associateInvoceSpending(spendingsUser, invoice);
				}
			}

			return invoice.getId().toString();
		} catch (Exception ex){
			log.error("Error al procesar el xml", ex);
			if (!invoiceAutho.getAssociatSpending())
				throw new ServiceException("Error al procesar el xml", ex);
		}
		return null;
	}

	public void associateInvoceSpending(List<Spending> spendingsUser, Invoice invoice) throws Exception {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date invoiceDate = sdf.parse(invoice.getDateInvoice());

		for (Spending spending: spendingsUser) {

			if(spending.getTransaction() != null
					&& spending.getDateStart() != null
					&& spending.getDateStart() != null
					&& spending.getSpendingTotal() != null) {

				String formatDateSpending = sdf.format(spending.getDateStart());
				Date dateSpending = sdf.parse(formatDateSpending);

				InvoiceSpendingAssociate invoiceSpendingAssociate = new InvoiceSpendingAssociate();

				invoiceSpendingAssociate.setDate(new Date());
				invoiceSpendingAssociate.setInvoice(invoice);
				invoiceSpendingAssociate.setSpending(spending);

				invoiceSpendingAssociate.setInvoiceAmount(Double.parseDouble(invoice.getTotal()));
				invoiceSpendingAssociate.setSpendingAmount(spending.getSpendingTotal());
				log.info("Total factura: " + invoiceSpendingAssociate.getInvoiceAmount());
				log.info("Total gasto: " + invoiceSpendingAssociate.getSpendingAmount());

				invoiceSpendingAssociate.setInvoiceRfc(invoice.getRfcTransmitter());
				invoiceSpendingAssociate.setSpendingRfc(spending.getTransaction().getRfcComercio());
				log.info("RFC factura: " + invoiceSpendingAssociate.getInvoiceRfc());
				log.info("RFC gasto: " + invoiceSpendingAssociate.getSpendingRfc());

				invoiceSpendingAssociate.setInvoiceDate(invoiceDate);
				invoiceSpendingAssociate.setSpendingDate(dateSpending);
				log.info("Fecha factura: " + invoiceSpendingAssociate.getInvoiceDate());
				log.info("Fecha gasto: " + invoiceSpendingAssociate.getSpendingDate());

				int result = 0;

				//Regla 1:
				if (invoiceSpendingAssociate.getInvoiceAmount().equals(invoiceSpendingAssociate.getSpendingAmount())) {
					result++;
					invoiceSpendingAssociate.setAmountValidation(Boolean.TRUE);
				} else {
					invoiceSpendingAssociate.setAmountValidation(Boolean.FALSE);
				}

				//Regla 2:
				if (invoiceSpendingAssociate.getInvoiceRfc().equals(invoiceSpendingAssociate.getSpendingRfc())) {
					result++;
					invoiceSpendingAssociate.setRfcValidation(Boolean.TRUE);
				} else {
					invoiceSpendingAssociate.setRfcValidation(Boolean.FALSE);
				}

				//Regla 3:
				LocalDate aux = invoiceSpendingAssociate.getInvoiceDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
				int invoiceMonth = aux.getMonthValue();
				LocalDate aux2 = invoiceSpendingAssociate.getSpendingDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
				int spendingMonth = aux2.getMonthValue();
				if (invoiceMonth == spendingMonth) {
					result++;
					invoiceSpendingAssociate.setDateValidation(Boolean.TRUE);
				} else {
					invoiceSpendingAssociate.setDateValidation(Boolean.FALSE);
				}

				if(result == 3) {
					invoice.setSpending(spending);
//					invoiceRepository.saveAndFlush(invoice);
					spending.setInvoice(invoice);
//					spendingRepository.saveAndFlush(spending);
					invoiceSpendingAssociate.setAssociate(Boolean.TRUE);
				} else {
					invoiceSpendingAssociate.setAssociate(Boolean.FALSE);
				}
				if (invoice.getInvoiceSpendingAssociateList() != null) {
					invoice.getInvoiceSpendingAssociateList().add(invoiceSpendingAssociate);
				} else {
					List<InvoiceSpendingAssociate> invoiceSpendingAssociateList = new ArrayList<>();
					invoiceSpendingAssociateList.add(invoiceSpendingAssociate);
					invoice.setInvoiceSpendingAssociateList(invoiceSpendingAssociateList);
				}
				invoiceRepository.save(invoice);
//				invoiceSpendingAssociateRepository.saveAndFlush(invoiceSpendingAssociate);

			} else {
				log.error("El gasto no cuenta con la información necesaria para ser procesado, spending id:" + spending.getId());
			}
		}
	}

	public String proccessPDF(InvoiceAutho invoiceAutho, Integer attachments, String name) throws Exception{

		log.info("Processing as pdf: " + invoiceAutho.getName() + ", file type:" + invoiceAutho.getExtension());

		try (InputStream inputStream = invoiceAutho.getInputStreamFile()) {

			try (PDDocument document = PDDocument.load(inputStream)) {

				if (!document.isEncrypted()) {

					PDFTextStripperByArea stripper = new PDFTextStripperByArea();
					stripper.setSortByPosition(true);
					PDFTextStripper tStripper = new PDFTextStripper();

					String pdfFileInText = tStripper.getText(document);
					log.debug("Text:" + pdfFileInText);

					Invoice invoice = null;

					if(attachments != null && attachments.intValue() > 2) {
						log.info("invoice file pdf name: " + name + ", file within extension:" + FilenameUtils.removeExtension(name) + ", attachments: " + attachments);
						InvoiceFile invoiceFileXml = invoiceFileRepository.findTopByNameIgnoreCaseOrderByIdDesc(FilenameUtils.removeExtension(name)+".xml");
						if( invoiceFileXml != null ) {
							invoice = invoiceRepository.findByMessageIdAndXmlStorageSystem(invoiceAutho.getMessageId(), invoiceFileXml.getAs3Key());
						}
					}else {
						invoice = invoiceRepository.findByMessageIdAndPdfStorageSystemIsNull(invoiceAutho.getMessageId());
					}

					if (invoice == null){
						if (!invoiceAutho.getAssociatSpending()) {
							throw new Exception("No se ha encontrado la factura con el id de mensaje: " + invoiceAutho.getMessageId());
						} else {
							log.error("No se ha encontrado la factura con el id de mensaje: " + invoiceAutho.getMessageId());
						}
					} else {
						invoice.setPdfStorageSystem(invoiceAutho.getName());
						invoice = invoiceRepository.saveAndFlush(invoice);
						return invoice.getId().toString();
					}

				} else {
					if (!invoiceAutho.getAssociatSpending())
						throw new Exception("PDF encriptado, no se puede leer");
					else
						log.error("PDF encriptado, no se puede leer");
				}

			} catch (Exception ex) {
				log.error("Error al procesar el pdf", ex);
			}

		} catch (Exception ex) {
			log.error("Error al procesar el pdf", ex);
			if (!invoiceAutho.getAssociatSpending())
				throw new ServiceException("Error al procesar el pdf", ex);
		}
		return null;
	}

	public void readPDF(String fileName) throws Exception {
		File initialFile = new File("/tmp/mercurio/"+fileName);
		InputStream inputStream = new FileInputStream(initialFile);
		try (PDDocument document = PDDocument.load(inputStream)) {
			log.info(document.getDocumentInformation().getTitle());
			log.info(document.getDocumentInformation().getAuthor());
			log.info(document.getDocumentInformation().getSubject());
			log.info(document.getDocumentInformation().getCreationDate());
			log.info(document.getNumberOfPages());
		} catch (Exception ex) {
			log.error("Error al procesar el pdf", ex);
		}
	}

	public byte[] getInvoice(Long invoiceId, String ext) throws Exception {
		log.info("getInvoice: "+invoiceId);
		try {

			Invoice invoice = invoiceRepository.findOne(invoiceId);

			if(ext.equalsIgnoreCase("pdf")){
				log.info("Downloading from S3: bucket: " + BUCKET + "id: " + invoice.getPdfStorageSystem());
				return s3Service.download(BUCKET, invoice.getPdfStorageSystem());
			} else {
				log.info("Downloading from S3: bucket: " + BUCKET + "id: " + invoice.getPdfStorageSystem());
				return s3Service.download(BUCKET, invoice.getXmlStorageSystem());
			}

		} catch (Exception e) {
			throw new ServiceException("Error al consultar evidencia", e);
		}
	}

	@Override
	public byte[] getEvidenceFile(Long evidenceId) throws Exception {
		log.info("getEvidence: "+evidenceId);
		try {

			ImageEvidenceDTO imageEvidenceDTO = UtilBean.spendingEvidenceToSpendingEvidenceDTO(imageEvidenceRepository.findOne(evidenceId));
			log.info("Downloading from S3: bucket: " + BUCKET + "id: " + imageEvidenceDTO.getNameStorage());
			return s3Service.download(BUCKET, imageEvidenceDTO.getNameStorage());

		} catch (Exception e) {
			throw new ServiceException("Error al consultar evidencia", e);
		}
	}

}

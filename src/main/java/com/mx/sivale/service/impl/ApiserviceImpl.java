package com.mx.sivale.service.impl;

import com.mx.sivale.model.dto.AppInfoDTO;
import com.mx.sivale.service.ApiService;
import com.mx.sivale.service.exception.ServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class ApiserviceImpl implements ApiService{

	private static Logger log = Logger.getLogger(ApiserviceImpl.class);
	
	@Value("${environment.profile.name}")
	private String profileEnvironmet;

	@Value("${version}")
	private String version;

	@Override
	public AppInfoDTO getAppVersion() throws ServiceException{
		AppInfoDTO appInfoDTO = new AppInfoDTO();
		appInfoDTO.setVersion(version);
		return appInfoDTO;
	}

	@Override
	public String getAppProfle() throws ServiceException {
		log.info("PROFILE ::::::>>> #####################################################################");
		log.info(System.getProperty("spring.profiles.active"));
		
		return System.getProperty("spring.profiles.active");
	}

	@Override
	public String getAppEnvironment() throws ServiceException {
		log.info("ENVIRONMENT ::::::>>> #####################################################################");
		log.info(profileEnvironmet);
		return profileEnvironmet;
	}
}

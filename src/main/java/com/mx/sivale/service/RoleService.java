package com.mx.sivale.service;

import com.mx.sivale.model.Role;
import com.mx.sivale.model.UserClient;

public interface RoleService {

    Role getCurrentRole() throws Exception;

    Boolean isCurrentClientAdmin(UserClient userClient, String origin) throws Exception;
}

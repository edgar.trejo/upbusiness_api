package com.mx.sivale.service;

import org.mx.wsdl.sat.consulta.Acuse;

public interface CfdiSatConsumeService {

    Acuse consulta(String re, String rr, String monto, String id);
}

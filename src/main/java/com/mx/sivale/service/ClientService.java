package com.mx.sivale.service;

import com.mx.sivale.model.Client;
import com.mx.sivale.model.dto.ClientDTO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ClientService {
	
	ClientDTO saveLogoClient(MultipartFile file, String dateCreated) throws Exception;

	Client getCurrentClient() throws Exception;

	Long getCurrentClientId() throws Exception;

	Client getClientById(Long clientId);

	List<Client> getAll();

}

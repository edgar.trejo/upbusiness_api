package com.mx.sivale.service;

import com.mx.sivale.model.CostCenter;

import io.swagger.annotations.ApiOperation;
import jxl.write.WritableWorkbook;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

public interface CostCenterService {

    CostCenter create(CostCenter costCenter) throws Exception;

    CostCenter update(CostCenter costCenter) throws Exception;

    void remove(Long id) throws Exception;

    List<CostCenter> findAll() throws Exception;

    List<CostCenter> findByClientId() throws Exception;

    CostCenter findOne(Long id) throws Exception;

    List<CostCenter> searchByName(String name) throws Exception;
    
    int createMassive(List<CostCenter> lCostCenter) throws Exception;
    
    WritableWorkbook createMassiveOutputExcel(HttpServletResponse response, MultipartFile file) throws Exception;

}

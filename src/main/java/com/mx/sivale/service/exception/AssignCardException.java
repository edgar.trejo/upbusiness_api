package com.mx.sivale.service.exception;

import org.apache.log4j.Logger;

public class AssignCardException extends Exception {

    public AssignCardException(String message) {
        super(message);
    }
}

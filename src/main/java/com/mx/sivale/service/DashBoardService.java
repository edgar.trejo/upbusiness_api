package com.mx.sivale.service;

import com.mx.sivale.model.dto.EventDashBoardDTO;
import com.mx.sivale.model.dto.SpendingDashBoardDTO;
import com.mx.sivale.model.dto.TransferDashBoardDTO;
import com.mx.sivale.service.exception.ServiceException;

import java.util.List;
import java.util.Map;

public interface DashBoardService {

    Map<Integer, Map<Integer, List<EventDashBoardDTO>>> getEvents( ) throws ServiceException;

    Map<Integer, Map<Integer, List<SpendingDashBoardDTO>>> getSpendings() throws ServiceException;

    Map<Integer, Map<Integer, List<TransferDashBoardDTO>>> getTransfers() throws ServiceException;
}

package com.mx.sivale.service;

import com.mx.sivale.model.CustomReportConfig;
import com.mx.sivale.model.dto.CustomReportConfigDTO;
import com.mx.sivale.model.dto.CustomReportDTO;
import com.mx.sivale.service.exception.ServiceException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.LinkedHashMap;
import java.util.List;

public interface CustomReportService {

    CustomReportConfigDTO getCustomReportConfigDefault() throws ServiceException;

    public Page<LinkedHashMap> report(Long from, Long to, Long costCenterId, Long spendingTypeId, Long approvalStatusId,
                                      String columnsOrder, Pageable pageable, long clientId) throws ServiceException;

    List<CustomReportConfigDTO> getCustomReportConfigsByUser(Long userId) throws ServiceException;

    CustomReportConfigDTO saveConfig(CustomReportConfigDTO customReportConfigDTO) throws ServiceException;

    CustomReportConfigDTO updateConfig(CustomReportConfigDTO customReportConfigDTO) throws ServiceException;

    void deleteConfig(Long id) throws ServiceException;

}

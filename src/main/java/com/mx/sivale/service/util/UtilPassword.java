package com.mx.sivale.service.util;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

@Service
public class UtilPassword {

    public String generatePassword(){
        int length = 8;
        return RandomStringUtils.random(length, Boolean.TRUE, Boolean.TRUE);
    }

}

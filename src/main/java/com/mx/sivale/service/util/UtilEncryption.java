package com.mx.sivale.service.util;


import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.util.io.pem.PemReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

@Service
public class UtilEncryption {

    private static final Logger log = LoggerFactory.getLogger(UtilEncryption.class);

    private static final String ENCRYPTION_ALGORITHM = "RSA";

    @Value("${encrypt.key-name}")
    private String rsa;

    public String encryptData(String data) throws Exception {
        byte[] base64EncryptedData = null;

        PublicKey pubKey = readPublicKeyFromFile();
        Cipher cipher;
        try {
            cipher = Cipher.getInstance(ENCRYPTION_ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, pubKey);
            byte[] encryptedData;
            encryptedData = cipher.doFinal(data.getBytes());
            base64EncryptedData = Base64.encodeBase64(encryptedData);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new Exception(e);
        } catch (InvalidKeyException e) {
            throw new Exception(e);
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            throw new Exception(e);
        }

        String encryp = new String(base64EncryptedData);
//        log.info("ENCRYPTE DATA :::::>>> " + encryp);
        return encryp;
    }

    private PublicKey readPublicKeyFromFile() throws Exception {

        PemReader reader = null;
        PublicKey publicKey = null;

        ClassLoader classLoader = getClass().getClassLoader();
        try {
            File file = new File(rsa);
            reader = new PemReader(new FileReader(file));
            byte[] pubKey = reader.readPemObject().getContent();
            X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(pubKey);
            KeyFactory keyFactory = KeyFactory.getInstance(ENCRYPTION_ALGORITHM);
            publicKey = keyFactory.generatePublic(publicKeySpec);
        } catch (FileNotFoundException e) {
            throw new Exception(e);
        } catch (IOException e) {
            throw new Exception(e);
        } catch (NoSuchAlgorithmException e) {
            throw new Exception(e);
        } catch (InvalidKeySpecException e) {
            throw new Exception(e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ex) {
                }
            }
        }

        return publicKey;
    }

    public String simpleEncrypt(String data) throws Exception {
        StandardPasswordEncoder encoder = new StandardPasswordEncoder("secret");
        return encoder.encode(data);
    }

    public static String maskCard(String card){
        if(card != null && !card.isEmpty() && card.length() > 3){
            return "**** **** **** " + card.substring(card.length()-4, card.length());
        }{
            return "";
        }
    }

}

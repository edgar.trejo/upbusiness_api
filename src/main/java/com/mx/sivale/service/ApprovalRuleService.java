package com.mx.sivale.service;

import com.mx.sivale.model.ApprovalRule;

import java.util.List;

/**
 *
 * @author armando.reyna
 *
 */

public interface ApprovalRuleService {

	ApprovalRule create(ApprovalRule approvalRule) throws Exception;

	ApprovalRule update(ApprovalRule approvalRule) throws Exception;

	void remove(Long id) throws Exception;

	ApprovalRule findOne(Long id) throws Exception;

	List<ApprovalRule> findByClientId() throws Exception;

//	boolean validOverlappingTeamsAndAmount(RuleDTO ruleDTO) throws Exception;
    
}

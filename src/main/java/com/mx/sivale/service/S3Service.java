package com.mx.sivale.service;

import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public interface S3Service {

    List<S3ObjectSummary> list();

    PutObjectResult upload(InputStream inputStream, String uploadKey);

    byte[] download(String bucket, String key);

    String downloadAsBase64(String bucket, String key, String fileExt);

    Boolean remove(String bucket, String key);

}

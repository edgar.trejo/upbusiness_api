package com.mx.sivale.service;

import com.mx.sivale.model.dto.BalanceDTO;
import com.mx.sivale.model.dto.CardDTO;
import com.mx.sivale.model.dto.TransactionMovementsDTO;
import com.mx.sivale.service.exception.AssignCardException;
import com.mx.sivale.service.exception.AssignedCardException;
import com.mx.sivale.service.exception.ServiceException;
import ws.sivale.com.mx.messages.response.appgasolina.TypeDatos;
import ws.sivale.com.mx.messages.response.tarjeta.ResponseSaldo;

import java.util.List;

public interface CardService {

	void validateCard(Long numberCard) throws ServiceException, AssignCardException, AssignedCardException;
	
	BalanceDTO getCardBalance(String iut) throws Exception;

	void associateCard(Long numberCard, Long sivaleId) throws Exception;

	String unAssociateCard(Long numberCard, Long sivaleId) throws Exception;
	
	List<CardDTO> findCardsUserByUserId(String userId) throws Exception;

	
	List<CardDTO> findCardsByClient(String clientId, String origin) throws Exception;
	
	TypeDatos getCardData(String iut, String origin) throws Exception;
	
	ResponseSaldo getCardMovements(String cardNumber, Integer numAbonos, Integer numCargos) throws Exception;

	List<TransactionMovementsDTO> findMovementsByIut(String iut) throws Exception;
}

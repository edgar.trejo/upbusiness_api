package com.mx.sivale.service;

import com.mx.sivale.model.Transaction;
import com.mx.sivale.model.Transfer;
import com.mx.sivale.service.exception.ServiceException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ws.sivale.com.mx.messages.types.TypeTransaccionDm;
import ws.sivale.com.mx.messages.types.TypeTransaccionEc;

import java.util.List;

public interface TransactionService {

    List<TypeTransaccionDm> getTransactionsDM(String iut) throws ServiceException;

    List<TypeTransaccionEc> getTransactionsEC(String iut) throws ServiceException;

    List<Transaction> getTransactionsEC(String[] iut) throws ServiceException;

    Page<Transaction> report(long from, long to, Pageable pageable, long clientId) throws Exception;

    void getTransactionsECProcess(String fechaInicio, String fechaFin);

    void getTransactionsECCron();
}

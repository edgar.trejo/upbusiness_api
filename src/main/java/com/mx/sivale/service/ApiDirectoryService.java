package com.mx.sivale.service;

import com.google.api.services.admin.directory.model.User;
import com.mx.sivale.model.UserClient;
import com.mx.sivale.model.dto.SenderEmailDTO;

import java.util.List;

public interface ApiDirectoryService {

    List<User> getUsers() throws Exception;

    com.mx.sivale.model.User createUser(com.mx.sivale.model.User user) throws Exception;

    void deleteUser(UserClient user) throws Exception;

    void deleteUser(String email) throws Exception;

    void consolidateInvoices() throws Exception;

    void sendEmailFiscalData(SenderEmailDTO senderEmailDTO) throws Exception;

    void cron();

    void proccessInvoiceUnassociate(UserClient user) throws Exception;

    void deleteGmailMessage(String email, String id) throws Exception;

}

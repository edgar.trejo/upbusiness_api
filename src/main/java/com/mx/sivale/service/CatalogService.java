package com.mx.sivale.service;

import com.mx.sivale.model.dto.CatalogDTO;

import java.util.List;

/**
 * 
 * @author amartinezmendoza
 *
 */
public interface CatalogService {

    List<CatalogDTO> findAllEntities() throws Exception;
    CatalogDTO findFederativeEntityById(Long id) throws Exception;
	CatalogDTO findFederativeEntityByName(String name) throws Exception;

    List<CatalogDTO> findAllRole() throws Exception;
	CatalogDTO findRoleById(Long id) throws Exception;

    List<CatalogDTO> findAllApprovalStatus() throws Exception;
    CatalogDTO findApprovalStatusById(Long id) throws Exception;

    List<CatalogDTO> findAllPayMethod() throws Exception;
    CatalogDTO findPayMethodById(Long id) throws Exception;

	List<CatalogDTO> findAllEvidenceType() throws Exception;
    CatalogDTO findEvidenceTypeById(Long id) throws Exception;
	CatalogDTO findEvidenceTypeByName(String name) throws Exception;

}

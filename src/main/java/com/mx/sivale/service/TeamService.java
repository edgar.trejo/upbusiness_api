package com.mx.sivale.service;

import com.mx.sivale.model.Team;

import java.util.List;

public interface TeamService {

    Team create(Team team) throws Exception;

    Team update(Team team) throws Exception;

    void remove(Long id) throws Exception;

    List<Team> findAll() throws Exception;

    List<Team> findByClientId() throws Exception;

    Team findOne(Long id) throws Exception;

    List<Team> searchByName(String name) throws Exception;

}

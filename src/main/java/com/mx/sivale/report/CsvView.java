package com.mx.sivale.report;

import com.mx.sivale.model.dto.CustomReportDTO;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class CsvView extends AbstractCsvView {

    @Override
    protected void buildCsvDocument(Map<String, Object> model,
                                    HttpServletRequest request,
                                    HttpServletResponse response) throws Exception {

        //VARIABLES REQUIRED IN MODEL
        List<String> headers = (List<String>) model.get("headers");
        List<String> descHeaders = (List<String>) model.get("descHeaders");
        List<CustomReportDTO> results = (List<CustomReportDTO>) model.get("rows");

        ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);

        String[] arrayHeaders = getHeaders(headers);
        String[] arrayDescHeaders = getHeaders(descHeaders);
        createHeaders(arrayDescHeaders, csvWriter);
        createRows(arrayHeaders,results,csvWriter);

        csvWriter.close();

    }

    protected String[] getHeaders(List<String> headers){
        //CAST LIST TO ARRAY STRING HEADERS
        String[] stringHeaders = new String[headers.size()];
        if(headers!= null && headers.size()>0){
            for (int cont = 0; cont < headers.size(); cont++) {
                stringHeaders[cont] = headers.get(cont).toString();
            }
        }
        return stringHeaders;
    }

    protected void createHeaders(String[] headers,
                                 ICsvBeanWriter csvWriter) throws IOException {

        //POPULATE HEADER COLUMNS
        csvWriter.writeHeader(headers);

    }

    protected void createRows(String[] headers,
                              List<CustomReportDTO>  results,
                              ICsvBeanWriter csvWriter) throws IOException {
        //POPULATE VALUE ROWS/COLUMNS
        if(results != null){
            for (CustomReportDTO row : results) {
                csvWriter.write(row, headers);
            }
        }
    }
}

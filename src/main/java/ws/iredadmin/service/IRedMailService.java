package ws.iredadmin.service;

import com.mx.sivale.service.exception.ServiceException;

public interface IRedMailService {

    Boolean login(String username, String password);

    String createUser(String username, String password) throws ServiceException;

    Boolean deleteUser(String mail);
}

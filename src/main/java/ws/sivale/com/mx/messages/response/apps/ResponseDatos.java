
package ws.sivale.com.mx.messages.response.apps;

import ws.sivale.com.mx.messages.response.ResponseEstadisticas;
import ws.sivale.com.mx.messages.response.appgasolina.TypeDatos;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ResponseDatos complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponseDatos">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mx.com.sivale.ws/messages/response/apps}ResponseBase">
 *       &lt;sequence>
 *         &lt;element name="datos" type="{http://mx.com.sivale.ws/messages/response/appgasolina}TypeDatos" minOccurs="0"/>
 *         &lt;element name="estadisticas" type="{http://mx.com.sivale.ws/messages/response}ResponseEstadisticas" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseDatos", propOrder = {
    "datos",
    "estadisticas"
})
public class ResponseDatos
    extends ResponseBase
{

    protected TypeDatos datos;
    protected ResponseEstadisticas estadisticas;

    /**
     * Obtiene el valor de la propiedad datos.
     * 
     * @return
     *     possible object is
     *     {@link TypeDatos }
     *     
     */
    public TypeDatos getDatos() {
        return datos;
    }

    /**
     * Define el valor de la propiedad datos.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeDatos }
     *     
     */
    public void setDatos(TypeDatos value) {
        this.datos = value;
    }

    /**
     * Obtiene el valor de la propiedad estadisticas.
     * 
     * @return
     *     possible object is
     *     {@link ResponseEstadisticas }
     *     
     */
    public ResponseEstadisticas getEstadisticas() {
        return estadisticas;
    }

    /**
     * Define el valor de la propiedad estadisticas.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseEstadisticas }
     *     
     */
    public void setEstadisticas(ResponseEstadisticas value) {
        this.estadisticas = value;
    }

}

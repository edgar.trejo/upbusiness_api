
package ws.sivale.com.mx.messages.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Clase Java para TypeEstadistica complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="TypeEstadistica">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="peticion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="respuesta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="proceso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="verificarAutorizador" type="{http://mx.com.sivale.ws/messages/types}TypeEstadistica" minOccurs="0"/>
 *         &lt;element name="tierService" type="{http://mx.com.sivale.ws/messages/types}TypeEstadistica" minOccurs="0"/>
 *         &lt;element name="beanService" type="{http://mx.com.sivale.ws/messages/types}TypeEstadistica" minOccurs="0"/>
 *         &lt;element name="actividades" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="actividad" type="{http://mx.com.sivale.ws/messages/types}TypeEstadistica" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TypeEstadistica", propOrder = {
    "descripcion",
    "peticion",
    "respuesta",
    "proceso",
    "verificarAutorizador",
    "tierService",
    "beanService",
    "actividades"
})
public class TypeEstadistica {

    protected String descripcion;
    protected String peticion;
    protected String respuesta;
    protected String proceso;
    protected TypeEstadistica verificarAutorizador;
    protected TypeEstadistica tierService;
    protected TypeEstadistica beanService;
    protected TypeEstadistica.Actividades actividades;

    /**
     * Obtiene el valor de la propiedad descripcion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Define el valor de la propiedad descripcion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcion(String value) {
        this.descripcion = value;
    }

    /**
     * Obtiene el valor de la propiedad peticion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPeticion() {
        return peticion;
    }

    /**
     * Define el valor de la propiedad peticion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPeticion(String value) {
        this.peticion = value;
    }

    /**
     * Obtiene el valor de la propiedad respuesta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRespuesta() {
        return respuesta;
    }

    /**
     * Define el valor de la propiedad respuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRespuesta(String value) {
        this.respuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad proceso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProceso() {
        return proceso;
    }

    /**
     * Define el valor de la propiedad proceso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProceso(String value) {
        this.proceso = value;
    }

    /**
     * Obtiene el valor de la propiedad verificarAutorizador.
     * 
     * @return
     *     possible object is
     *     {@link TypeEstadistica }
     *     
     */
    public TypeEstadistica getVerificarAutorizador() {
        return verificarAutorizador;
    }

    /**
     * Define el valor de la propiedad verificarAutorizador.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeEstadistica }
     *     
     */
    public void setVerificarAutorizador(TypeEstadistica value) {
        this.verificarAutorizador = value;
    }

    /**
     * Obtiene el valor de la propiedad tierService.
     * 
     * @return
     *     possible object is
     *     {@link TypeEstadistica }
     *     
     */
    public TypeEstadistica getTierService() {
        return tierService;
    }

    /**
     * Define el valor de la propiedad tierService.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeEstadistica }
     *     
     */
    public void setTierService(TypeEstadistica value) {
        this.tierService = value;
    }

    /**
     * Obtiene el valor de la propiedad beanService.
     * 
     * @return
     *     possible object is
     *     {@link TypeEstadistica }
     *     
     */
    public TypeEstadistica getBeanService() {
        return beanService;
    }

    /**
     * Define el valor de la propiedad beanService.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeEstadistica }
     *     
     */
    public void setBeanService(TypeEstadistica value) {
        this.beanService = value;
    }

    /**
     * Obtiene el valor de la propiedad actividades.
     * 
     * @return
     *     possible object is
     *     {@link TypeEstadistica.Actividades }
     *     
     */
    public TypeEstadistica.Actividades getActividades() {
        return actividades;
    }

    /**
     * Define el valor de la propiedad actividades.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeEstadistica.Actividades }
     *     
     */
    public void setActividades(TypeEstadistica.Actividades value) {
        this.actividades = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="actividad" type="{http://mx.com.sivale.ws/messages/types}TypeEstadistica" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "actividad"
    })
    public static class Actividades {

        protected List<TypeEstadistica> actividad;

        /**
         * Gets the value of the actividad property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the actividad property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getActividad().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link TypeEstadistica }
         * 
         * 
         */
        public List<TypeEstadistica> getActividad() {
            if (actividad == null) {
                actividad = new ArrayList<>();
            }
            return this.actividad;
        }

    }

}

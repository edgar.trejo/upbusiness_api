
package ws.sivale.com.mx.messages.request.tarjeta;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para RequestMovimientos complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="RequestMovimientos">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mx.com.sivale.ws/messages/request/tarjeta}RequestBase">
 *       &lt;sequence>
 *         &lt;element name="saldo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cargos" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="abonos" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestMovimientos", propOrder = {
    "saldo",
    "cargos",
    "abonos"
})
public class RequestMovimientos
    extends RequestBase
{

    protected String saldo;
    protected Integer cargos;
    protected Integer abonos;

    /**
     * Obtiene el valor de la propiedad saldo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSaldo() {
        return saldo;
    }

    /**
     * Define el valor de la propiedad saldo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSaldo(String value) {
        this.saldo = value;
    }

    /**
     * Obtiene el valor de la propiedad cargos.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCargos() {
        return cargos;
    }

    /**
     * Define el valor de la propiedad cargos.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCargos(Integer value) {
        this.cargos = value;
    }

    /**
     * Obtiene el valor de la propiedad abonos.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAbonos() {
        return abonos;
    }

    /**
     * Define el valor de la propiedad abonos.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAbonos(Integer value) {
        this.abonos = value;
    }

}

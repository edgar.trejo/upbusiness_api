
package ws.sivale.com.mx.messages.response.apps;

import ws.sivale.com.mx.messages.response.ResponseEstadisticas;
import ws.sivale.com.mx.messages.response.appgasolina.TypeUsuario;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Clase Java para ResponseUsuarios complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponseUsuarios">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mx.com.sivale.ws/messages/response/apps}ResponseBase">
 *       &lt;sequence>
 *         &lt;element name="usuarios" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="usuario" type="{http://mx.com.sivale.ws/messages/response/appgasolina}TypeUsuario" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="estadisticas" type="{http://mx.com.sivale.ws/messages/response}ResponseEstadisticas" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseUsuarios", propOrder = {
    "usuarios",
    "estadisticas"
})
public class ResponseUsuarios
    extends ResponseBase
{

    protected ResponseUsuarios.Usuarios usuarios;
    protected ResponseEstadisticas estadisticas;

    /**
     * Obtiene el valor de la propiedad usuarios.
     * 
     * @return
     *     possible object is
     *     {@link ResponseUsuarios.Usuarios }
     *     
     */
    public ResponseUsuarios.Usuarios getUsuarios() {
        return usuarios;
    }

    /**
     * Define el valor de la propiedad usuarios.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseUsuarios.Usuarios }
     *     
     */
    public void setUsuarios(ResponseUsuarios.Usuarios value) {
        this.usuarios = value;
    }

    /**
     * Obtiene el valor de la propiedad estadisticas.
     * 
     * @return
     *     possible object is
     *     {@link ResponseEstadisticas }
     *     
     */
    public ResponseEstadisticas getEstadisticas() {
        return estadisticas;
    }

    /**
     * Define el valor de la propiedad estadisticas.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseEstadisticas }
     *     
     */
    public void setEstadisticas(ResponseEstadisticas value) {
        this.estadisticas = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="usuario" type="{http://mx.com.sivale.ws/messages/response/appgasolina}TypeUsuario" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "usuario"
    })
    public static class Usuarios {

        protected List<TypeUsuario> usuario;

        /**
         * Gets the value of the usuario property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the usuario property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getUsuario().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link TypeUsuario }
         * 
         * 
         */
        public List<TypeUsuario> getUsuario() {
            if (usuario == null) {
                usuario = new ArrayList<TypeUsuario>();
            }
            return this.usuario;
        }

    }

}

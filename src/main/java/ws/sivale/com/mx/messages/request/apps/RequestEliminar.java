
package ws.sivale.com.mx.messages.request.apps;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para RequestEliminar complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="RequestEliminar">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="correoAdministrador" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="numCliente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="idUsuario" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="origen" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="rolSolicitante" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestEliminar", propOrder = {
    "correoAdministrador",
    "numCliente",
    "idUsuario",
    "origen",
    "rolSolicitante"
})
public class RequestEliminar {

    @XmlElement(required = true)
    protected String correoAdministrador;
    @XmlElement(required = true)
    protected String numCliente;
    @XmlElement(required = true)
    protected String idUsuario;
    @XmlElement(required = true)
    protected String origen;
    @XmlElement(required = true)
    protected String rolSolicitante;

    /**
     * Obtiene el valor de la propiedad correoAdministrador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorreoAdministrador() {
        return correoAdministrador;
    }

    /**
     * Define el valor de la propiedad correoAdministrador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorreoAdministrador(String value) {
        this.correoAdministrador = value;
    }

    /**
     * Obtiene el valor de la propiedad numCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumCliente() {
        return numCliente;
    }

    /**
     * Define el valor de la propiedad numCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumCliente(String value) {
        this.numCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad idUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdUsuario() {
        return idUsuario;
    }

    /**
     * Define el valor de la propiedad idUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdUsuario(String value) {
        this.idUsuario = value;
    }

    /**
     * Obtiene el valor de la propiedad origen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrigen() {
        return origen;
    }

    /**
     * Define el valor de la propiedad origen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrigen(String value) {
        this.origen = value;
    }

    /**
     * Obtiene el valor de la propiedad rolSolicitante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRolSolicitante() {
        return rolSolicitante;
    }

    /**
     * Define el valor de la propiedad rolSolicitante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRolSolicitante(String value) {
        this.rolSolicitante = value;
    }

}

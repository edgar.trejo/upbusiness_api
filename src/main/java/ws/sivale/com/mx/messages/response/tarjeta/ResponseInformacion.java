
package ws.sivale.com.mx.messages.response.tarjeta;

import ws.sivale.com.mx.messages.response.ResponseEstadisticas;
import ws.sivale.com.mx.messages.types.TypeInformacion;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ResponseInformacion complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ResponseInformacion">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mx.com.sivale.ws/messages/response/tarjeta}ResponseBase">
 *       &lt;sequence>
 *         &lt;element name="informacion" type="{http://mx.com.sivale.ws/messages/types}TypeInformacion" minOccurs="0"/>
 *         &lt;element name="estadisticas" type="{http://mx.com.sivale.ws/messages/response}ResponseEstadisticas" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResponseInformacion", propOrder = {
    "informacion",
    "estadisticas"
})
public class ResponseInformacion
    extends ResponseBase
{

    protected TypeInformacion informacion;
    protected ResponseEstadisticas estadisticas;

    /**
     * Obtiene el valor de la propiedad informacion.
     * 
     * @return
     *     possible object is
     *     {@link TypeInformacion }
     *     
     */
    public TypeInformacion getInformacion() {
        return informacion;
    }

    /**
     * Define el valor de la propiedad informacion.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeInformacion }
     *     
     */
    public void setInformacion(TypeInformacion value) {
        this.informacion = value;
    }

    /**
     * Obtiene el valor de la propiedad estadisticas.
     * 
     * @return
     *     possible object is
     *     {@link ResponseEstadisticas }
     *     
     */
    public ResponseEstadisticas getEstadisticas() {
        return estadisticas;
    }

    /**
     * Define el valor de la propiedad estadisticas.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseEstadisticas }
     *     
     */
    public void setEstadisticas(ResponseEstadisticas value) {
        this.estadisticas = value;
    }

}

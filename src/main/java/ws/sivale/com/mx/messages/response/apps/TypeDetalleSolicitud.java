
package ws.sivale.com.mx.messages.response.apps;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para TypeDetalleSolicitud complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="TypeDetalleSolicitud">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="numSolicitud" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numSecuencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoProceso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoMovimiento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numCuenta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numTarjetaOrigen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="abonoAjusteOrigen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="saldoAntTransOrigen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="saldoDespTransOrigen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="importeOrigen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="comisionOrigen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numTransferOrigen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numCuentaDest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numTarjetaDest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="abonoAjusteDest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="saldoAntTransDest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="saldoDespTransDest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="importeDest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="comisionDest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numTranfDest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cveRechazo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descRechazo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="estatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cveEmisor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numCliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="centroCostosOrigen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="centroCostosDestino" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="control" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idTarjetaOrigen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idTarjetaDestino" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TypeDetalleSolicitud", propOrder = {
    "numSolicitud",
    "numSecuencia",
    "tipoProceso",
    "tipoMovimiento",
    "numCuenta",
    "numTarjetaOrigen",
    "abonoAjusteOrigen",
    "saldoAntTransOrigen",
    "saldoDespTransOrigen",
    "importeOrigen",
    "comisionOrigen",
    "numTransferOrigen",
    "numCuentaDest",
    "numTarjetaDest",
    "abonoAjusteDest",
    "saldoAntTransDest",
    "saldoDespTransDest",
    "importeDest",
    "comisionDest",
    "numTranfDest",
    "cveRechazo",
    "descRechazo",
    "estatus",
    "cveEmisor",
    "numCliente",
    "idUsuario",
    "centroCostosOrigen",
    "centroCostosDestino",
    "control",
    "idTarjetaOrigen",
    "idTarjetaDestino"
})
public class TypeDetalleSolicitud {

    protected String numSolicitud;
    protected String numSecuencia;
    protected String tipoProceso;
    protected String tipoMovimiento;
    protected String numCuenta;
    protected String numTarjetaOrigen;
    protected String abonoAjusteOrigen;
    protected String saldoAntTransOrigen;
    protected String saldoDespTransOrigen;
    protected String importeOrigen;
    protected String comisionOrigen;
    protected String numTransferOrigen;
    protected String numCuentaDest;
    protected String numTarjetaDest;
    protected String abonoAjusteDest;
    protected String saldoAntTransDest;
    protected String saldoDespTransDest;
    protected String importeDest;
    protected String comisionDest;
    protected String numTranfDest;
    protected String cveRechazo;
    protected String descRechazo;
    protected String estatus;
    protected String cveEmisor;
    protected String numCliente;
    protected String idUsuario;
    protected String centroCostosOrigen;
    protected String centroCostosDestino;
    protected String control;
    protected String idTarjetaOrigen;
    protected String idTarjetaDestino;

    /**
     * Obtiene el valor de la propiedad numSolicitud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumSolicitud() {
        return numSolicitud;
    }

    /**
     * Define el valor de la propiedad numSolicitud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumSolicitud(String value) {
        this.numSolicitud = value;
    }

    /**
     * Obtiene el valor de la propiedad numSecuencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumSecuencia() {
        return numSecuencia;
    }

    /**
     * Define el valor de la propiedad numSecuencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumSecuencia(String value) {
        this.numSecuencia = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoProceso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoProceso() {
        return tipoProceso;
    }

    /**
     * Define el valor de la propiedad tipoProceso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoProceso(String value) {
        this.tipoProceso = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoMovimiento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoMovimiento() {
        return tipoMovimiento;
    }

    /**
     * Define el valor de la propiedad tipoMovimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoMovimiento(String value) {
        this.tipoMovimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad numCuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumCuenta() {
        return numCuenta;
    }

    /**
     * Define el valor de la propiedad numCuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumCuenta(String value) {
        this.numCuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad numTarjetaOrigen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumTarjetaOrigen() {
        return numTarjetaOrigen;
    }

    /**
     * Define el valor de la propiedad numTarjetaOrigen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumTarjetaOrigen(String value) {
        this.numTarjetaOrigen = value;
    }

    /**
     * Obtiene el valor de la propiedad abonoAjusteOrigen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAbonoAjusteOrigen() {
        return abonoAjusteOrigen;
    }

    /**
     * Define el valor de la propiedad abonoAjusteOrigen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAbonoAjusteOrigen(String value) {
        this.abonoAjusteOrigen = value;
    }

    /**
     * Obtiene el valor de la propiedad saldoAntTransOrigen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSaldoAntTransOrigen() {
        return saldoAntTransOrigen;
    }

    /**
     * Define el valor de la propiedad saldoAntTransOrigen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSaldoAntTransOrigen(String value) {
        this.saldoAntTransOrigen = value;
    }

    /**
     * Obtiene el valor de la propiedad saldoDespTransOrigen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSaldoDespTransOrigen() {
        return saldoDespTransOrigen;
    }

    /**
     * Define el valor de la propiedad saldoDespTransOrigen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSaldoDespTransOrigen(String value) {
        this.saldoDespTransOrigen = value;
    }

    /**
     * Obtiene el valor de la propiedad importeOrigen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImporteOrigen() {
        return importeOrigen;
    }

    /**
     * Define el valor de la propiedad importeOrigen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImporteOrigen(String value) {
        this.importeOrigen = value;
    }

    /**
     * Obtiene el valor de la propiedad comisionOrigen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComisionOrigen() {
        return comisionOrigen;
    }

    /**
     * Define el valor de la propiedad comisionOrigen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComisionOrigen(String value) {
        this.comisionOrigen = value;
    }

    /**
     * Obtiene el valor de la propiedad numTransferOrigen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumTransferOrigen() {
        return numTransferOrigen;
    }

    /**
     * Define el valor de la propiedad numTransferOrigen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumTransferOrigen(String value) {
        this.numTransferOrigen = value;
    }

    /**
     * Obtiene el valor de la propiedad numCuentaDest.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumCuentaDest() {
        return numCuentaDest;
    }

    /**
     * Define el valor de la propiedad numCuentaDest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumCuentaDest(String value) {
        this.numCuentaDest = value;
    }

    /**
     * Obtiene el valor de la propiedad numTarjetaDest.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumTarjetaDest() {
        return numTarjetaDest;
    }

    /**
     * Define el valor de la propiedad numTarjetaDest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumTarjetaDest(String value) {
        this.numTarjetaDest = value;
    }

    /**
     * Obtiene el valor de la propiedad abonoAjusteDest.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAbonoAjusteDest() {
        return abonoAjusteDest;
    }

    /**
     * Define el valor de la propiedad abonoAjusteDest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAbonoAjusteDest(String value) {
        this.abonoAjusteDest = value;
    }

    /**
     * Obtiene el valor de la propiedad saldoAntTransDest.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSaldoAntTransDest() {
        return saldoAntTransDest;
    }

    /**
     * Define el valor de la propiedad saldoAntTransDest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSaldoAntTransDest(String value) {
        this.saldoAntTransDest = value;
    }

    /**
     * Obtiene el valor de la propiedad saldoDespTransDest.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSaldoDespTransDest() {
        return saldoDespTransDest;
    }

    /**
     * Define el valor de la propiedad saldoDespTransDest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSaldoDespTransDest(String value) {
        this.saldoDespTransDest = value;
    }

    /**
     * Obtiene el valor de la propiedad importeDest.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImporteDest() {
        return importeDest;
    }

    /**
     * Define el valor de la propiedad importeDest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImporteDest(String value) {
        this.importeDest = value;
    }

    /**
     * Obtiene el valor de la propiedad comisionDest.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComisionDest() {
        return comisionDest;
    }

    /**
     * Define el valor de la propiedad comisionDest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComisionDest(String value) {
        this.comisionDest = value;
    }

    /**
     * Obtiene el valor de la propiedad numTranfDest.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumTranfDest() {
        return numTranfDest;
    }

    /**
     * Define el valor de la propiedad numTranfDest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumTranfDest(String value) {
        this.numTranfDest = value;
    }

    /**
     * Obtiene el valor de la propiedad cveRechazo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCveRechazo() {
        return cveRechazo;
    }

    /**
     * Define el valor de la propiedad cveRechazo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCveRechazo(String value) {
        this.cveRechazo = value;
    }

    /**
     * Obtiene el valor de la propiedad descRechazo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescRechazo() {
        return descRechazo;
    }

    /**
     * Define el valor de la propiedad descRechazo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescRechazo(String value) {
        this.descRechazo = value;
    }

    /**
     * Obtiene el valor de la propiedad estatus.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstatus() {
        return estatus;
    }

    /**
     * Define el valor de la propiedad estatus.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstatus(String value) {
        this.estatus = value;
    }

    /**
     * Obtiene el valor de la propiedad cveEmisor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCveEmisor() {
        return cveEmisor;
    }

    /**
     * Define el valor de la propiedad cveEmisor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCveEmisor(String value) {
        this.cveEmisor = value;
    }

    /**
     * Obtiene el valor de la propiedad numCliente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumCliente() {
        return numCliente;
    }

    /**
     * Define el valor de la propiedad numCliente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumCliente(String value) {
        this.numCliente = value;
    }

    /**
     * Obtiene el valor de la propiedad idUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdUsuario() {
        return idUsuario;
    }

    /**
     * Define el valor de la propiedad idUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdUsuario(String value) {
        this.idUsuario = value;
    }

    /**
     * Obtiene el valor de la propiedad centroCostosOrigen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCentroCostosOrigen() {
        return centroCostosOrigen;
    }

    /**
     * Define el valor de la propiedad centroCostosOrigen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCentroCostosOrigen(String value) {
        this.centroCostosOrigen = value;
    }

    /**
     * Obtiene el valor de la propiedad centroCostosDestino.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCentroCostosDestino() {
        return centroCostosDestino;
    }

    /**
     * Define el valor de la propiedad centroCostosDestino.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCentroCostosDestino(String value) {
        this.centroCostosDestino = value;
    }

    /**
     * Obtiene el valor de la propiedad control.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControl() {
        return control;
    }

    /**
     * Define el valor de la propiedad control.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControl(String value) {
        this.control = value;
    }

    /**
     * Obtiene el valor de la propiedad idTarjetaOrigen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdTarjetaOrigen() {
        return idTarjetaOrigen;
    }

    /**
     * Define el valor de la propiedad idTarjetaOrigen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdTarjetaOrigen(String value) {
        this.idTarjetaOrigen = value;
    }

    /**
     * Obtiene el valor de la propiedad idTarjetaDestino.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdTarjetaDestino() {
        return idTarjetaDestino;
    }

    /**
     * Define el valor de la propiedad idTarjetaDestino.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdTarjetaDestino(String value) {
        this.idTarjetaDestino = value;
    }

}

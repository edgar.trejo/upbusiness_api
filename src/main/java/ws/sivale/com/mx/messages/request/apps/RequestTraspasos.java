
package ws.sivale.com.mx.messages.request.apps;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para RequestTraspasos complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="RequestTraspasos">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="origen" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="solicitante" type="{http://mx.com.sivale.ws/messages/request/apps}TypeSolicitante"/>
 *         &lt;element name="cliente" type="{http://mx.com.sivale.ws/messages/request/apps}TypeCliente"/>
 *         &lt;element name="traspaso" type="{http://mx.com.sivale.ws/messages/request/apps}TypeTraspaso"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestTraspasos", propOrder = {
    "origen",
    "solicitante",
    "cliente",
    "traspaso"
})
public class RequestTraspasos {

    @XmlElement(required = true)
    protected String origen;
    @XmlElement(required = true)
    protected TypeSolicitante solicitante;
    @XmlElement(required = true)
    protected TypeCliente cliente;
    @XmlElement(required = true)
    protected TypeTraspaso traspaso;

    /**
     * Obtiene el valor de la propiedad origen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrigen() {
        return origen;
    }

    /**
     * Define el valor de la propiedad origen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrigen(String value) {
        this.origen = value;
    }

    /**
     * Obtiene el valor de la propiedad solicitante.
     * 
     * @return
     *     possible object is
     *     {@link TypeSolicitante }
     *     
     */
    public TypeSolicitante getSolicitante() {
        return solicitante;
    }

    /**
     * Define el valor de la propiedad solicitante.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeSolicitante }
     *     
     */
    public void setSolicitante(TypeSolicitante value) {
        this.solicitante = value;
    }

    /**
     * Obtiene el valor de la propiedad cliente.
     * 
     * @return
     *     possible object is
     *     {@link TypeCliente }
     *     
     */
    public TypeCliente getCliente() {
        return cliente;
    }

    /**
     * Define el valor de la propiedad cliente.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeCliente }
     *     
     */
    public void setCliente(TypeCliente value) {
        this.cliente = value;
    }

    /**
     * Obtiene el valor de la propiedad traspaso.
     * 
     * @return
     *     possible object is
     *     {@link TypeTraspaso }
     *     
     */
    public TypeTraspaso getTraspaso() {
        return traspaso;
    }

    /**
     * Define el valor de la propiedad traspaso.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeTraspaso }
     *     
     */
    public void setTraspaso(TypeTraspaso value) {
        this.traspaso = value;
    }

}


package ws.sivale.com.mx.exposition.servicios.apps;

import ws.sivale.com.mx.messages.request.appgasolina.RequestTarjeta;
import ws.sivale.com.mx.messages.request.appgs.RequestConsultarTarjetas;
import ws.sivale.com.mx.messages.request.apps.*;
import ws.sivale.com.mx.messages.response.appgasolina.ResponseTarjetasChofer;
import ws.sivale.com.mx.messages.response.apps.*;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ws.sivale.com.mx.exposition.servicios.apps package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _EntregarTarjetaRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "entregarTarjetaRequest");
    private final static QName _FinalizarTraspasoRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "finalizarTraspasoRequest");
    private final static QName _AgregarTarjetaResponse_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "agregarTarjetaResponse");
    private final static QName _RegistrarCorreoValidoResponse_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "registrarCorreoValidoResponse");
    private final static QName _EliminarTarjetaResponse_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "eliminarTarjetaResponse");
    private final static QName _TarjetasUsuarioRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "tarjetasUsuarioRequest");
    private final static QName _TarjetasUsuarioResponse_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "tarjetasUsuarioResponse");
    private final static QName _ConsultaComprasResponse_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "consultaComprasResponse");
    private final static QName _DatosTarjetaResponse_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "datosTarjetaResponse");
    private final static QName _EditarAutentificacionResponse_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "editarAutentificacionResponse");
    private final static QName _UsuariosRegistradosRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "usuariosRegistradosRequest");
    private final static QName _DatosTarjetaRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "datosTarjetaRequest");
    private final static QName _EliminarUsuario_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "eliminarUsuario");
    private final static QName _RegistrarCorreoValidoRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "registrarCorreoValidoRequest");
    private final static QName _ConsultaSolicitudesRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "consultaSolicitudesRequest");
    private final static QName _SolicitarAutentificacionRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "solicitarAutentificacionRequest");
    private final static QName _EditarAutentificacionRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "editarAutentificacionRequest");
    private final static QName _EditarUsuarioRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "editarUsuarioRequest");
    private final static QName _LoginRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "loginRequest");
    private final static QName _SolicitarTraspasoResponse_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "solicitarTraspasoResponse");
    private final static QName _ConsultaComprasRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "consultaComprasRequest");
    private final static QName _ProductosByClienteRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "productosByClienteRequest");
    private final static QName _ProductosByClienteResponse_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "productosByClienteResponse");
    private final static QName _RegistrarUsuario_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "registrarUsuario");
    private final static QName _FinalizarTraspasoResponse_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "finalizarTraspasoResponse");
    private final static QName _AgregarTarjetaRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "agregarTarjetaRequest");
    private final static QName _DetalleSolicitudRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "detalleSolicitudRequest");
    private final static QName _RegistrarUsuarioRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "registrarUsuarioRequest");
    private final static QName _DetalleSolicitudResponse_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "detalleSolicitudResponse");
    private final static QName _FinalizarTraspasosResponse_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "finalizarTraspasosResponse");
    private final static QName _TransaccionesECRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "transaccionesECRequest");
    private final static QName _ConsultarTarjetasRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "consultarTarjetasRequest");
    private final static QName _EditarUsuarioResponse_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "editarUsuarioResponse");
    private final static QName _SolicitarTraspasoRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "solicitarTraspasoRequest");
    private final static QName _EliminarTarjetaRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "eliminarTarjetaRequest");
    private final static QName _UsuariosRegistradosResponse_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "usuariosRegistradosResponse");
    private final static QName _ConsultarTarjetasResponse_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "consultarTarjetasResponse");
    private final static QName _EliminarUsuarioRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "eliminarUsuarioRequest");
    private final static QName _Login_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "login");
    private final static QName _SolicitarTraspasosResponse_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "solicitarTraspasosResponse");
    private final static QName _TransaccionesDMResponse_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "transaccionesDMResponse");
    private final static QName _FinalizarTraspasosRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "finalizarTraspasosRequest");
    private final static QName _SolicitarAutentificacionResponse_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "solicitarAutentificacionResponse");
    private final static QName _ConsultaSolicitudesResponse_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "consultaSolicitudesResponse");
    private final static QName _SolicitarTraspasosRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "solicitarTraspasosRequest");
    private final static QName _TransaccionesDMRequest_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "transaccionesDMRequest");
    private final static QName _TransaccionesECResponse_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "transaccionesECResponse");
    private final static QName _EntregarTarjetaResponse_QNAME = new QName("http://mx.com.sivale.ws/exposition/servicios/apps", "entregarTarjetaResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ws.sivale.com.mx.exposition.servicios.apps
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestEntregaTarjetas }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "entregarTarjetaRequest")
    public JAXBElement<RequestEntregaTarjetas> createEntregarTarjetaRequest(RequestEntregaTarjetas value) {
        return new JAXBElement<RequestEntregaTarjetas>(_EntregarTarjetaRequest_QNAME, RequestEntregaTarjetas.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestSolicitud }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "finalizarTraspasoRequest")
    public JAXBElement<RequestSolicitud> createFinalizarTraspasoRequest(RequestSolicitud value) {
        return new JAXBElement<RequestSolicitud>(_FinalizarTraspasoRequest_QNAME, RequestSolicitud.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseGenerico }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "agregarTarjetaResponse")
    public JAXBElement<ResponseGenerico> createAgregarTarjetaResponse(ResponseGenerico value) {
        return new JAXBElement<ResponseGenerico>(_AgregarTarjetaResponse_QNAME, ResponseGenerico.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseGenerico }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "registrarCorreoValidoResponse")
    public JAXBElement<ResponseGenerico> createRegistrarCorreoValidoResponse(ResponseGenerico value) {
        return new JAXBElement<ResponseGenerico>(_RegistrarCorreoValidoResponse_QNAME, ResponseGenerico.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseGenerico }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "eliminarTarjetaResponse")
    public JAXBElement<ResponseGenerico> createEliminarTarjetaResponse(ResponseGenerico value) {
        return new JAXBElement<ResponseGenerico>(_EliminarTarjetaResponse_QNAME, ResponseGenerico.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestClienteIdentificador }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "tarjetasUsuarioRequest")
    public JAXBElement<RequestClienteIdentificador> createTarjetasUsuarioRequest(RequestClienteIdentificador value) {
        return new JAXBElement<RequestClienteIdentificador>(_TarjetasUsuarioRequest_QNAME, RequestClienteIdentificador.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseTarjetasChofer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "tarjetasUsuarioResponse")
    public JAXBElement<ResponseTarjetasChofer> createTarjetasUsuarioResponse(ResponseTarjetasChofer value) {
        return new JAXBElement<ResponseTarjetasChofer>(_TarjetasUsuarioResponse_QNAME, ResponseTarjetasChofer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseCompras }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "consultaComprasResponse")
    public JAXBElement<ResponseCompras> createConsultaComprasResponse(ResponseCompras value) {
        return new JAXBElement<ResponseCompras>(_ConsultaComprasResponse_QNAME, ResponseCompras.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseDatos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "datosTarjetaResponse")
    public JAXBElement<ResponseDatos> createDatosTarjetaResponse(ResponseDatos value) {
        return new JAXBElement<ResponseDatos>(_DatosTarjetaResponse_QNAME, ResponseDatos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseGenerico }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "editarAutentificacionResponse")
    public JAXBElement<ResponseGenerico> createEditarAutentificacionResponse(ResponseGenerico value) {
        return new JAXBElement<ResponseGenerico>(_EditarAutentificacionResponse_QNAME, ResponseGenerico.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestUsuariosRegistrados }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "usuariosRegistradosRequest")
    public JAXBElement<RequestUsuariosRegistrados> createUsuariosRegistradosRequest(RequestUsuariosRegistrados value) {
        return new JAXBElement<RequestUsuariosRegistrados>(_UsuariosRegistradosRequest_QNAME, RequestUsuariosRegistrados.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestIut }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "datosTarjetaRequest")
    public JAXBElement<RequestIut> createDatosTarjetaRequest(RequestIut value) {
        return new JAXBElement<RequestIut>(_DatosTarjetaRequest_QNAME, RequestIut.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseGenerico }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "eliminarUsuario")
    public JAXBElement<ResponseGenerico> createEliminarUsuario(ResponseGenerico value) {
        return new JAXBElement<ResponseGenerico>(_EliminarUsuario_QNAME, ResponseGenerico.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestRegistrarCorreoValido }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "registrarCorreoValidoRequest")
    public JAXBElement<RequestRegistrarCorreoValido> createRegistrarCorreoValidoRequest(RequestRegistrarCorreoValido value) {
        return new JAXBElement<RequestRegistrarCorreoValido>(_RegistrarCorreoValidoRequest_QNAME, RequestRegistrarCorreoValido.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestSolicitudes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "consultaSolicitudesRequest")
    public JAXBElement<RequestSolicitudes> createConsultaSolicitudesRequest(RequestSolicitudes value) {
        return new JAXBElement<RequestSolicitudes>(_ConsultaSolicitudesRequest_QNAME, RequestSolicitudes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestCorreo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "solicitarAutentificacionRequest")
    public JAXBElement<RequestCorreo> createSolicitarAutentificacionRequest(RequestCorreo value) {
        return new JAXBElement<RequestCorreo>(_SolicitarAutentificacionRequest_QNAME, RequestCorreo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestEditAutentification }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "editarAutentificacionRequest")
    public JAXBElement<RequestEditAutentification> createEditarAutentificacionRequest(RequestEditAutentification value) {
        return new JAXBElement<RequestEditAutentification>(_EditarAutentificacionRequest_QNAME, RequestEditAutentification.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestEditar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "editarUsuarioRequest")
    public JAXBElement<RequestEditar> createEditarUsuarioRequest(RequestEditar value) {
        return new JAXBElement<RequestEditar>(_EditarUsuarioRequest_QNAME, RequestEditar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "loginRequest")
    public JAXBElement<RequestLogin> createLoginRequest(RequestLogin value) {
        return new JAXBElement<RequestLogin>(_LoginRequest_QNAME, RequestLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseTraspaso }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "solicitarTraspasoResponse")
    public JAXBElement<ResponseTraspaso> createSolicitarTraspasoResponse(ResponseTraspaso value) {
        return new JAXBElement<ResponseTraspaso>(_SolicitarTraspasoResponse_QNAME, ResponseTraspaso.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestConsultaCompras }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "consultaComprasRequest")
    public JAXBElement<RequestConsultaCompras> createConsultaComprasRequest(RequestConsultaCompras value) {
        return new JAXBElement<RequestConsultaCompras>(_ConsultaComprasRequest_QNAME, RequestConsultaCompras.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestCliente }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "productosByClienteRequest")
    public JAXBElement<RequestCliente> createProductosByClienteRequest(RequestCliente value) {
        return new JAXBElement<RequestCliente>(_ProductosByClienteRequest_QNAME, RequestCliente.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseProductos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "productosByClienteResponse")
    public JAXBElement<ResponseProductos> createProductosByClienteResponse(ResponseProductos value) {
        return new JAXBElement<ResponseProductos>(_ProductosByClienteResponse_QNAME, ResponseProductos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseRegistro }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "registrarUsuario")
    public JAXBElement<ResponseRegistro> createRegistrarUsuario(ResponseRegistro value) {
        return new JAXBElement<ResponseRegistro>(_RegistrarUsuario_QNAME, ResponseRegistro.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseTraspaso }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "finalizarTraspasoResponse")
    public JAXBElement<ResponseTraspaso> createFinalizarTraspasoResponse(ResponseTraspaso value) {
        return new JAXBElement<ResponseTraspaso>(_FinalizarTraspasoResponse_QNAME, ResponseTraspaso.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestTarjeta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "agregarTarjetaRequest")
    public JAXBElement<RequestTarjeta> createAgregarTarjetaRequest(RequestTarjeta value) {
        return new JAXBElement<RequestTarjeta>(_AgregarTarjetaRequest_QNAME, RequestTarjeta.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestSolicitud }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "detalleSolicitudRequest")
    public JAXBElement<RequestSolicitud> createDetalleSolicitudRequest(RequestSolicitud value) {
        return new JAXBElement<RequestSolicitud>(_DetalleSolicitudRequest_QNAME, RequestSolicitud.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestRegistro }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "registrarUsuarioRequest")
    public JAXBElement<RequestRegistro> createRegistrarUsuarioRequest(RequestRegistro value) {
        return new JAXBElement<RequestRegistro>(_RegistrarUsuarioRequest_QNAME, RequestRegistro.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseDetalleSolicitud }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "detalleSolicitudResponse")
    public JAXBElement<ResponseDetalleSolicitud> createDetalleSolicitudResponse(ResponseDetalleSolicitud value) {
        return new JAXBElement<ResponseDetalleSolicitud>(_DetalleSolicitudResponse_QNAME, ResponseDetalleSolicitud.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseTraspaso }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "finalizarTraspasosResponse")
    public JAXBElement<ResponseTraspaso> createFinalizarTraspasosResponse(ResponseTraspaso value) {
        return new JAXBElement<ResponseTraspaso>(_FinalizarTraspasosResponse_QNAME, ResponseTraspaso.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestTransacciones }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "transaccionesECRequest")
    public JAXBElement<RequestTransacciones> createTransaccionesECRequest(RequestTransacciones value) {
        return new JAXBElement<RequestTransacciones>(_TransaccionesECRequest_QNAME, RequestTransacciones.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestConsultarTarjetas }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "consultarTarjetasRequest")
    public JAXBElement<RequestConsultarTarjetas> createConsultarTarjetasRequest(RequestConsultarTarjetas value) {
        return new JAXBElement<RequestConsultarTarjetas>(_ConsultarTarjetasRequest_QNAME, RequestConsultarTarjetas.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseGenerico }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "editarUsuarioResponse")
    public JAXBElement<ResponseGenerico> createEditarUsuarioResponse(ResponseGenerico value) {
        return new JAXBElement<ResponseGenerico>(_EditarUsuarioResponse_QNAME, ResponseGenerico.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestTraspaso }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "solicitarTraspasoRequest")
    public JAXBElement<RequestTraspaso> createSolicitarTraspasoRequest(RequestTraspaso value) {
        return new JAXBElement<RequestTraspaso>(_SolicitarTraspasoRequest_QNAME, RequestTraspaso.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestTarjeta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "eliminarTarjetaRequest")
    public JAXBElement<RequestTarjeta> createEliminarTarjetaRequest(RequestTarjeta value) {
        return new JAXBElement<RequestTarjeta>(_EliminarTarjetaRequest_QNAME, RequestTarjeta.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseUsuarios }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "usuariosRegistradosResponse")
    public JAXBElement<ResponseUsuarios> createUsuariosRegistradosResponse(ResponseUsuarios value) {
        return new JAXBElement<ResponseUsuarios>(_UsuariosRegistradosResponse_QNAME, ResponseUsuarios.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseTarjetasChofer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "consultarTarjetasResponse")
    public JAXBElement<ResponseTarjetasChofer> createConsultarTarjetasResponse(ResponseTarjetasChofer value) {
        return new JAXBElement<ResponseTarjetasChofer>(_ConsultarTarjetasResponse_QNAME, ResponseTarjetasChofer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestEliminar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "eliminarUsuarioRequest")
    public JAXBElement<RequestEliminar> createEliminarUsuarioRequest(RequestEliminar value) {
        return new JAXBElement<RequestEliminar>(_EliminarUsuarioRequest_QNAME, RequestEliminar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "login")
    public JAXBElement<ResponseLogin> createLogin(ResponseLogin value) {
        return new JAXBElement<ResponseLogin>(_Login_QNAME, ResponseLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseTraspaso }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "solicitarTraspasosResponse")
    public JAXBElement<ResponseTraspaso> createSolicitarTraspasosResponse(ResponseTraspaso value) {
        return new JAXBElement<ResponseTraspaso>(_SolicitarTraspasosResponse_QNAME, ResponseTraspaso.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseTransaccionesDM }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "transaccionesDMResponse")
    public JAXBElement<ResponseTransaccionesDM> createTransaccionesDMResponse(ResponseTransaccionesDM value) {
        return new JAXBElement<ResponseTransaccionesDM>(_TransaccionesDMResponse_QNAME, ResponseTransaccionesDM.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestSolicitud }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "finalizarTraspasosRequest")
    public JAXBElement<RequestSolicitud> createFinalizarTraspasosRequest(RequestSolicitud value) {
        return new JAXBElement<RequestSolicitud>(_FinalizarTraspasosRequest_QNAME, RequestSolicitud.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseAutentificacion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "solicitarAutentificacionResponse")
    public JAXBElement<ResponseAutentificacion> createSolicitarAutentificacionResponse(ResponseAutentificacion value) {
        return new JAXBElement<ResponseAutentificacion>(_SolicitarAutentificacionResponse_QNAME, ResponseAutentificacion.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseSolicitudes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "consultaSolicitudesResponse")
    public JAXBElement<ResponseSolicitudes> createConsultaSolicitudesResponse(ResponseSolicitudes value) {
        return new JAXBElement<ResponseSolicitudes>(_ConsultaSolicitudesResponse_QNAME, ResponseSolicitudes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestTraspasos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "solicitarTraspasosRequest")
    public JAXBElement<RequestTraspasos> createSolicitarTraspasosRequest(RequestTraspasos value) {
        return new JAXBElement<RequestTraspasos>(_SolicitarTraspasosRequest_QNAME, RequestTraspasos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestTransaccionesDm }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "transaccionesDMRequest")
    public JAXBElement<RequestTransaccionesDm> createTransaccionesDMRequest(RequestTransaccionesDm value) {
        return new JAXBElement<RequestTransaccionesDm>(_TransaccionesDMRequest_QNAME, RequestTransaccionesDm.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseTransacciones }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "transaccionesECResponse")
    public JAXBElement<ResponseTransacciones> createTransaccionesECResponse(ResponseTransacciones value) {
        return new JAXBElement<ResponseTransacciones>(_TransaccionesECResponse_QNAME, ResponseTransacciones.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseTarjetas }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mx.com.sivale.ws/exposition/servicios/apps", name = "entregarTarjetaResponse")
    public JAXBElement<ResponseTarjetas> createEntregarTarjetaResponse(ResponseTarjetas value) {
        return new JAXBElement<ResponseTarjetas>(_EntregarTarjetaResponse_QNAME, ResponseTarjetas.class, null, value);
    }

}

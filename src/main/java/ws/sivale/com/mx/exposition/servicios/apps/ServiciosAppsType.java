
package ws.sivale.com.mx.exposition.servicios.apps;

import ws.sivale.com.mx.messages.request.appgasolina.RequestTarjeta;
import ws.sivale.com.mx.messages.request.appgs.RequestConsultarTarjetas;
import ws.sivale.com.mx.messages.request.apps.*;
import ws.sivale.com.mx.messages.response.appgasolina.ResponseTarjetasChofer;
import ws.sivale.com.mx.messages.response.apps.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.6-1b01 
 * Generated source version: 2.2
 * 
 */
@WebService(name = "ServiciosAppsType", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    ws.sivale.com.mx.messages.types.ObjectFactory.class,
    ws.sivale.com.mx.messages.request.apps.ObjectFactory.class,
    ws.sivale.com.mx.messages.response.apps.ObjectFactory.class,
    ws.sivale.com.mx.messages.response.appgasolina.ObjectFactory.class,
    ws.sivale.com.mx.exposition.servicios.apps.ObjectFactory.class,
    ws.sivale.com.mx.messages.request.appgasolina.ObjectFactory.class,
    ws.sivale.com.mx.messages.request.appgs.ObjectFactory.class,
    ws.sivale.com.mx.messages.response.ObjectFactory.class
})
public interface ServiciosAppsType {


    /**
     * 
     * @param loginRequest
     * @return
     *     returns ws.sivale.com.mx.messages.response.apps.ResponseLogin
     */
    @WebMethod(action = "urn:login")
    @WebResult(name = "login", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "loginResponse")
    ResponseLogin login(
            @WebParam(name = "loginRequest", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "loginRequest")
                    RequestLogin loginRequest);

    /**
     * 
     * @param registrarUsuarioRequest
     * @return
     *     returns ws.sivale.com.mx.messages.response.apps.ResponseRegistro
     */
    @WebMethod(action = "urn:registrarUsuario")
    @WebResult(name = "registrarUsuario", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "registrarUsuarioResponse")
    ResponseRegistro registrarUsuario(
            @WebParam(name = "registrarUsuarioRequest", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "registrarUsuarioRequest")
                    RequestRegistro registrarUsuarioRequest);

    /**
     * 
     * @param finalizarTraspasosRequest
     * @return
     *     returns ws.sivale.com.mx.messages.response.apps.ResponseTraspaso
     */
    @WebMethod(action = "urn:finalizarTraspasos")
    @WebResult(name = "finalizarTraspasosResponse", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "finalizarTraspasosResponse")
    ResponseTraspaso finalizarTraspasos(
            @WebParam(name = "finalizarTraspasosRequest", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "finalizarTraspasosRequest")
                    RequestSolicitud finalizarTraspasosRequest);

    /**
     * 
     * @param solicitarTraspasosRequest
     * @return
     *     returns ws.sivale.com.mx.messages.response.apps.ResponseTraspaso
     */
    @WebMethod(action = "urn:solicitarTraspasos")
    @WebResult(name = "solicitarTraspasosResponse", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "solicitarTraspasosResponse")
    ResponseTraspaso solicitarTraspasos(
            @WebParam(name = "solicitarTraspasosRequest", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "solicitarTraspasosRequest")
                    RequestTraspasos solicitarTraspasosRequest);

    /**
     * 
     * @param registrarCorreoValidoRequest
     * @return
     *     returns ws.sivale.com.mx.messages.response.apps.ResponseGenerico
     */
    @WebMethod(action = "urn:registrarCorreoValido")
    @WebResult(name = "registrarCorreoValidoResponse", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "eregistrarCorreoValidoResponse")
    ResponseGenerico registrarCorreoValido(
            @WebParam(name = "registrarCorreoValidoRequest", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "registrarCorreoValidoRequest")
                    RequestRegistrarCorreoValido registrarCorreoValidoRequest);

    /**
     * 
     * @param productosByClienteRequest
     * @return
     *     returns ws.sivale.com.mx.messages.response.apps.ResponseProductos
     */
    @WebMethod(action = "urn:productosByCliente")
    @WebResult(name = "productosByClienteResponse", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "productosByClienteResponse")
    ResponseProductos productosByCliente(
            @WebParam(name = "productosByClienteRequest", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "productosByClienteRequest")
                    RequestCliente productosByClienteRequest);

    /**
     * 
     * @param finalizarTraspasoRequest
     * @return
     *     returns ws.sivale.com.mx.messages.response.apps.ResponseTraspaso
     */
    @WebMethod(action = "urn:finalizarTraspaso")
    @WebResult(name = "finalizarTraspasoResponse", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "finalizarTraspasoResponse")
    ResponseTraspaso finalizarTraspaso(
            @WebParam(name = "finalizarTraspasoRequest", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "finalizarTraspasoRequest")
                    RequestSolicitud finalizarTraspasoRequest);

    /**
     * 
     * @param entregarTarjetaRequest
     * @return
     *     returns ws.sivale.com.mx.messages.response.apps.ResponseTarjetas
     */
    @WebMethod(action = "urn:entregarTarjeta")
    @WebResult(name = "entregarTarjetaResponse", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "entregarTarjetaResponse")
    ResponseTarjetas entregarTarjeta(
            @WebParam(name = "entregarTarjetaRequest", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "entregarTarjetaRequest")
                    RequestEntregaTarjetas entregarTarjetaRequest);

    /**
     * 
     * @param solicitarTraspasoRequest
     * @return
     *     returns ws.sivale.com.mx.messages.response.apps.ResponseTraspaso
     */
    @WebMethod(action = "urn:solicitarTraspaso")
    @WebResult(name = "solicitarTraspasoResponse", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "solicitarTraspasoResponse")
    ResponseTraspaso solicitarTraspaso(
            @WebParam(name = "solicitarTraspasoRequest", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "solicitarTraspasoRequest")
                    RequestTraspaso solicitarTraspasoRequest);

    /**
     * 
     * @param solicitarAutentificacionRequest
     * @return
     *     returns ws.sivale.com.mx.messages.response.apps.ResponseAutentificacion
     */
    @WebMethod(action = "urn:solicitarAutentificacion")
    @WebResult(name = "solicitarAutentificacionResponse", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "solicitarAutentificacionResponse")
    ResponseAutentificacion solicitarAutentificacion(
            @WebParam(name = "solicitarAutentificacionRequest", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "solicitarAutentificacionRequest")
                    RequestCorreo solicitarAutentificacionRequest);

    /**
     * 
     * @param editarAutentificacionRequest
     * @return
     *     returns ws.sivale.com.mx.messages.response.apps.ResponseGenerico
     */
    @WebMethod(action = "urn:editarAutentificacion")
    @WebResult(name = "editarAutentificacionResponse", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "editarAutentificacionResponse")
    ResponseGenerico editarAutentificacion(
            @WebParam(name = "editarAutentificacionRequest", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "editarAutentificacionRequest")
                    RequestEditAutentification editarAutentificacionRequest);

    /**
     * 
     * @param eliminarTarjetaRequest
     * @return
     *     returns ws.sivale.com.mx.messages.response.apps.ResponseGenerico
     */
    @WebMethod(action = "urn:eliminarTarjeta")
    @WebResult(name = "eliminarTarjetaResponse", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "eliminarTarjetaResponse")
    ResponseGenerico eliminarTarjeta(
            @WebParam(name = "eliminarTarjetaRequest", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "eliminarTarjetaRequest")
                    RequestTarjeta eliminarTarjetaRequest);

    /**
     * 
     * @param agregarTarjetaRequest
     * @return
     *     returns ws.sivale.com.mx.messages.response.apps.ResponseGenerico
     */
    @WebMethod(action = "urn:agregarTarjeta")
    @WebResult(name = "agregarTarjetaResponse", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "agregarTarjetaResponse")
    ResponseGenerico agregarTarjeta(
            @WebParam(name = "agregarTarjetaRequest", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "agregarTarjetaRequest")
                    RequestTarjeta agregarTarjetaRequest);

    /**
     * 
     * @param detalleSolicitudRequest
     * @return
     *     returns ws.sivale.com.mx.messages.response.apps.ResponseDetalleSolicitud
     */
    @WebMethod(action = "urn:detalleSolicitud")
    @WebResult(name = "detalleSolicitudResponse", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "detalleSolicitudResponse")
    ResponseDetalleSolicitud detalleSolicitud(
            @WebParam(name = "detalleSolicitudRequest", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "detalleSolicitudRequest")
                    RequestSolicitud detalleSolicitudRequest);

    /**
     * 
     * @param consultarTarjetasRequest
     * @return
     *     returns ws.sivale.com.mx.messages.response.appgasolina.ResponseTarjetasChofer
     */
    @WebMethod(action = "urn:consultarTarjetas")
    @WebResult(name = "consultarTarjetasResponse", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "consultarTarjetasResponse")
    ResponseTarjetasChofer consultarTarjetas(
            @WebParam(name = "consultarTarjetasRequest", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "consultarTarjetasRequest")
                    RequestConsultarTarjetas consultarTarjetasRequest);

    /**
     * 
     * @param consultaComprasRequest
     * @return
     *     returns ws.sivale.com.mx.messages.response.apps.ResponseCompras
     */
    @WebMethod(action = "urn:consultaCompras")
    @WebResult(name = "consultaComprasResponse", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "consultaComprasResponse")
    ResponseCompras consultaCompras(
            @WebParam(name = "consultaComprasRequest", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "consultaComprasRequest")
                    RequestConsultaCompras consultaComprasRequest);

    /**
     * 
     * @param eliminarUsuarioRequest
     * @return
     *     returns ws.sivale.com.mx.messages.response.apps.ResponseGenerico
     */
    @WebMethod(action = "urn:eliminarUsuario")
    @WebResult(name = "eliminarUsuario", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "eliminarUsuarioResponse")
    ResponseGenerico eliminarUsuario(
            @WebParam(name = "eliminarUsuarioRequest", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "eliminarUsuarioRequest")
                    RequestEliminar eliminarUsuarioRequest);

    /**
     * 
     * @param usuariosRegistradosRequest
     * @return
     *     returns ws.sivale.com.mx.messages.response.apps.ResponseUsuarios
     */
    @WebMethod(action = "urn:usuariosRegistrados")
    @WebResult(name = "usuariosRegistradosResponse", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "usuariosRegistradosResponse")
    ResponseUsuarios usuariosRegistrados(
            @WebParam(name = "usuariosRegistradosRequest", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "usuariosRegistradosRequest")
                    RequestUsuariosRegistrados usuariosRegistradosRequest);

    /**
     * 
     * @param consultaSolicitudesRequest
     * @return
     *     returns ws.sivale.com.mx.messages.response.apps.ResponseSolicitudes
     */
    @WebMethod(action = "urn:consultaSolicitudes")
    @WebResult(name = "consultaSolicitudesResponse", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "consultaSolicitudesResponse")
    ResponseSolicitudes consultaSolicitudes(
            @WebParam(name = "consultaSolicitudesRequest", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "consultaSolicitudesRequest")
                    RequestSolicitudes consultaSolicitudesRequest);

    /**
     * 
     * @param transaccionesDMRequest
     * @return
     *     returns ws.sivale.com.mx.messages.response.apps.ResponseTransaccionesDM
     */
    @WebMethod(action = "urn:transaccionesDM")
    @WebResult(name = "transaccionesDMResponse", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "transaccionesDMResponse")
    ResponseTransaccionesDM transaccionesDM(
            @WebParam(name = "transaccionesDMRequest", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "transaccionesDMRequest")
                    RequestTransaccionesDm transaccionesDMRequest);

    /**
     * 
     * @param transaccionesECRequest
     * @return
     *     returns ws.sivale.com.mx.messages.response.apps.ResponseTransacciones
     */
    @WebMethod(action = "urn:transaccionesEC")
    @WebResult(name = "transaccionesECResponse", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "transaccionesECResponse")
    ResponseTransacciones transaccionesEC(
            @WebParam(name = "transaccionesECRequest", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "transaccionesECRequest")
                    RequestTransacciones transaccionesECRequest);

    /**
     * 
     * @param datosTarjetaRequest
     * @return
     *     returns ws.sivale.com.mx.messages.response.apps.ResponseDatos
     */
    @WebMethod(action = "urn:datosTarjeta")
    @WebResult(name = "datosTarjetaResponse", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "datosTarjetaResponse")
    ResponseDatos datosTarjeta(
            @WebParam(name = "datosTarjetaRequest", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "datosTarjetaRequest")
                    RequestIut datosTarjetaRequest);

    /**
     * 
     * @param editarUsuarioRequest
     * @return
     *     returns ws.sivale.com.mx.messages.response.apps.ResponseGenerico
     */
    @WebMethod(action = "urn:editarUsuario")
    @WebResult(name = "editarUsuarioResponse", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "editarUsuarioResponse")
    ResponseGenerico editarUsuario(
            @WebParam(name = "editarUsuarioRequest", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "editarUsuarioRequest")
                    RequestEditar editarUsuarioRequest);

    /**
     * 
     * @param tarjetasUsuarioRequest
     * @return
     *     returns ws.sivale.com.mx.messages.response.appgasolina.ResponseTarjetasChofer
     */
    @WebMethod(action = "urn:tarjetasUsuario")
    @WebResult(name = "tarjetasUsuarioResponse", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "tarjetasUsuarioResponse")
    ResponseTarjetasChofer tarjetasUsuario(
            @WebParam(name = "tarjetasUsuarioRequest", targetNamespace = "http://mx.com.sivale.ws/exposition/servicios/apps", partName = "tarjetasUsuarioRequest")
                    RequestClienteIdentificador tarjetasUsuarioRequest);

}

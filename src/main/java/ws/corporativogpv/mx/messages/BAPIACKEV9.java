
package ws.corporativogpv.mx.messages;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Posting in accounting: CO-PA acct assignment value fields
 * 
 * <p>Clase Java para BAPIACKEV9 complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="BAPIACKEV9">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ITEMNO_ACC" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *               &lt;pattern value="\d+"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="FIELDNAME" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="30"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CURR_TYPE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CURRENCY" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CURRENCY_ISO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AMT_VALCOM" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="23"/>
 *               &lt;fractionDigits value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BASE_UOM" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BASE_UOM_ISO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="QUA_VALCOM" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="15"/>
 *               &lt;fractionDigits value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AMT_VALCOM_LONG" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="31"/>
 *               &lt;fractionDigits value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BAPIACKEV9", propOrder = {
    "itemnoacc",
    "fieldname",
    "currtype",
    "currency",
    "currencyiso",
    "amtvalcom",
    "baseuom",
    "baseuomiso",
    "quavalcom",
    "amtvalcomlong"
})
public class BAPIACKEV9 {

    @XmlElement(name = "ITEMNO_ACC")
    protected String itemnoacc;
    @XmlElement(name = "FIELDNAME")
    protected String fieldname;
    @XmlElement(name = "CURR_TYPE")
    protected String currtype;
    @XmlElement(name = "CURRENCY")
    protected String currency;
    @XmlElement(name = "CURRENCY_ISO")
    protected String currencyiso;
    @XmlElement(name = "AMT_VALCOM")
    protected BigDecimal amtvalcom;
    @XmlElement(name = "BASE_UOM")
    protected String baseuom;
    @XmlElement(name = "BASE_UOM_ISO")
    protected String baseuomiso;
    @XmlElement(name = "QUA_VALCOM")
    protected BigDecimal quavalcom;
    @XmlElement(name = "AMT_VALCOM_LONG")
    protected BigDecimal amtvalcomlong;

    /**
     * Obtiene el valor de la propiedad itemnoacc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getITEMNOACC() {
        return itemnoacc;
    }

    /**
     * Define el valor de la propiedad itemnoacc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setITEMNOACC(String value) {
        this.itemnoacc = value;
    }

    /**
     * Obtiene el valor de la propiedad fieldname.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFIELDNAME() {
        return fieldname;
    }

    /**
     * Define el valor de la propiedad fieldname.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFIELDNAME(String value) {
        this.fieldname = value;
    }

    /**
     * Obtiene el valor de la propiedad currtype.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCURRTYPE() {
        return currtype;
    }

    /**
     * Define el valor de la propiedad currtype.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCURRTYPE(String value) {
        this.currtype = value;
    }

    /**
     * Obtiene el valor de la propiedad currency.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCURRENCY() {
        return currency;
    }

    /**
     * Define el valor de la propiedad currency.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCURRENCY(String value) {
        this.currency = value;
    }

    /**
     * Obtiene el valor de la propiedad currencyiso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCURRENCYISO() {
        return currencyiso;
    }

    /**
     * Define el valor de la propiedad currencyiso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCURRENCYISO(String value) {
        this.currencyiso = value;
    }

    /**
     * Obtiene el valor de la propiedad amtvalcom.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAMTVALCOM() {
        return amtvalcom;
    }

    /**
     * Define el valor de la propiedad amtvalcom.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAMTVALCOM(BigDecimal value) {
        this.amtvalcom = value;
    }

    /**
     * Obtiene el valor de la propiedad baseuom.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBASEUOM() {
        return baseuom;
    }

    /**
     * Define el valor de la propiedad baseuom.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBASEUOM(String value) {
        this.baseuom = value;
    }

    /**
     * Obtiene el valor de la propiedad baseuomiso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBASEUOMISO() {
        return baseuomiso;
    }

    /**
     * Define el valor de la propiedad baseuomiso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBASEUOMISO(String value) {
        this.baseuomiso = value;
    }

    /**
     * Obtiene el valor de la propiedad quavalcom.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getQUAVALCOM() {
        return quavalcom;
    }

    /**
     * Define el valor de la propiedad quavalcom.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setQUAVALCOM(BigDecimal value) {
        this.quavalcom = value;
    }

    /**
     * Obtiene el valor de la propiedad amtvalcomlong.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAMTVALCOMLONG() {
        return amtvalcomlong;
    }

    /**
     * Define el valor de la propiedad amtvalcomlong.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAMTVALCOMLONG(BigDecimal value) {
        this.amtvalcomlong = value;
    }

}

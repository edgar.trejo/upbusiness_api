
package ws.corporativogpv.mx.messages;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Ref. structure for BAPI parameter ExtensionIn/ExtensionOut
 * 
 * <p>Clase Java para BAPIPAREX complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="BAPIPAREX">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="STRUCTURE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="30"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="VALUEPART1" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="240"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="VALUEPART2" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="240"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="VALUEPART3" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="240"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="VALUEPART4" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="240"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BAPIPAREX", propOrder = {
    "structure",
    "valuepart1",
    "valuepart2",
    "valuepart3",
    "valuepart4"
})
public class BAPIPAREX {

    @XmlElement(name = "STRUCTURE")
    protected String structure;
    @XmlElement(name = "VALUEPART1")
    protected String valuepart1;
    @XmlElement(name = "VALUEPART2")
    protected String valuepart2;
    @XmlElement(name = "VALUEPART3")
    protected String valuepart3;
    @XmlElement(name = "VALUEPART4")
    protected String valuepart4;

    /**
     * Obtiene el valor de la propiedad structure.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTRUCTURE() {
        return structure;
    }

    /**
     * Define el valor de la propiedad structure.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTRUCTURE(String value) {
        this.structure = value;
    }

    /**
     * Obtiene el valor de la propiedad valuepart1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVALUEPART1() {
        return valuepart1;
    }

    /**
     * Define el valor de la propiedad valuepart1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVALUEPART1(String value) {
        this.valuepart1 = value;
    }

    /**
     * Obtiene el valor de la propiedad valuepart2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVALUEPART2() {
        return valuepart2;
    }

    /**
     * Define el valor de la propiedad valuepart2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVALUEPART2(String value) {
        this.valuepart2 = value;
    }

    /**
     * Obtiene el valor de la propiedad valuepart3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVALUEPART3() {
        return valuepart3;
    }

    /**
     * Define el valor de la propiedad valuepart3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVALUEPART3(String value) {
        this.valuepart3 = value;
    }

    /**
     * Obtiene el valor de la propiedad valuepart4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVALUEPART4() {
        return valuepart4;
    }

    /**
     * Define el valor de la propiedad valuepart4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVALUEPART4(String value) {
        this.valuepart4 = value;
    }

}

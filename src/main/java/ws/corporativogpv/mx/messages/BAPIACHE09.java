
package ws.corporativogpv.mx.messages;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Header
 * 
 * <p>Clase Java para BAPIACHE09 complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="BAPIACHE09">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="OBJ_TYPE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OBJ_KEY" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OBJ_SYS" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BUS_ACT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="USERNAME" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="12"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="HEADER_TXT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="25"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="COMP_CODE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DOC_DATE" type="{urn:sap-com:document:sap:rfc:functions}date" minOccurs="0"/>
 *         &lt;element name="PSTNG_DATE" type="{urn:sap-com:document:sap:rfc:functions}date" minOccurs="0"/>
 *         &lt;element name="TRANS_DATE" type="{urn:sap-com:document:sap:rfc:functions}date" minOccurs="0"/>
 *         &lt;element name="FISC_YEAR" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *               &lt;pattern value="\d+"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="FIS_PERIOD" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *               &lt;pattern value="\d+"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DOC_TYPE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="REF_DOC_NO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="16"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AC_DOC_NO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OBJ_KEY_R" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="REASON_REV" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="COMPO_ACC" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="REF_DOC_NO_LONG" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="35"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ACC_PRINCIPLE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="NEG_POSTNG" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OBJ_KEY_INV" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BILL_CATEGORY" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="VATDATE" type="{urn:sap-com:document:sap:rfc:functions}date" minOccurs="0"/>
 *         &lt;element name="INVOICE_REC_DATE" type="{urn:sap-com:document:sap:rfc:functions}date" minOccurs="0"/>
 *         &lt;element name="ECS_ENV" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PARTIAL_REV" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DOC_STATUS" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BAPIACHE09", propOrder = {
    "objtype",
    "objkey",
    "objsys",
    "busact",
    "username",
    "headertxt",
    "compcode",
    "docdate",
    "pstngdate",
    "transdate",
    "fiscyear",
    "fisperiod",
    "doctype",
    "refdocno",
    "acdocno",
    "objkeyr",
    "reasonrev",
    "compoacc",
    "refdocnolong",
    "accprinciple",
    "negpostng",
    "objkeyinv",
    "billcategory",
    "vatdate",
    "invoicerecdate",
    "ecsenv",
    "partialrev",
    "docstatus"
})
public class BAPIACHE09 {

    @XmlElement(name = "OBJ_TYPE")
    protected String objtype;
    @XmlElement(name = "OBJ_KEY")
    protected String objkey;
    @XmlElement(name = "OBJ_SYS")
    protected String objsys;
    @XmlElement(name = "BUS_ACT")
    protected String busact;
    @XmlElement(name = "USERNAME")
    protected String username;
    @XmlElement(name = "HEADER_TXT")
    protected String headertxt;
    @XmlElement(name = "COMP_CODE")
    protected String compcode;
    @XmlElement(name = "DOC_DATE")
    protected String docdate;
    @XmlElement(name = "PSTNG_DATE")
    protected String pstngdate;
    @XmlElement(name = "TRANS_DATE")
    protected String transdate;
    @XmlElement(name = "FISC_YEAR")
    protected String fiscyear;
    @XmlElement(name = "FIS_PERIOD")
    protected String fisperiod;
    @XmlElement(name = "DOC_TYPE")
    protected String doctype;
    @XmlElement(name = "REF_DOC_NO")
    protected String refdocno;
    @XmlElement(name = "AC_DOC_NO")
    protected String acdocno;
    @XmlElement(name = "OBJ_KEY_R")
    protected String objkeyr;
    @XmlElement(name = "REASON_REV")
    protected String reasonrev;
    @XmlElement(name = "COMPO_ACC")
    protected String compoacc;
    @XmlElement(name = "REF_DOC_NO_LONG")
    protected String refdocnolong;
    @XmlElement(name = "ACC_PRINCIPLE")
    protected String accprinciple;
    @XmlElement(name = "NEG_POSTNG")
    protected String negpostng;
    @XmlElement(name = "OBJ_KEY_INV")
    protected String objkeyinv;
    @XmlElement(name = "BILL_CATEGORY")
    protected String billcategory;
    @XmlElement(name = "VATDATE")
    protected String vatdate;
    @XmlElement(name = "INVOICE_REC_DATE")
    protected String invoicerecdate;
    @XmlElement(name = "ECS_ENV")
    protected String ecsenv;
    @XmlElement(name = "PARTIAL_REV")
    protected String partialrev;
    @XmlElement(name = "DOC_STATUS")
    protected String docstatus;

    /**
     * Obtiene el valor de la propiedad objtype.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOBJTYPE() {
        return objtype;
    }

    /**
     * Define el valor de la propiedad objtype.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOBJTYPE(String value) {
        this.objtype = value;
    }

    /**
     * Obtiene el valor de la propiedad objkey.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOBJKEY() {
        return objkey;
    }

    /**
     * Define el valor de la propiedad objkey.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOBJKEY(String value) {
        this.objkey = value;
    }

    /**
     * Obtiene el valor de la propiedad objsys.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOBJSYS() {
        return objsys;
    }

    /**
     * Define el valor de la propiedad objsys.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOBJSYS(String value) {
        this.objsys = value;
    }

    /**
     * Obtiene el valor de la propiedad busact.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUSACT() {
        return busact;
    }

    /**
     * Define el valor de la propiedad busact.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUSACT(String value) {
        this.busact = value;
    }

    /**
     * Obtiene el valor de la propiedad username.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUSERNAME() {
        return username;
    }

    /**
     * Define el valor de la propiedad username.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUSERNAME(String value) {
        this.username = value;
    }

    /**
     * Obtiene el valor de la propiedad headertxt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHEADERTXT() {
        return headertxt;
    }

    /**
     * Define el valor de la propiedad headertxt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHEADERTXT(String value) {
        this.headertxt = value;
    }

    /**
     * Obtiene el valor de la propiedad compcode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOMPCODE() {
        return compcode;
    }

    /**
     * Define el valor de la propiedad compcode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOMPCODE(String value) {
        this.compcode = value;
    }

    /**
     * Obtiene el valor de la propiedad docdate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDOCDATE() {
        return docdate;
    }

    /**
     * Define el valor de la propiedad docdate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDOCDATE(String value) {
        this.docdate = value;
    }

    /**
     * Obtiene el valor de la propiedad pstngdate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPSTNGDATE() {
        return pstngdate;
    }

    /**
     * Define el valor de la propiedad pstngdate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPSTNGDATE(String value) {
        this.pstngdate = value;
    }

    /**
     * Obtiene el valor de la propiedad transdate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTRANSDATE() {
        return transdate;
    }

    /**
     * Define el valor de la propiedad transdate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTRANSDATE(String value) {
        this.transdate = value;
    }

    /**
     * Obtiene el valor de la propiedad fiscyear.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFISCYEAR() {
        return fiscyear;
    }

    /**
     * Define el valor de la propiedad fiscyear.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFISCYEAR(String value) {
        this.fiscyear = value;
    }

    /**
     * Obtiene el valor de la propiedad fisperiod.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFISPERIOD() {
        return fisperiod;
    }

    /**
     * Define el valor de la propiedad fisperiod.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFISPERIOD(String value) {
        this.fisperiod = value;
    }

    /**
     * Obtiene el valor de la propiedad doctype.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDOCTYPE() {
        return doctype;
    }

    /**
     * Define el valor de la propiedad doctype.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDOCTYPE(String value) {
        this.doctype = value;
    }

    /**
     * Obtiene el valor de la propiedad refdocno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREFDOCNO() {
        return refdocno;
    }

    /**
     * Define el valor de la propiedad refdocno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREFDOCNO(String value) {
        this.refdocno = value;
    }

    /**
     * Obtiene el valor de la propiedad acdocno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACDOCNO() {
        return acdocno;
    }

    /**
     * Define el valor de la propiedad acdocno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACDOCNO(String value) {
        this.acdocno = value;
    }

    /**
     * Obtiene el valor de la propiedad objkeyr.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOBJKEYR() {
        return objkeyr;
    }

    /**
     * Define el valor de la propiedad objkeyr.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOBJKEYR(String value) {
        this.objkeyr = value;
    }

    /**
     * Obtiene el valor de la propiedad reasonrev.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREASONREV() {
        return reasonrev;
    }

    /**
     * Define el valor de la propiedad reasonrev.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREASONREV(String value) {
        this.reasonrev = value;
    }

    /**
     * Obtiene el valor de la propiedad compoacc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOMPOACC() {
        return compoacc;
    }

    /**
     * Define el valor de la propiedad compoacc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOMPOACC(String value) {
        this.compoacc = value;
    }

    /**
     * Obtiene el valor de la propiedad refdocnolong.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREFDOCNOLONG() {
        return refdocnolong;
    }

    /**
     * Define el valor de la propiedad refdocnolong.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREFDOCNOLONG(String value) {
        this.refdocnolong = value;
    }

    /**
     * Obtiene el valor de la propiedad accprinciple.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACCPRINCIPLE() {
        return accprinciple;
    }

    /**
     * Define el valor de la propiedad accprinciple.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACCPRINCIPLE(String value) {
        this.accprinciple = value;
    }

    /**
     * Obtiene el valor de la propiedad negpostng.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNEGPOSTNG() {
        return negpostng;
    }

    /**
     * Define el valor de la propiedad negpostng.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNEGPOSTNG(String value) {
        this.negpostng = value;
    }

    /**
     * Obtiene el valor de la propiedad objkeyinv.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOBJKEYINV() {
        return objkeyinv;
    }

    /**
     * Define el valor de la propiedad objkeyinv.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOBJKEYINV(String value) {
        this.objkeyinv = value;
    }

    /**
     * Obtiene el valor de la propiedad billcategory.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBILLCATEGORY() {
        return billcategory;
    }

    /**
     * Define el valor de la propiedad billcategory.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBILLCATEGORY(String value) {
        this.billcategory = value;
    }

    /**
     * Obtiene el valor de la propiedad vatdate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVATDATE() {
        return vatdate;
    }

    /**
     * Define el valor de la propiedad vatdate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVATDATE(String value) {
        this.vatdate = value;
    }

    /**
     * Obtiene el valor de la propiedad invoicerecdate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINVOICERECDATE() {
        return invoicerecdate;
    }

    /**
     * Define el valor de la propiedad invoicerecdate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINVOICERECDATE(String value) {
        this.invoicerecdate = value;
    }

    /**
     * Obtiene el valor de la propiedad ecsenv.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getECSENV() {
        return ecsenv;
    }

    /**
     * Define el valor de la propiedad ecsenv.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setECSENV(String value) {
        this.ecsenv = value;
    }

    /**
     * Obtiene el valor de la propiedad partialrev.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPARTIALREV() {
        return partialrev;
    }

    /**
     * Define el valor de la propiedad partialrev.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPARTIALREV(String value) {
        this.partialrev = value;
    }

    /**
     * Obtiene el valor de la propiedad docstatus.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDOCSTATUS() {
        return docstatus;
    }

    /**
     * Define el valor de la propiedad docstatus.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDOCSTATUS(String value) {
        this.docstatus = value;
    }

    @Override
    public String toString() {
        return "DOCUMENTHEADER{" +
                "username='" + username + '\'' +
                ", headertxt='" + headertxt + '\'' +
                ", compcode='" + compcode + '\'' +
                ", docdate='" + docdate + '\'' +
                ", pstngdate='" + pstngdate + '\'' +
                ", doctype='" + doctype + '\'' +
                ", refdocno='" + refdocno + '\'' +
                '}';
    }
}

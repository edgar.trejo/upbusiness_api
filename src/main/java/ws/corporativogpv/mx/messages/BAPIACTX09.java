
package ws.corporativogpv.mx.messages;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Tax item
 * 
 * <p>Clase Java para BAPIACTX09 complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="BAPIACTX09">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ITEMNO_ACC" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *               &lt;pattern value="\d+"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="GL_ACCOUNT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="COND_KEY" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ACCT_KEY" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TAX_CODE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TAX_RATE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="7"/>
 *               &lt;fractionDigits value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TAX_DATE" type="{urn:sap-com:document:sap:rfc:functions}date" minOccurs="0"/>
 *         &lt;element name="TAXJURCODE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="15"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TAXJURCODE_DEEP" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="15"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TAXJURCODE_LEVEL" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ITEMNO_TAX" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="6"/>
 *               &lt;pattern value="\d+"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DIRECT_TAX" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BAPIACTX09", propOrder = {
    "itemnoacc",
    "glaccount",
    "condkey",
    "acctkey",
    "taxcode",
    "taxrate",
    "taxdate",
    "taxjurcode",
    "taxjurcodedeep",
    "taxjurcodelevel",
    "itemnotax",
    "directtax"
})
public class BAPIACTX09 {

    @XmlElement(name = "ITEMNO_ACC")
    protected String itemnoacc;
    @XmlElement(name = "GL_ACCOUNT")
    protected String glaccount;
    @XmlElement(name = "COND_KEY")
    protected String condkey;
    @XmlElement(name = "ACCT_KEY")
    protected String acctkey;
    @XmlElement(name = "TAX_CODE")
    protected String taxcode;
    @XmlElement(name = "TAX_RATE")
    protected BigDecimal taxrate;
    @XmlElement(name = "TAX_DATE")
    protected String taxdate;
    @XmlElement(name = "TAXJURCODE")
    protected String taxjurcode;
    @XmlElement(name = "TAXJURCODE_DEEP")
    protected String taxjurcodedeep;
    @XmlElement(name = "TAXJURCODE_LEVEL")
    protected String taxjurcodelevel;
    @XmlElement(name = "ITEMNO_TAX")
    protected String itemnotax;
    @XmlElement(name = "DIRECT_TAX")
    protected String directtax;

    /**
     * Obtiene el valor de la propiedad itemnoacc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getITEMNOACC() {
        return itemnoacc;
    }

    /**
     * Define el valor de la propiedad itemnoacc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setITEMNOACC(String value) {
        this.itemnoacc = value;
    }

    /**
     * Obtiene el valor de la propiedad glaccount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGLACCOUNT() {
        return glaccount;
    }

    /**
     * Define el valor de la propiedad glaccount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGLACCOUNT(String value) {
        this.glaccount = value;
    }

    /**
     * Obtiene el valor de la propiedad condkey.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCONDKEY() {
        return condkey;
    }

    /**
     * Define el valor de la propiedad condkey.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCONDKEY(String value) {
        this.condkey = value;
    }

    /**
     * Obtiene el valor de la propiedad acctkey.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getACCTKEY() {
        return acctkey;
    }

    /**
     * Define el valor de la propiedad acctkey.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setACCTKEY(String value) {
        this.acctkey = value;
    }

    /**
     * Obtiene el valor de la propiedad taxcode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTAXCODE() {
        return taxcode;
    }

    /**
     * Define el valor de la propiedad taxcode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTAXCODE(String value) {
        this.taxcode = value;
    }

    /**
     * Obtiene el valor de la propiedad taxrate.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTAXRATE() {
        return taxrate;
    }

    /**
     * Define el valor de la propiedad taxrate.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTAXRATE(BigDecimal value) {
        this.taxrate = value;
    }

    /**
     * Obtiene el valor de la propiedad taxdate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTAXDATE() {
        return taxdate;
    }

    /**
     * Define el valor de la propiedad taxdate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTAXDATE(String value) {
        this.taxdate = value;
    }

    /**
     * Obtiene el valor de la propiedad taxjurcode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTAXJURCODE() {
        return taxjurcode;
    }

    /**
     * Define el valor de la propiedad taxjurcode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTAXJURCODE(String value) {
        this.taxjurcode = value;
    }

    /**
     * Obtiene el valor de la propiedad taxjurcodedeep.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTAXJURCODEDEEP() {
        return taxjurcodedeep;
    }

    /**
     * Define el valor de la propiedad taxjurcodedeep.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTAXJURCODEDEEP(String value) {
        this.taxjurcodedeep = value;
    }

    /**
     * Obtiene el valor de la propiedad taxjurcodelevel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTAXJURCODELEVEL() {
        return taxjurcodelevel;
    }

    /**
     * Define el valor de la propiedad taxjurcodelevel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTAXJURCODELEVEL(String value) {
        this.taxjurcodelevel = value;
    }

    /**
     * Obtiene el valor de la propiedad itemnotax.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getITEMNOTAX() {
        return itemnotax;
    }

    /**
     * Define el valor de la propiedad itemnotax.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setITEMNOTAX(String value) {
        this.itemnotax = value;
    }

    /**
     * Obtiene el valor de la propiedad directtax.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDIRECTTAX() {
        return directtax;
    }

    /**
     * Define el valor de la propiedad directtax.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDIRECTTAX(String value) {
        this.directtax = value;
    }

    @Override
    public String toString() {
        return "BAPIACTX09{" +
                "itemnoacc='" + itemnoacc + '\'' +
                ", glaccount='" + glaccount + '\'' +
                ", taxcode='" + taxcode + '\'' +
                '}';
    }
}

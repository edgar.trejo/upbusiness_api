
package ws.corporativogpv.mx.messages;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Posting in accounting: Partner billing doc (load receivable)
 * 
 * <p>Clase Java para BAPIACPA09 complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="BAPIACPA09">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NAME" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="35"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="NAME_2" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="35"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="NAME_3" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="35"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="NAME_4" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="35"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="POSTL_CODE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CITY" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="35"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="COUNTRY" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="COUNTRY_ISO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="STREET" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="35"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PO_BOX" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="POBX_PCD" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="POBK_CURAC" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="16"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BANK_ACCT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="18"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BANK_NO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="15"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BANK_CTRY" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BANK_CTRY_ISO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TAX_NO_1" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="16"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TAX_NO_2" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="11"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TAX" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="EQUAL_TAX" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="REGION" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CTRL_KEY" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="INSTR_KEY" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DME_IND" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="LANGU_ISO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="IBAN" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="34"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SWIFT_CODE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="11"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TAX_NO_3" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="18"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TAX_NO_4" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="18"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TITLE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="15"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TAX_NO_5" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="60"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BAPIACPA09", propOrder = {
    "name",
    "name2",
    "name3",
    "name4",
    "postlcode",
    "city",
    "country",
    "countryiso",
    "street",
    "pobox",
    "pobxpcd",
    "pobkcurac",
    "bankacct",
    "bankno",
    "bankctry",
    "bankctryiso",
    "taxno1",
    "taxno2",
    "tax",
    "equaltax",
    "region",
    "ctrlkey",
    "instrkey",
    "dmeind",
    "languiso",
    "iban",
    "swiftcode",
    "taxno3",
    "taxno4",
    "title",
    "taxno5"
})
public class BAPIACPA09 {

    @XmlElement(name = "NAME")
    protected String name;
    @XmlElement(name = "NAME_2")
    protected String name2;
    @XmlElement(name = "NAME_3")
    protected String name3;
    @XmlElement(name = "NAME_4")
    protected String name4;
    @XmlElement(name = "POSTL_CODE")
    protected String postlcode;
    @XmlElement(name = "CITY")
    protected String city;
    @XmlElement(name = "COUNTRY")
    protected String country;
    @XmlElement(name = "COUNTRY_ISO")
    protected String countryiso;
    @XmlElement(name = "STREET")
    protected String street;
    @XmlElement(name = "PO_BOX")
    protected String pobox;
    @XmlElement(name = "POBX_PCD")
    protected String pobxpcd;
    @XmlElement(name = "POBK_CURAC")
    protected String pobkcurac;
    @XmlElement(name = "BANK_ACCT")
    protected String bankacct;
    @XmlElement(name = "BANK_NO")
    protected String bankno;
    @XmlElement(name = "BANK_CTRY")
    protected String bankctry;
    @XmlElement(name = "BANK_CTRY_ISO")
    protected String bankctryiso;
    @XmlElement(name = "TAX_NO_1")
    protected String taxno1;
    @XmlElement(name = "TAX_NO_2")
    protected String taxno2;
    @XmlElement(name = "TAX")
    protected String tax;
    @XmlElement(name = "EQUAL_TAX")
    protected String equaltax;
    @XmlElement(name = "REGION")
    protected String region;
    @XmlElement(name = "CTRL_KEY")
    protected String ctrlkey;
    @XmlElement(name = "INSTR_KEY")
    protected String instrkey;
    @XmlElement(name = "DME_IND")
    protected String dmeind;
    @XmlElement(name = "LANGU_ISO")
    protected String languiso;
    @XmlElement(name = "IBAN")
    protected String iban;
    @XmlElement(name = "SWIFT_CODE")
    protected String swiftcode;
    @XmlElement(name = "TAX_NO_3")
    protected String taxno3;
    @XmlElement(name = "TAX_NO_4")
    protected String taxno4;
    @XmlElement(name = "TITLE")
    protected String title;
    @XmlElement(name = "TAX_NO_5")
    protected String taxno5;

    /**
     * Obtiene el valor de la propiedad name.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNAME() {
        return name;
    }

    /**
     * Define el valor de la propiedad name.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNAME(String value) {
        this.name = value;
    }

    /**
     * Obtiene el valor de la propiedad name2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNAME2() {
        return name2;
    }

    /**
     * Define el valor de la propiedad name2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNAME2(String value) {
        this.name2 = value;
    }

    /**
     * Obtiene el valor de la propiedad name3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNAME3() {
        return name3;
    }

    /**
     * Define el valor de la propiedad name3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNAME3(String value) {
        this.name3 = value;
    }

    /**
     * Obtiene el valor de la propiedad name4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNAME4() {
        return name4;
    }

    /**
     * Define el valor de la propiedad name4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNAME4(String value) {
        this.name4 = value;
    }

    /**
     * Obtiene el valor de la propiedad postlcode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOSTLCODE() {
        return postlcode;
    }

    /**
     * Define el valor de la propiedad postlcode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOSTLCODE(String value) {
        this.postlcode = value;
    }

    /**
     * Obtiene el valor de la propiedad city.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCITY() {
        return city;
    }

    /**
     * Define el valor de la propiedad city.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCITY(String value) {
        this.city = value;
    }

    /**
     * Obtiene el valor de la propiedad country.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOUNTRY() {
        return country;
    }

    /**
     * Define el valor de la propiedad country.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOUNTRY(String value) {
        this.country = value;
    }

    /**
     * Obtiene el valor de la propiedad countryiso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOUNTRYISO() {
        return countryiso;
    }

    /**
     * Define el valor de la propiedad countryiso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOUNTRYISO(String value) {
        this.countryiso = value;
    }

    /**
     * Obtiene el valor de la propiedad street.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTREET() {
        return street;
    }

    /**
     * Define el valor de la propiedad street.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTREET(String value) {
        this.street = value;
    }

    /**
     * Obtiene el valor de la propiedad pobox.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOBOX() {
        return pobox;
    }

    /**
     * Define el valor de la propiedad pobox.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOBOX(String value) {
        this.pobox = value;
    }

    /**
     * Obtiene el valor de la propiedad pobxpcd.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOBXPCD() {
        return pobxpcd;
    }

    /**
     * Define el valor de la propiedad pobxpcd.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOBXPCD(String value) {
        this.pobxpcd = value;
    }

    /**
     * Obtiene el valor de la propiedad pobkcurac.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOBKCURAC() {
        return pobkcurac;
    }

    /**
     * Define el valor de la propiedad pobkcurac.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOBKCURAC(String value) {
        this.pobkcurac = value;
    }

    /**
     * Obtiene el valor de la propiedad bankacct.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBANKACCT() {
        return bankacct;
    }

    /**
     * Define el valor de la propiedad bankacct.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBANKACCT(String value) {
        this.bankacct = value;
    }

    /**
     * Obtiene el valor de la propiedad bankno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBANKNO() {
        return bankno;
    }

    /**
     * Define el valor de la propiedad bankno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBANKNO(String value) {
        this.bankno = value;
    }

    /**
     * Obtiene el valor de la propiedad bankctry.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBANKCTRY() {
        return bankctry;
    }

    /**
     * Define el valor de la propiedad bankctry.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBANKCTRY(String value) {
        this.bankctry = value;
    }

    /**
     * Obtiene el valor de la propiedad bankctryiso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBANKCTRYISO() {
        return bankctryiso;
    }

    /**
     * Define el valor de la propiedad bankctryiso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBANKCTRYISO(String value) {
        this.bankctryiso = value;
    }

    /**
     * Obtiene el valor de la propiedad taxno1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTAXNO1() {
        return taxno1;
    }

    /**
     * Define el valor de la propiedad taxno1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTAXNO1(String value) {
        this.taxno1 = value;
    }

    /**
     * Obtiene el valor de la propiedad taxno2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTAXNO2() {
        return taxno2;
    }

    /**
     * Define el valor de la propiedad taxno2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTAXNO2(String value) {
        this.taxno2 = value;
    }

    /**
     * Obtiene el valor de la propiedad tax.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTAX() {
        return tax;
    }

    /**
     * Define el valor de la propiedad tax.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTAX(String value) {
        this.tax = value;
    }

    /**
     * Obtiene el valor de la propiedad equaltax.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEQUALTAX() {
        return equaltax;
    }

    /**
     * Define el valor de la propiedad equaltax.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEQUALTAX(String value) {
        this.equaltax = value;
    }

    /**
     * Obtiene el valor de la propiedad region.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREGION() {
        return region;
    }

    /**
     * Define el valor de la propiedad region.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREGION(String value) {
        this.region = value;
    }

    /**
     * Obtiene el valor de la propiedad ctrlkey.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCTRLKEY() {
        return ctrlkey;
    }

    /**
     * Define el valor de la propiedad ctrlkey.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCTRLKEY(String value) {
        this.ctrlkey = value;
    }

    /**
     * Obtiene el valor de la propiedad instrkey.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSTRKEY() {
        return instrkey;
    }

    /**
     * Define el valor de la propiedad instrkey.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSTRKEY(String value) {
        this.instrkey = value;
    }

    /**
     * Obtiene el valor de la propiedad dmeind.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDMEIND() {
        return dmeind;
    }

    /**
     * Define el valor de la propiedad dmeind.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDMEIND(String value) {
        this.dmeind = value;
    }

    /**
     * Obtiene el valor de la propiedad languiso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLANGUISO() {
        return languiso;
    }

    /**
     * Define el valor de la propiedad languiso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLANGUISO(String value) {
        this.languiso = value;
    }

    /**
     * Obtiene el valor de la propiedad iban.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIBAN() {
        return iban;
    }

    /**
     * Define el valor de la propiedad iban.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIBAN(String value) {
        this.iban = value;
    }

    /**
     * Obtiene el valor de la propiedad swiftcode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSWIFTCODE() {
        return swiftcode;
    }

    /**
     * Define el valor de la propiedad swiftcode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSWIFTCODE(String value) {
        this.swiftcode = value;
    }

    /**
     * Obtiene el valor de la propiedad taxno3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTAXNO3() {
        return taxno3;
    }

    /**
     * Define el valor de la propiedad taxno3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTAXNO3(String value) {
        this.taxno3 = value;
    }

    /**
     * Obtiene el valor de la propiedad taxno4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTAXNO4() {
        return taxno4;
    }

    /**
     * Define el valor de la propiedad taxno4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTAXNO4(String value) {
        this.taxno4 = value;
    }

    /**
     * Obtiene el valor de la propiedad title.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTITLE() {
        return title;
    }

    /**
     * Define el valor de la propiedad title.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTITLE(String value) {
        this.title = value;
    }

    /**
     * Obtiene el valor de la propiedad taxno5.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTAXNO5() {
        return taxno5;
    }

    /**
     * Define el valor de la propiedad taxno5.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTAXNO5(String value) {
        this.taxno5 = value;
    }

}

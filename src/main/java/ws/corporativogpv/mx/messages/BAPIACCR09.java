
package ws.corporativogpv.mx.messages;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Currency Items
 * 
 * <p>Clase Java para BAPIACCR09 complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="BAPIACCR09">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ITEMNO_ACC" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *               &lt;pattern value="\d+"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CURR_TYPE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CURRENCY" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CURRENCY_ISO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AMT_DOCCUR" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="23"/>
 *               &lt;fractionDigits value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="EXCH_RATE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="9"/>
 *               &lt;fractionDigits value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="EXCH_RATE_V" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="9"/>
 *               &lt;fractionDigits value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AMT_BASE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="23"/>
 *               &lt;fractionDigits value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DISC_BASE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="23"/>
 *               &lt;fractionDigits value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DISC_AMT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="23"/>
 *               &lt;fractionDigits value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TAX_AMT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="23"/>
 *               &lt;fractionDigits value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AMT_DOCCUR_LONG" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="31"/>
 *               &lt;fractionDigits value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AMT_BASE_LONG" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="31"/>
 *               &lt;fractionDigits value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DISC_BASE_LONG" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="31"/>
 *               &lt;fractionDigits value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DISC_AMT_LONG" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="31"/>
 *               &lt;fractionDigits value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TAX_AMT_LONG" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="31"/>
 *               &lt;fractionDigits value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BAPIACCR09", propOrder = {
    "itemnoacc",
    "currtype",
    "currency",
    "currencyiso",
    "amtdoccur",
    "exchrate",
    "exchratev",
    "amtbase",
    "discbase",
    "discamt",
    "taxamt",
    "amtdoccurlong",
    "amtbaselong",
    "discbaselong",
    "discamtlong",
    "taxamtlong"
})
public class BAPIACCR09 {

    @XmlElement(name = "ITEMNO_ACC")
    protected String itemnoacc;
    @XmlElement(name = "CURR_TYPE")
    protected String currtype;
    @XmlElement(name = "CURRENCY")
    protected String currency;
    @XmlElement(name = "CURRENCY_ISO")
    protected String currencyiso;
    @XmlElement(name = "AMT_DOCCUR")
    protected BigDecimal amtdoccur;
    @XmlElement(name = "EXCH_RATE")
    protected BigDecimal exchrate;
    @XmlElement(name = "EXCH_RATE_V")
    protected BigDecimal exchratev;
    @XmlElement(name = "AMT_BASE")
    protected BigDecimal amtbase;
    @XmlElement(name = "DISC_BASE")
    protected BigDecimal discbase;
    @XmlElement(name = "DISC_AMT")
    protected BigDecimal discamt;
    @XmlElement(name = "TAX_AMT")
    protected BigDecimal taxamt;
    @XmlElement(name = "AMT_DOCCUR_LONG")
    protected BigDecimal amtdoccurlong;
    @XmlElement(name = "AMT_BASE_LONG")
    protected BigDecimal amtbaselong;
    @XmlElement(name = "DISC_BASE_LONG")
    protected BigDecimal discbaselong;
    @XmlElement(name = "DISC_AMT_LONG")
    protected BigDecimal discamtlong;
    @XmlElement(name = "TAX_AMT_LONG")
    protected BigDecimal taxamtlong;

    /**
     * Obtiene el valor de la propiedad itemnoacc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getITEMNOACC() {
        return itemnoacc;
    }

    /**
     * Define el valor de la propiedad itemnoacc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setITEMNOACC(String value) {
        this.itemnoacc = value;
    }

    /**
     * Obtiene el valor de la propiedad currtype.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCURRTYPE() {
        return currtype;
    }

    /**
     * Define el valor de la propiedad currtype.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCURRTYPE(String value) {
        this.currtype = value;
    }

    /**
     * Obtiene el valor de la propiedad currency.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCURRENCY() {
        return currency;
    }

    /**
     * Define el valor de la propiedad currency.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCURRENCY(String value) {
        this.currency = value;
    }

    /**
     * Obtiene el valor de la propiedad currencyiso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCURRENCYISO() {
        return currencyiso;
    }

    /**
     * Define el valor de la propiedad currencyiso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCURRENCYISO(String value) {
        this.currencyiso = value;
    }

    /**
     * Obtiene el valor de la propiedad amtdoccur.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAMTDOCCUR() {
        return amtdoccur;
    }

    /**
     * Define el valor de la propiedad amtdoccur.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAMTDOCCUR(BigDecimal value) {
        this.amtdoccur = value;
    }

    /**
     * Obtiene el valor de la propiedad exchrate.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getEXCHRATE() {
        return exchrate;
    }

    /**
     * Define el valor de la propiedad exchrate.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setEXCHRATE(BigDecimal value) {
        this.exchrate = value;
    }

    /**
     * Obtiene el valor de la propiedad exchratev.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getEXCHRATEV() {
        return exchratev;
    }

    /**
     * Define el valor de la propiedad exchratev.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setEXCHRATEV(BigDecimal value) {
        this.exchratev = value;
    }

    /**
     * Obtiene el valor de la propiedad amtbase.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAMTBASE() {
        return amtbase;
    }

    /**
     * Define el valor de la propiedad amtbase.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAMTBASE(BigDecimal value) {
        this.amtbase = value;
    }

    /**
     * Obtiene el valor de la propiedad discbase.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDISCBASE() {
        return discbase;
    }

    /**
     * Define el valor de la propiedad discbase.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDISCBASE(BigDecimal value) {
        this.discbase = value;
    }

    /**
     * Obtiene el valor de la propiedad discamt.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDISCAMT() {
        return discamt;
    }

    /**
     * Define el valor de la propiedad discamt.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDISCAMT(BigDecimal value) {
        this.discamt = value;
    }

    /**
     * Obtiene el valor de la propiedad taxamt.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTAXAMT() {
        return taxamt;
    }

    /**
     * Define el valor de la propiedad taxamt.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTAXAMT(BigDecimal value) {
        this.taxamt = value;
    }

    /**
     * Obtiene el valor de la propiedad amtdoccurlong.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAMTDOCCURLONG() {
        return amtdoccurlong;
    }

    /**
     * Define el valor de la propiedad amtdoccurlong.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAMTDOCCURLONG(BigDecimal value) {
        this.amtdoccurlong = value;
    }

    /**
     * Obtiene el valor de la propiedad amtbaselong.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAMTBASELONG() {
        return amtbaselong;
    }

    /**
     * Define el valor de la propiedad amtbaselong.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAMTBASELONG(BigDecimal value) {
        this.amtbaselong = value;
    }

    /**
     * Obtiene el valor de la propiedad discbaselong.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDISCBASELONG() {
        return discbaselong;
    }

    /**
     * Define el valor de la propiedad discbaselong.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDISCBASELONG(BigDecimal value) {
        this.discbaselong = value;
    }

    /**
     * Obtiene el valor de la propiedad discamtlong.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDISCAMTLONG() {
        return discamtlong;
    }

    /**
     * Define el valor de la propiedad discamtlong.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDISCAMTLONG(BigDecimal value) {
        this.discamtlong = value;
    }

    /**
     * Obtiene el valor de la propiedad taxamtlong.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTAXAMTLONG() {
        return taxamtlong;
    }

    /**
     * Define el valor de la propiedad taxamtlong.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTAXAMTLONG(BigDecimal value) {
        this.taxamtlong = value;
    }

    @Override
    public String toString() {
        return "CURRENCYAMOUNT{" +
                "itemnoacc='" + itemnoacc + '\'' +
                ", currency='" + currency + '\'' +
                ", amtdoccur=" + amtdoccur + '\'' +
                ", amtbase=" + amtbase +
                '}';
    }
}

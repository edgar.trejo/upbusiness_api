
package ws.corporativogpv.mx.messages;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Withholding Tax Information
 * 
 * <p>Clase Java para BAPIACWT09 complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="BAPIACWT09">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ITEMNO_ACC" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *               &lt;pattern value="\d+"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="WT_TYPE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="WT_CODE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BAS_AMT_LC" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="23"/>
 *               &lt;fractionDigits value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BAS_AMT_TC" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="23"/>
 *               &lt;fractionDigits value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BAS_AMT_L2" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="23"/>
 *               &lt;fractionDigits value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BAS_AMT_L3" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="23"/>
 *               &lt;fractionDigits value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="MAN_AMT_LC" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="23"/>
 *               &lt;fractionDigits value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="MAN_AMT_TC" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="23"/>
 *               &lt;fractionDigits value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="MAN_AMT_L2" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="23"/>
 *               &lt;fractionDigits value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="MAN_AMT_L3" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="23"/>
 *               &lt;fractionDigits value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AWH_AMT_LC" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="23"/>
 *               &lt;fractionDigits value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AWH_AMT_TC" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="23"/>
 *               &lt;fractionDigits value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AWH_AMT_L2" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="23"/>
 *               &lt;fractionDigits value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AWH_AMT_L3" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="23"/>
 *               &lt;fractionDigits value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BAS_AMT_IND" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="MAN_AMT_IND" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BAS_AMT_LC_LONG" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="31"/>
 *               &lt;fractionDigits value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BAS_AMT_TC_LONG" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="31"/>
 *               &lt;fractionDigits value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BAS_AMT_L2_LONG" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="31"/>
 *               &lt;fractionDigits value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BAS_AMT_L3_LONG" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="31"/>
 *               &lt;fractionDigits value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="MAN_AMT_LC_LONG" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="31"/>
 *               &lt;fractionDigits value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="MAN_AMT_TC_LONG" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="31"/>
 *               &lt;fractionDigits value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="MAN_AMT_L2_LONG" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="31"/>
 *               &lt;fractionDigits value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="MAN_AMT_L3_LONG" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="31"/>
 *               &lt;fractionDigits value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AWH_AMT_LC_LONG" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="31"/>
 *               &lt;fractionDigits value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AWH_AMT_TC_LONG" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="31"/>
 *               &lt;fractionDigits value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AWH_AMT_L2_LONG" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="31"/>
 *               &lt;fractionDigits value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AWH_AMT_L3_LONG" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="31"/>
 *               &lt;fractionDigits value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BAPIACWT09", propOrder = {
    "itemnoacc",
    "wttype",
    "wtcode",
    "basamtlc",
    "basamttc",
    "basamtl2",
    "basamtl3",
    "manamtlc",
    "manamttc",
    "manamtl2",
    "manamtl3",
    "awhamtlc",
    "awhamttc",
    "awhamtl2",
    "awhamtl3",
    "basamtind",
    "manamtind",
    "basamtlclong",
    "basamttclong",
    "basamtl2LONG",
    "basamtl3LONG",
    "manamtlclong",
    "manamttclong",
    "manamtl2LONG",
    "manamtl3LONG",
    "awhamtlclong",
    "awhamttclong",
    "awhamtl2LONG",
    "awhamtl3LONG"
})
public class BAPIACWT09 {

    @XmlElement(name = "ITEMNO_ACC")
    protected String itemnoacc;
    @XmlElement(name = "WT_TYPE")
    protected String wttype;
    @XmlElement(name = "WT_CODE")
    protected String wtcode;
    @XmlElement(name = "BAS_AMT_LC")
    protected BigDecimal basamtlc;
    @XmlElement(name = "BAS_AMT_TC")
    protected BigDecimal basamttc;
    @XmlElement(name = "BAS_AMT_L2")
    protected BigDecimal basamtl2;
    @XmlElement(name = "BAS_AMT_L3")
    protected BigDecimal basamtl3;
    @XmlElement(name = "MAN_AMT_LC")
    protected BigDecimal manamtlc;
    @XmlElement(name = "MAN_AMT_TC")
    protected BigDecimal manamttc;
    @XmlElement(name = "MAN_AMT_L2")
    protected BigDecimal manamtl2;
    @XmlElement(name = "MAN_AMT_L3")
    protected BigDecimal manamtl3;
    @XmlElement(name = "AWH_AMT_LC")
    protected BigDecimal awhamtlc;
    @XmlElement(name = "AWH_AMT_TC")
    protected BigDecimal awhamttc;
    @XmlElement(name = "AWH_AMT_L2")
    protected BigDecimal awhamtl2;
    @XmlElement(name = "AWH_AMT_L3")
    protected BigDecimal awhamtl3;
    @XmlElement(name = "BAS_AMT_IND")
    protected String basamtind;
    @XmlElement(name = "MAN_AMT_IND")
    protected String manamtind;
    @XmlElement(name = "BAS_AMT_LC_LONG")
    protected BigDecimal basamtlclong;
    @XmlElement(name = "BAS_AMT_TC_LONG")
    protected BigDecimal basamttclong;
    @XmlElement(name = "BAS_AMT_L2_LONG")
    protected BigDecimal basamtl2LONG;
    @XmlElement(name = "BAS_AMT_L3_LONG")
    protected BigDecimal basamtl3LONG;
    @XmlElement(name = "MAN_AMT_LC_LONG")
    protected BigDecimal manamtlclong;
    @XmlElement(name = "MAN_AMT_TC_LONG")
    protected BigDecimal manamttclong;
    @XmlElement(name = "MAN_AMT_L2_LONG")
    protected BigDecimal manamtl2LONG;
    @XmlElement(name = "MAN_AMT_L3_LONG")
    protected BigDecimal manamtl3LONG;
    @XmlElement(name = "AWH_AMT_LC_LONG")
    protected BigDecimal awhamtlclong;
    @XmlElement(name = "AWH_AMT_TC_LONG")
    protected BigDecimal awhamttclong;
    @XmlElement(name = "AWH_AMT_L2_LONG")
    protected BigDecimal awhamtl2LONG;
    @XmlElement(name = "AWH_AMT_L3_LONG")
    protected BigDecimal awhamtl3LONG;

    /**
     * Obtiene el valor de la propiedad itemnoacc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getITEMNOACC() {
        return itemnoacc;
    }

    /**
     * Define el valor de la propiedad itemnoacc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setITEMNOACC(String value) {
        this.itemnoacc = value;
    }

    /**
     * Obtiene el valor de la propiedad wttype.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWTTYPE() {
        return wttype;
    }

    /**
     * Define el valor de la propiedad wttype.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWTTYPE(String value) {
        this.wttype = value;
    }

    /**
     * Obtiene el valor de la propiedad wtcode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWTCODE() {
        return wtcode;
    }

    /**
     * Define el valor de la propiedad wtcode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWTCODE(String value) {
        this.wtcode = value;
    }

    /**
     * Obtiene el valor de la propiedad basamtlc.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBASAMTLC() {
        return basamtlc;
    }

    /**
     * Define el valor de la propiedad basamtlc.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBASAMTLC(BigDecimal value) {
        this.basamtlc = value;
    }

    /**
     * Obtiene el valor de la propiedad basamttc.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBASAMTTC() {
        return basamttc;
    }

    /**
     * Define el valor de la propiedad basamttc.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBASAMTTC(BigDecimal value) {
        this.basamttc = value;
    }

    /**
     * Obtiene el valor de la propiedad basamtl2.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBASAMTL2() {
        return basamtl2;
    }

    /**
     * Define el valor de la propiedad basamtl2.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBASAMTL2(BigDecimal value) {
        this.basamtl2 = value;
    }

    /**
     * Obtiene el valor de la propiedad basamtl3.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBASAMTL3() {
        return basamtl3;
    }

    /**
     * Define el valor de la propiedad basamtl3.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBASAMTL3(BigDecimal value) {
        this.basamtl3 = value;
    }

    /**
     * Obtiene el valor de la propiedad manamtlc.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMANAMTLC() {
        return manamtlc;
    }

    /**
     * Define el valor de la propiedad manamtlc.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMANAMTLC(BigDecimal value) {
        this.manamtlc = value;
    }

    /**
     * Obtiene el valor de la propiedad manamttc.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMANAMTTC() {
        return manamttc;
    }

    /**
     * Define el valor de la propiedad manamttc.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMANAMTTC(BigDecimal value) {
        this.manamttc = value;
    }

    /**
     * Obtiene el valor de la propiedad manamtl2.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMANAMTL2() {
        return manamtl2;
    }

    /**
     * Define el valor de la propiedad manamtl2.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMANAMTL2(BigDecimal value) {
        this.manamtl2 = value;
    }

    /**
     * Obtiene el valor de la propiedad manamtl3.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMANAMTL3() {
        return manamtl3;
    }

    /**
     * Define el valor de la propiedad manamtl3.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMANAMTL3(BigDecimal value) {
        this.manamtl3 = value;
    }

    /**
     * Obtiene el valor de la propiedad awhamtlc.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAWHAMTLC() {
        return awhamtlc;
    }

    /**
     * Define el valor de la propiedad awhamtlc.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAWHAMTLC(BigDecimal value) {
        this.awhamtlc = value;
    }

    /**
     * Obtiene el valor de la propiedad awhamttc.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAWHAMTTC() {
        return awhamttc;
    }

    /**
     * Define el valor de la propiedad awhamttc.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAWHAMTTC(BigDecimal value) {
        this.awhamttc = value;
    }

    /**
     * Obtiene el valor de la propiedad awhamtl2.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAWHAMTL2() {
        return awhamtl2;
    }

    /**
     * Define el valor de la propiedad awhamtl2.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAWHAMTL2(BigDecimal value) {
        this.awhamtl2 = value;
    }

    /**
     * Obtiene el valor de la propiedad awhamtl3.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAWHAMTL3() {
        return awhamtl3;
    }

    /**
     * Define el valor de la propiedad awhamtl3.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAWHAMTL3(BigDecimal value) {
        this.awhamtl3 = value;
    }

    /**
     * Obtiene el valor de la propiedad basamtind.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBASAMTIND() {
        return basamtind;
    }

    /**
     * Define el valor de la propiedad basamtind.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBASAMTIND(String value) {
        this.basamtind = value;
    }

    /**
     * Obtiene el valor de la propiedad manamtind.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMANAMTIND() {
        return manamtind;
    }

    /**
     * Define el valor de la propiedad manamtind.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMANAMTIND(String value) {
        this.manamtind = value;
    }

    /**
     * Obtiene el valor de la propiedad basamtlclong.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBASAMTLCLONG() {
        return basamtlclong;
    }

    /**
     * Define el valor de la propiedad basamtlclong.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBASAMTLCLONG(BigDecimal value) {
        this.basamtlclong = value;
    }

    /**
     * Obtiene el valor de la propiedad basamttclong.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBASAMTTCLONG() {
        return basamttclong;
    }

    /**
     * Define el valor de la propiedad basamttclong.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBASAMTTCLONG(BigDecimal value) {
        this.basamttclong = value;
    }

    /**
     * Obtiene el valor de la propiedad basamtl2LONG.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBASAMTL2LONG() {
        return basamtl2LONG;
    }

    /**
     * Define el valor de la propiedad basamtl2LONG.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBASAMTL2LONG(BigDecimal value) {
        this.basamtl2LONG = value;
    }

    /**
     * Obtiene el valor de la propiedad basamtl3LONG.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBASAMTL3LONG() {
        return basamtl3LONG;
    }

    /**
     * Define el valor de la propiedad basamtl3LONG.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBASAMTL3LONG(BigDecimal value) {
        this.basamtl3LONG = value;
    }

    /**
     * Obtiene el valor de la propiedad manamtlclong.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMANAMTLCLONG() {
        return manamtlclong;
    }

    /**
     * Define el valor de la propiedad manamtlclong.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMANAMTLCLONG(BigDecimal value) {
        this.manamtlclong = value;
    }

    /**
     * Obtiene el valor de la propiedad manamttclong.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMANAMTTCLONG() {
        return manamttclong;
    }

    /**
     * Define el valor de la propiedad manamttclong.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMANAMTTCLONG(BigDecimal value) {
        this.manamttclong = value;
    }

    /**
     * Obtiene el valor de la propiedad manamtl2LONG.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMANAMTL2LONG() {
        return manamtl2LONG;
    }

    /**
     * Define el valor de la propiedad manamtl2LONG.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMANAMTL2LONG(BigDecimal value) {
        this.manamtl2LONG = value;
    }

    /**
     * Obtiene el valor de la propiedad manamtl3LONG.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMANAMTL3LONG() {
        return manamtl3LONG;
    }

    /**
     * Define el valor de la propiedad manamtl3LONG.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMANAMTL3LONG(BigDecimal value) {
        this.manamtl3LONG = value;
    }

    /**
     * Obtiene el valor de la propiedad awhamtlclong.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAWHAMTLCLONG() {
        return awhamtlclong;
    }

    /**
     * Define el valor de la propiedad awhamtlclong.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAWHAMTLCLONG(BigDecimal value) {
        this.awhamtlclong = value;
    }

    /**
     * Obtiene el valor de la propiedad awhamttclong.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAWHAMTTCLONG() {
        return awhamttclong;
    }

    /**
     * Define el valor de la propiedad awhamttclong.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAWHAMTTCLONG(BigDecimal value) {
        this.awhamttclong = value;
    }

    /**
     * Obtiene el valor de la propiedad awhamtl2LONG.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAWHAMTL2LONG() {
        return awhamtl2LONG;
    }

    /**
     * Define el valor de la propiedad awhamtl2LONG.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAWHAMTL2LONG(BigDecimal value) {
        this.awhamtl2LONG = value;
    }

    /**
     * Obtiene el valor de la propiedad awhamtl3LONG.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAWHAMTL3LONG() {
        return awhamtl3LONG;
    }

    /**
     * Define el valor de la propiedad awhamtl3LONG.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAWHAMTL3LONG(BigDecimal value) {
        this.awhamtl3LONG = value;
    }

}

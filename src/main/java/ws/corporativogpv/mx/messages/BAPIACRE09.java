
package ws.corporativogpv.mx.messages;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Real Estate Account Assignment Data
 * 
 * <p>Clase Java para BAPIACRE09 complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="BAPIACRE09">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ITEMNO_ACC" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *               &lt;pattern value="\d+"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BUSINESS_ENTITY" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="BUILDING" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PROPERTY" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="RENTAL_OBJECT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SERV_CHARGE_KEY" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SETTLEMENT_UNIT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="5"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CONTRACT_NO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="13"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="FLOW_TYPE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CORR_ITEM" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="10"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="REF_DATE" type="{urn:sap-com:document:sap:rfc:functions}date" minOccurs="0"/>
 *         &lt;element name="OPTION_RATE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;totalDigits value="9"/>
 *               &lt;fractionDigits value="6"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BAPIACRE09", propOrder = {
    "itemnoacc",
    "businessentity",
    "building",
    "property",
    "rentalobject",
    "servchargekey",
    "settlementunit",
    "contractno",
    "flowtype",
    "corritem",
    "refdate",
    "optionrate"
})
public class BAPIACRE09 {

    @XmlElement(name = "ITEMNO_ACC")
    protected String itemnoacc;
    @XmlElement(name = "BUSINESS_ENTITY")
    protected String businessentity;
    @XmlElement(name = "BUILDING")
    protected String building;
    @XmlElement(name = "PROPERTY")
    protected String property;
    @XmlElement(name = "RENTAL_OBJECT")
    protected String rentalobject;
    @XmlElement(name = "SERV_CHARGE_KEY")
    protected String servchargekey;
    @XmlElement(name = "SETTLEMENT_UNIT")
    protected String settlementunit;
    @XmlElement(name = "CONTRACT_NO")
    protected String contractno;
    @XmlElement(name = "FLOW_TYPE")
    protected String flowtype;
    @XmlElement(name = "CORR_ITEM")
    protected String corritem;
    @XmlElement(name = "REF_DATE")
    protected String refdate;
    @XmlElement(name = "OPTION_RATE")
    protected BigDecimal optionrate;

    /**
     * Obtiene el valor de la propiedad itemnoacc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getITEMNOACC() {
        return itemnoacc;
    }

    /**
     * Define el valor de la propiedad itemnoacc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setITEMNOACC(String value) {
        this.itemnoacc = value;
    }

    /**
     * Obtiene el valor de la propiedad businessentity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUSINESSENTITY() {
        return businessentity;
    }

    /**
     * Define el valor de la propiedad businessentity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUSINESSENTITY(String value) {
        this.businessentity = value;
    }

    /**
     * Obtiene el valor de la propiedad building.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBUILDING() {
        return building;
    }

    /**
     * Define el valor de la propiedad building.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBUILDING(String value) {
        this.building = value;
    }

    /**
     * Obtiene el valor de la propiedad property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPROPERTY() {
        return property;
    }

    /**
     * Define el valor de la propiedad property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPROPERTY(String value) {
        this.property = value;
    }

    /**
     * Obtiene el valor de la propiedad rentalobject.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRENTALOBJECT() {
        return rentalobject;
    }

    /**
     * Define el valor de la propiedad rentalobject.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRENTALOBJECT(String value) {
        this.rentalobject = value;
    }

    /**
     * Obtiene el valor de la propiedad servchargekey.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSERVCHARGEKEY() {
        return servchargekey;
    }

    /**
     * Define el valor de la propiedad servchargekey.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSERVCHARGEKEY(String value) {
        this.servchargekey = value;
    }

    /**
     * Obtiene el valor de la propiedad settlementunit.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSETTLEMENTUNIT() {
        return settlementunit;
    }

    /**
     * Define el valor de la propiedad settlementunit.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSETTLEMENTUNIT(String value) {
        this.settlementunit = value;
    }

    /**
     * Obtiene el valor de la propiedad contractno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCONTRACTNO() {
        return contractno;
    }

    /**
     * Define el valor de la propiedad contractno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCONTRACTNO(String value) {
        this.contractno = value;
    }

    /**
     * Obtiene el valor de la propiedad flowtype.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFLOWTYPE() {
        return flowtype;
    }

    /**
     * Define el valor de la propiedad flowtype.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFLOWTYPE(String value) {
        this.flowtype = value;
    }

    /**
     * Obtiene el valor de la propiedad corritem.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCORRITEM() {
        return corritem;
    }

    /**
     * Define el valor de la propiedad corritem.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCORRITEM(String value) {
        this.corritem = value;
    }

    /**
     * Obtiene el valor de la propiedad refdate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREFDATE() {
        return refdate;
    }

    /**
     * Define el valor de la propiedad refdate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREFDATE(String value) {
        this.refdate = value;
    }

    /**
     * Obtiene el valor de la propiedad optionrate.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getOPTIONRATE() {
        return optionrate;
    }

    /**
     * Define el valor de la propiedad optionrate.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setOPTIONRATE(BigDecimal value) {
        this.optionrate = value;
    }

}

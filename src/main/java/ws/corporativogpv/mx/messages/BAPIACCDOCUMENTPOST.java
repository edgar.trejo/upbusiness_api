
package ws.corporativogpv.mx.messages;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="CONTRACTHEADER" type="{urn:sap-com:document:sap:rfc:functions}BAPIACCAHD" minOccurs="0"/>
 *         &lt;element name="CUSTOMERCPD" type="{urn:sap-com:document:sap:rfc:functions}BAPIACPA09" minOccurs="0"/>
 *         &lt;element name="DOCUMENTHEADER" type="{urn:sap-com:document:sap:rfc:functions}BAPIACHE09"/>
 *         &lt;element name="ACCOUNTGL" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACGL09" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ACCOUNTPAYABLE" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACAP09" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ACCOUNTRECEIVABLE" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACAR09" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ACCOUNTTAX" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACTX09" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ACCOUNTWT" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACWT09" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="CONTRACTITEM" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACCAIT" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="CRITERIA" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACKEC9" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="CURRENCYAMOUNT">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACCR09" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="EXTENSION1" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACEXTC" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="EXTENSION2" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIPAREX" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="PAYMENTCARD" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACPC09" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="REALESTATE" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACRE09" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="RETURN">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIRET2" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="VALUEFIELD" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACKEV9" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
@XmlRootElement(name = "BAPI_ACC_DOCUMENT_POST")
public class BAPIACCDOCUMENTPOST {

    @XmlElement(name = "CONTRACTHEADER")
    protected BAPIACCAHD contractheader;
    @XmlElement(name = "CUSTOMERCPD")
    protected BAPIACPA09 customercpd;
    @XmlElement(name = "DOCUMENTHEADER", required = true)
    protected BAPIACHE09 documentheader;
    @XmlElement(name = "ACCOUNTGL")
    protected BAPIACCDOCUMENTPOST.ACCOUNTGL accountgl;
    @XmlElement(name = "ACCOUNTPAYABLE")
    protected BAPIACCDOCUMENTPOST.ACCOUNTPAYABLE accountpayable;
    @XmlElement(name = "ACCOUNTRECEIVABLE")
    protected BAPIACCDOCUMENTPOST.ACCOUNTRECEIVABLE accountreceivable;
    @XmlElement(name = "ACCOUNTTAX")
    protected BAPIACCDOCUMENTPOST.ACCOUNTTAX accounttax;
    @XmlElement(name = "ACCOUNTWT")
    protected BAPIACCDOCUMENTPOST.ACCOUNTWT accountwt;
    @XmlElement(name = "CONTRACTITEM")
    protected BAPIACCDOCUMENTPOST.CONTRACTITEM contractitem;
    @XmlElement(name = "CRITERIA")
    protected BAPIACCDOCUMENTPOST.CRITERIA criteria;
    @XmlElement(name = "CURRENCYAMOUNT", required = true)
    protected BAPIACCDOCUMENTPOST.CURRENCYAMOUNT currencyamount;
    @XmlElement(name = "EXTENSION1")
    protected BAPIACCDOCUMENTPOST.EXTENSION1 extension1;
    @XmlElement(name = "EXTENSION2")
    protected BAPIACCDOCUMENTPOST.EXTENSION2 extension2;
    @XmlElement(name = "PAYMENTCARD")
    protected BAPIACCDOCUMENTPOST.PAYMENTCARD paymentcard;
    @XmlElement(name = "REALESTATE")
    protected BAPIACCDOCUMENTPOST.REALESTATE realestate;
    @XmlElement(name = "RETURN", required = true)
    protected BAPIACCDOCUMENTPOST.RETURN _return;
    @XmlElement(name = "VALUEFIELD")
    protected BAPIACCDOCUMENTPOST.VALUEFIELD valuefield;

    /**
     * Obtiene el valor de la propiedad contractheader.
     * 
     * @return
     *     possible object is
     *     {@link BAPIACCAHD }
     *     
     */
    public BAPIACCAHD getCONTRACTHEADER() {
        return contractheader;
    }

    /**
     * Define el valor de la propiedad contractheader.
     * 
     * @param value
     *     allowed object is
     *     {@link BAPIACCAHD }
     *     
     */
    public void setCONTRACTHEADER(BAPIACCAHD value) {
        this.contractheader = value;
    }

    /**
     * Obtiene el valor de la propiedad customercpd.
     * 
     * @return
     *     possible object is
     *     {@link BAPIACPA09 }
     *     
     */
    public BAPIACPA09 getCUSTOMERCPD() {
        return customercpd;
    }

    /**
     * Define el valor de la propiedad customercpd.
     * 
     * @param value
     *     allowed object is
     *     {@link BAPIACPA09 }
     *     
     */
    public void setCUSTOMERCPD(BAPIACPA09 value) {
        this.customercpd = value;
    }

    /**
     * Obtiene el valor de la propiedad documentheader.
     * 
     * @return
     *     possible object is
     *     {@link BAPIACHE09 }
     *     
     */
    public BAPIACHE09 getDOCUMENTHEADER() {
        return documentheader;
    }

    /**
     * Define el valor de la propiedad documentheader.
     * 
     * @param value
     *     allowed object is
     *     {@link BAPIACHE09 }
     *     
     */
    public void setDOCUMENTHEADER(BAPIACHE09 value) {
        this.documentheader = value;
    }

    /**
     * Obtiene el valor de la propiedad accountgl.
     * 
     * @return
     *     possible object is
     *     {@link BAPIACCDOCUMENTPOST.ACCOUNTGL }
     *     
     */
    public BAPIACCDOCUMENTPOST.ACCOUNTGL getACCOUNTGL() {
        return accountgl;
    }

    /**
     * Define el valor de la propiedad accountgl.
     * 
     * @param value
     *     allowed object is
     *     {@link BAPIACCDOCUMENTPOST.ACCOUNTGL }
     *     
     */
    public void setACCOUNTGL(BAPIACCDOCUMENTPOST.ACCOUNTGL value) {
        this.accountgl = value;
    }

    /**
     * Obtiene el valor de la propiedad accountpayable.
     * 
     * @return
     *     possible object is
     *     {@link BAPIACCDOCUMENTPOST.ACCOUNTPAYABLE }
     *     
     */
    public BAPIACCDOCUMENTPOST.ACCOUNTPAYABLE getACCOUNTPAYABLE() {
        return accountpayable;
    }

    /**
     * Define el valor de la propiedad accountpayable.
     * 
     * @param value
     *     allowed object is
     *     {@link BAPIACCDOCUMENTPOST.ACCOUNTPAYABLE }
     *     
     */
    public void setACCOUNTPAYABLE(BAPIACCDOCUMENTPOST.ACCOUNTPAYABLE value) {
        this.accountpayable = value;
    }

    /**
     * Obtiene el valor de la propiedad accountreceivable.
     * 
     * @return
     *     possible object is
     *     {@link BAPIACCDOCUMENTPOST.ACCOUNTRECEIVABLE }
     *     
     */
    public BAPIACCDOCUMENTPOST.ACCOUNTRECEIVABLE getACCOUNTRECEIVABLE() {
        return accountreceivable;
    }

    /**
     * Define el valor de la propiedad accountreceivable.
     * 
     * @param value
     *     allowed object is
     *     {@link BAPIACCDOCUMENTPOST.ACCOUNTRECEIVABLE }
     *     
     */
    public void setACCOUNTRECEIVABLE(BAPIACCDOCUMENTPOST.ACCOUNTRECEIVABLE value) {
        this.accountreceivable = value;
    }

    /**
     * Obtiene el valor de la propiedad accounttax.
     * 
     * @return
     *     possible object is
     *     {@link BAPIACCDOCUMENTPOST.ACCOUNTTAX }
     *     
     */
    public BAPIACCDOCUMENTPOST.ACCOUNTTAX getACCOUNTTAX() {
        return accounttax;
    }

    /**
     * Define el valor de la propiedad accounttax.
     * 
     * @param value
     *     allowed object is
     *     {@link BAPIACCDOCUMENTPOST.ACCOUNTTAX }
     *     
     */
    public void setACCOUNTTAX(BAPIACCDOCUMENTPOST.ACCOUNTTAX value) {
        this.accounttax = value;
    }

    /**
     * Obtiene el valor de la propiedad accountwt.
     * 
     * @return
     *     possible object is
     *     {@link BAPIACCDOCUMENTPOST.ACCOUNTWT }
     *     
     */
    public BAPIACCDOCUMENTPOST.ACCOUNTWT getACCOUNTWT() {
        return accountwt;
    }

    /**
     * Define el valor de la propiedad accountwt.
     * 
     * @param value
     *     allowed object is
     *     {@link BAPIACCDOCUMENTPOST.ACCOUNTWT }
     *     
     */
    public void setACCOUNTWT(BAPIACCDOCUMENTPOST.ACCOUNTWT value) {
        this.accountwt = value;
    }

    /**
     * Obtiene el valor de la propiedad contractitem.
     * 
     * @return
     *     possible object is
     *     {@link BAPIACCDOCUMENTPOST.CONTRACTITEM }
     *     
     */
    public BAPIACCDOCUMENTPOST.CONTRACTITEM getCONTRACTITEM() {
        return contractitem;
    }

    /**
     * Define el valor de la propiedad contractitem.
     * 
     * @param value
     *     allowed object is
     *     {@link BAPIACCDOCUMENTPOST.CONTRACTITEM }
     *     
     */
    public void setCONTRACTITEM(BAPIACCDOCUMENTPOST.CONTRACTITEM value) {
        this.contractitem = value;
    }

    /**
     * Obtiene el valor de la propiedad criteria.
     * 
     * @return
     *     possible object is
     *     {@link BAPIACCDOCUMENTPOST.CRITERIA }
     *     
     */
    public BAPIACCDOCUMENTPOST.CRITERIA getCRITERIA() {
        return criteria;
    }

    /**
     * Define el valor de la propiedad criteria.
     * 
     * @param value
     *     allowed object is
     *     {@link BAPIACCDOCUMENTPOST.CRITERIA }
     *     
     */
    public void setCRITERIA(BAPIACCDOCUMENTPOST.CRITERIA value) {
        this.criteria = value;
    }

    /**
     * Obtiene el valor de la propiedad currencyamount.
     * 
     * @return
     *     possible object is
     *     {@link BAPIACCDOCUMENTPOST.CURRENCYAMOUNT }
     *     
     */
    public BAPIACCDOCUMENTPOST.CURRENCYAMOUNT getCURRENCYAMOUNT() {
        return currencyamount;
    }

    /**
     * Define el valor de la propiedad currencyamount.
     * 
     * @param value
     *     allowed object is
     *     {@link BAPIACCDOCUMENTPOST.CURRENCYAMOUNT }
     *     
     */
    public void setCURRENCYAMOUNT(BAPIACCDOCUMENTPOST.CURRENCYAMOUNT value) {
        this.currencyamount = value;
    }

    /**
     * Obtiene el valor de la propiedad extension1.
     * 
     * @return
     *     possible object is
     *     {@link BAPIACCDOCUMENTPOST.EXTENSION1 }
     *     
     */
    public BAPIACCDOCUMENTPOST.EXTENSION1 getEXTENSION1() {
        return extension1;
    }

    /**
     * Define el valor de la propiedad extension1.
     * 
     * @param value
     *     allowed object is
     *     {@link BAPIACCDOCUMENTPOST.EXTENSION1 }
     *     
     */
    public void setEXTENSION1(BAPIACCDOCUMENTPOST.EXTENSION1 value) {
        this.extension1 = value;
    }

    /**
     * Obtiene el valor de la propiedad extension2.
     * 
     * @return
     *     possible object is
     *     {@link BAPIACCDOCUMENTPOST.EXTENSION2 }
     *     
     */
    public BAPIACCDOCUMENTPOST.EXTENSION2 getEXTENSION2() {
        return extension2;
    }

    /**
     * Define el valor de la propiedad extension2.
     * 
     * @param value
     *     allowed object is
     *     {@link BAPIACCDOCUMENTPOST.EXTENSION2 }
     *     
     */
    public void setEXTENSION2(BAPIACCDOCUMENTPOST.EXTENSION2 value) {
        this.extension2 = value;
    }

    /**
     * Obtiene el valor de la propiedad paymentcard.
     * 
     * @return
     *     possible object is
     *     {@link BAPIACCDOCUMENTPOST.PAYMENTCARD }
     *     
     */
    public BAPIACCDOCUMENTPOST.PAYMENTCARD getPAYMENTCARD() {
        return paymentcard;
    }

    /**
     * Define el valor de la propiedad paymentcard.
     * 
     * @param value
     *     allowed object is
     *     {@link BAPIACCDOCUMENTPOST.PAYMENTCARD }
     *     
     */
    public void setPAYMENTCARD(BAPIACCDOCUMENTPOST.PAYMENTCARD value) {
        this.paymentcard = value;
    }

    /**
     * Obtiene el valor de la propiedad realestate.
     * 
     * @return
     *     possible object is
     *     {@link BAPIACCDOCUMENTPOST.REALESTATE }
     *     
     */
    public BAPIACCDOCUMENTPOST.REALESTATE getREALESTATE() {
        return realestate;
    }

    /**
     * Define el valor de la propiedad realestate.
     * 
     * @param value
     *     allowed object is
     *     {@link BAPIACCDOCUMENTPOST.REALESTATE }
     *     
     */
    public void setREALESTATE(BAPIACCDOCUMENTPOST.REALESTATE value) {
        this.realestate = value;
    }

    /**
     * Obtiene el valor de la propiedad return.
     * 
     * @return
     *     possible object is
     *     {@link BAPIACCDOCUMENTPOST.RETURN }
     *     
     */
    public BAPIACCDOCUMENTPOST.RETURN getRETURN() {
        return _return;
    }

    /**
     * Define el valor de la propiedad return.
     * 
     * @param value
     *     allowed object is
     *     {@link BAPIACCDOCUMENTPOST.RETURN }
     *     
     */
    public void setRETURN(BAPIACCDOCUMENTPOST.RETURN value) {
        this._return = value;
    }

    /**
     * Obtiene el valor de la propiedad valuefield.
     * 
     * @return
     *     possible object is
     *     {@link BAPIACCDOCUMENTPOST.VALUEFIELD }
     *     
     */
    public BAPIACCDOCUMENTPOST.VALUEFIELD getVALUEFIELD() {
        return valuefield;
    }

    /**
     * Define el valor de la propiedad valuefield.
     * 
     * @param value
     *     allowed object is
     *     {@link BAPIACCDOCUMENTPOST.VALUEFIELD }
     *     
     */
    public void setVALUEFIELD(BAPIACCDOCUMENTPOST.VALUEFIELD value) {
        this.valuefield = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACGL09" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "item"
    })
    public static class ACCOUNTGL {

        protected List<BAPIACGL09> item;

        /**
         * Gets the value of the item property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the item property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BAPIACGL09 }
         * 
         * 
         */
        public List<BAPIACGL09> getItem() {
            if (item == null) {
                item = new ArrayList<BAPIACGL09>();
            }
            return this.item;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACAP09" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "item"
    })
    public static class ACCOUNTPAYABLE {

        protected List<BAPIACAP09> item;

        /**
         * Gets the value of the item property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the item property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BAPIACAP09 }
         * 
         * 
         */
        public List<BAPIACAP09> getItem() {
            if (item == null) {
                item = new ArrayList<BAPIACAP09>();
            }
            return this.item;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACAR09" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "item"
    })
    public static class ACCOUNTRECEIVABLE {

        protected List<BAPIACAR09> item;

        /**
         * Gets the value of the item property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the item property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BAPIACAR09 }
         * 
         * 
         */
        public List<BAPIACAR09> getItem() {
            if (item == null) {
                item = new ArrayList<BAPIACAR09>();
            }
            return this.item;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACTX09" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "item"
    })
    public static class ACCOUNTTAX {

        protected List<BAPIACTX09> item;

        /**
         * Gets the value of the item property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the item property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BAPIACTX09 }
         * 
         * 
         */
        public List<BAPIACTX09> getItem() {
            if (item == null) {
                item = new ArrayList<BAPIACTX09>();
            }
            return this.item;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACWT09" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "item"
    })
    public static class ACCOUNTWT {

        protected List<BAPIACWT09> item;

        /**
         * Gets the value of the item property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the item property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BAPIACWT09 }
         * 
         * 
         */
        public List<BAPIACWT09> getItem() {
            if (item == null) {
                item = new ArrayList<BAPIACWT09>();
            }
            return this.item;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACCAIT" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "item"
    })
    public static class CONTRACTITEM {

        protected List<BAPIACCAIT> item;

        /**
         * Gets the value of the item property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the item property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BAPIACCAIT }
         * 
         * 
         */
        public List<BAPIACCAIT> getItem() {
            if (item == null) {
                item = new ArrayList<BAPIACCAIT>();
            }
            return this.item;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACKEC9" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "item"
    })
    public static class CRITERIA {

        protected List<BAPIACKEC9> item;

        /**
         * Gets the value of the item property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the item property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BAPIACKEC9 }
         * 
         * 
         */
        public List<BAPIACKEC9> getItem() {
            if (item == null) {
                item = new ArrayList<BAPIACKEC9>();
            }
            return this.item;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACCR09" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "item"
    })
    public static class CURRENCYAMOUNT {

        protected List<BAPIACCR09> item;

        /**
         * Gets the value of the item property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the item property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BAPIACCR09 }
         * 
         * 
         */
        public List<BAPIACCR09> getItem() {
            if (item == null) {
                item = new ArrayList<BAPIACCR09>();
            }
            return this.item;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACEXTC" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "item"
    })
    public static class EXTENSION1 {

        protected List<BAPIACEXTC> item;

        /**
         * Gets the value of the item property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the item property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BAPIACEXTC }
         * 
         * 
         */
        public List<BAPIACEXTC> getItem() {
            if (item == null) {
                item = new ArrayList<BAPIACEXTC>();
            }
            return this.item;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIPAREX" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "item"
    })
    public static class EXTENSION2 {

        protected List<BAPIPAREX> item;

        /**
         * Gets the value of the item property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the item property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BAPIPAREX }
         * 
         * 
         */
        public List<BAPIPAREX> getItem() {
            if (item == null) {
                item = new ArrayList<BAPIPAREX>();
            }
            return this.item;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACPC09" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "item"
    })
    public static class PAYMENTCARD {

        protected List<BAPIACPC09> item;

        /**
         * Gets the value of the item property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the item property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BAPIACPC09 }
         * 
         * 
         */
        public List<BAPIACPC09> getItem() {
            if (item == null) {
                item = new ArrayList<BAPIACPC09>();
            }
            return this.item;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACRE09" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "item"
    })
    public static class REALESTATE {

        protected List<BAPIACRE09> item;

        /**
         * Gets the value of the item property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the item property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BAPIACRE09 }
         * 
         * 
         */
        public List<BAPIACRE09> getItem() {
            if (item == null) {
                item = new ArrayList<BAPIACRE09>();
            }
            return this.item;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIRET2" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "item"
    })
    public static class RETURN {

        protected List<BAPIRET2> item;

        /**
         * Gets the value of the item property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the item property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BAPIRET2 }
         * 
         * 
         */
        public List<BAPIRET2> getItem() {
            if (item == null) {
                item = new ArrayList<BAPIRET2>();
            }
            return this.item;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="item" type="{urn:sap-com:document:sap:rfc:functions}BAPIACKEV9" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "item"
    })
    public static class VALUEFIELD {

        protected List<BAPIACKEV9> item;

        /**
         * Gets the value of the item property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the item property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BAPIACKEV9 }
         * 
         * 
         */
        public List<BAPIACKEV9> getItem() {
            if (item == null) {
                item = new ArrayList<BAPIACKEV9>();
            }
            return this.item;
        }

    }

}

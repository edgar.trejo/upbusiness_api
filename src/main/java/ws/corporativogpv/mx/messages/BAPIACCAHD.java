
package ws.corporativogpv.mx.messages;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Add. Contract Accounts Recievable and Payable Header Line
 * 
 * <p>Clase Java para BAPIACCAHD complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="BAPIACCAHD">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DOC_NO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="12"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DOC_TYPE_CA" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="RES_KEY" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="30"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="FIKEY" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="12"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="PAYMENT_FORM_REF" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="30"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BAPIACCAHD", propOrder = {
    "docno",
    "doctypeca",
    "reskey",
    "fikey",
    "paymentformref"
})
public class BAPIACCAHD {

    @XmlElement(name = "DOC_NO")
    protected String docno;
    @XmlElement(name = "DOC_TYPE_CA")
    protected String doctypeca;
    @XmlElement(name = "RES_KEY")
    protected String reskey;
    @XmlElement(name = "FIKEY")
    protected String fikey;
    @XmlElement(name = "PAYMENT_FORM_REF")
    protected String paymentformref;

    /**
     * Obtiene el valor de la propiedad docno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDOCNO() {
        return docno;
    }

    /**
     * Define el valor de la propiedad docno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDOCNO(String value) {
        this.docno = value;
    }

    /**
     * Obtiene el valor de la propiedad doctypeca.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDOCTYPECA() {
        return doctypeca;
    }

    /**
     * Define el valor de la propiedad doctypeca.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDOCTYPECA(String value) {
        this.doctypeca = value;
    }

    /**
     * Obtiene el valor de la propiedad reskey.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRESKEY() {
        return reskey;
    }

    /**
     * Define el valor de la propiedad reskey.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRESKEY(String value) {
        this.reskey = value;
    }

    /**
     * Obtiene el valor de la propiedad fikey.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFIKEY() {
        return fikey;
    }

    /**
     * Define el valor de la propiedad fikey.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFIKEY(String value) {
        this.fikey = value;
    }

    /**
     * Obtiene el valor de la propiedad paymentformref.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPAYMENTFORMREF() {
        return paymentformref;
    }

    /**
     * Define el valor de la propiedad paymentformref.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPAYMENTFORMREF(String value) {
        this.paymentformref = value;
    }

}

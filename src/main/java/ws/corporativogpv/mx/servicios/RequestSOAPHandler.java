package ws.corporativogpv.mx.servicios;

import com.mx.sivale.config.WsdlConfig;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import javax.xml.namespace.QName;
import javax.xml.soap.*;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.io.ByteArrayOutputStream;
import java.util.Iterator;
import java.util.Set;

/**
 * @author areyna
 */
public class RequestSOAPHandler implements SOAPHandler<SOAPMessageContext> {

    private static final Logger log = Logger.getLogger(RequestSOAPHandler.class);

    public static final String REQUEST_XML="REQUEST_XML";

    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        if (isRequest(context)) {
            SOAPMessage soapMessage = context.getMessage();
            try {

                context.put(BindingProvider.USERNAME_PROPERTY, WsdlConfig.PROP_WS_GPV_USER);
                context.put(BindingProvider.PASSWORD_PROPERTY, WsdlConfig.PROP_WS_GPV_PASS);

                writeMessageLogging(context);

                context.getMessage();

            } catch (Exception e) {
                log.error("Exception GPV SAP SOAP ", e);
                throw new RuntimeException(e);
            }
        }
        return true;
    }

    private SOAPHeader createOrObtainHeader(SOAPEnvelope soapEnvelope) throws SOAPException {
        SOAPHeader header = soapEnvelope.getHeader();
        if (header == null)
            header = soapEnvelope.addHeader();

        return header;
    }

    private boolean isRequest(SOAPMessageContext context) {
        return Boolean.TRUE.equals(context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY));
    }

    /**
     * Called when there is a faulty soap incoming / outgoing message from the
     * webservice. Right now is logged only the faulty soap message and the soap
     * body and the error codes. No restrictions on the processing of the message,
     * the flow of the soap message is continued.
     */
    @Override
    public boolean handleFault(SOAPMessageContext context) {
        try {
            final SOAPMessage message = context.getMessage();
            log.error("soap handle fault message - " + message);
            final SOAPBody body = message.getSOAPBody();
            log.error("soap handle fault body - " + message);
            final SOAPFault fault = body.getFault();
            final String code = fault.getFaultCode();
            log.error("soap handle fault code - " + code);
            final String faultString = fault.getFaultString();
            log.error("soap handle fault string - " + faultString);
            Detail detail = fault.getDetail();
            if (detail != null) {
                @SuppressWarnings("unchecked")
                Iterator<SOAPElement> iter = detail.getChildElements();
                // Getting first level of detail
                // HashMap<String, String> detailMap = new HashMap<String, String>();
                while (iter.hasNext()) {
                    SOAPElement element = iter.next();
                    // detailMap.put(element.getLocalName(), element.getValue());
                    log.error("soap handle fault detail - " + element.getLocalName() + "=" + element.getValue());
                }
            }
        } catch (SOAPException e) {
            log.error("Error while handle the soap fault.", e);
        }
        return true;
    }

    @Override
    public void close(MessageContext context) {
        log.debug("soap : close()......");
    }

    @Override
    public Set<QName> getHeaders() {
        log.debug("soap : getHeaders()......");
        return null;
    }

    private void writeMessageLogging(SOAPMessageContext smc) {
        Boolean outboundProperty = (Boolean) smc
                .get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

        SOAPMessage message = smc.getMessage();
        ByteArrayOutputStream out=null;
        try {
            out = new ByteArrayOutputStream();
            message.writeTo(out);
            String strMsg = new String(out.toByteArray());

            if (!outboundProperty) {
                String requestXML=(String)smc.get(REQUEST_XML);
                log.info("Request of Response:"+requestXML);
            }else{
                smc.put(REQUEST_XML,strMsg);
            }
            log.info("GPV Request XML: " + strMsg);
            out.close();
        } catch (Exception e) {
            log.error("Exception in handler:", e);
        }finally{
            IOUtils.closeQuietly(out);
        }

    }

}

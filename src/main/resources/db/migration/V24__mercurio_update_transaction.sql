
ALTER TABLE
    mercurio.transaction ADD (num_cuenta VARCHAR(20));
ALTER TABLE
    mercurio.transaction ADD (num_tarjeta VARCHAR(15));
ALTER TABLE
    mercurio.transaction ADD (user_id BIGINT);
ALTER TABLE
    mercurio.transaction ADD CONSTRAINT FK_transaction_user FOREIGN KEY (user_id)
    REFERENCES mercurio.user (id);
ALTER TABLE
    mercurio.transaction ADD (fecha_creacion TIMESTAMP NULL);

UPDATE mercurio.transaction SET fecha_creacion = CONCAT(SUBSTR(fecha, 7, 4),"-",SUBSTR(fecha, 4, 2),"-",SUBSTR(fecha, 1, 2), " ", hora);
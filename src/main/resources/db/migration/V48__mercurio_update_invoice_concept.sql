ALTER TABLE invoice_concept MODIFY description varchar(300) NULL;

ALTER TABLE invoice_concept MODIFY identification_number varchar(50) NULL;
CREATE TABLE mercurio.invoice_spending_associate
(
  id                  BIGINT         NOT NULL AUTO_INCREMENT,
  invoice_id          BIGINT         NOT NULL,
  spending_id         BIGINT         NOT NULL,
  date                TIMESTAMP      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  invoice_amount      DECIMAL(10, 2) NULL,
  spending_amount     DECIMAL(10, 2) NULL,
  invoice_date        TIMESTAMP NULL,
  spending_date       TIMESTAMP NULL,
  invoice_rfc         VARCHAR(16)    NULL,
  spending_rfc        VARCHAR(16)    NULL,
  amount_validation   BOOL           NOT NULL,
  rfc_validation      BOOL           NOT NULL,
  date_validation     BOOL           NOT NULL,
  associate           BOOL           NOT NULL,
  PRIMARY KEY (id),
  KEY (invoice_id),
  KEY (spending_id)
);

ALTER TABLE mercurio.invoice_spending_associate
  ADD CONSTRAINT FK_invoice_spending_associate_invoice
FOREIGN KEY (invoice_id) REFERENCES mercurio.invoice (id);

ALTER TABLE mercurio.invoice_spending_associate
  ADD CONSTRAINT FK_invoice_spending_associate_spending
FOREIGN KEY (spending_id) REFERENCES mercurio.spending (id);
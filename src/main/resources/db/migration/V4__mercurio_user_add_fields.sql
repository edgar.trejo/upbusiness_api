ALTER TABLE mercurio.user ADD COLUMN si_vale_id BIGINT NULL;
ALTER TABLE mercurio.user ADD COLUMN active BOOL NULL DEFAULT TRUE;

UPDATE mercurio.user SET active = TRUE;

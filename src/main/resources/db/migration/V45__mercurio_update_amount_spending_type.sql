ALTER TABLE mercurio.amount_spending_type ADD COLUMN invoice_concept_id bigint;
ALTER TABLE mercurio.amount_spending_type ADD FOREIGN KEY (invoice_concept_id) REFERENCES mercurio.invoice_concept(id);

CREATE TABLE mercurio.configuration(
	id 	INT	NOT NULL AUTO_INCREMENT,
    name	VARCHAR(100) null,
    description	VARCHAR(150) null,
    value VARCHAR(50) null,
    constraint UQ_configuration_id
		unique (id)
);

alter table mercurio.configuration
	add primary key (id);
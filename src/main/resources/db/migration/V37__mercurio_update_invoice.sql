
UPDATE mercurio.invoice i
INNER JOIN mercurio.user_client c ON c.user_id = i.user_id
SET i.client_id= c.client_id
WHERE i.client_id IS NULL
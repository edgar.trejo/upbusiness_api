SET FOREIGN_KEY_CHECKS = 0;

CREATE TABLE mercurio.advance_approver_report
(
  id                  BIGINT NOT NULL AUTO_INCREMENT,
  advance_required_id BIGINT NULL,
  approver_user       BIGINT NULL,
  pre_status          BIGINT NULL,
  automatic           BOOL   NULL,
  admin               BOOL   NULL,
  PRIMARY KEY (id),
  UNIQUE UQ_advance_approver_report_id(id),
  KEY (advance_required_id),
  KEY (pre_status),
  KEY (approver_user)
);

CREATE TABLE mercurio.advance_required
(
  id                 BIGINT         NOT NULL AUTO_INCREMENT,
  user_id            BIGINT         NOT NULL,
  client_id          BIGINT         NOT NULL,
  iut                VARCHAR(20)    NOT NULL,
  name               VARCHAR(150)   NULL,
  description        VARCHAR(200)   NULL,
  card_number        INTEGER        NOT NULL,
  associat_card      BOOL           NOT NULL,
  amount_required    DECIMAL(10, 2) NOT NULL,
  transfer_date      TIMESTAMP      NOT NULL,
  approval_status_id BIGINT         NULL,
  active             BOOL           NULL,
  PRIMARY KEY (id),
  UNIQUE UQ_advance_request_id(id),
  KEY (approval_status_id),
  KEY (client_id),
  KEY (user_id)
);


CREATE TABLE mercurio.advance_required_approver
(
  id                       BIGINT  NOT NULL AUTO_INCREMENT,
  client_id                BIGINT  NULL,
  advance_required_id      BIGINT  NULL,
  approval_rule_advance_id BIGINT  NULL,
  approver_user_id         BIGINT  NULL,
  team_id                  BIGINT  NULL,
  position                 INTEGER NULL,
  admin                    BOOL    NOT NULL,
  PRIMARY KEY (id),
  UNIQUE UQ_advance_required_approver_id(id),
  KEY (advance_required_id),
  KEY (approval_rule_advance_id),
  KEY (approver_user_id),
  KEY (client_id),
  KEY (team_id)
);


CREATE TABLE mercurio.advance_status
(
  id          BIGINT       NOT NULL AUTO_INCREMENT,
  name        VARCHAR(50)  NULL,
  code        VARCHAR(50)  NULL,
  description VARCHAR(150) NULL,
  PRIMARY KEY (id),
  UNIQUE UQ_advance_status_id(id)
);


CREATE TABLE mercurio.amount_spending_type
(
  id               BIGINT         NOT NULL AUTO_INCREMENT,
  spending_id      BIGINT         NULL,
  spending_type_id BIGINT         NULL,
  amount           DECIMAL(10, 2) NULL,
  PRIMARY KEY (id),
  UNIQUE UQ_amount_spending_type_id(id),
  KEY (spending_id),
  KEY (spending_type_id)
);


CREATE TABLE mercurio.approval_rule
(
  id                         BIGINT         NOT NULL AUTO_INCREMENT,
  client_id                  BIGINT         NOT NULL,
  approval_rule_flow_type_id BIGINT         NOT NULL,
  name                       VARCHAR(100)   NOT NULL,
  description                VARCHAR(150)   NULL,
  start_amount               DECIMAL(10, 2) NULL,
  end_amount                 DECIMAL(10, 2) NULL,
  levels                     INT(1)         NULL,
  active                     BOOL           NOT NULL,
  PRIMARY KEY (id),
  UNIQUE UQ_approval_rule_id(id),
  KEY (approval_rule_flow_type_id),
  KEY (client_id)
);


CREATE TABLE mercurio.approval_rule_advance
(
  id                         BIGINT         NOT NULL AUTO_INCREMENT,
  client_id                  BIGINT         NOT NULL,
  approval_rule_flow_type_id BIGINT         NOT NULL,
  name                       VARCHAR(100)   NULL,
  description                VARCHAR(150)   NULL,
  start_amount               DECIMAL(10, 2) NULL,
  end_amount                 DECIMAL(10, 2) NULL,
  leves                      INT(1)         NULL,
  active                     BOOL           NULL,
  PRIMARY KEY (id),
  UNIQUE UQ_approval_rule_advance_id(id),
  KEY (approval_rule_flow_type_id),
  KEY (client_id)
);


CREATE TABLE mercurio.approval_rule_advance_applies_to
(
  approval_rule_advance_id BIGINT NULL,
  applies_to_id            BIGINT NULL,
  KEY (approval_rule_advance_id),
  KEY (applies_to_id)
);


CREATE TABLE mercurio.approval_rule_advance_approver_teams
(
  approval_rule_advance_id BIGINT NULL,
  approver_teams_id        BIGINT NULL,
  KEY (approval_rule_advance_id),
  KEY (approver_teams_id)
);


CREATE TABLE mercurio.approval_rule_advance_approver_users
(
  approval_rule_advance_id BIGINT NULL,
  approver_users_id        BIGINT NULL,
  KEY (approval_rule_advance_id),
  KEY (approver_users_id)
);


CREATE TABLE mercurio.approval_rule_applies_to
(
  approval_rule_id BIGINT NOT NULL,
  applies_to_id    BIGINT NOT NULL,
  KEY (approval_rule_id),
  KEY (applies_to_id)
);


CREATE TABLE mercurio.approval_rule_approver_teams
(
  approval_rule_id  BIGINT NOT NULL,
  approver_teams_id BIGINT NOT NULL,
  KEY (approval_rule_id),
  KEY (approver_teams_id)
);


CREATE TABLE mercurio.approval_rule_approver_users
(
  approval_rule_id  BIGINT NOT NULL,
  approver_users_id BIGINT NOT NULL,
  KEY (approval_rule_id),
  KEY (approver_users_id)
);


CREATE TABLE mercurio.approval_rule_flow_type
(
  id          BIGINT       NOT NULL AUTO_INCREMENT,
  name        VARCHAR(100) NULL,
  description VARCHAR(150) NULL,
  PRIMARY KEY (id),
  UNIQUE UQ_approval_rule_flow_type_id(id)
);


CREATE TABLE mercurio.approval_status
(
  id          BIGINT       NOT NULL AUTO_INCREMENT,
  name        VARCHAR(150) NULL,
  code        VARCHAR(50)  NULL,
  description VARCHAR(200) NULL,
  PRIMARY KEY (id),
  UNIQUE UQ_approval_status_id(id)
);


CREATE TABLE mercurio.approver_user
(
  id       BIGINT  NOT NULL AUTO_INCREMENT,
  position INTEGER NULL,
  user_id  BIGINT  NULL,
  PRIMARY KEY (id),
  UNIQUE UQ_approver_user_id(id),
  KEY (user_id)
);


CREATE TABLE mercurio.client
(
  id            BIGINT       NOT NULL AUTO_INCREMENT,
  number_client BIGINT       NULL,
  name          VARCHAR(200) NULL,
  logo          BLOB         NULL,
  active        BOOL         NULL,
  PRIMARY KEY (id),
  UNIQUE UQ_client_id(id)
);


CREATE TABLE mercurio.client_rfc
(
  client_id BIGINT NOT NULL,
  rfc_id    BIGINT NOT NULL,
  active    BOOL   NOT NULL,
  KEY (client_id),
  KEY (rfc_id)
);


CREATE TABLE mercurio.cost_center
(
  id          BIGINT       NOT NULL AUTO_INCREMENT,
  client_id   BIGINT       NULL,
  name        VARCHAR(150) NULL,
  code        VARCHAR(50)  NULL,
  description VARCHAR(200) NULL,
  active      BOOL         NULL,
  PRIMARY KEY (id),
  UNIQUE UQ_cost_center_id(id),
  KEY (client_id)
);


CREATE TABLE mercurio.event
(
  id                 BIGINT       NOT NULL AUTO_INCREMENT,
  user_id            BIGINT       NULL,
  approval_status_id BIGINT       NULL,
  name               VARCHAR(200) NULL,
  code               VARCHAR(50)  NULL,
  description        VARCHAR(250) NULL,
  date_start         TIMESTAMP    NULL,
  date_end           TIMESTAMP    NULL,
  date_created       TIMESTAMP    NULL,
  date_finished      TIMESTAMP    NULL,
  active             BOOL         NULL,
  client_id          BIGINT       NULL,
  PRIMARY KEY (id),
  UNIQUE UQ_event_id(id),
  KEY (approval_status_id),
  KEY (client_id),
  KEY (user_id)
);


CREATE TABLE mercurio.event_approver
(
  id               BIGINT  NOT NULL AUTO_INCREMENT,
  client_id        BIGINT  NOT NULL,
  event_id         BIGINT  NOT NULL,
  approval_rule_id BIGINT  NOT NULL,
  approver_user_id BIGINT  NULL,
  team_id          BIGINT  NULL,
  position         INTEGER NULL,
  admin            BOOL    NOT NULL,
  PRIMARY KEY (id),
  UNIQUE UQ_event_approver_id(id),
  KEY (approval_rule_id),
  KEY (approver_user_id),
  KEY (client_id),
  KEY (event_id),
  KEY (team_id)
);


CREATE TABLE mercurio.evidence_type
(
  id          BIGINT       NOT NULL AUTO_INCREMENT,
  name        VARCHAR(150) NULL,
  code        VARCHAR(50)  NULL,
  description VARCHAR(150) NULL,
  active      BOOL         NULL,
  PRIMARY KEY (id),
  UNIQUE UQ_evidence_type_id(id)
);


CREATE TABLE mercurio.federative_entity
(
  id          BIGINT       NOT NULL AUTO_INCREMENT,
  name        VARCHAR(150) NULL,
  code        VARCHAR(50)  NULL,
  description VARCHAR(150) NULL,
  active      BOOL         NULL,
  PRIMARY KEY (id),
  UNIQUE UQ_federative_entity_id(id)
);


CREATE TABLE mercurio.image_evidence
(
  id                  BIGINT       NOT NULL AUTO_INCREMENT,
  evidence_type_id    BIGINT       NULL,
  name_storage_system VARCHAR(100) NULL,
  PRIMARY KEY (id),
  UNIQUE UQ_image_evidence_id(id),
  KEY (evidence_type_id)
);


CREATE TABLE mercurio.image_rfc
(
  id              BIGINT       NOT NULL AUTO_INCREMENT,
  time_created    TIMESTAMP    NULL,
  user_creater_id BIGINT       NULL,
  rfc             VARCHAR(15)  NULL,
  name            VARCHAR(150) NULL,
  fiscal_address  VARCHAR(250) NULL,
  PRIMARY KEY (id),
  UNIQUE UQ_image_rfc_id(id),
  KEY (user_creater_id)
);


CREATE TABLE mercurio.invoice
(
  id                  BIGINT       NOT NULL AUTO_INCREMENT,
  establishment       VARCHAR(150) NULL,
  rfc_receiver        VARCHAR(16)  NULL,
  rfc_transmitter     VARCHAR(16)  NULL,
  fiscal_address      VARCHAR(200) NULL,
  expedition_address  VARCHAR(200) NULL,
  date_invoice        VARCHAR(12)  NULL,
  subtotal            VARCHAR(12)  NULL,
  total               VARCHAR(12)  NULL,
  uuid                VARCHAR(40)  NULL,
  evidence_type_id    BIGINT       NULL,
  user_id             BIGINT       NULL,
  name_storage_system VARCHAR(100) NULL,
  spending_id         BIGINT       NULL,
  iva                 VARCHAR(12)  NULL,
  client_id           BIGINT       NULL,
  PRIMARY KEY (id),
  UNIQUE UQ_invoice_id(id),
  KEY (client_id),
  KEY (evidence_type_id),
  KEY (spending_id),
  KEY (user_id)
);


CREATE TABLE mercurio.invoice_batch_process
(
  id         BIGINT    NOT NULL AUTO_INCREMENT,
  start_date TIMESTAMP NULL,
  end_date   TIMESTAMP NULL,
  success    BOOL      NULL,
  PRIMARY KEY (id),
  UNIQUE UQ_invoice_batch_process_id(id)
);


CREATE TABLE mercurio.invoice_file
(
  id            BIGINT       NOT NULL AUTO_INCREMENT,
  id_usuario    BIGINT       NULL,
  email_date    TIMESTAMP    NULL,
  name          VARCHAR(100) NULL,
  as3_key       VARCHAR(100) NULL,
  creation_date TIMESTAMP    NULL,
  PRIMARY KEY (id),
  UNIQUE UQ_invoice_file_id(id),
  KEY (id_usuario)
);


CREATE TABLE mercurio.job_position
(
  id          BIGINT       NOT NULL AUTO_INCREMENT,
  client_id   BIGINT       NULL,
  name        VARCHAR(150) NULL,
  code        VARCHAR(50)  NULL,
  description VARCHAR(150) NULL,
  position    INTEGER      NULL,
  active      BOOL         NULL,
  PRIMARY KEY (id),
  UNIQUE UQ_job_position_id(id),
  KEY (client_id)
);


CREATE TABLE mercurio.pay_method
(
  id          BIGINT       NOT NULL AUTO_INCREMENT,
  name        VARCHAR(150) NULL,
  code        VARCHAR(50)  NULL,
  description VARCHAR(150) NULL,
  active      BOOL         NULL,
  PRIMARY KEY (id),
  UNIQUE UQ_pay_method_id(id)
);


CREATE TABLE mercurio.project
(
  id          BIGINT       NOT NULL AUTO_INCREMENT,
  client_id   BIGINT       NULL,
  name        VARCHAR(150) NULL,
  code        VARCHAR(50)  NULL,
  description VARCHAR(150) NULL,
  active      BOOL         NULL,
  PRIMARY KEY (id),
  UNIQUE UQ_project_id(id),
  KEY (client_id)
);


CREATE TABLE mercurio.project_users
(
  id         BIGINT NOT NULL AUTO_INCREMENT,
  project_id BIGINT NULL,
  users_id   BIGINT NULL,
  PRIMARY KEY (id),
  UNIQUE UQ_project_users_id(id),
  KEY (project_id),
  KEY (users_id)
);


CREATE TABLE mercurio.role
(
  id          BIGINT       NOT NULL AUTO_INCREMENT,
  name        VARCHAR(150) NULL,
  code        VARCHAR(50)  NULL,
  description VARCHAR(150) NULL,
  active      BOOL         NULL,
  PRIMARY KEY (id),
  UNIQUE UQ_role_id(id)
);


CREATE TABLE mercurio.spending
(
  id                 BIGINT         NOT NULL AUTO_INCREMENT,
  client_id          BIGINT         NULL,
  name               VARCHAR(100)   NULL,
  date_start         TIMESTAMP      NULL,
  date_end           TIMESTAMP      NULL,
  date_created       TIMESTAMP      NULL,
  date_finished      TIMESTAMP      NULL,
  spending_total     DECIMAL(10, 2) NULL,
  spending_comments  VARCHAR(255)   NULL,
  approval_status_id BIGINT         NULL,
  user_id            BIGINT         NULL,
  event_id           BIGINT         NULL,
  pay_methd_id       BIGINT         NULL,
  transaction_id     BIGINT         NULL,
  image_evidence_id  BIGINT         NULL,
  PRIMARY KEY (id),
  UNIQUE UQ_spending_id(id),
  KEY (approval_status_id),
  KEY (client_id),
  KEY (event_id),
  KEY (image_evidence_id),
  KEY (pay_methd_id),
  KEY (transaction_id),
  KEY (user_id)
);


CREATE TABLE mercurio.spending_type
(
  id          BIGINT       NOT NULL AUTO_INCREMENT,
  client_id   BIGINT       NULL,
  name        VARCHAR(150) NULL,
  code        VARCHAR(50)  NULL,
  description VARCHAR(200) NULL,
  active      BOOL         NULL,
  icon        VARCHAR(100) NULL,
  PRIMARY KEY (id),
  UNIQUE UQ_spending_type_id(id)
);


CREATE TABLE mercurio.team
(
  id          BIGINT       NOT NULL AUTO_INCREMENT,
  client_id   BIGINT       NULL,
  name        VARCHAR(50)  NULL,
  code        VARCHAR(50)  NULL,
  description VARCHAR(250) NULL,
  active      BOOL         NULL,
  PRIMARY KEY (id),
  UNIQUE UQ_team_id(id),
  KEY (client_id)
);


CREATE TABLE mercurio.team_users
(
  id       BIGINT NOT NULL AUTO_INCREMENT,
  team_id  BIGINT NULL,
  users_id BIGINT NULL,
  PRIMARY KEY (id),
  UNIQUE UQ_team_users_id(id),
  KEY (team_id),
  KEY (users_id)
);


CREATE TABLE mercurio.transaction
(
  id               BIGINT         NOT NULL AUTO_INCREMENT,
  iut              VARCHAR(15)    NULL,
  tipo_tarjeta     VARCHAR(5)     NULL,
  fecha            VARCHAR(15)    NULL,
  hora             VARCHAR(10)    NULL,
  movimiento       VARCHAR(20)    NULL,
  producto         VARCHAR(30)    NULL,
  consumo_neto     DECIMAL(10, 2) NULL,
  importe          DECIMAL(10, 5) NULL,
  consumo          DECIMAL(10, 5) NULL,
  ieps             DECIMAL(10, 5) NULL,
  iva              DECIMAL(10, 5) NULL,
  litros           DECIMAL(8, 2)  NULL,
  num_autorizacion VARCHAR(15)    NULL,
  rfc_comercio     VARCHAR(16)    NULL,
  afiliacion       VARCHAR(15)    NULL,
  nombre_comercio  VARCHAR(100)   NULL,
  giro             VARCHAR(10)    NULL,
  procedencia      VARCHAR(100)   NULL,
  client_id        BIGINT         NULL,
  PRIMARY KEY (id),
  UNIQUE UQ_transaction_id(id),
  KEY (client_id)
);


CREATE TABLE mercurio.user
(
  id                     BIGINT       NOT NULL AUTO_INCREMENT,
  name                   VARCHAR(250) NULL,
  first_name             VARCHAR(250) NULL,
  last_name              VARCHAR(50)  NULL,
  complete_name          VARCHAR(255) NULL,
  number_employee        VARCHAR(30)  NULL,
  gender                 CHAR(1)      NULL,
  birth_date             TIMESTAMP    NULL,
  federative_entity_id   BIGINT       NULL,
  reference              VARCHAR(50)  NULL,
  phone_number           VARCHAR(20)  NULL,
  email                  VARCHAR(250) NULL,
  email_admin            VARCHAR(250) NULL,
  email_approver         VARCHAR(250) NULL,
  device_token           VARCHAR(255) NULL,
  cost_center_id         BIGINT       NULL,
  role_id                BIGINT       NULL,
  job_position_id        BIGINT       NULL,
  authentication_token   VARCHAR(255) NULL,
  invoice_email          VARCHAR(100) NULL,
  invoice_email_password VARCHAR(100) NULL,
  PRIMARY KEY (id),
  UNIQUE UQ_user_id(id),
  KEY (cost_center_id),
  KEY (federative_entity_id),
  KEY (job_position_id),
  KEY (role_id)
);


CREATE TABLE mercurio.user_client
(
  user_id    BIGINT      NULL,
  contact_id VARCHAR(20) NULL,
  client_id  BIGINT      NULL,
  active     BOOL        NULL,
  KEY (client_id),
  KEY (user_id)
);


SET FOREIGN_KEY_CHECKS = 1;

ALTER TABLE mercurio.advance_approver_report
  ADD CONSTRAINT FK_advance_approver_report_advance_required
FOREIGN KEY (advance_required_id) REFERENCES mercurio.advance_required (id);

ALTER TABLE mercurio.advance_approver_report
  ADD CONSTRAINT FK_advance_approver_report_advance_status
FOREIGN KEY (pre_status) REFERENCES mercurio.advance_status (id);

ALTER TABLE mercurio.advance_approver_report
  ADD CONSTRAINT FK_advance_approver_report_user
FOREIGN KEY (approver_user) REFERENCES mercurio.user (id);

ALTER TABLE mercurio.advance_required
  ADD CONSTRAINT FK_advance_required_advance_status
FOREIGN KEY (approval_status_id) REFERENCES mercurio.advance_status (id);

ALTER TABLE mercurio.advance_required
  ADD CONSTRAINT FK_advance_required_client
FOREIGN KEY (client_id) REFERENCES mercurio.client (id);

ALTER TABLE mercurio.advance_required
  ADD CONSTRAINT FK_advance_required_user
FOREIGN KEY (user_id) REFERENCES mercurio.user (id);

ALTER TABLE mercurio.advance_required_approver
  ADD CONSTRAINT FK_advance_required_approver_advance_required
FOREIGN KEY (advance_required_id) REFERENCES mercurio.advance_required (id);

ALTER TABLE mercurio.advance_required_approver
  ADD CONSTRAINT FK_advance_required_approver_approval_rule_advance
FOREIGN KEY (approval_rule_advance_id) REFERENCES mercurio.approval_rule_advance (id);

ALTER TABLE mercurio.advance_required_approver
  ADD CONSTRAINT FK_advance_required_approver_approver_user
FOREIGN KEY (approver_user_id) REFERENCES mercurio.approver_user (id);

ALTER TABLE mercurio.advance_required_approver
  ADD CONSTRAINT FK_advance_required_approver_client
FOREIGN KEY (client_id) REFERENCES mercurio.client (id);

ALTER TABLE mercurio.advance_required_approver
  ADD CONSTRAINT FK_advance_required_approver_team
FOREIGN KEY (team_id) REFERENCES mercurio.team (id);

ALTER TABLE mercurio.amount_spending_type
  ADD CONSTRAINT FK_amount_spending_type_spending
FOREIGN KEY (spending_id) REFERENCES mercurio.spending (id);

ALTER TABLE mercurio.amount_spending_type
  ADD CONSTRAINT FK_amount_spending_type_spending_type
FOREIGN KEY (spending_type_id) REFERENCES mercurio.spending_type (id);

ALTER TABLE mercurio.approval_rule
  ADD CONSTRAINT FK_approval_rule_approval_rule_flow_type
FOREIGN KEY (approval_rule_flow_type_id) REFERENCES mercurio.approval_rule_flow_type (id);

ALTER TABLE mercurio.approval_rule
  ADD CONSTRAINT FK_approval_rule_client
FOREIGN KEY (client_id) REFERENCES mercurio.client (id);

ALTER TABLE mercurio.approval_rule_advance
  ADD CONSTRAINT FK_approval_rule_advance_approval_rule_flow_type
FOREIGN KEY (approval_rule_flow_type_id) REFERENCES mercurio.approval_rule_flow_type (id);

ALTER TABLE mercurio.approval_rule_advance
  ADD CONSTRAINT FK_approval_rule_advance_client
FOREIGN KEY (client_id) REFERENCES mercurio.client (id);

ALTER TABLE mercurio.approval_rule_advance_applies_to
  ADD CONSTRAINT FK_approval_rule_advance_applies_to_approval_rule_advance
FOREIGN KEY (approval_rule_advance_id) REFERENCES mercurio.approval_rule_advance (id);

ALTER TABLE mercurio.approval_rule_advance_applies_to
  ADD CONSTRAINT FK_approval_rule_advance_applies_to_team
FOREIGN KEY (applies_to_id) REFERENCES mercurio.team (id);

ALTER TABLE mercurio.approval_rule_advance_approver_teams
  ADD CONSTRAINT FK_approval_rule_advance_approver_teams_approval_rule_advance
FOREIGN KEY (approval_rule_advance_id) REFERENCES mercurio.approval_rule_advance (id);

ALTER TABLE mercurio.approval_rule_advance_approver_teams
  ADD CONSTRAINT FK_approval_rule_advance_approver_teams_team
FOREIGN KEY (approver_teams_id) REFERENCES mercurio.team (id);

ALTER TABLE mercurio.approval_rule_advance_approver_users
  ADD CONSTRAINT FK_approval_rule_advance_approver_users_approval_rule_advance
FOREIGN KEY (approval_rule_advance_id) REFERENCES mercurio.approval_rule_advance (id);

ALTER TABLE mercurio.approval_rule_advance_approver_users
  ADD CONSTRAINT FK_approval_rule_advance_approver_users_approver_user
FOREIGN KEY (approver_users_id) REFERENCES mercurio.approver_user (id);

ALTER TABLE mercurio.approval_rule_applies_to
  ADD CONSTRAINT FK_approval_rule_applies_to_approval_rule
FOREIGN KEY (approval_rule_id) REFERENCES mercurio.approval_rule (id);

ALTER TABLE mercurio.approval_rule_applies_to
  ADD CONSTRAINT FK_approval_rule_applies_to_team
FOREIGN KEY (applies_to_id) REFERENCES mercurio.team (id);

ALTER TABLE mercurio.approval_rule_approver_teams
  ADD CONSTRAINT FK_approval_rule_approver_teams_approval_rule
FOREIGN KEY (approval_rule_id) REFERENCES mercurio.approval_rule (id);

ALTER TABLE mercurio.approval_rule_approver_teams
  ADD CONSTRAINT FK_approval_rule_approver_teams_team
FOREIGN KEY (approver_teams_id) REFERENCES mercurio.team (id);

ALTER TABLE mercurio.approval_rule_approver_users
  ADD CONSTRAINT FK_approval_rule_approver_users_approval_rule
FOREIGN KEY (approval_rule_id) REFERENCES mercurio.approval_rule (id);

ALTER TABLE mercurio.approval_rule_approver_users
  ADD CONSTRAINT FK_approval_rule_approver_users_approver_user
FOREIGN KEY (approver_users_id) REFERENCES mercurio.approver_user (id);

ALTER TABLE mercurio.approver_user
  ADD CONSTRAINT FK_approver_user_user
FOREIGN KEY (user_id) REFERENCES mercurio.user (id);

ALTER TABLE mercurio.client_rfc
  ADD CONSTRAINT FK_client_rfc_client
FOREIGN KEY (client_id) REFERENCES mercurio.client (id);

ALTER TABLE mercurio.client_rfc
  ADD CONSTRAINT FK_client_rfc_image_rfc
FOREIGN KEY (rfc_id) REFERENCES mercurio.image_rfc (id);

ALTER TABLE mercurio.cost_center
  ADD CONSTRAINT FK_cost_center_client
FOREIGN KEY (client_id) REFERENCES mercurio.client (id);

ALTER TABLE mercurio.event
  ADD CONSTRAINT FK_event_approval_status
FOREIGN KEY (approval_status_id) REFERENCES mercurio.approval_status (id);

ALTER TABLE mercurio.event
  ADD CONSTRAINT FK_event_client
FOREIGN KEY (client_id) REFERENCES mercurio.client (id);

ALTER TABLE mercurio.event
  ADD CONSTRAINT FK_event_user
FOREIGN KEY (user_id) REFERENCES mercurio.user (id);

ALTER TABLE mercurio.event_approver
  ADD CONSTRAINT FK_event_approver_approval_rule
FOREIGN KEY (approval_rule_id) REFERENCES mercurio.approval_rule (id);

ALTER TABLE mercurio.event_approver
  ADD CONSTRAINT FK_event_approver_approver_user
FOREIGN KEY (approver_user_id) REFERENCES mercurio.approver_user (id);

ALTER TABLE mercurio.event_approver
  ADD CONSTRAINT FK_event_approver_client
FOREIGN KEY (client_id) REFERENCES mercurio.client (id);

ALTER TABLE mercurio.event_approver
  ADD CONSTRAINT FK_event_approver_event
FOREIGN KEY (event_id) REFERENCES mercurio.event (id);

ALTER TABLE mercurio.event_approver
  ADD CONSTRAINT FK_event_approver_team
FOREIGN KEY (team_id) REFERENCES mercurio.team (id);

ALTER TABLE mercurio.image_evidence
  ADD CONSTRAINT FK_image_evidence_evidence_type
FOREIGN KEY (evidence_type_id) REFERENCES mercurio.evidence_type (id);

ALTER TABLE mercurio.image_rfc
  ADD CONSTRAINT FK_image_rfc_user
FOREIGN KEY (user_creater_id) REFERENCES mercurio.user (id);

ALTER TABLE mercurio.invoice
  ADD CONSTRAINT FK_invoice_client
FOREIGN KEY (client_id) REFERENCES mercurio.client (id);

ALTER TABLE mercurio.invoice
  ADD CONSTRAINT FK_invoice_evidence_type
FOREIGN KEY (evidence_type_id) REFERENCES mercurio.evidence_type (id);

ALTER TABLE mercurio.invoice
  ADD CONSTRAINT FK_invoice_spending
FOREIGN KEY (spending_id) REFERENCES mercurio.spending (id);

ALTER TABLE mercurio.invoice
  ADD CONSTRAINT FK_invoice_user
FOREIGN KEY (user_id) REFERENCES mercurio.user (id);

ALTER TABLE mercurio.invoice_file
  ADD CONSTRAINT FK_invoice_file_user
FOREIGN KEY (id_usuario) REFERENCES mercurio.user (id);

ALTER TABLE mercurio.job_position
  ADD CONSTRAINT FK_job_position_client
FOREIGN KEY (client_id) REFERENCES mercurio.client (id);

ALTER TABLE mercurio.project
  ADD CONSTRAINT FK_project_client
FOREIGN KEY (client_id) REFERENCES mercurio.client (id);

ALTER TABLE mercurio.project_users
  ADD CONSTRAINT FK_project_users_project
FOREIGN KEY (project_id) REFERENCES mercurio.project (id);

ALTER TABLE mercurio.project_users
  ADD CONSTRAINT FK_project_users_user
FOREIGN KEY (users_id) REFERENCES mercurio.user (id);

ALTER TABLE mercurio.spending
  ADD CONSTRAINT FK_spending_approval_status
FOREIGN KEY (approval_status_id) REFERENCES mercurio.approval_status (id);

ALTER TABLE mercurio.spending
  ADD CONSTRAINT FK_spending_client
FOREIGN KEY (client_id) REFERENCES mercurio.client (id);

ALTER TABLE mercurio.spending
  ADD CONSTRAINT FK_spending_event
FOREIGN KEY (event_id) REFERENCES mercurio.event (id);

ALTER TABLE mercurio.spending
  ADD CONSTRAINT FK_spending_image_evidence
FOREIGN KEY (image_evidence_id) REFERENCES mercurio.image_evidence (id);

ALTER TABLE mercurio.spending
  ADD CONSTRAINT FK_spending_pay_method
FOREIGN KEY (pay_methd_id) REFERENCES mercurio.pay_method (id);

ALTER TABLE mercurio.spending
  ADD CONSTRAINT FK_spending_transaction
FOREIGN KEY (transaction_id) REFERENCES mercurio.transaction (id);

ALTER TABLE mercurio.spending
  ADD CONSTRAINT FK_spending_user
FOREIGN KEY (user_id) REFERENCES mercurio.user (id);

ALTER TABLE mercurio.team
  ADD CONSTRAINT FK_team_client
FOREIGN KEY (client_id) REFERENCES mercurio.client (id);

ALTER TABLE mercurio.team_users
  ADD CONSTRAINT FK_team_users_team
FOREIGN KEY (team_id) REFERENCES mercurio.team (id);

ALTER TABLE mercurio.team_users
  ADD CONSTRAINT FK_team_users_user
FOREIGN KEY (users_id) REFERENCES mercurio.user (id);

ALTER TABLE mercurio.transaction
  ADD CONSTRAINT FK_transaction_client
FOREIGN KEY (client_id) REFERENCES mercurio.client (id);

ALTER TABLE mercurio.user
  ADD CONSTRAINT FK_user_cost_center
FOREIGN KEY (cost_center_id) REFERENCES mercurio.cost_center (id);

ALTER TABLE mercurio.user
  ADD CONSTRAINT FK_user_federative_entity
FOREIGN KEY (federative_entity_id) REFERENCES mercurio.federative_entity (id);

ALTER TABLE mercurio.user
  ADD CONSTRAINT FK_user_job_position
FOREIGN KEY (job_position_id) REFERENCES mercurio.job_position (id);

ALTER TABLE mercurio.user
  ADD CONSTRAINT FK_user_role
FOREIGN KEY (role_id) REFERENCES mercurio.role (id);

ALTER TABLE mercurio.user_client
  ADD CONSTRAINT FK_user_client_client
FOREIGN KEY (client_id) REFERENCES mercurio.client (id);

ALTER TABLE mercurio.user_client
  ADD CONSTRAINT FK_user_client_user
FOREIGN KEY (user_id) REFERENCES mercurio.user (id);


INSERT INTO mercurio.federative_entity (name, code, description, active)
VALUES ('AGUASCALIENTES', 'AS', 'ESTADO DE AGUASCALIENTES', TRUE);
INSERT INTO mercurio.federative_entity (name, code, description, active)
VALUES ('BAJA CALIFORNIA NORTE', 'BC', 'ESTADO DE BAJA CALIFORNIA NORTE', TRUE);
INSERT INTO mercurio.federative_entity (name, code, description, active)
VALUES ('BAJA CALIFORNIA SUR', 'BS', 'ESTADO DE BAJA CALIFORNIA SUR', TRUE);
INSERT INTO mercurio.federative_entity (name, code, description, active)
VALUES ('CAMPECHE', 'CC', 'ESTADO DE CAMPECHE', TRUE);
INSERT INTO mercurio.federative_entity (name, code, description, active)
VALUES ('COAHUILA', 'CL', 'ESTADO DE COAHUILA', TRUE);
INSERT INTO mercurio.federative_entity (name, code, description, active)
VALUES ('COLIMA', 'CM', 'ESTADO DE COLIMA', TRUE);
INSERT INTO mercurio.federative_entity (name, code, description, active)
VALUES ('CHIAPAS', 'CS', 'ESTADO DE CHIAPAS', TRUE);
INSERT INTO mercurio.federative_entity (name, code, description, active)
VALUES ('CHIHUAHUA', 'CH', 'ESTADO DE CHIHUAHUA', TRUE);
INSERT INTO mercurio.federative_entity (name, code, description, active)
VALUES ('CIUDAD DE MÉXICO', 'DF', 'ESTADO DE CIUDAD DE MÉXICO', TRUE);
INSERT INTO mercurio.federative_entity (name, code, description, active)
VALUES ('DURANGO', 'DG', 'ESTADO DE DURANGO', TRUE);
INSERT INTO mercurio.federative_entity (name, code, description, active)
VALUES ('GUANAJUATO', 'GT', 'ESTADO DE GUANAJUATO', TRUE);
INSERT INTO mercurio.federative_entity (name, code, description, active)
VALUES ('GUERRERO', 'GR', 'ESTADO DE GUERRERO', TRUE);
INSERT INTO mercurio.federative_entity (name, code, description, active)
VALUES ('HIDALGO', 'HG', 'ESTADO DE HIDALGO', TRUE);
INSERT INTO mercurio.federative_entity (name, code, description, active)
VALUES ('JALISCO', 'JC', 'ESTADO DE JALISCO', TRUE);
INSERT INTO mercurio.federative_entity (name, code, description, active)
VALUES ('ESTADO DE MÉXICO', 'MC', 'ESTADO DE ESTADO DE MÉXICO', TRUE);
INSERT INTO mercurio.federative_entity (name, code, description, active)
VALUES ('MICHOACÁN', 'MN', 'ESTADO DE MICHOACÁN', TRUE);
INSERT INTO mercurio.federative_entity (name, code, description, active)
VALUES ('MORELOS', 'MS', 'ESTADO DE MORELOS', TRUE);
INSERT INTO mercurio.federative_entity (name, code, description, active)
VALUES ('NAYARIT', 'NT', 'ESTADO DE NAYARIT', TRUE);
INSERT INTO mercurio.federative_entity (name, code, description, active)
VALUES ('NUEVO LEÓN', 'NL', 'ESTADO DE NUEVO LEÓN', TRUE);
INSERT INTO mercurio.federative_entity (name, code, description, active)
VALUES ('OAXACA', 'OC', 'ESTADO DE OAXACA', TRUE);
INSERT INTO mercurio.federative_entity (name, code, description, active)
VALUES ('PUEBLA', 'PL', 'ESTADO DE PUEBLA', TRUE);
INSERT INTO mercurio.federative_entity (name, code, description, active)
VALUES ('QUERÉTARO', 'QT', 'ESTADO DE QUERÉTARO', TRUE);
INSERT INTO mercurio.federative_entity (name, code, description, active)
VALUES ('QUINTANA ROO', 'QR', 'ESTADO DE QUINTANA ROO', TRUE);
INSERT INTO mercurio.federative_entity (name, code, description, active)
VALUES ('SAN LUIS POTOSÍ', 'SP', 'ESTADO DE SAN LUIS POTOSÍ', TRUE);
INSERT INTO mercurio.federative_entity (name, code, description, active)
VALUES ('SINALOA', 'SL', 'ESTADO DE SINALOA', TRUE);
INSERT INTO mercurio.federative_entity (name, code, description, active)
VALUES ('SONORA', 'SR', 'ESTADO DE SONORA', TRUE);
INSERT INTO mercurio.federative_entity (name, code, description, active)
VALUES ('TABASCO', 'TC', 'ESTADO DE TABASCO', TRUE);
INSERT INTO mercurio.federative_entity (name, code, description, active)
VALUES ('TAMAULIPAS', 'TL', 'ESTADO DE TAMAULIPAS', TRUE);
INSERT INTO mercurio.federative_entity (name, code, description, active)
VALUES ('TLAXCALA', 'TS', 'ESTADO DE TLAXCALA', TRUE);
INSERT INTO mercurio.federative_entity (name, code, description, active)
VALUES ('VERACRUZ', 'VZ', 'ESTADO DE VERACRUZ', TRUE);
INSERT INTO mercurio.federative_entity (name, code, description, active)
VALUES ('YUCATÁN', 'YN', 'ESTADO DE YUCATÁN', TRUE);
INSERT INTO mercurio.federative_entity (name, code, description, active)
VALUES ('ZACATECAS', 'ZS', 'ESTADO DE ZACATECAS', TRUE);

INSERT INTO mercurio.role (name, code, description, active) VALUES ('ADMIN', 'ADMIN', 'ADMINISTRADOR', TRUE);
INSERT INTO mercurio.role (name, code, description, active) VALUES ('APPROVER', 'APPROVER', 'APROBADOR', TRUE);
INSERT INTO mercurio.role (name, code, description, active) VALUES ('OBSERVER', 'OBSERVER', 'OBSERVADOR', TRUE);
INSERT INTO mercurio.role (name, code, description, active)
VALUES ('OBSERVER_SV', 'OBSERVER_SV', 'OBSERVADOR SI VALE', TRUE);
INSERT INTO mercurio.role (name, code, description, active) VALUES ('TRAVELER', 'TRAVELER', 'VIAJERO', TRUE);

INSERT INTO mercurio.approval_status (name, code, description) VALUES ('EDICIÓN', 'EDIT', 'ESTATUS DE EDICIÓN');
INSERT INTO mercurio.approval_status (name, code, description)
VALUES ('PENDIENTE', 'LOOKED', 'PARA VIAJERO ESTATUS EN REVISIÓN/ESTATUS PENDIENTE PARA ADMIN');
INSERT INTO mercurio.approval_status (name, code, description)
VALUES ('APROBADO', 'APPROVAL', 'ESTADO APROBADO PARA GASTO Y EVENTO');
INSERT INTO mercurio.approval_status (name, code, description)
VALUES ('RECHAZADO', 'REFUSED', 'PARA VIAJERO ESTATUS EN VERIFICAR/ESTATUS RECHAZADO PARA ADMIN');

INSERT INTO mercurio.pay_method (name, code, description, active)
VALUES ('PRODUCTO SI VALE', '1', 'metodo de pago', TRUE);
INSERT INTO mercurio.pay_method (name, code, description, active)
VALUES ('OTRA TARJETA DE LA EMPRESA', '2', 'metodo de pago', TRUE);
INSERT INTO mercurio.pay_method (name, code, description, active)
VALUES ('EFECTIVO DE LA EMPRESA', '3', 'metodo de pago', TRUE);
INSERT INTO mercurio.pay_method (name, code, description, active)
VALUES ('TARJETA PERSONAL', '4', 'metodo de pago', TRUE);
INSERT INTO mercurio.pay_method (name, code, description, active)
VALUES ('EFECTIVO PERSONAL', '5', 'metodo de pago', TRUE);

INSERT INTO mercurio.evidence_type (name, code, description, active) VALUES ('XML', 'XML', 'Archivo xml', TRUE);
INSERT INTO mercurio.evidence_type (name, code, description, active) VALUES ('PDF', 'PDF', 'Archivo pdf', TRUE);
INSERT INTO mercurio.evidence_type (name, code, description, active) VALUES ('PNG', 'PNG', 'Archivo png', TRUE);
INSERT INTO mercurio.evidence_type (name, code, description, active) VALUES ('JPEG', 'JPEG', 'Archivo jpeg', TRUE);

INSERT INTO mercurio.advance_status (name, code, description)
VALUES ('PENDIENTE', 'PENDING', 'Anticipo adquiere un estado de Pendiente');
INSERT INTO mercurio.advance_status (name, code, description)
VALUES ('APROBADO', 'APPROVER', 'Anticipo adquiere un estado de Aprobado');
INSERT INTO mercurio.advance_status (name, code, description)
VALUES ('RECHAZADO', 'REJECT', 'Anticipo adquiere un estado de Rechazado');
INSERT INTO mercurio.advance_status (name, code, description)
VALUES ('DISPERSADO', 'SCATTERED', 'Anticipo adquiere un estado de Dispersado');

-- Remove
INSERT INTO client VALUES
  (1, 10197060, 'SI VALE MEXICO, S.A. DE C.V.', NULL, 1),
  (2, 10224030, 'SI VALE MEXICO, S.A. DE C.V.', NULL, 1);
INSERT INTO user VALUES
  (1, 'MAGALY ANDREA', 'NIÑO GARCIA', NULL, 'MAGALY ANDREA NIÑO GARCIA ', '', '', NULL, NULL, '', '',
    'manino@sivale.com.mx', '', '', '', NULL, 1, NULL,
    'eyJhbGciOiJIUzI1NiJ9.eyJyb2xlSWQiOiIxIiwib3JpZ2luIjoiV0VCTSIsImVtYWlsIjoibWFuaW5vQHNpdmFsZS5jb20ubXgifQ.map12V4Yl7WtdXy-M0Q_2M4q1-c1BJ6LgkGZsIQAEzs',
    NULL, NULL),
  (2, 'CLARITA', 'SOSA', NULL, 'CLARITA SOSA ', '', '', NULL, NULL, '', '', 'csosa@sivale.com.mx', '', '', '', NULL, 1,
                                                                            NULL,
                                                                            'eyJhbGciOiJIUzI1NiJ9.eyJyb2xlSWQiOiIxIiwib3JpZ2luIjoiV0VCTSIsImVtYWlsIjoiY3Nvc2FAc2l2YWxlLmNvbS5teCJ9.BIZgBr_DC338rOcaJc04uP14LnQmtgojvOHpgPwg2F0',
                                                                            NULL, NULL);
INSERT INTO user_client VALUES (1, '1227384', 2, 1), (2, '419375', 2, 1);
--


INSERT INTO spending_type (client_id, name, code, description, active)
VALUES (1, 'TRANSPORTE TERRESTRE', 'GASTO', 'TIPO DE GASTO', TRUE);
INSERT INTO spending_type (client_id, name, code, description, active)
VALUES (1, 'TRANSPORTE AEREO', 'GASTO', 'TIPO DE GASTO', TRUE);
INSERT INTO spending_type (client_id, name, code, description, active)
VALUES (1, 'MANTENIMIENTO', 'GASTO', 'TIPO DE GASTO', TRUE);
INSERT INTO spending_type (client_id, name, code, description, active)
VALUES (1, 'SEGUROS', 'GASTO', 'TIPO DE GASTO', TRUE);
INSERT INTO spending_type (client_id, name, code, description, active)
VALUES (1, 'ESTACIONAMIENTO', 'GASTO', 'TIPO DE GASTO ', TRUE);
INSERT INTO spending_type (client_id, name, code, description, active)
VALUES (1, 'ALIMENTOS', 'GASTO', 'TIPO DE GASTO', TRUE);
INSERT INTO spending_type (client_id, name, code, description, active)
VALUES (1, 'HOSPEDAJE', 'GASTO', 'TIPO DE GASTO', TRUE);
INSERT INTO spending_type (client_id, name, code, description, active)
VALUES (1, 'PROPINA', 'GASTO', 'TIPO DE GASTO', TRUE);
INSERT INTO spending_type (client_id, name, code, description, active)
VALUES (1, 'IVA', 'GASTO', 'TIPO DE GASTO', TRUE);
INSERT INTO spending_type (client_id, name, code, description, active)
VALUES (1, 'IEPS', 'GASTO', 'TIPO DE GASTO', TRUE);
INSERT INTO spending_type (client_id, name, code, description, active)
VALUES (1, 'OTRO IMPUESTO', 'GASTO', 'TIPO DE GASTO', TRUE);
INSERT INTO spending_type (client_id, name, code, description, active)
VALUES (1, 'OTRO', 'GASTO', 'TIPO DE GASTO', TRUE);


INSERT INTO mercurio.cost_center (client_id, name, code, description, active)
VALUES (1, 'DIRECCIÓN GENERAL', 'DG', 'area', TRUE);
INSERT INTO mercurio.cost_center (client_id, name, code, description, active)
VALUES (1, 'ADMINISTRACIÓN', 'AD', 'area', TRUE);
INSERT INTO mercurio.cost_center (client_id, name, code, description, active)
VALUES (1, 'RECURSOS HUMANOS', 'RH', 'area', TRUE);
INSERT INTO mercurio.cost_center (client_id, name, code, description, active)
VALUES (1, 'FINANZAS', 'FZ', 'area', TRUE);
INSERT INTO mercurio.cost_center (client_id, name, code, description, active)
VALUES (1, 'MERCADOTECNICA', 'MT', 'area', TRUE);
INSERT INTO mercurio.cost_center (client_id, name, code, description, active)
VALUES (1, 'SISTEMAS INFORMÁTICOS', 'SI', 'area', TRUE);
INSERT INTO mercurio.cost_center (client_id, name, code, description, active)
VALUES (1, 'COMERCIAL', 'CL', 'area', TRUE);
INSERT INTO mercurio.cost_center (client_id, name, code, description, active)
VALUES (1, 'DISTRIBUCIÓN', 'DI', 'area', TRUE);
INSERT INTO mercurio.cost_center (client_id, name, code, description, active)
VALUES (1, 'PRODUCCIÓN', 'PR', 'area', TRUE);
INSERT INTO mercurio.cost_center (client_id, name, code, description, active)
VALUES (1, 'RECURSOS MATERIALES', 'RM', 'area', TRUE);
INSERT INTO mercurio.cost_center (client_id, name, code, description, active) VALUES (1, 'COMPRAS', 'CS', 'area', TRUE);
INSERT INTO mercurio.cost_center (client_id, name, code, description, active)
VALUES (1, 'SERVICIO A CLIENTES', 'SA', 'area', TRUE);
INSERT INTO mercurio.cost_center (client_id, name, code, description, active) VALUES (1, 'OTRO', 'OO', 'area', TRUE);

INSERT INTO approval_rule_flow_type (name, description)
VALUES ('APROBAR', 'flujo de aprobación aprobar automaticamente');
INSERT INTO approval_rule_flow_type (name, description)
VALUES ('RECHAZAR', 'flujo de aprobación rechazar automaticamente');
INSERT INTO approval_rule_flow_type (name, description)
VALUES ('GRUPOS', 'flujo de aprobación aprueban grupos');
INSERT INTO approval_rule_flow_type (name, description)
VALUES ('NIVELES JERARQUICOS', 'flujo de aprobación aprueban niveles jerarquicos');
INSERT INTO approval_rule_flow_type (name, description)
VALUES ('USUARIOS', 'flujo de aprobación aprueban usuarios');
INSERT INTO approval_rule_flow_type (name, description)
VALUES ('ADMINISTRADOR', 'flujo de aprobación aprueba administrador');

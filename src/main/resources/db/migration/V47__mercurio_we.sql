INSERT INTO mercurio.approval_status (name, code, description) VALUES ('PAGADO', 'PAID', 'ESTATUS DE PAGADO');
ALTER TABLE mercurio.event ADD COLUMN date_paid timestamp NULL;
CREATE TABLE mercurio.accounting_report
(
  id                  BIGINT         NOT NULL AUTO_INCREMENT,
  type_operation_id   INT            NULL,
  advance_required_id BIGINT         NULL,
  event_id            BIGINT         NULL,
  transfer_id         BIGINT         NULL,
  date                TIMESTAMP      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  return_type         VARCHAR(50)    NULL,
  return_id           VARCHAR(50)    NULL,
  return_number       VARCHAR(10)    NULL,
  return_message      VARCHAR(220)   NULL,
  success             tinyint(1)     NULL,
  PRIMARY KEY (id)
)

ALTER TABLE mercurio.accounting_report
  ADD CONSTRAINT FK_accounting_report_accounting_type_operation
FOREIGN KEY (type_operation_id) REFERENCES mercurio.accounting_type_operation (id);

ALTER TABLE mercurio.accounting_report
  ADD CONSTRAINT FK_accounting_report_advance_required
FOREIGN KEY (advance_required_id) REFERENCES mercurio.advance_required (id);

ALTER TABLE mercurio.accounting_report
  ADD CONSTRAINT FK_advance_required_event
FOREIGN KEY (event_id) REFERENCES mercurio.event (id);

ALTER TABLE mercurio.accounting_report
  ADD CONSTRAINT FK_advance_required_transfer
FOREIGN KEY (transfer_id) REFERENCES mercurio.transfer (id);
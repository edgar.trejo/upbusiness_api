CREATE TABLE mercurio.invoice_concept(
	id bigint NOT NULL AUTO_INCREMENT,
	invoice_id bigint,
	product_service_key varchar(10),
	identification_number varchar(10),
	unit_key varchar(5),
	unit varchar(40),
	description varchar(150),
	quantity varchar(5),
	unit_value varchar(12),
	amount varchar(12),
	PRIMARY KEY (id),
	CONSTRAINT FK_invoice_id FOREIGN KEY (invoice_id) REFERENCES mercurio.invoice (id)
);

CREATE TABLE mercurio.concept_tax(
	id bigint NOT NULL AUTO_INCREMENT,
	concept_id bigint,
	tax varchar(5),
	factor_type varchar(10),
	base varchar(12),
	fee varchar(12),
	amount varchar(12),
	PRIMARY KEY (id),
	CONSTRAINT FK_concept_id FOREIGN KEY (concept_id) REFERENCES mercurio.invoice_concept (id)
);
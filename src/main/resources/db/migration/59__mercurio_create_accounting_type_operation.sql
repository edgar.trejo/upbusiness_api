CREATE TABLE mercurio.accounting_type_operation(
	id 	INT	NOT NULL AUTO_INCREMENT,
    name	VARCHAR(100) null,
    description	VARCHAR(150) null,
    constraint UQ_accounting_type_operation_id
		unique (id)
);

alter table mercurio.accounting_type_operation
	add primary key (id);
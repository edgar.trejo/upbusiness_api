CREATE TABLE mercurio.log_email
(
  id                  BIGINT       NOT NULL AUTO_INCREMENT,
  user_id             BIGINT       NOT NULL,
  date                DATETIME     NULL,
  sender              VARCHAR(200) NOT NULL,
  subject             VARCHAR(500) NULL,
  attachments         INT          NULL,
  PRIMARY KEY (id)
);

ALTER TABLE mercurio.log_email
  ADD CONSTRAINT FK_log_email_user
FOREIGN KEY (user_id) REFERENCES mercurio.user (id);
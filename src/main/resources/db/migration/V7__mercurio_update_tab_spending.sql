ALTER TABLE mercurio.spending ADD COLUMN invoice_evidence_id BIGINT NULL;

ALTER TABLE mercurio.spending
  ADD CONSTRAINT FK_spending_invoice_evidence
FOREIGN KEY (invoice_evidence_id) REFERENCES mercurio.image_evidence (id);
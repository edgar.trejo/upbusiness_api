ALTER TABLE
    mercurio.transfer ADD (concentradora_amount DECIMAL(10,2) DEFAULT 0.0);

ALTER TABLE
    mercurio.transfer ADD (solicitude_id BIGINT DEFAULT 0);

create table custom_report_type_column
(
	id bigint auto_increment,
	name varchar(100) not null,
	active tinyint(1) null,
	constraint UQ_custom_report_type_column_id
		unique (id)
);

alter table custom_report_type_column
	add primary key (id);


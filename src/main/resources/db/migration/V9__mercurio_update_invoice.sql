ALTER TABLE mercurio.invoice ADD COLUMN message_id VARCHAR(100) NULL;
ALTER TABLE mercurio.invoice DROP COLUMN name_storage_system;
ALTER TABLE mercurio.invoice ADD COLUMN xml_storage_system VARCHAR(100) NULL;
ALTER TABLE mercurio.invoice ADD COLUMN pdf_storage_system VARCHAR(100) NULL;
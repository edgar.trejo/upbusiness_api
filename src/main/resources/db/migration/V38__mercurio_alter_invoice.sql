
ALTER TABLE mercurio.invoice ADD (folio VARCHAR(20));
ALTER TABLE mercurio.invoice ADD (receiver VARCHAR(150));
ALTER TABLE mercurio.invoice ADD (currency VARCHAR(10));
ALTER TABLE mercurio.invoice ADD (ish VARCHAR(12));
ALTER TABLE mercurio.invoice ADD (tua VARCHAR(12));
ALTER TABLE mercurio.invoice ADD (original_date DATETIME);
ALTER TABLE mercurio.invoice ADD (created_date DATETIME);
ALTER TABLE mercurio.invoice MODIFY COLUMN uuid VARCHAR(40) NOT NULL;
ALTER TABLE mercurio.invoice MODIFY COLUMN user_id BIGINT NOT NULL;

UPDATE mercurio.user_client uc
INNER JOIN mercurio.user u
SET uc.email_admin = u.email_admin,
    uc.email_approver = u.email_approver,
    uc.cost_center_id = u.cost_center_id,
    uc.role_id = u.role_id,
    uc.job_position_id = u.job_position_id,
    uc.phone_number = u.phone_number,
    uc.invoice_email = u.invoice_email,
    uc.invoice_email_password = u.invoice_email_password
WHERE uc.user_id = u.id AND u.email IS NOT NULL;
# Mercurio API

***

[![Travis Widget]][Travis]

[Travis]: https://travis-ci.com/dherby/mercurio_api
[Travis Widget]: https://travis-ci.com/dherby/mercurio_api.svg?token=1hNG2sk9A3bD7BqNsEp3&branch=develop


# Content

* [How to install](#install)
  * [Configure database](#db)
  * [Environment variables](#env)
* [Development environment](#dev)
  * [Important data](#dev-data)

## <a name="install"></a> Local environment:

* Start by cloning the repository in your computer.

  ```
  $ git clone git@github.com:dherby/mercurio_api.git api
  ```

* Dependencies:
  - [JDK 8](http://www.oracle.com/technetwork/java/javase/downloads/index.html)
  - [Maven](https://maven.apache.org/download.cgi)
  
* Stack Technologies:
  - Spring Boot
  - JPA
  - MySQL
  - Maven
  - GSuite Services (Goolgle Java SDK)
  
## <a name="install"></a> Configure environment
  - Set com.mx.sivale.SivaleApplication class as the Main class
  - Set local as the active profile in your IDE 
  
### <a name="db"></a> Configure database:

* Install MySQL
  - [MySQL Server](https://dev.mysql.com/downloads/mysql/)
  
  - Create a Database (mercurio)
  
    ```
    create schema mercurio;
    ```
    
* Migrations
  - To create a new version: Create a .sql file V<version number>__<purpose of the migration>.sql. 
    - i.e. "V1__create_database_structure.sql"
  - No steps are required, Flyway as a maven dependency will execute the migrations.

### <a name="db"></a> Configure keys:

* Add the rsa keys needed:
  - Windows: C:\keys\mercurio
  - Linux: \keys\mercurio
* Keys needed:
  - Si Vale
  - Google

## <a name="dev"></a> Development environment

The development environment url is:
```
https://mercurio.api.dev.inteligas.mx
```

Swagger

```
https://mercurio.api.dev.inteligas.mx/swagger-ui.html
```

If you wish to know what api version is deployed, you can check it:

```
https://mercurio.api.dev.inteligas.mx/version
```

## <a name="dev"></a> CI - Travis CI

The CI configuration is under .travis.yml
Shell scripting is used inside the travis container ci/

* Directory ~/deploy should exist in the instance server
* The app will start/stop as a service, on the configuration for the first time the app shoud be created as a service:
    ```
    sudo ln -s ~/deploy/mercurio{snapshot}.jar /etc/init.d/mercurio
    ```
* Enable service:
    ```
    systemctl enable mercurio.service
    ```
* Edit file /etc/init.d/mercurio.service
    ```
    [Unit]
    Description=mercurio
    After=syslog.target
    
    [Service]
    User=deploy
    ExecStart=/home/ubuntu/deploy/mercurio{snapshot}.jar
    SuccessExitStatus=143
    
    [Install]
    WantedBy=multi-user.target
    ```
* mercurio{snapshot}.conf should exist at the same jar level and should be equal to the jar name
* mercurio{snapshot}.conf:
    ```
    JAVA_OPTS=-Dspring.profiles.active={profile}
    ```
    